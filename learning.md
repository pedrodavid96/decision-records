# Learning

[Marty Lobdell - Study Less Study Smart](https://www.youtube.com/watch?v=IlU-zDU6aQ0)
Awesome awesome talk 🥇⭐

[How to Train Your Brain to Remember Almost Anything by Thomas Oppong](https://elemental.medium.com/how-to-train-your-brain-to-remember-almost-anything-77cb653a0c04)

1. Spaced repetition
   Repeating intake of what you are trying to retain over a period of time.
   For example, when you read a book and really enjoy it, instead of putting it away,
   reread it again after a month, then again after three months, then again after
   six months, and then again after a year.
2. The 50/50 rule
   Dedicate 50% of your time to learning anything new and the rest of your time to
   sharing or explaining what you have learned to someone or your audience.
3. Topic demonstrations
4. Sleep
   Finally, use sleep as a powerful aid between learning sessions.
   Sleep after learning is a critical part of the memory-creation process,
   and sleep before learning strengthens your capacity.

[Top 10 Most Useful Online Courses That Are Free](https://medium.com/swlh/the-top-10-most-useful-online-courses-you-can-take-for-free-411aaa0dd80d)
1. Justice — Michael Sandel (Harvard)
2. Physics — Walter Lewin (MIT)
3. Learning How to Learn — Terrence Sejnowski and Barbara Oakley (UCSD)
4. Machine Learning — Andrew Ng (Stanford)
5. Quantum Mechanics — Richard Feynman
6. Medical Neuroscience — Leonard White (Duke)
7. Organic Chemistry — Michael McBride (Yale)
8. Immunology — Alma Novotny (Rice)
9. World History — John Green (Crash Course)
10. Microeconomics — Tyler Cowen and Alex Tabbarock (MRU)
Honorable Mentions:
* Nonlinear Dynamics and Chaos by Steven Strogatz
  The math behind the Butterfly Effect and why reality can be inherently unpredictable.
* Systems Biology by Uri Alon
  Fascinating machinery of human cells, from gene regulation to why we get Type II diabetes.
* Programming Paradigms by Jerry Cain
  One of my first-ever online courses. Part of the impetus to do the MIT Challenge.
* Intro Biology by Eric Lander
* Poker Theory and Analytics by Kevin Desmond
  Fun class on the math behind poker betting. I took this when working on a poker programming project.
* Being and Time by Hubert Dreyfus
  Dreyfus has a ton of audio-only courses on Contintental philosophers. His one on Heidegger is the best.

# Things to learn

Rust
Haskell
F# (OCaml)
Scala
Erlang (Elixir)
    [First impressions of Gleam: lots of joys and some rough edges | nicole@web](https://www.ntietz.com/blog/first-impressions-of-gleam/)
Elm (vs Typescript)
Clojure
Assembly (modern)
Prolog
Golang
Python

Android NDK
Redis

Git Hooks
Git Submodules

HTTP2/0 (Java 9)
HTTP Cache Control

Cpp (live reloading)
Clang-tidy
SIMD - check Intel ISPC

Linux / Unix (arch setup, configs, bash shortcuts, used cmd tools, extended tools...)

Matrix (views...)
Vulkan

--------------------------------------------------------


C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat
------------------------------------------

Test React Components

Code Structure
https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1

API

https://chsakell.com/2015/05/10/asp-net-web-api-unit-testing/
https://docs.microsoft.com/en-us/ef/core/providers/in-memory/
https://docs.microsoft.com/en-us/ef/core/

---

[Github classroom](https://classroom.github.com/)
Automate your course and focus on teaching
