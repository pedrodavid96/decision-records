# Painting

## Oil Painting

[Which Oil Paint to Buy](https://www.youtube.com/watch?v=os1srwUj038)
[Oil Painting for Beginners - Basic Techniques + Step by Step Demonstration](https://www.youtube.com/watch?v=w3hbZfX0Abg)

[Beginner's Guide](https://www.reddit.com/r/HappyTrees/comments/3rjc7z/beginners_guide_because_maybe_you_were_watching/)
https://www.reddit.com/r/HappyTrees/comments/igz95k/a_long_time_ago_i_saw_bob_ross_on_tv_and_i_wanted/
Awesome Colorfull trees oilpainting

https://mitchalbala.com/split-primaries-landscape-painting-palette/

### To buy

[Tamanho: 25mm - Cabo Curto - Trincha nº 1](https://www.pontodasartes.com/pt/catalogo/pinceis/pinceis-para-pintura/trincha/cerda/trincha-talens-360/)


### Colors

https://www.designtools.pt/pt/produto/lefranc-e-bourgeois/tinta-a-oleo-lefranc-bourgeois--150ml-1
Tinta a Óleo Lefranc Bourgeois - 40ml
Variante : Azul Ftalo
Variante : Vermelho Vivo
Variante : Terra Verde
Variante : Preto Marfim

### Portugal Stores

https://www.designtools.pt
https://www.pontodasartes.com/pt/
https://www.provoca-arte.pt/pt/

## Water Color

[Easy Watercolor Misty Ocean ★ Watercolor Tutorial For Beginners](https://www.youtube.com/watch?v=Kv_zOi0uxcQ)
Awesome Simple and small dark blue paint (sky and ocean)
