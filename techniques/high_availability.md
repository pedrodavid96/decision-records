[Setting Up A High-Availability Load Balancer (With Failover and Session Support) With HAProxy/Heartbeat On Debian Etch | Falko Timme](https://www.howtoforge.com/high-availability-load-balancer-haproxy-heartbeat-debian-etch#setting-up-a-highavailability-load-balancer-with-failover-and-session-support-with-haproxyheartbeat-on-debian-etch-) 

[Building Blue-Green Deployment with Docker | Hanzel Jesheen](https://botleg.com/stories/blue-green-deployment-with-docker/)
->
[code](https://github.com/botleg/blue-green)

[Create a load Balancer in Docker](https://docs-stage.docker.com/v17.12/docker-cloud/apps/load-balance-hello-world/)

[DNS Load Balancing and Using Multiple Load Balancers in the Cloud](https://blogs.flexera.com/cloud/enterprise-cloud-strategies/dns-load-balancing-and-using-multiple-load-balancers-in-the-cloud/)