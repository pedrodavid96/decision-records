# Visual Studio Code

## Shortcut "clash"

[visual studio code - How can I have the integrated VSCode terminal not capture CTRL-E, CTRL-X, and CTRL-A? - Stack Overflow](https://stackoverflow.com/questions/56398766/how-can-i-have-the-integrated-vscode-terminal-not-capture-ctrl-e-ctrl-x-and-ct)

vscode `settings.json`

```
{
    "terminal.integrated.commandsToSkipShell": [
        "-workbench.action.quickOpen"
    ]
}
```
