# Workout

## Why?

1. None of the apps has Android Wear integration

2. None of the apps work with new Android HealthConnect
   * If an workout is edited it doesn’t re-sync it

3. A lot of the apps seem to focus on Macros but then doesn't even have a good way to submit them
   The last part I honestly find normal / expected as it is not the focus, but then why even have it?

4. There seems to be a lot of "social" focus as well which I find a bit weird...
   An option to share to social networks seems important but I'd say that should be about it
   Maybe also share workout routines to other users ?

5. UX-wise, it's weird when the Homepage focus seems to be point 3 or 4

6. Missing "quick replace by similar exercise"

## Check the competition

[2023 Hevy Update : Hevy](https://www.reddit.com/r/Hevy/comments/18noxdi/2023_hevy_update/)
[Hevy app - free vs. paid (vs. other apps) : xxfitness](https://www.reddit.com/r/xxfitness/comments/155dm0g/hevy_app_free_vs_paid_vs_other_apps/)

## Other resources

[Royalty-Free Images for Custom Exercises? : Hevy](https://www.reddit.com/r/Hevy/comments/10cmvfy/royaltyfree_images_for_custom_exercises/)
> Hevy (and most other similar apps) get theirs from gymvisual.com, however they are not free.

---

### Automatic rep counter?

[Motion sensors  |  Sensors and location  |  Android Developers](https://developer.android.com/develop/sensors-and-location/sensors/sensors_motion)

[We made a Fitness app that counts reps automatically for you (like a Fitbit) Also - free goodies! : apple](https://www.reddit.com/r/apple/comments/1c3w0r4/we_made_a_fitness_app_that_counts_reps/)


### "Long running programs" (e.g.: 5/3/1)

### Icons for each workout template to be able to see them in calendar view

### You haven't hit these muscles in the past few weeks
