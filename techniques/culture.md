# Culture

[The Remote Manifesto](https://about.gitlab.com/blog/2015/04/08/the-remote-manifesto)
[5. Daily stand-up meetings are for bonding, blockers and the future](https://about.gitlab.com/blog/2015/04/08/the-remote-manifesto/#sts=5.%20Daily%20stand-up%20meetings%20are%20for%20bonding,%20blockers%20and%20the%20future)


[GitLab Agenda Document Template](https://docs.google.com/document/d/1WQe-0oiMCzB3MPBNdKluCEIfgTRpaIi-SJ8FmUJ2xHo/edit#)
* Host(s)
* Background information
* Questions

[Communication](https://about.gitlab.com/handbook/communication/)
[All-Remote Meetings | GitLab](https://about.gitlab.com/company/culture/all-remote/meetings/)
* Make meeting attendance optional
* Using GitLab to replace meetings
* **Live doc meetings**
* **Document everything live (yes, everything)**
* Cancel unnecessary recurring meetings
* **Use the right tools** - such as Calendly, which can show you as busy in chat tools like Slack.
* Avoid hybrid calls - mix of participants in the same physical room, together with others who are remote.
* Start on time, end on time
  * prefer the "speedy meetings" setting in our Google Calendar.
  * Start in minute 1
* It's OK to look away - **in internal meetings**, manage your own attention, it's okay to ask someone to repeat
[Communication - video calls](https://about.gitlab.com/handbook/communication/#video-calls)


[Embracing asynchronous communication](https://about.gitlab.com/company/culture/all-remote/asynchronous)
[When to pivot to synchronous](https://about.gitlab.com/company/culture/all-remote/asynchronous/#when-to-pivot-to-synchronous)


[Daily Standup Meetings: Everything You Need to Know (Standup Agenda, Purpose, Common Pitfalls, and More!)](https://geekbot.com/blog/daily-standup-meeting/)

Shush

$4.99 tool for macOS that lets you set a hotkey (e.g. fn) to mute your microphone ("push-to-talk" or "push-to-mute").
