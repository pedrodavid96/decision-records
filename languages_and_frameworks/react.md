# React

Didact: a DIY guide to build your own React

https://engineering.hexacta.com/didact-learning-how-react-works-by-building-it-from-scratch-51007984e5c5
https://pomb.us/build-your-own-react/

Diagrams:
https://www.npmjs.com/package/graphviz-react
https://www.npmjs.com/package/d3-graphviz

[Beyond React 16 by Dan Abramov - JSConf Iceland ⚛](https://www.youtube.com/watch?v=v6iR3Zk4oDY)
React Suspense

[React as an Implementation Detail - Blogomatano](https://chriskiehl.com/article/react-as-an-implementation-detail)