# Mutation Testing

Mutation testing is used to design new software tests and evaluate the quality of existing software tests.

---

Mutation testing is mainly a mean to test the quality of your tests (in academia), that said, and as per multiple practitioners experience, it also helps you finding bugs.

## Why is it needed?
**Even if** you have 100% line (and branch) coverage you might not be making the correct assertions.
Mutation testing will modify your source code (in hopes of inducing an error) and assert that your tests you fail.

[Explanation and Intro](https://medium.com/appsflyer/tests-coverage-is-dead-long-live-mutation-testing-7fd61020330e)

[What is Mutation Testing? (Example)](https://www.guru99.com/mutation-testing.html)
[Testing Technique with a Simple Example](https://www.softwaretestinghelp.com/what-is-mutation-testing/)

[Mutation Testing in practice](https://medium.com/@boxed/mutation-testing-in-practice-bc56f9e5b591)

[Java Framework](http://mutation-testing.org/)

## [Mutation Testing in .NET](https://medium.com/comparethemarket/mutation-testing-in-net-an-experiment-afc0d8ef9a44)

[Library created by the author](https://github.com/ComparetheMarket/fettle)

---

[Kill Evil Mutants! | Dave Aronson](https://www.youtube.com/watch?v=0ZchSZPjsY0)

[GOTO 2019 • Making Mutants Work for You • Henry Coles](https://www.youtube.com/watch?v=LoFJajoJQ2g)
Very cool presentation

[Introduction to Mutation Testing](https://www.youtube.com/watch?v=mZjPzlX9YJY)

[Mutation Driven Testing – When TDD Just Isn’t Good Enough – Software the Hard way](https://software.rajivprab.com/2021/02/04/mutation-driven-testing-when-tdd-just-isnt-good-enough/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/lcf4qv/mutation_driven_testing_when_tdd_just_isnt_good/)
Author focuses manual mutation testing (he says there's no mainstream tooling to automate it).
Basically he checks the code for where he could create bugs and checks if the tests detect them.

[Category Direction - Fuzz Testing](https://about.gitlab.com/direction/secure/fuzz-testing/fuzz-testing/)
[Coverage guided fuzz testing released in GitLab 13.3!](https://www.youtube.com/watch?v=3wdWMDRLdp4)
[Coverage-Fuzzing.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab/-/blob/fa33a6a4294ca63a06741a77a83b07e60475dd4b/lib/gitlab/ci/templates/Security/Coverage-Fuzzing.gitlab-ci.yml)
[Coverage Guided Fuzz Testing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/)
