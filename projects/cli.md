[Command Line Interface Guidelines](https://clig.dev/)
> An open-source guide to help you write better command-line programs, taking traditional UNIX principles and updating them for the modern day.

The article is **huge**. Should touch most of the topics one should take into consideration
 when developing a CLI.
[Discussion 1](https://www.reddit.com/r/programming/comments/k6iq4g/an_opensource_guide_to_help_you_write_better/)
[Discussion 2](https://www.reddit.com/r/programming/comments/k8jal6/a_guide_to_help_you_write_better_cli/)

[Arguments against CLI](https://arogozhnikov.github.io/2020/10/01/dont-write-cli.html)
* CLI support is an additional logic in your program that makes no real work
* While typically being dumb, CLI logic is frequently filled with mistakes; thus it requires constant maintenance and an additional testing.
* Error (exception) handling with CLI is very poor. Another layer of (bad faulty) code is required to make it possible
* Scaling/extending is not as easy compared to programming language APIs (see example in the end)
* CLIs are detached from essential code, which in most cases is disadvantage.
