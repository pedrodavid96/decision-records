# sitespeed.io

Sitespeed.io is a set of Open Source tools that makes it easy to monitor and measure the performance of your web site.


## Docker

Use our Docker container to get an environment with Firefox, Chrome, XVFB and sitespeed.io up and running as fast as you can download them. They work extremely well together with Graphite/InfluxDB and Grafana that you can use to monitor your web site.

`docker run --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io:9.2.1 https://www.sitespeed.io/`


## Compare Performance
https://www.sitespeed.io/video/#compare-har-files-using-comparesitespeedio
