# Justice League

## Justice League Snyder Cut

8/10

Undoubtedly better than the theater cut but in my opinion still not great.

A lot more character development and less "plot holes"
in the sense that a lot of decision in the theater cut
were too rushed or without much reasoning behind them
but that's what you can get with 2 more hours that would
be impossible in theaters.

The story is much more fleshed out, presenting way better
back story for each character, specially Cyborg.

Wonder Woman is also much better, with less "fem power" and more actual
power, both in the much better fight sequences as well as being
less sexualized and more of an "guide by example".

Aquaman is less of "another joker" (Flash role) and much more powerful.

Humor is different, but good.

The antagonist (not the, also better "teased" Darkseid) motives
are also much better fleshed out (also, his visuals are magnitudes better).

The teasers and alternate timelines and interesting and Darkseid looks
menacing and great.

Martian Manhunter inclusion is a bit weird.

A lot of unneeded slow-mo.
