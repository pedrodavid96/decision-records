# Mac (OSX)

## Automation

[Anyone using ansible to manage Mac OSX installations? : ansible](https://www.reddit.com/r/ansible/comments/ix57rd/anyone_using_ansible_to_manage_mac_osx/)

[geerlingguy/mac-dev-playbook: Mac setup and configuration via Ansible.](https://github.com/geerlingguy/mac-dev-playbook)
[samdoran/ansible-collection-macos: Ansible collection of roles and modules for use on macOS](https://github.com/samdoran/ansible-collection-macos/tree/main)
[community.general.osx_defaults module – Manage macOS user defaults — Ansible Community Documentation](https://docs.ansible.com/ansible/latest/collections/community/general/osx_defaults_module.html)

[Allow a remote computer to access your Mac](https://support.apple.com/zh-sg/guide/mac-help/mchlp1066/mac)

[install.sh/config at main · donnybrilliant/install.sh](https://github.com/donnybrilliant/install.sh/blob/main/config)

[Easy, automated macOS setup : MacOS](https://www.reddit.com/r/MacOS/comments/1b5axy6/easy_automated_macos_setup/)

[macos - Command Line - Enable Remote Login and Remote Management - Ask Different](https://apple.stackexchange.com/questions/278744/command-line-enable-remote-login-and-remote-management)
[macos - How to enable remote login via terminal for single user - Super User](https://superuser.com/questions/1328968/how-to-enable-remote-login-via-terminal-for-single-user)
[Anyone ever experience Bootstrap failed: 5: Input/output error when running sudo launchctl bootstrap system /System/Library/LaunchDaemons/com.apple.xsan.plist? : macsysadmin](https://www.reddit.com/r/macsysadmin/comments/176jmx1/anyone_ever_experience_bootstrap_failed_5/)

## Virtualization

[UTM | Virtual machines for Mac](https://mac.getutm.app/)

[Download and Install macOS 15.2 (24C101) for MacBook Air (13-inch, M3, 2024) / IPSW Downloads](https://ipsw.me/install/Mac15,12/24C101)
