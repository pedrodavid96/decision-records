# Calendar

https://calendly.com/
Calendly helps you schedule meetings without the back-and-forth emails

[How This Google Calendar Hack Helps Me Save More Than 16% of Meeting Time Every Month | Avoma Blog](https://www.avoma.com/blog/google-calendar-hack-save-meeting-time)
How This Google Calendar Hack Helps Me Save More Than 16% of Meeting Time Every Month
* change your default meeting duration to 15 or 30 minutes using Google Calendar’s settings.
* Enable “Speedy meetings” - 30m -> 25m, 60m -> 50m, you just saved 16% of your meeting time.
