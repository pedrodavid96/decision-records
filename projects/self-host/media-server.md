# Media Server

[Installing Servarr Stack with Docker Compose](https://blog.kye.dev/proxmox-servarr-stack)

## Radarr/Sonarr

[Unable to access 1337x.to, blocked by CloudFlare Protection. : pihole](https://www.reddit.com/r/pihole/comments/1dx0zo4/unable_to_access_1337xto_blocked_by_cloudflare/)
> I have a similar setup, you need [FlareSolverr (TRaSH Guides)](https://trash-guides.info/Prowlarr/prowlarr-setup-flaresolverr/).

## Books

[Readarr](https://readarr.com/)

[Compare LazyLibrarian vs. Readarr for me : LazyLibrarian](https://www.reddit.com/r/LazyLibrarian/comments/13gtrsq/compare_lazylibrarian_vs_readarr_for_me/)
> Lazylibrarian supports libgen and z-lib, so the number of books it's able to download is far greater than Readarr.

[evan-buss/openbooks: Search and Download eBooks](https://github.com/evan-buss/openbooks)

### Folder structure

[radarr: search results - different disks](https://www.reddit.com/r/radarr/search?q=different+disks&restrict_sr=on&include_over_18=on&sort=relevance&t=all)
Eventually if I decide to split my media in different disks.

### Hard Links

[Check if Hardlinks Are Working - TRaSH Guides](https://trash-guides.info/File-and-Folder-Structure/Check-if-hardlinks-are-working/)
[jdupes](https://codeberg.org/jbruchon/jdupes) is useful to detect something with hard links.

## Youtube

[alexta69/metube: Self-hosted YouTube downloader (web UI for youtube-dl / yt-dlp)](https://github.com/alexta69/metube)

[tubearchivist/tubearchivist: Your self hosted YouTube media server](https://github.com/tubearchivist/tubearchivist)

[kieraneglin/pinchflat: Your next YouTube media manager](https://github.com/kieraneglin/pinchflat?tab=readme-ov-file)
Heard of on https://selfh.st/newsletter/2025-01-10/

## Download from RTP Play

[Red_Acid: yt-dlp fork](https://forum.zwame.pt/threads/download-videos-rtp.964274/post-16943848)
PR: [[ie/RTP] Fix RTP extractor by red-acid · Pull Request #11244 · yt-dlp/yt-dlp](https://github.com/yt-dlp/yt-dlp/pull/11244)

## Notifications

TODO: [To setup](https://wiki.servarr.com/radarr/supported#notifications)

## Quick add media to Jellyfin

TODO: Currently it takes a while for Jellyfin to pick media up, or a "media scan".

[Sonarr/Radarr not triggering Jellyfin update : jellyfin](https://www.reddit.com/r/jellyfin/comments/unesuv/sonarrradarr_not_triggering_jellyfin_update/)


## Deleting media

TODO:

[What is your approach to deleting media? : radarr](https://www.reddit.com/r/radarr/comments/njpyjy/what_is_your_approach_to_deleting_media/)
[How do I get Sonarr to delete files after moving them to my media folder? : sonarr](https://www.reddit.com/r/sonarr/comments/un104q/how_do_i_get_sonarr_to_delete_files_after_moving/)
[How do you manage deleting? : radarr](https://www.reddit.com/r/radarr/comments/xvvycb/how_do_you_manage_deleting/)
[StuffAnThings/qbit_manage](https://github.com/StuffAnThings/qbit_manage)
This tool will help manage tedious tasks in qBittorrent and automate them. Tag, categorize, remove Orphaned data, remove unregistered torrents and much much more.
> Tag any torrents that have no hard links outside the root folder (for multi-file torrents the largest file is used)


## Downgrade Quality

[Automatic Search: why does it download the biggest file? : radarr](https://www.reddit.com/r/radarr/comments/a0w306/automatic_search_why_does_it_download_the_biggest/)
[Help downloading DOWNGRADE version of existing movie. : radarr](https://www.reddit.com/r/radarr/comments/11mswmi/help_downloading_downgrade_version_of_existing/)
[Downgrading the quality of a movie? : radarr](https://www.reddit.com/r/radarr/comments/95rfqq/downgrading_the_quality_of_a_movie/)

## <Other>Arr

[RandomNinjaAtk/arr-scripts](https://github.com/RandomNinjaAtk/arr-scripts)
Extended Container Scripts - Automation scripts to make life easier!

[giuseppe99barchetta/SuggestArr](https://github.com/giuseppe99barchetta/SuggestArr)
Effortlessly request recommended movies, TV shows and anime to Jellyseer/Overseer based on your recently watched content on Jellyfin, Plex or Emby—let SuggestArr handle it all automatically, keeping your library fresh with new and exciting content!

[First release of Broadcastarr : selfhosted](https://www.reddit.com/r/selfhosted/comments/1gwhup0/first_release_of_broadcastarr/)

[SELF-HOSTED Podcast Grabber in Docker! - Podgrab - YouTube](https://www.youtube.com/watch?v=tQgGTI8wfqw)
[Introduction - PodFetch Documentation](https://www.samtv.fyi/PodFetch/Introduction.html)
[madeofpendletonwool/PinePods: Pinepods is a complete podcast management system and allows you to play, download, and keep track of podcasts you enjoy. All self hosted and enjoyed on your own server!](https://github.com/madeofpendletonwool/PinePods)
[advplyr/audiobookshelf: Self-hosted audiobook and podcast server](https://github.com/advplyr/audiobookshelf)
[Alternative to Podgrab? : selfhosted](https://www.reddit.com/r/selfhosted/comments/16gulhn/alternative_to_podgrab/)

[Calibre, Readarr, and Kavita : selfhosted](https://www.reddit.com/r/selfhosted/comments/x1tqan/calibre_readarr_and_kavita/)

[tedhinklater/Jellyfin-Featured-Content-Bar: Featured Bar now with Fullscreen Version](https://github.com/tedhinklater/Jellyfin-Featured-Content-Bar?tab=readme-ov-file)

### Bazarr

Bulk edit sync with embedded subtitles.

#### Translation

For some reason it seems we can't translate from embedded subtitles.
Seems like a weird limitation taking into account you can sync from it.

[auto google translate : bazarr](https://www.reddit.com/r/bazarr/comments/wmp4sj/auto_google_translate/)

[API of some kind ? (for a GPTsubtrans adaptation) · Issue #2450 · morpheus65535/bazarr](https://github.com/morpheus65535/bazarr/issues/2450)

[GitHub - LibreTranslate/argos-translate-files: Translate files using Argos Translate](https://github.com/LibreTranslate/argos-translate-files)
Missing translation of `.srt` files.

[GitHub - ahmetoner/whisper-asr-webservice](https://github.com/ahmetoner/whisper-asr-webservice)
OpenAI Whisper ASR (automatic speech recognition) Webservice API

[srt-parser-2 - npm](https://www.npmjs.com/package/srt-parser-2)


## Jellyfin

### Jellyfin SSO

[9p4/jellyfin-plugin-sso](https://github.com/9p4/jellyfin-plugin-sso)
This plugin allows users to sign in through an SSO provider (such as Google, Microsoft, or your own provider). This enables one-click signin.

[jellyfin-plugin-sso/providers.md at main · 9p4/jellyfin-plugin-sso](https://github.com/9p4/jellyfin-plugin-sso/blob/main/providers.md#keycloak-oidc)
[Document How to set login disclaimer + branding css to add SSO links to frontpage · Issue #16 · 9p4/jellyfin-plugin-sso](https://github.com/9p4/jellyfin-plugin-sso/issues/16)

Doesn't work in the Android App
[google oauth - getting 403 disallowed user agent in Auth0 Lock for Android - Stack Overflow](https://stackoverflow.com/questions/43972730/getting-403-disallowed-user-agent-in-auth0-lock-for-android)
Could this workaround work?
```
webView.getSettings().setUserAgentString(System.getProperty("http.agent"));
```

### Show upcoming

[upcoming tab for tv shows not working](https://forum.jellyfin.org/t-solved-upcoming-tab-for-tv-shows-not-working)
[Not displaying correct dates for upcoming TV Shows · Issue #4387 · jellyfin/jellyfin](https://github.com/jellyfin/jellyfin/issues/4387)
[Show missing episodes within seasons feature : jellyfin](https://www.reddit.com/r/jellyfin/comments/mdcui4/show_missing_episodes_within_seasons_feature/)

### Skipper

Web-OS not ready
[Ask to skip for media segments does not show · Issue #272 · jellyfin/jellyfin-webos](https://github.com/jellyfin/jellyfin-webos/issues/272)

> [!TODO]
> Enabled injected server side skip, which should be replaced soon by the new APIs

### Trailers

Trailer button (don't know if it's even built in or some sort of Plugin) often doesn't work and
it's basically just a link to a Youtube WebView.
[*arr for Trailers : radarr](https://www.reddit.com/r/radarr/comments/wc03kp/arr_for_trailers/)
[No trailers found. Install the Trailer channel to enhance your movie experience by adding a library of internet trailers · Issue #2963 · jellyfin/jellyfin](https://github.com/jellyfin/jellyfin/issues/2963)
[Does anybody know how to use the 'Trailers' section in 'Movies'? : jellyfin](https://www.reddit.com/r/jellyfin/comments/g901hv/does_anybody_know_how_to_use_the_trailers_section/)
[Trailers plugin? : jellyfin](https://www.reddit.com/r/jellyfin/comments/rnjly0/trailers_plugin/)
[Preroll Videos or custom trailers in Jellyfin Tutorial(SOLVED) : jellyfin](https://www.reddit.com/r/jellyfin/comments/kqwnp7/preroll_videos_or_custom_trailers_in_jellyfin/)
[Playback error · Issue #133 · crobibero/jellyfin-plugin-tmdb-trailers](https://github.com/crobibero/jellyfin-plugin-tmdb-trailers/issues/133)
[YoutubeExplode.Exceptions.VideoUnplayableException: Video 'FKBN1qAzW3s' is unplayable. Reason: 'Please sign in'. · Issue #128 · crobibero/jellyfin-plugin-tmdb-trailers](https://github.com/crobibero/jellyfin-plugin-tmdb-trailers/issues/128)
[No Trailers anywhere, because crawling the first fails · Issue #116 · crobibero/jellyfin-plugin-tmdb-trailers](https://github.com/crobibero/jellyfin-plugin-tmdb-trailers/issues/116)

[Most trailers won't load, and those that do are limited to 15 seconds · Issue #3 · crobibero/jellyfin-plugin-tmdb-trailers](https://github.com/crobibero/jellyfin-plugin-tmdb-trailers/issues/3)

### Watch together

Idea for feature, to keep multiple accounts with shows in sync when you watch together.
[watch together (watch with multiples accounts) · Jellyfin Feature Requests](https://features.jellyfin.org/posts/389/watch-together-watch-with-multiples-accounts)

### Android Client

[streamyfin/streamyfin: A Jellyfin client built with Expo](https://github.com/streamyfin/streamyfin)
More modern design and seems to have generally more features.

## Jellyseerr

[Login with Jellyfin not working after update to 2.0.1 · Issue #1023 · Fallenbagel/jellyseerr](https://github.com/Fallenbagel/jellyseerr/issues/1023)

[[Feature Request] Support login with OIDC · Issue #183 · Fallenbagel/jellyseerr](https://github.com/Fallenbagel/jellyseerr/issues/183)
[feat: support OIDC authentication by michaelhthomas · Pull Request #184 · Fallenbagel/jellyseerr](https://github.com/Fallenbagel/jellyseerr/pull/184#issuecomment-1800121937)
Currently not built in, achievable by a stale branch that is still before 2.0.1 so we can't interchangeably
switch between both versions with same configs **but** the OIDC configuration at runtime is quite awesome tbh.

Currently facing this issue https://github.com/Fallenbagel/jellyseerr/pull/184#issuecomment-2155032099
Where the users don't have the email_verified claim.


## Provisioning / Deployment

[Guide to Deploying Jellyfin and Jellyseerr on Kubernetes with Terraform. : jellyfin](https://www.reddit.com/r/jellyfin/comments/10kv75r/guide_to_deploying_jellyfin_and_jellyseerr_on/)
[Jellyseerr | Wiki.js](https://wiki.ravianand.me/en/home-server/apps/media-server/jellyseerr)
