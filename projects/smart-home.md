# Smart Home

```yaml tags
tags:
- smart-home
- home-automation
```

## [Home Assistant](https://www.home-assistant.io)

[Installing Home Assistant on Proxmox in less than 5 minutes - YouTube](https://www.youtube.com/watch?v=OXiAV4V2MsA)

### Bluetooth

[xubuntu - Bluetooth manager not connecting to devices after upgrading to Ubuntu 23.04 - Ask Ubuntu](https://askubuntu.com/questions/1465957/bluetooth-manager-not-connecting-to-devices-after-upgrading-to-ubuntu-23-04)

[Bluetooth passthrough : Proxmox](https://www.reddit.com/r/Proxmox/comments/ofwzog/bluetooth_passthrough/)

```
$ journalctl -b | grep blue
```

## Energy consumption

[The Best Electricity Usage Monitors of 2021 - Reviews by YBD](https://www.yourbestdigs.com/reviews/best-electricity-usage-monitor/)

[Best Smart Plugs for Home Assistant (EU) - SmartHomeScene](https://smarthomescene.com/top-picks/best-smart-plugs-with-energy-monitoring-for-home-assistant-eu/?utm_source=chatgpt.com)
Awesome analysis with good choices.

[Home - Sense.com](https://sense.com/)
[Amazon.es : EMPORIA ENERGY](https://www.amazon.es/s?k=EMPORIA+ENERGY&geniuslink=true&tag=yobedisp-21)

[Kasa powerstrip as PDU - XtremeOwnage.com](https://static.xtremeownage.com/blog/2022/kasa-powerstrip-as-pdu/)

[Monitoring House Power Consumption with ESP32 and Shelly Power Meter - YouTube](https://www.youtube.com/watch?v=O3T22vV0TT8)

[OpenEnergyMonitor](https://openenergymonitor.org/)

## Smart vacuum

[Best Robot Vacuum 2021 : homeautomation](https://www.reddit.com/r/homeautomation/comments/nbdd4x/best_robot_vacuum_2021/)
[Aspirador Robô IROBOT Roomba i7+ (Autonomia: 75 min) | Worten.pt](https://www.worten.pt/pequenos-eletrodomesticos/aspiradores/aspiradores-robo/aspirador-robo-irobot-roomba-i7-autonomia-75-min-6838012)

[Roborock - Home Assistant](https://www.home-assistant.io/integrations/roborock#how-can-i-clean-a-specific-room)

## Misc

[Curtain](https://www.switch-bot.com/products/switchbot-curtain?variant=35903995510951)
[Curtain Solar Panel](https://www.switch-bot.com/products/switchbot-solar-panel?variant=40478667899047)

[Track temperature and humidity](https://www.switch-bot.com/products/switchbot-meter-1?variant=41453322731687)

[SwitchBot Bot](https://www.switch-bot.com/products/switchbot-bot?variant=37452343541927)
[Made my toggle door buzzer smart with Switchbot : homeautomation](https://www.reddit.com/r/homeautomation/comments/promee/made_my_toggle_door_buzzer_smart_with_switchbot/)

[SwitchBot Hub](https://www.switch-bot.com/products/switchbot-hub-mini?variant=33839747530885)

[SwitchBot Motion Sensor](https://www.switch-bot.com/products/motion-sensor?variant=40306857541799)


[SONOFF MINIR2](https://sonoff.tech/product/diy-smart-switch/minir2/)
Get Your House Lighted via Your Phone

[Shelly RGBW2](https://shelly.cloud/products/shelly-rgbw2-smart-home-automation-led-controller/)
Make your LED strips smart in one easy step. Shelly RGBW2 can connect like any LED controller and allows you to control all your lighting directly from a mobile device or tablet.

[A beginner’s guide to making a home smarter. : smarthome](https://www.reddit.com/r/smarthome/comments/5qbhgr/a_beginners_guide_to_making_a_home_smarter/)

[Is there such thing like master controller for led strips that can replace the 10s of controllers I need to use now? : homeautomation](https://www.reddit.com/r/homeautomation/comments/pyha2v/is_there_such_thing_like_master_controller_for/)

[Best Smart Home functions : homeautomation](https://www.reddit.com/r/homeautomation/comments/aojm1b/best_smart_home_functions/)
- washer and dryer automations
- Automatically activating the home alarm at night or when everyone has left the house.

[NFC.PT | Loja online de produtos e serviços NFC](https://www.nfc.pt/)

[How to use embedded barcode scanner with Raspberry Pi](https://www.rtscan.net/embedded-barcode-scanner-for-raspberry-pi-use-embedded-barcode-scanners-with-raspberry-pi/)

[5 Unique Smart Home Sensors I NEVER knew I needed! - YouTube](https://www.youtube.com/watch?v=OWiPaKARNcU)

[Faking an IR remote control using ESPHome - Community Guides - Home Assistant Community](https://community.home-assistant.io/t/faking-an-ir-remote-control-using-esphome/369071/52)

## Temperature & Humidity Sensor

[Telink Flasher v9.6](https://pvvx.github.io/ATC_MiThermometer/TelinkMiFlasher.html)

## Smart Lock

[5 Best Smart Locks in 2021 - Which One Is Best For You? - YouTube](https://www.youtube.com/watch?v=7Z6l9UI7XlM)
[Wyze Lock Review - Unboxing, Features, Setup, Settings, Installation, Testing - YouTube](https://www.youtube.com/watch?v=_l8CKWUPzqU)
[Which Smart Lock Do You Recommend AND Why? : homeassistant](https://www.reddit.com/r/homeassistant/comments/ow2y8n/which_smart_lock_do_you_recommend_and_why/)
[Best Smart Lock for front door? : homeautomation](https://www.reddit.com/r/homeautomation/comments/c9mkzc/best_smart_lock_for_front_door/)
[Smart Lock recommendations : homeautomation](https://www.reddit.com/r/homeautomation/comments/apxjs2/smart_lock_recommendations/)
[NUKI](https://shop.nuki.io/en/choose-your-smart-lock-3-0-product/?c=220724)

## Smart Sprinkler

[Rachio Smart Sprinkler Controllers | Rachio](https://rachio.com/)

---

[How can I automate my standing desk to press the '1' and '2' keys at particular intervals? Switchbot? : homeautomation](https://www.reddit.com/r/homeautomation/comments/ox6yjh/how_can_i_automate_my_standing_desk_to_press_the/)

https://www.withings.com/us/en/sleep
Know your nights. Master your days.

---

BroadLink RM4 pro

Aqara Vibration Sensor (por baixo do sofá)

## TV

[Unable to turn on LG TV with Alexa](https://www.reddit.com/r/amazonecho/comments/onaib5/unable_to_turn_on_lg_tv_with_alexa/)
[[ThinQ] How to Turn TV On/Off](https://www.lg.com/eg_en/support/product-help/CT20124005-20151500013423)
https://www.home-assistant.io/integrations/mitemp_bt/

https://mycroft.ai/

[Show HN: Pi-C.A.R.D, a Raspberry Pi Voice Assistant | Hacker News](https://news.ycombinator.com/item?id=40346995)

https://www.home-assistant.io/blog/2019/11/20/privacy-focused-voice-assistant/

https://www.aminhacasadigital.com/2018/12/estores-inteligentes-interruptores-que.html

https://www.smarthomepoint.com/smart-bulbs-vs-switches/

https://thesmartcave.com/smart-switch-with-smart-bulb/

https://thesmartcave.com/smart-switch-with-smart-bulb/
Open source firmware for ESP devices
Total local control with quick setup and updates.
Control using MQTT, Web UI, HTTP or serial.
Automate using timers, rules or scripts.
Integration with home automation solutions.
Incredibly expandable and flexible.


https://forum.cpha.pt/t/shelly-vs-sonoff-mini-vs-outro/2755

https://www.kuantokusta.pt/p/53269/broadlink-rm4-pro-google-home-alexa?utm_source=price_alert&utm_medium=email&utm_campaign=catalogo&from=price_alert
https://www.reddit.com/r/homeautomation/comments/4rr24f/a_few_things_to_consider_before_buying_the/

## Firmware

Basically all smart home device comes with it's own brand firmware, and usually implicates connecting it to the internet / vendor cloud.

I don't like this because of security / privacy (*), there's also the risk of the vendor just stop supporting the hardware **and**
honestly it is super annoying in practice.

Also, for wifi devices you're effectively giving these companies your WiFi password.
It's often recommended to put these devices on a different network to mitigate this (and potentially harmful devices)
but this isn't usually an option for non tech-savvy people, and even tech-savvy people might have trouble with this if their
ISP Router doesn't allow it nor bringing your own router.

Usually having to control everything from a phone cluttered with different apps and in practice vendors are constantly re-designing
their apps, completely changing functionalities and some times (more often than expected) even releasing a new app and then you need to
keep both on your phone!?

As a tech-savvy person with a small 24/7 server at home I'd much rather my **home** automation stays there.
There's a few inconveniences on that (it's not "trivial" to have the automation outside of your home... I mean, it is, but you have to
become your own "bodyguard") but the pros outweigh them without any doubt (at least for me).

Enter the following projects:
* [Tasmota Documentation - Tasmota](https://tasmota.github.io/docs/)
* [ESPHome — ESPHome](https://esphome.io/)

These projects seem to overlap a lot in concept but without effectively having used them yet I'm tempted to favor ESPHome since it
seems it might be able to "mirror" the concept of IaC (Infrastructure as Code) I'm so used (and really enjoy) as a Software Engineer.

The concept is "basic". You flash this new firmware into the chips of those smart home devices (a lot of them are the old and "decommissioned" ESP8266, more recent ones might be variations of ESP32, these chips are basically the recent, and thus more powerful "Arduinos", usually with on-board connectivity via Wi-FI / Bluetooth / Zigbee).

The "act" of flashing it might be slightly trickier.
If you're in luck someone already made a tool to flash it OTA (Over the Air).
What this means is that someone has reverse engineered the on-board Firmware or the Servers it connects to
so you can upload your own firmware (well, most likely Tasmota / ESPHome, not exactly "your own" but I guess since it is Open Source it is "everyone's" firmware).

I'm not really sure how these people do it but I guess most smart home devices already have "facilities" to update their own firmware and
these might be cracked to install your own.

Unfortunately (at least for me that has forgotten all of my electronics courses and mostly focuses on Software Engineering) a lot of these
devices will require you to get dirty and a bit of soldering...

## Connectivity

### Hardware

#### Wifi

Convenient but up until recently Routers / APs weren't properly "ready" to get so many devices connecting to it.
I'd say connecting these devices to the same network as the rest of your "usual" devices poses some security concerns
 (that might be slightly mitigated by flashing your own [firmware](#firmware)).
Most importantly **it's usually not really an efficient method for a lot of the small smart devices**.
It usually drains more battery / energy than other options.
The high bandwidth makes it a good choice for Cameras for example thou.

#### Bluetooth

+ Low power consumption
- Short range
- Lower bandwidth
- May require a hub (often the phone ends up with that "role")

#### Zigbee

+ Low Power
+ Mesh networking extends range
- Requires hub / gateway

#### Thread

+ Low Power
~ Often used as a transport for Matter (the combination being known as Matter over Thread)
~ [OpenThread](https://openthread.io/)
- Requires a "Thread Border Router"

### Software

This somewhat ties with [firmware](#firmware) but it has to be mentioned that beyond the hardware
used to communicate, the way the devices communicate is also important...

Historically every vendor used their own "messages" to communicate between their devices but
the [Matter](https://github.com/project-chip/connectedhomeip) is designed to unify smart home ecosystems.
Founded by Amazon, Apple, Google and the Zigbee Alliance, now called the Connectivity Standards Alliance (CSA).

## Smart "Plug" / Relay

[Sonoff Mini Relay | devices.esphome.io](https://devices.esphome.io/devices/Sonoff-Mini-Relay)
[Flashing Tasmota on Sonoff Mini R2 (Over the Air, no soldering, FW3.4) - Goodbye TP-Link HS200! - YouTube](https://www.youtube.com/watch?v=Xn1PmBLFHIM)

[Shelly Plus 1 ESP32 Smart Relay | TASMOTA and Home Assistant - YouTube](https://www.youtube.com/watch?v=eLoOT3mXcMs)

## Lamp

[Shelly Vintage G125 - starts making noise and then dies few days afterwards : r/shellycloud](https://www.reddit.com/r/shellycloud/comments/161tjz9/shelly_vintage_g125_starts_making_noise_and_then/)
It seems they simply die...
> inside each one only failed component was fusible resistor.

## Configuring Tasmota

[Getting started with Tasmota & Home Assistant](https://www.youtube.com/watch?v=n8DEjncTxyg)

Seems like devices need MQTT by default.

## Camera / Surveillance

[Self-Hosted 6: Low Cost Home Camera System](https://selfhosted.show/6)
[Frigate NVR](https://frigate.video/)
