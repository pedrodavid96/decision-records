## Terraform

[Introduction to cloud-init - cloud-init 23.3.3 documentation](https://cloudinit.readthedocs.io/en/latest/explanation/introduction.html#introduction)

[Manage Terraform With GitLab CI](https://medium.com/@dbourgeois23/manage-terraform-with-gitlab-ci-5c24005eb62a)

[Setting Up an AWS EC2 instance with SSH access using Terraform](https://medium.com/@hmalgewatta/setting-up-an-aws-ec2-instance-with-ssh-access-using-terraform-c336c812322f)
Seems severely outdated but still useful for somewhat of a "minimal setup".

[Invoking the AWS CLI with Terraform](https://medium.com/faun/invoking-the-aws-cli-with-terraform-4ae5fd9de277)
Leveraging `null_resource` for when, for example, no Terraform resource for the AWS service, the API action is only available through the CLI/SDK

[Building Feature Toggles into Terraform](https://medium.com/capital-one-tech/building-feature-toggles-into-terraform-d75806217647)
Interpolating the Count Parameter

[Create a secure and H/A VPC on AWS with Terraform](https://medium.com/muhammet-arslan/create-a-secure-and-h-a-vpc-on-aws-with-terraform-71b9b0a61151)
Very self explanatory architecture diagrams 🥇⭐

What we will have the end of the session?
1. VPC with enough IPs
6. Subnets (3 public and 3 private)
4. Routetables (1 for public and 3 for private)
1. Internet Gateway
3. NATs

[How to manage terraform state](https://blog.gruntwork.io/how-to-manage-terraform-state-28f5697e68fa#aeb7)

[Using Terraform for zero downtime updates of an Auto Scaling group in AWS](https://medium.com/@endofcake/using-terraform-for-zero-downtime-updates-of-an-auto-scaling-group-in-aws-60faca582664)
**Blue / Green deployments**
**Rolling deployments**

## Managing versions

[tenv | OpenTofu / Terraform / Terragrunt and Atmos version manager](https://tofuutils.github.io/tenv/#about-the-project)
Compatible with OpenTofu

## Ideas

Create extension to manage known-hosts and ssh-config.
May be able to use https://github.com/cyjake/ssh-config

Example:
```hcl
resource "ssh-host" "test-host" {
    host = 40.160.123.30
    name = dev
    identity_file = pathexpand("~/.ssh/aws_id_rsa")
}
```

## Misc

[Set up Google Cloud Platform (GCP) authentication for Terraform Cloud](https://stackoverflow.com/a/68290091)

[gruntwork-io/cloud-nuke: A tool for cleaning up your cloud accounts by nuking (deleting) all resources within it](https://github.com/gruntwork-io/cloud-nuke)

[Building a Packer Image with Terraform](https://austincloud.guru/2020/02/27/building-packer-image-with-terraform/)