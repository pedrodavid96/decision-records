# 2FA

[That's not how 2FA works](https://shkspr.mobi/blog/2021/01/thats-not-how-2fa-works/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/kzzxc4/thats_not_how_2fa_works/)
Anecdote on a phishing attempt which multiple people just commented with some sort of
> this is the reason I have 2 factor auth enabled everywhere
But 2FA wouldn't have solved the issue since it would have just been redirected from the phishing site.
