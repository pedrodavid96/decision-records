# Diagrams as code

[Diagrams - Diagram as Code](https://diagrams.mingrammer.com/)
> Diagram as Code allows you to track the architecture diagram changes in any version control system.

[C4, Diagrams as Code & Architectural Joy](https://lukemerrett.com/c4-diagrams-as-code-architectural-joy/)
[GOTO 2020 • Five Things Every Developer Should Know about Software Architecture • Simon Brown](https://www.youtube.com/watch?v=9Az0q2XHtH8)

[Using Mermaid](https://about.gitlab.com/handbook/tools-and-tips/#using-mermaid)

https://twitter.com/iximiuz/status/1356308063603585024
- http://sketch.io/sketchpad (software)
- Wacom Intuos pen tablet (hardware)


https://kroki.io/
https://structurizr.com/
https://github.com/structurizr/java

[GitHub - terrastruct/d2: D2 is a modern diagram scripting language that turns text to diagrams.](https://github.com/terrastruct/d2)
[D2 is now open source – a new, modern language that turns text to diagrams : programming](https://www.reddit.com/r/programming/comments/z1qoxc/d2_is_now_open_source_a_new_modern_language_that/)

https://text-to-diagram.com/

[I built an open-source runtime to visualise complex engineering flows : programming](https://www.reddit.com/r/programming/comments/1f2j3nk/i_built_an_opensource_runtime_to_visualise/)
[GitHub - metz-sh/simulacrum: Code-playground to visualise complex engineering flows.](https://github.com/metz-sh/simulacrum)
[metz — The modern way to build architecture](https://metz.sh/)
[Eraser – Docs and Diagrams for Engineering Teams](https://www.eraser.io/)

[We need visual programming. No, not like that. : programming](https://www.reddit.com/r/programming/comments/1e3mx7g/we_need_visual_programming_no_not_like_that/)
