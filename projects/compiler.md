# Compilers

[Crafting Interpreters](https://craftinginterpreters.com/)
->
[Some discussion](https://www.reddit.com/r/programming/comments/bqrwyv/jumping_back_and_forth_crafting_interpreters/)

[Let's Build a Compiler Pretty](https://xmonader.github.io/letsbuildacompiler-pretty/)

[Languages I want to write | Wesley Aptekar-Cassels](https://blog.wesleyac.com/posts/language-todos)

For Metaprogramming, investigate D approach.

[Building a Programming Language Pt. 3 - Interpreting by Drew Youngwerth's](https://drew.ltd/blog/posts/2020-8-2.html)

[What Color is Your Function? ](https://journal.stuffwithstuff.com/2015/02/01/what-color-is-your-function/)

[So, I Created a Programming Language](https://medium.com/young-coder/so-i-created-a-programming-language-4d9c11038d22)

Create easy way to "test interfaces" (aka, enforce that every implementation follows the contract)

[Developing Statically Typed Programming Language](https://blog.mgechev.com/2017/08/05/typed-lambda-calculus-create-type-checker-transpiler-compiler-javascript/)

[Compiling a Functional Language Using C++, Part 9 - Garbage Collection](https://danilafe.com/blog/09_compiler_garbage_collection/)

[LetsBeRealAboutDependencies](https://wiki.alopex.li/LetsBeRealAboutDependencies)
[Reddit discussion](https://www.reddit.com/r/programming/comments/f26gj8/lets_be_real_about_dependencies/)
Interesting discussions on dependencies for different environments

[A Complete Guide to LLVM for Programming Language Creators](https://mukulrathi.co.uk/create-your-own-programming-language/llvm-ir-cpp-api-tutorial/)

[Clean Rust API for error detection and message to user](https://mobile.twitter.com/scrabsha/status/1338224656223006720)
The respective blog post:
[Error recovery with parser combinators (using nom)](https://www.eyalkalderon.com/nom-error-recovery/)

[CS 6120: Advanced Compilers: The Self-Guided Online Course](https://www.cs.cornell.edu/courses/cs6120/2020fa/self-guided/)

[Metamine - A completely declarative programming language.](https://github.com/ymte/metamine)
e.g.:
```
out = draw objects
objects = [ circle(mouse.x, mouse.y, 10) ]
```
This results in a canvas with a circle that **follows** the mouse (not just a circle on the mouse coordinates for that exact frame)

[Writing high performance F# Code](https://bartoszsypytkowski.com/writing-high-performance-f-code/)
Looks awesome 🥇⭐

Surprisingly in-depth article, which should be pretty useful not only for F# but for basically any language (including taking ideas to build an high performance language).

Goes in depth on the following topics:
* Understand memory layout of value types
* Readonly and ref-like structs
* Padding
* CPU cache lines
* False sharing
* Using structs and collections together
* Discriminated unions as value types (comparison with Rust)
* Learn to work with virtual calls
* Make use of vectorization
* Immutable code and atomic updates
* Atomic Compare and Swap
* Inlining

No semicolons, besides more terse, diffs are less misleading, in the following example a single addition should be enough
```
- private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
+ private static final ObjectMapper OBJECT MAPPER = new ObjectMapper()
+                 .registerModule(new Jdk8Module());
```

[Extending a client with the language server protocol - LogRocket Blog](https://blog.logrocket.com/how-to-use-the-language-server-protocol-to-extending-a-client-764da0e7863c/?utm_source=pocket_reader)

[Type erasure and reification - Eli Bendersky's website](https://eli.thegreenplace.net/2018/type-erasure-and-reification)
The solution was to use type erasure to implement generics entirely in the compiler. Here's a quote from the official Java generics tutorial:

    Generics were introduced to the Java language to provide tighter type checks at compile time and to support generic programming. To implement generics, the Java compiler applies type erasure to:

        * Replace all type parameters in generic types with their bounds or Object if the type parameters are unbounded. The produced bytecode, therefore, contains only ordinary classes, interfaces, and methods.
        * Insert type casts if necessary to preserve type safety.
        * Generate bridge methods to preserve polymorphism in extended generic types.

Using type erasure to implement generics with backwards compatibility is a neat idea, but it has its issues. Some folks complain that not having the types available at runtime is a limitation (e.g. not being able to use instanceof and other reflection capabilities).
Other languages, like C# and Dart 2, have reified generics which do preserve the type information at run-time.

[My experience crafting an interpreter with Rust – Manuel Cerón](https://ceronman.com/2021/07/22/my-experience-crafting-an-interpreter-with-rust/)

[Creating Your Own Programming Language - Computerphile](https://youtu.be/Q2UDHY5as90?si=aIlyEd35JwUmPsi2)