# SpellChecker

[Find and Fix More Typos - olafalders.com](https://www.olafalders.com/2024/02/06/find-and-fix-more-typos/)

[ELI5: ispell, aspell, hspell, hunspell, enchant : linuxquestions](https://www.reddit.com/r/linuxquestions/comments/51k73m/eli5_ispell_aspell_hspell_hunspell_enchant/)
tl;dr: Nowadays hunspell and aspell are mainly used.

After trying to use `aspell` for a bit I'm finding it quite frustrating.
Flags are not intuitive at all, the format for non-interactive runs is quite weird.
Documentation is rather lacking.
Added a word to the dictionary in interactive mode and I'm not sure where I can find that word
or configure it for other machines.

`hunspell` seems pretty powerful and is used by the likes of Openoffice, LibreOffice, Firefox and Chrome.
Might be as simple as:

```
hunspell -p custom_word_list  <test.txt
```
custom_word_list is a file where each line is something we want to add to a custom "dictionary"

```
hunspell -d en_US,custom_dict  <test.txt
```
custom_dict is actually a custom_dictionary that has to be on hunspell path (check it with `hunspell -D`).
A dictionary needs a `.dic` file (word list, where the first line is the number of words) and an `.aff` file (can be empty for simple use cases).

The output isn't very simple but understandable with the **great caveat** that it doesn't print the line number...
The line number can be printed with `-U`, `-u`, `-u2`, `-u3` but that also loses a lot of information (only shows 1 suggestion, doesn't show column...)

[SpellChecker in Typescript](https://github.com/streetsidesoftware/cspell)
Seem more modern and possibly more extensible and it would be easier to create a PR.
The documentation seems much better.
Tbh, reading Typescript is usually easier than C (?) and I'm not even sure where `aspell` code is hosted.

Couldn't be used after `yum install -y npm && npm install --global cspell && cspell test.txt` because it wasn't compatible with distro npm.


```
hunspell -d en_US -u  <test.txt | sed 's/Line //g;s/ -> /:/g' | column --separator ":" --json --table-name typos --table-columns "line,word,mainSuggestion" | jq '.typos | map({ word: .word, line: .line, mainSuggestion: .mainsuggestion, suggestions: []})'
```

```
sed -n 3s/conatins/contains/p test.txt
```

```
hunspell -u2 <test.txt | sed 's/g;/p;/g' | cut -d";" -f1 | xargs -n 1 sh -c 'sed -n "$@" test.txt' sh
```

[How to Use Spell Check on your Linux Terminal](https://www.freecodecamp.org/news/spell-check-on-your-linux-terminal/)

[ispell - Rust](https://docs.rs/ispell/latest/ispell/)
[GitHub - lise-henry/rust-ispell: Rust library for easily calling ispell and aspell](https://github.com/lise-henry/rust-ispell)