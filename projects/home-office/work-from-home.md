# Work from Home

[The Handbook | The GitLab Handbook](https://handbook.gitlab.com/handbook/)

## Seamingless change from Home Desktop to Work Laptop (and back)

> [!TODO]
> [wireless - Wake up from suspend using USB device - Ask Ubuntu](https://askubuntu.com/questions/848698/wake-up-from-suspend-using-usb-device)

### KVM switches

[GREATHTEK KVM Switch 2 monitores 4 portas 4 K @ 60 Hz](https://www.amazon.es/-/pt/gp/product/B0BGLQSM4G/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
* USB2.0 KVM Switch HDMI
* Hotkey Switch
* Button Switch KVM HDMI 4 portas
* HDCP2.0 HDMI2.0 Switch KVM HDMI,
* com 8 HDMI cabos, 4 cabos USB A-B

Bought 17th Novemeber 2022 (IT Budget)
Stopped working 6th November 2024 - USB switch with kb / camera / headphones / mic,
Only kb works when all connected.
Changing the USB switch to directly connect to device (either Desktop or Laptop)
"fixes" it.

Before noticing this I was investigating Software issues like:
[[Solved] Error spa.alsa: set_hw_params: No space left on device / Multimedia and Games / Arch Linux Forums](https://bbs.archlinux.org/viewtopic.php?id=293810)
This didn't yield anything even after lots of downgrades etc...

[ USB 3.0 comutador KVM HDMI 2 portas 4K@60Hz](https://www.amazon.es/-/pt/gp/product/B0CQ53XD4S/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
* Interruptores KVM para 2 PC 1 monitor
* com 3 portas USB3.0, HDMI2.0, HDCP2.2
* suporte para função EDID
* inclui 2 cabos USB 3.0

Let's see if this "fixes" the problem as well.


## 4K 60Hz Display

USB-C to HDMI cable not working properly, seems to "overload" and screen constantly "black blinks".
It could be related to:
[[TN]Randomly black screen with 4K @ 60Hz in Ubuntu 20.04.LTS with two monitors (#4321) · Issues · drm / i915 / kernel · GitLab](https://gitlab.freedesktop.org/drm/i915/kernel/-/issues/4321)
[i5-1135G7 Display flickering USB-C to DP external 4K HiDPI and FIFO underrun (#4703) · Issues · drm / i915 / kernel · GitLab](https://gitlab.freedesktop.org/drm/i915/kernel/-/issues/4703)
[Screen corruption and artifacts for Intel Arc with both i915 and xe driver (starting with 6.10) (#11762) · Issues · drm / i915 / kernel · GitLab](https://gitlab.freedesktop.org/drm/i915/kernel/-/issues/11762)

But changing it to the "normal HDMI cable" (didn't work in previous Work Laptop because the HDMI port version didn't support 4k@60Hz)
seems to be working without "blinks".

## 8k 60Hz Display (/Widescreen (3600/1600) at 144Hz)

[GREATHTEK 8K@60Hz](https://www.amazon.es/dp/B0CQR9RFGQ?ref=ppx_yo2ov_dt_b_fed_asin_title&th=1)
* KVM Switch HDMI 2 portas
* USB3.0 KVM Switch 2 PC 1 monitor
* HDMI2.1, HDCP2.3
* interruptor KVM
* com 3 portas USB3.0
* controlador de mesa com cabo
* EDID adaptativo

[LG 38GN950-B Review - RTINGS.com](https://www.rtings.com/monitor/reviews/lg/38gn950-b)
Only source stating the HDMI ports are HDMI 2.0
8k60Hz only works with HDMI 2.1.

## 8k 60Hz DisplayPort

[8K60Hz Switch KVM DisplayPort 2 computadores que compartilham 1](https://www.amazon.es/gp/product/B0DDTQY993/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&th=1)
* monitor KVM DP1.4 USB 3.0
* 4 portas USB adequadas para rato teclado impressora
* controlador de extensão externa
* 2 cabos USB

## KVM Switch button "merger"

[SUCESO Cabo divisor Splitter Jack auriculares 3,5 mm macho a 2 fêmea cabo nylon entrançado áudio estéreo Splitter para Samsung, Huawei, HTC, Sony, LG, Tablet, auriculares, alto-falante, MP3, Laptop-35 : Amazon.es: Eletrónica](https://www.amazon.es/-/pt/dp/B07K44B7HL?crid=31INS4W5T321T&dib=eyJ2IjoiMSJ9.XtWjQF25x2DSz7Ve7o9M1AErfZX9QhBYHN9kBIN1sZWJ54bz7ncEXzym68VYO4r5Q0Jsqsn5ed20Xc8sIHGcnvwJaQ96V4zP5044LVGGInAV6Q0N35IDvgwaFy0GyO7xNN72ggz3Kq8vYIcgiW-Seydu_nsPi5npx8vCKIuRLEvAaWNpw9HLWdLP7gAJjaQRSnB47ZLo0xUPul49f43z9hEOPOSOm3Sih7SLP93AM-_7_DyVEBJSwQsZLvRI1TCoxMeXVQIC-hqjzQ_LivqezaToTFgkkRqz_hjDhopBw0o.fa-kvhLE346slbXx9oKD8kGXK-LMeYlFo_zNDrWZnCY&dib_tag=se&keywords=3.5mm+splitter&nsdOptOutParam=true&qid=1734259067&sprefix=3.5mm+spli%2Caps%2C123&sr=8-8)
[Cabo adaptador jack de 3,5 mm, Splitter, para Samsung Ace Note N7000 Wave Y 2 3 : Amazon.es: Eletrónica](https://www.amazon.es/-/pt/dp/B00F34FRCE?crid=31INS4W5T321T&dib=eyJ2IjoiMSJ9.XtWjQF25x2DSz7Ve7o9M1AErfZX9QhBYHN9kBIN1sZWJ54bz7ncEXzym68VYO4r5Q0Jsqsn5ed20Xc8sIHGcnvwJaQ96V4zP5044LVGGInAV6Q0N35IDvgwaFy0GyO7xNN72ggz3Kq8vYIcgiW-Seydu_nsPi5npx8vCKIuRLEvAaWNpw9HLWdLP7gAJjaQRSnB47ZLo0xUPul49f43z9hEOPOSOm3Sih7SLP93AM-_7_DyVEBJSwQsZLvRI1TCoxMeXVQIC-hqjzQ_LivqezaToTFgkkRqz_hjDhopBw0o.fa-kvhLE346slbXx9oKD8kGXK-LMeYlFo_zNDrWZnCY&dib_tag=se&keywords=3.5mm+splitter&nsdOptOutParam=true&qid=1734259067&sprefix=3.5mm+spli%2Caps%2C123&sr=8-16)

## Blinking Screen issue

[Issues with LG 38GN950-B : ultrawidemasterrace](https://www.reddit.com/r/ultrawidemasterrace/comments/hsfgzp/issues_with_lg_38gn950b/)
[I found a fix for monitor black screen with AMD GPU (5700XT) : Amd](https://www.reddit.com/r/Amd/comments/klkk49/i_found_a_fix_for_monitor_black_screen_with_amd/)
> Open up AMD Radeon Software, go to display tab then global display settings.
> Click on the display that is having the issue.
> There should be a tab on the right side called "Overrides".
> Click on that to open the drop down menu.
> Go to **HDCP Support and make sure you disable that**. Restart your computer.
