[Kailh Box Brown or White, which do you prefer?](https://www.reddit.com/r/AnnePro/comments/a3cbk8/kailh_box_brown_or_white_which_do_you_prefer/)
[[HELP] Kailh BOX Browns vs Reds](https://www.reddit.com/r/AnnePro/comments/avqkzg/help_kailh_box_browns_vs_reds/)

## Anne Pro 2

[My anne pro with dedicated delete key](https://www.reddit.com/r/AnnePro/comments/6rqkfz/my_anne_pro_with_dedicated_delete_key/)
[Media keys](https://www.google.com/search?client=firefox-b-d&q=anne+pro+2+media+keys)

## DIY

### Double Sleeve

[DIY Custom Sleeved USB Cable for Mechanical Keyboards (Updated)](https://www.youtube.com/watch?v=GCexLMPaNqo)
Double Sleeved - Paracord + Techflex

### Aviator Connector

[DIY Aviator Connector Tutorial - Mechanical Keyboards](https://www.youtube.com/watch?v=fYRHLyW6Mvs)

### Coil cables

[DIY Cable Coiling Tutorial - Coiled Cables for Mechanical Keyboards](https://www.youtube.com/watch?v=hBUZGjgT1t0)

## Showcase

[Nullbits Nibble with some crappy Amazon keycaps. My first build btw](https://www.reddit.com/r/MechanicalKeyboards/comments/k8khfk/nullbits_nibble_with_some_crappy_amazon_keycaps/)
