# Monitoring

[Uptime Kuma](https://uptime.kuma.pet/)

[The RED Method: How to Instrument Your Services | Grafana Labs](https://grafana.com/blog/2018/08/02/the-red-method-how-to-instrument-your-services/)

## Notifications

[Pushover: Simple Notifications for Android, iPhone, iPad, and Desktop](https://pushover.net/)

Initially found in [Which lifetime licenses have been worth it for you? : selfhosted](https://www.reddit.com/r/selfhosted/comments/1fc4cq5/which_lifetime_licenses_have_been_worth_it_for_you/)

[ntfy.sh | Send push notifications to your phone via PUT/POST](https://ntfy.sh/#free-software)
Self Hosted alternative.

[Ntfy or Pushover.net](https://www.reddit.com/r/selfhosted/comments/1fdoug5/ntfy_or_pushovernet/#:~:text=the%20thing%20about,probably%20this.)
> The thing about monitoring and alerts, you really want to receive them when your servers down or having other issues.. so if there is something you'd want to outsource, it's probably this.

[caronc/apprise: Apprise](https://github.com/caronc/apprise)
> Push Notifications that work with just about every platform!
