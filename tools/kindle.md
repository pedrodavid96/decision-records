# Kindle

## Extract Highlights

1. Use Kindle for Android to share highlights on personal documents

2. Open HTML and copy manually

3. Extract Information with the following regex:

```regexp
Highlight \(yellow\) - Chapter 6: Distill—Find the Essence > Location (\d{4})\n(.*)
```