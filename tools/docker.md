# Docker

[A Docker Tutorial for Beginners](https://docker-curriculum.com/)

[Best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

[bocker: Docker implemented in around 100 lines of bash](https://github.com/p8952/bocker)

[Run Multiple Docker Environments (qa, stage, prod) from the Same docker-compose File](https://staxmanade.com/2016/07/run-multiple-docker-environments--qa--beta--prod--from-the-same-docker-compose-file-/)

[GitHub - Tecnativa/docker-socket-proxy: Proxy over your Docker socket to restrict which requests it accepts](https://github.com/Tecnativa/docker-socket-proxy)

[5 Tips for Building Container Friendly Applications by Mike Treadway](https://medium.com/faun/5-tips-for-building-container-friendly-applications-9920db4f3dc9)
Clean article with multiple bullet points and a "Why" section for each tip
1. Use Environment Variables for Configuration
Users cannot change configuration files in a container easily. If you package up your application in a container image, the configuration files are part of that image.
2. Allow Sensitive Configuration to be Passed in Separately
The reasoning for this tip has more to do with good deployment practices than just being container friendly.
Most container orchestration tools allow for sensitive information to be stored separately from non-sensitive configuration data (e.g.: k8s secrets)
3. Parameterize File System Paths
Storage in a container is ephemeral. Whatever data you write on the container’s file system, is lost when the container stops running. By allowing the file system path to be passed in, you can mount a storage volume to a path within the container that doesn’t go away when the container stops.
4. Allow Logging Only to Stdout & Stderr
Most container orchestration platforms expect containers to write log data in the stdout and stderr streams and have mechanisms to obtain what was written even after the container stops running.
5. Design for Effortless Horizontal Scaling

[How To Create Optimized Docker Images For Production](https://haydenjames.io/how-to-create-optimized-docker-images-for-production/)
Why?
Let’s say you have a huge, 1GB image. Every time you push a change and trigger a build, it will create huge stress on your network and make your CI/CD pipeline sluggish. All of this costs you time, resources, and money. Sending data over the network in not cheap in the cloud. Cloud providers like AWS are billing you for every byte you send.

One important point to consider as well is security. Keeping images tiny reduces the attack vector for potential attacks. Smaller images equal smaller attack surface, because they have fewer installed packages.

How to create optimized Docker images?
1. Lean base (e.g.: Alpine Linux)
2. Multi-stage build proccess
Excluding build tools by using multi-stage builds
3. Layers

[Version Numbers for Continuous Delivery with Maven and Docker](https://phauer.com/2016/version-numbers-continuous-delivery-maven-docker/)

[Don't Put Fat Jars in Docker Images](https://phauer.com/2019/no-fat-jar-in-docker-image/)
TL;DR
* A fat jar contains all dependencies that usually don’t change between releases. But those dependencies are copied again and again into each fat jar leading to a waste of space, bandwidth and time.
* For instance, the fat jar of our Spring Boot application was 72 MB big but contained only 2 MB code. Usually, the code is the only part that has been changed.
* Fortunately, we can leverage Docker’s image layering: By putting the dependencies and resources in different layers, we can reuse them and only update the code for each artifact/release.
* [Jib](https://github.com/GoogleContainerTools/jib) provides an easy-to-use plugin for Maven and Gradle to implement this approach. No need to write a Dockerfile manually.
    - `containerizingMode` can be `packaged` in order to ship a JAR instead of `.class` only

[Dive](https://github.com/wagoodman/dive) - A tool for exploring each layer in a docker image

[Reducing Docker image size and cutting Google Cloud costs](https://www.mattzeunert.com/2019/10/13/reducing-docker-image-size-and-cutting-gcp-cost.html)

[Control Docker containers from within container](https://fredrikaverpil.github.io/2018/12/14/control-docker-containers-from-within-container/)
[How can I get Docker Linux container information from within the container itself?](https://stackoverflow.com/questions/20995351/how-can-i-get-docker-linux-container-information-from-within-the-container-itsel)

[The Difference Between Docker Compose And Docker Stack](https://vsupalov.com/difference-docker-compose-and-docker-stack/)

[Labels in docker-compose](https://docs.docker.com/compose/compose-file/#labels)

## ARG vs ENV
ARG is for build time, env is for runtime.

## Distroless containers
[Technology Radar](https://www.thoughtworks.com/radar/techniques/distroless-docker-images)
[Distroless is for Security if not for Size](https://medium.com/@dwdraju/distroless-is-for-security-if-not-for-size-6eac789f695f)

[VS Code’s Docker extension generates insecure and broken images](https://pythonspeed.com/articles/vscode-broken-docker/)
Problem #1: The container runs as root
            [Less capabilities, more security: minimizing privilege escalation in Docker](https://pythonspeed.com/articles/root-capabilities-docker-security/)
            #### Dropping capabilities
            ```bash
            $ docker run --cap-drop
            ```
            > --cap-drop 	Drop Linux capabilities
            #### The correct way to run as a non-root user
            ```Dockerfile
            RUN useradd --create-home appuser
            WORKDIR /home/appuser
            USER appuser
            ```
            _edit:_
            https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#user
            ```Dockerfile
            RUN groupadd -r postgres && useradd --no-log-init -r -g postgres postgres
            ```
            Consider an explicit UID/GID
Problem #2: The base packages aren’t getting security updates
            ```Dockerfile
            RUN apt-get update && apt-get -y upgrade
            ```

[OpenContainers ImageSpec annotations](https://github.com/opencontainers/image-spec/blob/master/annotations.md)

[Top 20 Docker Security Tips](https://towardsdatascience.com/top-20-docker-security-tips-81c41dd06f57)
1. Access Management — Limit Privileges
   `--cap-drop` and `--cap-add`
2. Avoid running as root
   You can specify a userid other than root at build time like this:
   `docker run -u 1000 my_image`
   The -- user or -u flag, can specify either a username or a userid. It's fine if the userid doesn't exist.
   In the example above 1000 is is an arbitrary, unprivileged userid. In Linux, userids between 0 and   499 are generally reserved. Choose a userid over 500 to avoid running as a default system user.
   Rather than set the user from the command line, it’s best to change the user from root in your image. Then folks don’t have to remember to change it at build time. **Just include the USER Dockerfile instruction in your image after Dockerfile instructions that require the capabilities that come with root**.
3. Capabilities
   A best policy is to drop all a container's privileges with `--cap-drop all` and add back the ones needed with `--cap-add`
4. Access Management — Restrict Resources
   specifying the `--memory` flag or `-m` for short
5. Use trustworthy images
6. Reduce your attack surface
   You can use a minimal base image.
   Less code inside
   Similar, only install packages you actually need
7. Require signed images
   Content trust is disabled by default. Enable it with `export DOCKER_CONTENT_TRUST=1`
8. Managing Secrets
   Overall, volumes are a pretty good solution.
   Even better than volumes, use Docker secrets. Secrets are encrypted.
   Some [Docker docs](https://docs.docker.com/engine/swarm/secrets/) state that you can use secrets with Docker Swarm only.
   Nevertheless, you can use secrets in Docker without Swarm.
   You can use BuildKit. BuildKit is a better backend than the current build tool for building Docker images.
   It cuts build time significantly and has other nice features, including build-time secrets support.
9. Update Things
## Additional Tips
- Don’t ever run a container as `--privileged` unless you know what you're doing
  (docker in docker may need it).
- Favor COPY instead of ADD.
  Avoid using ADD to reduce susceptibility to attacks through remote URLs and Zip files.
- If you run any other processes on the same server, run them in Docker containers.
- If you use a web server and API to create containers, check parameters carefully so new containers you  don’t want can’t be created.
- If you expose a REST API, secure API endpoints with HTTPS or SSH.
- Consider a checkup with Docker Bench for Security to see how well your containers follow their security guidelines.
- Store sensitive data only in volumes, never in a container.
- If using a single-host app with networking, don’t use the default bridge network.
  It has technical shortcomings and is not recommended for production use.
  If you publish a port, all containers on the bridge network become accessible.
- Use Lets Encrypt for HTTPS certificates for serving. See an example with NGINX [here](https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71).
- Mount volumes as read-only when you only need to read from them. See several ways to do this [here](https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/Docker_Security_Cheat_Sheet.md).

[Docker Security](https://levelup.gitconnected.com/docker-security-5f4df118948c)
Most tips should be already on previous articles but they seemed "advanced enough" to maybe contain some tip that isn't in the others.

---

[Build images with BuildKit](https://docs.docker.com/develop/develop-images/build_enhancements/)

## More security ips

CVE - Common Vulnerabilities and Exposures
[Adding CVE Scanning to a CI/CD Pipeline](https://medium.com/better-programming/adding-cve-scanning-to-a-ci-cd-pipeline-d0f5695a555a)
Uses [clair](https://github.com/quay/clair)
> Clair is an open source project for the static analysis of vulnerabilities in application containers (currently including appc and docker).
**Edit:** Also tests with [aquasecurity / microscanner](https://github.com/aquasecurity/microscanner)

[Access the Docker Daemon via SSH](https://medium.com/better-programming/docker-tips-access-the-docker-daemon-via-ssh-97cd6b44a53)

[You don't need an image to run a container](https://iximiuz.com/en/posts/you-dont-need-an-image-to-run-a-container/?utm_medium=reddit&utm_source=r_programming)
His whole [blog](https://iximiuz.com/en/) looks like a cool resource for Docker
He uses [runc](https://github.com/opencontainers/runc):
`runc` is a CLI tool for spawning and running containers according to the OCI specification.
[OCI](https://opencontainers.org/)

[Docker RUN vs CMD vs ENTRYPOINT](https://goinbigdata.com/docker-run-vs-cmd-vs-entrypoint/)
In a nutshell:
* RUN executes command(s) in a new layer and creates a new image. E.g.,
  it is often used for installing software packages.
* CMD sets default command and/or parameters, which can be overwritten
  from command line when docker container runs.
* ENTRYPOINT configures a container that will run as an executable.
[on the same topic](https://stackoverflow.com/a/21564990)

[Super-Slim Docker Containers](https://medium.com/better-programming/super-slim-docker-containers-fdaddc47e560)
Squashing the image: `--squash`
Delete Caches:
```
APK: ... && rm -rf /etc/apk/cache
YUM: ... && rm -rf /var/cache/yum
APT: ... && rm -rf /var/cache/apt
```

[Build Containers the Hard Way (WIP)](https://containers.gitbook.io/build-containers-the-hard-way/)
> This guide is geared towards anyone interested in learning the low-level details regarding how to build containers. This guide is not intended for those who wish to learn to build container images with high-level tools like Docker.
Created by https://github.com/coollog, author of Google Jib (Docker plugin)

[You’re using docker-compose wrong](https://blog.earthly.dev/youre-using-docker-compose-wrong/)
1. You’re using the host network
   By default, `docker-compose` spins up its own network called `<folder-name>_default` so just don't override it
   * More isolated network, less likely to depend on system specificities
   * Won't expose the ports by default
   * You can talk between services by using their compose names as host names, e.g.: `db:5432` is more intuitive than `localhost:5432`
   * Most ports don’t need to be opened up to the host too - which means that they are not competing on global resources, should you need to increase the replication via `--scale`.
2. You’re binding ports on the host’s 0.0.0.0
   > Binding ports as `8080:8080`. At first glance, this looks innocuous. But the devil is in the details. This extremely common port bind is not just forwarding a container port to the localhost - it forwards it to be accessible on every network interface on your system - including whatever you use to connect to the internet.
   **The fix is very easy:** Just add `127.0.0.1:` in front. So for example `127.0.0.1:8080:8080`. This simply tells docker to only expose the port to the loopback network interface and nothing else.
3. You’re using sleep to coordinate service startup
4. You’re running the DB in docker-compose, but the test on the host

[Dockerfile best practices](https://github.com/hexops/dockerfile)
* Run as a non-root user
* Do not use a UID below 10,000
* Use a static UID and GID
* Do not use latest, pin your image tags
* Use tini as your ENTRYPOINT
* Only store arguments in CMD
* Install bind-tools if you care about DNS resolution on some older Docker versions
[Reddit discussion](https://www.reddit.com/r/programming/comments/kc0i5u/wrote_up_a_set_of_dockerfile_bestpractices_based/)
Commenter shameless plug on [docker-lock](https://github.com/safe-waters/docker-lock)

[Docker Security Best Practices from the Dockerfile](https://cloudberry.engineering/article/dockerfile-security-best-practices/)
> I've compiled a list of common docker security issues and how to avoid them.
> For every issue I’ve also written an Open Policy Agent (OPA) rule ready to be used to statically analyze your Dockerfiles with conftest.
[Open Policy Agent](https://www.openpolicyagent.org/)
Policy-based control for cloud native environments - Flexible, fine-grained control for administrators across the stack
[Conftest](https://www.conftest.dev/) - Conftest is a utility to help you write tests against structured configuration data. For instance, you could write tests for your Kubernetes configurations, Tekton pipeline definitions, Terraform code, Serverless configs or any other structured data.

1. Do not store secrets in environment variables
2. Only use trusted base images
3. Do not use ‘latest’ tag for base image
4. Avoid curl bashing
   * use a trusted source
   * use a secure connection
   * verify the authenticity and integrity of what you download
5. Do not upgrade your system packages (you want to pin the version of your software dependencies)
6. Do not use ADD if possible
7. Do not root
8. Do not sudo
[Reddit discussion](https://www.reddit.com/r/programming/comments/jb2jwq/dockerfile_security_best_practices/)

[Buildah](https://buildah.io/)
A tool that facilitates building OCI container images
[Kaniko](https://github.com/GoogleContainerTools/kaniko)
A tool to build container images from a Dockerfile, inside a container or Kubernetes cluster.
Doesn't depend on a Docker daemon and executes each command within a Dockerfile completely in userspace.
[A quick look at Google's Kaniko project](https://blog.alexellis.io/quick-look-at-google-kaniko/)
[Use kaniko to build Docker images](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)
[Using docker build](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
A lot of idiosyncrasies since it's running already in a docker container and might need root which we might not have. Kaniko might be a good alternative.

[On Docker security: 'docker' group considered harmful](https://blog.zopyx.com/on-docker-security-docker-group-considered-harmful/)
Original article unavailable, could find it in [web archive](https://web.archive.org/web/20201116170027/https://blog.zopyx.com/on-docker-security-docker-group-considered-harmful/)
> By default I can not access /etc/shadow because it is only readable by root or group wheel:
```bash
ajung@demo:~$ whoami
ajung    pts/2        Feb  5 07:03
ajung@demo:~$ groups
ajung docker
ajung@demo:~$ ls -la /etc/shadow
-rw-r----- 1 root shadow 897 Jan 25 10:05 /etc/shadow
ajung@demo:~$ cat /etc/shadow
cat: /etc/shadow: Permission denied
```
> Now I create a simple Docker image that exposes /data as mount point for a volume
```Dockerfile
FROM phusion/baseimage
VOLUME /data
```
```bash
ajung@demo:~$ docker run -v /etc:/data zopyx/test cat /data/shadow
root:$6$rnW9d.................awVOOsWtCb41DY01:16457:0:99999:7:::
daemon:*:16457:0:99999:7:::
bin:*:16457:0:99999:7:::
sys:*:16457:0:99999:7:::
sync:*:16457:0:99999:7:::
games:*:16457:0:99999:7:::
```
> So user accounts belonging to the UNIX group 'docker' are fully exploitable.
> Standard UNIX users can gain elevated rights on the local machine if they belong
>  to the 'docker' group and can perhaps exploit other machines as well by
>  tampering SSH keys etc....many attack vectors are possible.

> Update (2015-02-05, 16:00 UTC)
> The discovered behavior is in fact intentional and documented in the Docker security documentation.
> The first sentence is already completely broken.
> "Only trusted users should be allowed to control your Docker daemon"
> Docker leaves security to the user and administrators instead of providing a secure way for building secure containers for deployment.

[Container networking is simple](https://iximiuz.com/en/posts/container-networking-is-simple/)
Seems like a great in-depth article.

[Best practices for writing a Dockerfile](https://blog.bitsrc.io/best-practices-for-writing-a-dockerfile-68893706c3)
2. Reduce Image Size
   Remove Unnecessary Dependencies
   ```
   RUN apt-get update && apt-get -y install --no-install-recommends
   ```

[How to (not) use Docker to share your password with hackers](https://pythonspeed.com/articles/leaking-secrets-docker/)
* Some evidence this actually happens.
* Leaking build time secrets.
* Accidental leaks with COPY.
* Leaking runtime secrets.
* Some (partially?) missing tooling that would help fix the problem.

[Don’t leak your Docker image’s build secrets](https://pythonspeed.com/articles/docker-build-secrets/)
* Some seemingly reasonable but actually insecure or problematic solutions.
* The easy solution, if you can use modern Docker features.
* The sneaky, backwards-compatible solution: getting secrets in through the network.
* Other potential approaches.

[run_locally_or_in_docker](https://gitlab.com/gitlab-org/gitlab/-/blob/c39d19e9606575d9533aa9f6855b993bf98759c8/scripts/lint-doc.sh#L108)

[What's are the benefits for using Data Volume Container as a single-database "backend" in Docker? - Stack Overflow](https://stackoverflow.com/questions/28969428/whats-are-the-benefits-for-using-data-volume-container-as-a-single-database-ba)

[BuildKit | Docker Documentation](https://docs.docker.com/build/buildkit/)
Docker secrets without Docker Swarm

```
sudo docker run --detach     --name watchtower     --volume /var/run/docker.sock:/var/run/docker.sock -e WATCHTOWER_MONITOR_ONLY=true -e WATCHTOWER_RUN_ONCE=true     containrrr/watchtower
```

[How Container Networking Works - Building a Linux Bridge Network From Scratch](https://labs.iximiuz.com/tutorials/container-networking-from-scratch)

[Cloud Native Buildpacks · Cloud Native Buildpacks](https://buildpacks.io/)

[Diun](https://crazymax.dev/diun/)
> Docker Image Update Notifier

[LinuxServer.io](https://docs.linuxserver.io/)

## Updates

[getwud/wud at selfh.st](https://github.com/getwud/wud?ref=selfh.st)

## Alternatives

[Top 3 Docker Alternatives to Consider in 2023 | by BuildPiper | BuildPiper | Medium](https://medium.com/buildpiper/top-3-docker-alternatives-to-consider-in-2023-712b1ca0cdd1)

* Podman
* Containerd
* LXD

[abiosoft/colima: Container runtimes on macOS (and Linux) with minimal setup](https://github.com/abiosoft/colima)
