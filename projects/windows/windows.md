# Windows

## Setup

* [Install WSL on external drive](#install-wsl-on-external-drive)
* [Change location of docker images when using Docker Desktop on WSL2](https://stackoverflow.com/a/77237206)
* [Remove Pagefile (aka Swap)](https://www.makeuseof.com/windows-pagefile-sys-guide/)
* Disable Hibernation (got back  30GB of disk space...)
  ```
  powercfg.exe /hibernate off
  ```

## Install WSL on external drive

[Installing WSL on another drive in Windows - DEV Community](https://dev.to/mefaba/installing-wsl-on-another-drive-in-windows-5c4a)
[Basic commands for WSL | Microsoft Learn](https://learn.microsoft.com/en-us/windows/wsl/basic-commands)

```
wsl --list --verbose
wsl --terminate Ubuntu
wsl --export Ubuntu "W:\ubuntu-export.tar"
wsl --unregister Ubuntu
wsl --import Ubuntu "W:\AppData\Local\wsl\ubuntu" "W:\ubuntu-export.tar"
wsl --set-default Ubuntu
cd %userprofile%\AppData\Local\Microsoft\WindowsApps
<ubuntu> config --default-user <username>
```

Get WSL IP
```
wsl hostname -I
```

## [How do I disable anything CoPilot/AI related on Win 11? : Windows11](https://www.reddit.com/r/Windows11/comments/1bh7n4d/how_do_i_disable_anything_copilotai_related_on/)

### Remove CoPilot from the Taskbar:

1. Go to Settings > Personalization.
2. Scroll down and select Taskbar.
3. Toggle off CoPilot.

### Disable CoPilot Completely:

1. Click Start and search for "gpedit" to open the Group Policy Editor.
2. Navigate to User Configuration > Administrative Templates > Windows Components > Windows CoPilot.
3. Double-click Turn off Windows CoPilot.
4. Click Enabled, then Apply, and OK.


## [Fix for Windows Update KB5034441 that keeps failing][windows-update-fix]

[windows-update-fix]: https://www.reddit.com/r/Windows10/comments/1bjqq8g/fix_for_windows_update_kb5034441_that_keeps/

1. Use a program like Veeam, Macrium or Acronis to backup your PC before continuing.
   Not that these steps are very unlikely to cause any system issues, but they do include altering system files, so better be safe than sorry.
2. Open CMD as Admin and write `reagentc /info` to determine if Windows RE is enabled or not.
   If it's already disabled, continue, otherwise write `reagentc /disable` to disable it.
3. Go to the [Windows site][windows-site] to create Installation Media for Windows and download the corresponding Tool for your system
4. Open the Tool and choose "Create Installation Media" > Next > ISO file > download the ISO file somewhere on your C-drive,
   e.g. C:\ISO\ > Mount the ISO-file in Explorer > Take note of the Drive Letter (e.g. E:)
5. Go back to CMD and write:
   `DISM.exe /Get-ImageInfo /ImageFile:[Drive Letter]:\sources\install.esd /Index:1`
6. Write in CMD:
   `DISM.exe /Export-Image /SourceImageFile:[Drive Letter]:\sources\install.esd /SourceIndex:1 /DestinationImageFile:C:\install.wim /Compress:fast /CheckIntegrity`
7. Create a new dir: C:\MountDir
8. Write in CMD:
   `DISM.exe /Mount-Wim /WimFile:C:\install.wim /index:1 /MountDir:C:\MountDir /ReadOnly`
9. Open `C:\MountDir\Windows\System32\Recovery\` and copy both `ReAgent.xml` and `Winre.wim`. Paste both files in a new directory somewhere else, as these files will be used later.
10. Write in CMD:
    `DISM.exe /Unmount-Wim /MountDir:C:\MountDir /discard`
    This will empty the directory and unmount the DISM image.
11. Delete the folders C:\MountDir, C:\ISO (unmount first) and the file C:\install.wim.
12. Copy the two files from the directory you copied them to earlier, and paste them into C:\Windows\System32\Recovery\. Replace the file(s) if they already exists.
    _Personal note:_ There was a file "ReAgent_Merged.xml1" there, which I ignored and left there
13. Write in CMD:
    `reagentc /enable`
14. Attempt to run a Windows Update again, and this time it should install the KB5034441 update.

The nice thing is now that you have a portable solution to the problem. The next machine you need to fix for this issue, you simply have to do the following:

1. Open CMD and write: `reagentc /disable`
2. Copy the two files to C:\Windows\System32\Recovery\
3. Write in CMD: `reagentc /enable`
4. Run Windows Update, and voila

[windows-site]: https://support.microsoft.com/en-us/windows/create-installation-media-for-windows-99a58364-8c02-206f-aa6f-40c3b507420d

---

[my tpm 2.0 and secure boot is enabled and i still cant launch my game : ValorantTechSupport](https://www.reddit.com/r/ValorantTechSupport/comments/qgalra/my_tpm_20_and_secure_boot_is_enabled_and_i_still/)

```
i had the same problem, i fixed. it was the secure boot which wasn't actually enabled. when i restore my default settings of key managment (secure boot), the system restore just the pk key and leave 0 value on the other 3 keys.. so windows doesn't recognize secure boot. I just figure it out by chance... i hope it can be useful to you.

To see if secure boot and tpm are actually enabled on windows do as follow:
open a power shell as administrator
type: "get-tpm" to see if tpm is enabled
type: "Confirm-SecureBootUEFI" to see if secure boot is enabled
if both of them are actually enabled the game should work properly.

Remeber, to enable secure boot you need to:
disbale csm
disbale fast boot
enable secure boot as Windows UEFI
restore default key managment values
```

## Terminal

Need to migrate to PowerShell 7
[Migrating from Windows PowerShell 5.1 to PowerShell 7 - PowerShell | Microsoft Learn](https://learn.microsoft.com/en-us/powershell/scripting/whats-new/migrating-from-windows-powershell-51-to-powershell-7?view=powershell-7.4)
[Chocolatey Software | powershell-core (Install) 7.4.6.20241029](https://community.chocolatey.org/packages/powershell-core/7.4.6.20241029)

[PowerShell/PSReadLine: A bash inspired readline implementation for PowerShell](https://github.com/PowerShell/PSReadLine)

[mridgers/clink: Bash's powerful command line editing in cmd.exe](https://github.com/mridgers/clink)

### Oh My Posh

[Customize | Oh My Posh](https://ohmyposh.dev/docs/installation/customize)
[Themes | Oh My Posh](https://ohmyposh.dev/docs/themes#catppuccin)
[catppuccin/windows-terminal: 🪟 Soothing pastel theme for Windows Terminal](https://github.com/catppuccin/windows-terminal/tree/main)

## Misc

> [!TODO]
> [Sudo for Windows | Microsoft Learn](https://learn.microsoft.com/en-us/windows/sudo/)

> [!TODO]
> [ahk-share/WinZones at main · NeuronHuskie/ahk-share](https://github.com/NeuronHuskie/ahk-share/tree/main/WinZones)


[NixOS on Microsoft Windows 11 using WSL2 | Nathan Bijnens](https://nathan.gs/2023/12/14/nixos-on-windows/)


## Share screen (widescreen)

[tom-englert/RegionToShare: Helper app to share only a part of a screen via video conference apps](https://github.com/tom-englert/RegionToShare)
