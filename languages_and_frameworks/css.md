# CSS

[State of CSS in 2022](https://youtu.be/Vwq93hBVp6g)

[CSS resets | twitter discussion](https://mobile.twitter.com/b0rk/status/1291744996404727808)
[Main CSS Reset](https://meyerweb.com/eric/tools/css/reset/)
[CSS Resets reasoning](http://meyerweb.com/eric/thoughts/2007/04/18/reset-reasoning/)

[A modern alternative to CSS resets](https://github.com/necolas/normalize.css)
[Or a more lean approach](https://github.com/sindresorhus/modern-normalize)
[Normalize CSS or CSS Reset?!. CSS Architecture — Part 1 | by Elad Shechter | Medium](https://elad.medium.com/normalize-css-or-css-reset-9d75175c5d1e)

[cssDb | What's next for CSS](https://cssdb.org/)

## [Hyperstyle Vim Plugin](http://ricostacruz.com/vim-hyperstyle/)

## CSS Grid

[The noob’s guide to CSS Grid](https://blog.logrocket.com/the-simpletons-guide-to-css-grid-1767565b3cf7/)
[A Complete Guide to Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)

[Ten modern layouts in one line of CSS](https://web.dev/one-line-layouts/)
>  This post highlights a few powerful lines of CSS that do some serious heavy lifting and help you build robust modern layouts.
Most use CSS Grid

[Flexbox vs. CSS Grid — Which is Better?](https://www.youtube.com/watch?v=hs3piaN4b5I)

[Building A Conference Schedule With CSS Grid | CSS-Tricks](https://css-tricks.com/building-a-conference-schedule-with-css-grid/)

## Linter
### Auto Fixing errors
https://stylelint.io/user-guide/cli/#autofixing-errors

## CSS animations
https://thoughtbot.com/blog/css-animation-for-beginners

## Flexbox
https://css-tricks.com/snippets/css/a-guide-to-flexbox/

## TypeWriter effect
https://css-tricks.com/snippets/css/typewriter-effect/

## Blinking cursor
https://codepen.io/ArtemGordinsky/pen/GnLBq?css-preprocessor=less

## Horizontal social icon list

https://codepen.io/bsngr/pen/godys

https://stackoverflow.com/questions/32406364/flexbox-with-unordered-list
https://css-tricks.com/centering-list-items-horizontally-slightly-trickier-than-you-might-think/

## Reverse color
https://css-tricks.com/reverse-text-color-mix-blend-mode/

## CSS grid: Trying CSS Grid for the first time by Fun Fun Function
https://www.youtube.com/watch?v=3Ne9-9n5Oq0

## CSS Size units / Font Scaling
https://medium.com/code-better/css-units-for-font-size-px-em-rem-79f7e592bb97
https://stackoverflow.com/questions/16056591/font-scaling-based-on-width-of-container
https://kyleschaeffer.com/css-font-size-em-vs-px-vs-pt-vs-percent

## Keep the footer at the bottom
https://medium.com/@zerox/keep-that-damn-footer-at-the-bottom-c7a921cb9551

## Scrolling to an Anchor (beautifully)

https://stackoverflow.com/questions/25020582/scrolling-to-an-anchor-using-transition-css3

## Colors

Reduce eye strain by using a "dilluted colors" (not complete white and complete black)

Backgroung: #eee
Foreground: #444

## Typography

https://medium.com/@zkareemz/golden-ratio-62b3b6d4282a
https://pearsonified.com/characters-per-line/

## Good minimalism references

http://motherfuckingwebsite.com/
http://bettermotherfuckingwebsite.com/
https://thebestmotherfucking.website/
[Starter kit for lightweight sites](https://minwiz.com/#h)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/krooqi/a_full_website_in_17_kb_all_assets_included/)

## Logical Styling Based on the Number of Given Elements
https://css-tricks.com/solved-with-css-logical-styling-based-on-the-number-of-given-elements/

## Dropdown Menus
https://css-tricks.com/solved-with-css-dropdown-menus/


## Child max-width getting shrinked by aligning items center with flexbox
https://stackoverflow.com/questions/35836415/flexbox-align-items-center-shrinks-a-childs-max-width

## [CSS Counters](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Lists_and_Counters/Using_CSS_counters)

## Timelines
[What I'm trying to emulate](https://codepen.io/banik/pen/ELpWNJ?editors=1100#0)
[Close and nice color usage, with dates](https://codepen.io/rossmcneil/pen/rcxGb/)
[Simple "labely" with dates concept](https://codepen.io/NilsWe/pen/FemfK)
[Simplist | cool animations!](https://codepen.io/Fischaela/pen/sJdqg/)
[Simple W3 Schools tutorial](https://www.w3schools.com/howto/howto_css_timeline.asp)
[Custom Counter with cool effect around elements](https://codepen.io/letsbleachthis/pen/YJgNpv)
	Might be cooler for "simple" lists.
[CV Template](https://www.canstockphoto.co.uk/modern-cv-resume-with-detailed-timeline-39666122.html)
[CV Template](https://www.123rf.com/photo_51242263_stock-vector-modern-resume-cv-curriculum-vitae-template-beginning-with-timeline.html)
[Data visualization (image, non css)](https://visual.ly/community/infographic/other/curriculum-vitae)
[Jonas Hietala: A simple timeline using CSS flexbox](https://www.jonashietala.se/blog/2024/08/25/a_simple_timeline_using_css_flexbox/)

## Cards
[W3 Schools beginer tutorial](https://www.w3schools.com/howto/howto_css_cards.asp)
[Examples](https://speckyboy.com/css-content-cards/)
[How to Create a Card Layout Using CSS3 Grids](https://www.atyantik.com/creating-card-layout-with-css3-grid/)
[74 CSS Cards](https://freefrontend.com/css-cards/#stacked-cards)
https://codepen.io/anon/pen/BXNpvG
https://codepen.io/veronicadev/pen/WJyOwG
https://codepen.io/Moiety/pen/gGKRog
https://codepen.io/JanneLeppanen/pen/EMRrOX

---

[To Grid or to Flex?](https://css-irl.info/to-grid-or-to-flex/)

[Stepping away from Sass](https://cathydutton.co.uk/posts/why-i-stopped-using-sass/)


## [Build a Responsive, Modern Dashboard Layout With CSS Grid and Flexbox](https://medium.com/better-programming/build-a-responsive-modern-dashboard-layout-with-css-grid-and-flexbox-bd343776a97e)

## [Responsive grids and how to actually use them](https://uxdesign.cc/responsive-grids-and-how-to-actually-use-them-970de4c16e01)

[Methodical UI: removing frustration from HTML and CSS development](https://jdauriemma.com/programming/methodical-html-css-development)

[5 Little CSS Tricks to Make Plain Pages Look Awesome](https://medium.com/better-marketing/5-little-css-tricks-to-make-your-website-go-a-long-way-7cda61fcd7d0)


[You might not need a CSS framework](https://hacks.mozilla.org/2016/04/you-might-not-need-a-css-framework/)
1. Technical debt
2. Unsemantic HTML code
3. Over-specific CSS selectors
4. Rules you don’t need
5. Owning your opinions and decisions

[Ridiculously troublesome CSS hacks - made stupidly easy with Flexbox. - discussion](https://www.reddit.com/r/programming/comments/dueqe9/ridiculously_troublesome_css_hacks_made_stupidly/)

[Purple sidebar Menu](https://codepen.io/sreisner/pen/KazJmO)

[Input Masking](https://css-tricks.com/input-masking/)

[Perfectly aligning checkboxes with text](https://twitter.com/Xewl/status/1217881481726636033?s=09)
Even when the text wraps on to multiple lines.
```html
	<div class="flex items-center">
		<!-- Zero-width space character, used to align checkbox properly -->
		&#8203;
		<input type="checkbox" />
```
```css
content: '\200B'
```

[CSS User Rating](https://www.w3schools.com/howto/howto_css_user_rating.asp)

[The Simpsons in CSS](https://pattle.github.io/simpsons-in-css/)

[18 CSS Text Glitch Effect – GSCODE](https://gscode.in/css-text-glitch-effect/)

**Pure CSS slideshow / carousel**
https://codepen.io/maheshambure21/pen/qZZrxy
https://codepen.io/stursberg/pen/lpjIH

[How to Build HTML Forms Right: Styling](https://austingil.com/build-html-forms-right-styling/#Styling_Forms_is_Not_Great)
[How to build HTML Forms Right: User Experience](https://austingil.com/build-html-forms-right-user-experience/)

[How to Title Case and Other Upper and Lower Case CSS Tricks](https://love2dev.com/blog/css-text-transform)
```css
p {
  text-transform: lowercase;
  line-height: 3;
}

p::first-letter {
  text-transform: uppercase;
  font-size: 3em;
  font-weight: bold;
  font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
  letter-spacing: 5px;
}
```

[SVG Backgrounds](https://www.svgbackgrounds.com/)
**Customize and apply backgrounds fast**
SVGs enable full-screen hi-res visuals with a file-size near 5KB and
are well-supported by all modern browsers. What's not to love?
[Discussion](https://www.reddit.com/r/programming/comments/i3kd6b/svg_backgrounds_are_much_smaller_than_using_a/)


[CSS testing checklist](https://mobile.twitter.com/b0rk/status/1320352867253932032?s=09)

[Centering in CSS](https://web.dev/centering-in-css/)

[Full-Bleed Layout Using CSS Grid](https://www.joshwcomeau.com/css/full-bleed/)
How to implement the "Holy Grail layout" with CSS Grid.

[prefers-color-scheme: Hello darkness, my old friend](https://web.dev/prefers-color-scheme/)

[Smooth scrolling with zero JavaScript](https://twitter.com/denicmarko/status/1345325767115288576)
```css
html {
    scroll-behavior: smooth;
}
```

[CSS Tips you won't see in most tutorials](https://denic.hashnode.dev/css-tips-you-wont-see-in-most-tutorials)
* Smooth scrolling
* Truncate text
* Truncate the text to a specific number of lines.
* Center
* Drop shadow
* Typing Effect
* ::selection CSS pseudo-element
* Anything resizable
* CSS modals
* calc()
* Style empty elements
* position: sticky;
* _edit:_ Article added "CSS Scroll Snap" -> `scroll-snap-type` and `scroll-snap-align`


[Using :target to build a semantically-consistent pure-CSS slide renderer](https://madmurphy.github.io/takefive.css/#nowhere)
https://developer.mozilla.org/en-US/docs/Web/CSS/:target
https://github.com/zeplia/minwiz/commit/4dd8ec72fb19c0bb76ed5caf358abfa9c242950d
Try to avoid the radio button hack in https://css-tricks.com/functional-css-tabs-revisited/

[5 Amazing Things Pure CSS Can Do. Awesome things that you can create with… | by Mehdi Aoussiad | JavaScript in Plain English | Medium](https://medium.com/javascript-in-plain-english/5-amazing-things-pure-css-can-do-20b8fe3738b)
1. Custom checkboxes
2. One-page navigation - pretty cool
3. Animated gradient background
4. The first letter
5. Smooth Scroll

Prerender your pages with `<link rel="prerender" ... />`

[Inclusively Hidden | scottohara.me](https://www.scottohara.me/blog/2017/04/14/inclusively-hidden.html)

[How is CSS pseudo content treated by screen readers? | Accessible Web](https://accessibleweb.com/question-answer/how-is-css-pseudo-content-treated-by-screen-readers/)
> CSS pseudo element content, either :before or :after, must only be used for decorative purposes.

[CSS finally adds vertical centering in 2024 | Blog | build-your-own.org](https://build-your-own.org/blog/20240813_css_vertical_center/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/1ev9v7n/css_finally_adds_vertical_centering_in_2024/)


[Gradient Borders In CSS | CSS-Tricks](https://css-tricks.com/gradient-borders-in-css/)

[A Clever Sticky Footer Technique | CSS-Tricks](https://css-tricks.com/a-clever-sticky-footer-technique/)

## Misc

[CSS performance optimization - Learn web development | MDN](https://developer.mozilla.org/en-US/docs/Learn/Performance/CSS)
[Getting Started – Lightning CSS](https://lightningcss.dev/docs.html)

## Dark Mode

[Using @import in CSS to Conditionally Load Syntax Highlighting Styles in Dark Mode - Jim Nielsen’s Blog](https://blog.jim-nielsen.com/2019/conditional-syntax-highlighting-in-dark-mode-with-css-imports/)

[CSS Dark Mode: A Comprehensive Guide - Makemychance](https://makemychance.com/css-dark-mode/)
Nothing special besides the note regarding the transition.

## Styling radio buttons

[Styling Radio Buttons with CSS (59 Custom Examples)](https://www.sliderrevolution.com/resources/styling-radio-buttons/)
[Radio button toggle switch](https://codepen.io/JiveDig/pen/jbdJXR)
