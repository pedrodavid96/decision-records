# Game Development

[What are simplest games that can be implemented in few lines of code? : gamedev](https://www.reddit.com/r/gamedev/comments/9lyahj/what_are_simplest_games_that_can_be_implemented/)


## PBR Textures
[ArmorPaint](https://armorpaint.org/)
alternative to Substance Painter: https://www.allegorithmic.com/products/substance-painter
https://twitter.com/Atrix256/status/1151159522943279114?s=19

## AI

[Pac-Man Ghost AI Explained : programming](https://www.reddit.com/r/programming/comments/ccvvbm/pacman_ghost_ai_explained/)
[Pac-Man Ghost AI Explained - YouTube](https://www.youtube.com/watch?v=ataGotQ7ir8)

[javidx9 | Essential Mathematics For Aspiring Game Developers](https://www.youtube.com/watch?v=DPfxjQ6sqrc)

[Beautiful video on the (unexpected) intricacies in producing 3D spheres programatically, and making procedural planets](https://www.reddit.com/r/programming/comments/hpcw2c/beautiful_video_on_the_unexpected_intricacies_in/)

[Why video game doors are so hard to get right](https://www.youtube.com/watch?v=AYEWsLdLmcc)
[Reddit discussion](https://www.reddit.com/r/Games/comments/ph5kal/why_video_game_doors_are_hard_to_get_right/)
[Another take on it](https://www.gamedeveloper.com/design/-quot-the-door-problem-quot-of-game-design)

Triplanar mapping
[I don't like UV mapping, so I made this shader in Unity. Tutorial link in comments. : gamedev](https://www.reddit.com/r/gamedev/comments/f88j6y/i_dont_like_uv_mapping_so_i_made_this_shader_in/)


* Introduction to Hierarchical State Machines (HSMs): https://barrgroup.com/Embedded-Systems/How-To/Introduction-Hierarchical-State-Machines
* State Machines for Event-Driven Systems: https://barrgroup.com/Embedded-Systems/How-To/State-Machines-Event-Driven-Systems

[The Total Beginner's Guide to Game AI - Artificial Intelligence - Tutorials - GameDev.net](https://www.gamedev.net/articles/programming/artificial-intelligence/the-total-beginners-guide-to-game-ai-r4942/)