# Firefox

## Developing extensions

[Context menu items - Mozilla | MDN](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/user_interface/Context_menu_items)
[menus - Mozilla | MDN](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/menus)
[menus.ContextType - Mozilla | MDN](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/menus/ContextType)

[Interact with the clipboard - Mozilla | MDN](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Interact_with_the_clipboard#reading_from_the_clipboard)

[Mozilla Services Documentation — Mozilla Services](https://mozilla-services.readthedocs.io/en/latest/index.html)
[Mozilla Services · GitHub](https://github.com/mozilla-services)