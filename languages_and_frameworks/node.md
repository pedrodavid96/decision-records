# Node

[Virtual hosting with node](https://stackoverflow.com/questions/8503841/virtual-hosting-with-standalone-node-js-server)

## Backpressuring in Streams
Includes info about error handling of pipes

https://nodejs.org/en/docs/guides/backpressuring-in-streams/

## [Koa - next generation web framework for node.js](https://koajs.com/)
