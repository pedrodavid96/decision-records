# DIY (Do it yourself)

[Infinity Mirror Coffee Table](https://www.youtube.com/watch?v=OasbgnLOuPI)
[How to Make a Modern LED Infinity Illusion Mirror - tabletop stand](https://www.youtube.com/watch?v=sAPGw0SD1DE)
[DIY Arcade Machine Coffee Table](https://www.youtube.com/watch?v=pyaUb9LwLjw)

## Pegboard
[IKEA SKADIS Pegboard](https://www.ikea.com/gb/en/p/skadis-pegboard-white-10321618/)
[Universal controller hook](https://www.thingiverse.com/thing:3470056)
[Pegboard Controllers reference](https://www.google.com/search?q=pegboard+controllers&client=firefox-b-d&sxsrf=ACYBGNRKKX9u04gopq63okLfXhUxnVStSQ:1575247641714&tbm=isch&source=iu&ictx=1&fir=d7nG7XLEMf3zbM%253A%252CiS3N7mqBqCCHYM%252C_&vet=1&usg=AI4_-kRfNddUugLKpUQ7GswUR5zJ-zFSkA&sa=X&ved=2ahUKEwjRhvjg3pXmAhUM8hQKHcqFDh0Q9QEwA3oECAkQDw#imgrc=_&vet=1)

---

["Floating PC"](https://youtu.be/rCFeL8EwBSc?t=536)

Sleeve CAT5 cables: https://www.cable-sleeving.com/cable-sleeving-m
