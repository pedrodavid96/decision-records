# [VERT.X](https://vertx.io/)

Tool-kit for building reactive applications on the JVM.

## Scale

Eclipse Vert.x is event driven and non blocking. This means your app can handle a lot of concurrency using a small number of kernel threads. Vert.x lets your app scale with minimal hardware.

## Vert.x is fun

Enjoy being a developer again.

Unlike restrictive traditional application containers, Vert.x gives you incredible power and agility to create compelling, scalable, 21st century applications the way you want to, with a minimum of fuss, in the language you want.

* Vert.x is lightweight - Vert.x core is around 650kB in size.
* Vert.x is fast. Here are some independent numbers.
* Vert.x is not an application server. There's no monolithic Vert.x instance into which you deploy applications. You just run your apps wherever you want to.
* Vert.x is modular - when you need more bits just add the bits you need and nothing more.
* Vert.x is simple but not simplistic. Vert.x allows you to create powerful apps, simply.
* Vert.x is an ideal choice for creating light-weight, high-performance, microservices.


## [Vert.x-redis](https://vertx.io/docs/vertx-redis-client/kotlin/#_using_vert_x_redis)
