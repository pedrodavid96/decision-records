# HTTP / WebServer Framework

## Inspiration

[Inventing the Service trait | Tokio - An asynchronous Rust runtime](https://tokio.rs/blog/2021-05-14-inventing-the-service-trait)
Fantastic read on the interface of a "Service" (akin to what are usually - see express.js or similar - middlewares).