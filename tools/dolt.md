# [Dolt](https://github.com/dolthub/dolt)

> Dolt is a SQL database that you can fork, clone, branch, merge, push and pull just like a git repository.
> Connect to Dolt just like any MySQL database to run queries or update the data using SQL commands.
> Use the command line interface to import CSV files, commit your changes, push them to a remote, or merge your teammate's changes.

Might be cool to store project configuration / metadata and having it "roll backs" out of the box

[Dolt vs Mysql](https://www.dolthub.com/blog/2021-02-03-dolt-vs-mysql/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/lbxdxi/dolt_vs_mysql_how_it_started_how_its_going/)
