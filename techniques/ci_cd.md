# CI / CD (Continuous Integration / Deployment)

[This is how auto-merge works at Bumble, how we optimised processes and removed restrictions on the number of tickets per day : programming](https://www.reddit.com/r/programming/comments/p82rhs/this_is_how_automerge_works_at_bumble_how_we/)

## Gitlab: Test and deploy a Scala application to Heroku
https://docs.gitlab.com/ee/ci/examples/test-scala-application.html

## Deploy tool
https://github.com/travis-ci/dpl

## Virtual hosting (create multiple staging environments under the same domain)
https://stackoverflow.com/questions/8503841/virtual-hosting-with-standalone-node-js-server

## Autoscaling gitlab runners on AWS
https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/

## Gitlab Environments and deployments
https://docs.gitlab.com/ee/ci/environments.html

[Merge requests API](https://docs.gitlab.com/ee/api/merge_requests.html)
[GitBot – automating boring Git operations with CI](https://about.gitlab.com/blog/2017/11/02/automating-boring-git-operations-gitlab-ci/)
[Applying GitLab Labels Automatically](https://about.gitlab.com/blog/2016/08/19/applying-gitlab-labels-automatically/)
[GitLab Triage](https://gitlab.com/gitlab-org/gitlab-triage)
> This project allows to automate triaging of issues and merge requests for GitLab projects or groups.
[Cancel potentially redundant pipelines automatically](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/8998)

https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml
https://gitlab.com/gitlab-org/ci-cd/codequality/-/tree/master/
https://gitlab.com/pedro.david.sandbox/gitlab-code-quality-java-example

[Repositories Analytics | GitLab](https://docs.gitlab.com/ee/user/group/repositories_analytics/index.html#latest-project-test-coverage-list)
Code coverage charts.

https://docs.gitlab.com/ee/ci/yaml/#parallel-matrix-jobs
deploystacks

[Static Application Security Testing (SAST) | GitLab](https://docs.gitlab.com/ee/user/application_security/sast/#making-sast-analyzers-available-to-all-gitlab-tiers)

[Accessibility Testing | GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html#accessibility-merge-request-widget)

---

[Anti Patterns of Continuous Integration](https://hackernoon.com/anti-patterns-of-continuous-integration-e1cafd47556d)

[Universal CICD pipeline for K8S on AWS](https://medium.com/swlh/universal-cicd-pipeline-on-aws-and-k8s-7b4129fac5d4)

[CI/CD Pipelines: Monad, Arrow or Dart?](https://roscidus.com/blog/blog/2019/11/14/cicd-pipelines/)
Article has a very cool image in the end that "shows" how the pipeline looks (huge scale).

[onedev - Super Easy All-In-One DevOps Platform](https://github.com/theonedev/onedev)
With Issue Tracking, Git Management, Pull Request, and Build Farm. Simple yet Powerful.

[Managing Continuous Integration Pipelines with Jenkins](https://auth0.com/blog/continuous-integration-and-continuous-deployment-with-jenkins/)
[Continuous Deployment Pipelines and Open-Source Node.js Web Apps](https://auth0.com/blog/continuous-deployment-pipelines-and-open-source-node-js-web-apps/)

[relay](https://relay.sh/)
> Put your infrastructure on autopilot
> Event-driven automation that connects the cloud providers, DevOps tools, and other APIs you already use.

[Using Bash for DevOps. How to improve Bash error handling with… | by Dan Collins | Expedia Group Technology | Medium](https://medium.com/expedia-group-tech/using-bash-for-devops-7046eed1aa63)
> Sensitive data
> you can add set `+o xtrace` to turn command tracing off

[Danger](https://danger.systems/ruby/)
Danger runs during your CI process, and gives teams the chance to automate common code review chores.

---

## Spinnaker vs. Argo CD vs. Tekton vs. Jenkins X: Cloud-Native CI/CD

* Spinnaker, compared to the other tools, is quite old.

  It is an open source, multi-cloud continuous delivery platform for releasing software changes
  with high velocity and confidence and has been around since 2014.
  Netflix open sourced it in 2015.

* Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes.

  Work on Argo CD started in early 2018, roughly one month after Jenkins X.

  Pretty much it just syncs the repo with the environment (not much flexibility and functionality)

* Tekton Pipelines is the youngest of the tools being compared.

  It emerged from the Knative pipeline component at the end of August 2018 and
  describes itself as a tool that provides k8s-style resources for declaring CI/CD-style pipelines.

* Jenkins X was introduced in January 2018 and promises an open source opinionated way to do
  continuous delivery with Kubernetes, natively.

  Basically it abstracts prow (kubernetes own CI tool) + Tekton and other tools.

[Concourse CI](https://concourse-ci.org/)