# Home Network

## General Setup

[Looking for PXE solution : selfhosted](https://www.reddit.com/r/selfhosted/comments/rn0bl7/looking_for_pxe_solution/)
[Your favorite operating systems in one place! | netboot.xyz](https://netboot.xyz/)
[Meet netboot.xyz - Network Boot Any Operating System - YouTube](https://www.youtube.com/watch?v=4btW5x_clpg)

## VPN for Docker

[qdm12/gluetun: VPN client in a thin Docker container for multiple VPN providers, written in Go, and using OpenVPN or Wireguard, DNS over TLS, with a few proxy servers built-in.](https://github.com/qdm12/gluetun)

## Disable SWAP

I'd rather have a system crash and reboot fresh than completely irresponsive.
https://serverfault.com/a/684792
https://askubuntu.com/a/1424767

```
# check for swaps with `cat /proc/swaps``
swapoff -a
# rm swap files
```

* Ubuntu Server
  rm /swap.img

* Raspberry
  rm /var/swap

## Static IPs (and DNS servers(?))

[BattermanZ/FlareSync: A simple Rust app to update automatically your DNS records on Cloudflare](https://github.com/BattermanZ/FlareSync)

* Raspberry

    [How to Set a Static IP Address on Raspberry Pi | Tom's Hardware](https://www.tomshardware.com/how-to/static-ip-raspberry-pi)

    /etc/dhcpcd.conf
    ```
    interface [INTERFACE]
    static_routers=[ROUTER IP]
    static domain_name_servers=[DNS IP]
    static ip_address=[STATIC IP ADDRESS YOU WANT]/24
    ```

    _Edit:_ Had to add `static domain_name_servers=127.0.0.1 1.1.1.1 8.8.8.8` so that containers could connect to the internet.

* Ubuntu
  https://ubuntu.com/server/docs/configuring-networks#static-ip-address-assignment
  /etc/netplan/99_config.yaml
  ```
  version: 2
  renderer: networkd
  ethernets:
    enp1s0:
      dhcp4: no
      addresses:
        - 192.168.1.4/24
      routes:
        - to: default
          via: 192.168.1.1
      nameservers:
        addresses:
          - 192.168.1.3
  ```bash
  $ netplan apply
  ```

In ubuntu if you `dig` you can see the DNS server ip address is actually 127.0.0.53.
This is actually systemd-resolved local DNS that only then calls the configured one.
```
sudo systemctl status systemd-resolved
```

## Configure .local for machines

https://en.wikipedia.org/wiki/Multicast_DNS
> ... It uses IP multicast User Datagram Protocol (UDP) packets and is implemented by the Apple Bonjour and open-source Avahi software packages, included in most Linux distributions.

.local advertising

sudo apt install avahi-daemon
sudo systemctl status avahi-daemon

## Configure SNMP

[prometheus/snmp_exporter: SNMP Exporter for Prometheus](https://github.com/prometheus/snmp_exporter/tree/main)
[Prometheus SNMP Exporter: Network Monitoring Tutorial! - YouTube](https://www.youtube.com/watch?v=P9p2MmAT3PA)

* Configure APC UPS SNMP?
* Configure Ubiquiti switch with both a coordinator server and snmp?
* Configure Synology encrypted credentials on exporter
  [Monitoring Synology NAS with Prometheus & SNMP | colby.gg](https://colby.gg/posts/2023-10-17-monitoring-synology/)

## Grub

https://askubuntu.com/a/932602

GRUB_RECORDFAIL_TIMEOUT=5 in /etc/default/grub
sudo update-grub

## SSH

[visual studio code - VSCode Remote SSH Connection Failed - Stack Overflow](https://stackoverflow.com/questions/60507713/vscode-remote-ssh-connection-failed)
> Changing `AllowTcpForwarding` from `no` to `yes` in sshd_config and restarting `sshd` works for me.
[Remote Tunnels](https://code.visualstudio.com/docs/remote/tunnels)
