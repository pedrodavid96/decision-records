# K8S - Kubernetes

## Provisioning

[What tool suggestions do you have for someone who's gonna set up an on-premise k8 cluster? Which tools do you use? : kubernetes](https://www.reddit.com/r/kubernetes/comments/12zg3am/what_tool_suggestions_do_you_have_for_someone/)

[Kubeadm based bootstrap - The Cluster API Book](https://cluster-api.sigs.k8s.io/tasks/bootstrap/kubeadm-bootstrap/)

[k8s-proxmox/cluster-api-provider-proxmox: Cluster API provider implementation for Proxmox VE](https://github.com/k8s-proxmox/cluster-api-provider-proxmox)

[Extend the integration of Kubespray with the bootstrap and control plane of ClusterAPI. · Issue #9859 · kubernetes-sigs/cluster-api](https://github.com/kubernetes-sigs/cluster-api/issues/9859)

[kubernetes-sigs/kubespray: Deploy a Production Ready Kubernetes Cluster](https://github.com/kubernetes-sigs/kubespray?tab=readme-ov-file)
[Comparisons](https://kubespray.io/#/docs/getting_started/comparisons)

[Kubernetes at Home with Kubespray and Ansible - YouTube](https://www.youtube.com/watch?v=1W4w2ziRU8Q)

[Cluster API and GitOps: the key to Kubernetes lifecycle management - YouTube](https://www.youtube.com/watch?v=Pe3Z4dgtNuo)

[How To Create, Provision, And Operate Kubernetes With Cluster API (CAPI) - YouTube](https://www.youtube.com/watch?v=8yUDUhZ6ako)

## Misc

[Overview | OpenCost — open source cost monitoring for cloud native environments](https://opencost.io/docs/)

[Fluentd | CNCF](https://www.cncf.io/projects/fluentd/)

[Linkerd | CNCF](https://www.cncf.io/projects/linkerd/)

[kubernetes-sigs/kubespray: Deploy a Production Ready Kubernetes Cluster](https://github.com/kubernetes-sigs/kubespray?tab=readme-ov-file#requirements)

[The Architecture Behind A One-Person Tech Startup](https://anthonynsimon.com/blog/one-man-saas-architecture/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/mmnfqz/the_architecture_behind_a_oneperson_tech_startup/)
Incredible blog post explaining the architecture choices, namely k8s,
 behind a One-Person startup 🥇⭐

[Cloud Native Computing Foundation (CNCF)](https://www.cncf.io/)
> Building sustainable ecosystems for cloud native software
[Graduated and incubating projects | CNCF](https://www.cncf.io/projects/)
[Sandbox projects | CNCF](https://www.cncf.io/sandbox-projects/)

[cloudevents](https://cloudevents.io/)
> A specification for describing event data in a common way
[KEDA](https://github.com/kedacore/keda)
> Kubernetes-based Event Driven Autoscaling

[MiniKube, Kubeadm, Kind, K3S, how to get started on Kubernetes?](https://www.padok.fr/en/blog/minikube-kubeadm-kind-k3s)

[Universal CICD pipeline for K8S on AWS](https://medium.com/swlh/universal-cicd-pipeline-on-aws-and-k8s-7b4129fac5d4)

[Securing Your Kubernetes Cluster in Google Cloud Platform](https://medium.com/@rajtmana/securing-your-kubernetes-cluster-in-google-cloud-platform-cbad59f62ce3)

[Network IP Ranges of a Private Kubernetes Cluster in Google Cloud Platform](https://medium.com/@rajtmana/network-ip-ranges-of-a-private-kubernetes-cluster-in-google-cloud-platform-93bd556d6f2f)

[Kubernetes on AWS: Setup and Lessons Learned](https://medium.com/axons/kubernetes-on-aws-setup-and-lessons-learned-93f8322bc13a)

[Debugging network stalls on Kubernetes ](https://github.blog/2019-11-21-debugging-network-stalls-on-kubernetes/)

[Make your very own Kubernetes cluster from Raspberry PI](https://medium.com/nycdev/k8s-on-pi-9cc14843d43)
[Building a microcloud with a few Raspberry Pis and Kubernetes (Part 1)](https://mirailabs.io/blog/building-a-microcloud/)
[I broke my Kubernetes cluster running on Raspberry Pi](https://itnext.io/i-broke-my-kubernetes-cluster-running-on-raspberry-pi-355234a24d)
[Building a kubernetes cluster on Raspberry Pi and low-end equipment. Part 1](https://itnext.io/building-a-kubernetes-cluster-on-raspberry-pi-and-low-end-equipment-part-1-a768359fbba3)
Looks freaking awesome!!! 🥇⭐
[Walk-through — install Kubernetes to your Raspberry Pi in 15 minutes | by Alex Ellis | Medium](https://alexellisuk.medium.com/walk-through-install-kubernetes-to-your-raspberry-pi-in-15-minutes-84a8492dc95a)
Deploys OpenFaaS
[Five years of Raspberry Pi Clusters](https://medium.com/@alexellisuk/five-years-of-raspberry-pi-clusters-77e56e547875)

[Kubernetes the hard way](https://github.com/kelseyhightower/kubernetes-the-hard-way)

[Kubernetes for Full-Stack Developers by DigitalOcean](https://www.digitalocean.com/community/curriculums/kubernetes-for-full-stack-developers)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/epb2bo/free_beginner_friendly_kubernetes_course_for_full/)

---

[Etcd, or, why modern software makes me sad](https://www.roguelazer.com/2020/07/etcd-or-why-modern-software-makes-me-sad/)
[Discussion](https://www.reddit.com/r/programming/comments/hrbv1h/etcd_or_why_modern_software_makes_me_sad/)
Rant regarding k8s and how it made `etcd` worse.

# Intro to Kubernetes

## Get your hands dirty

(give your meat a good ol rub meme)

### Requirements and brief description

```bash
# Note: You will be prompted for an host module for virtualbox
# https://wiki.archlinux.org/index.php/VirtualBox#Install_the_core_packages
# In my case, I've choosen (2)`virtualbox-host-modules-arch`

$ sudo pacman -S docker virtualbox minikube kubectl
```

1. docker

Installation was really straightforward - install and enable its service (so it starts at computer startup)

```bash
$ sudo systemctl enable docker
# Verify it's working by issuing a command that requires its deamon, e.g.:
$ docker ps -a
```

Docker is used to build the images and run the containers.
k8s can in fact run without Docker (and use something else behind the scenes) but most of the reference / documentation / internet experience will be based on Docker.
Besides, I couldn't easily find alternatives to Docker images for k8s.

2. virtualbox

The less sexy of the bunch (as in, old and not as trendy, who uses VMs today?).
You probably already know this one.

And this one was more of a PITA to get up and running.
After installation I wasn't able to start a VM right away.

[Problems in question](https://stackoverflow.com/questions/18149546/vagrant-up-failed-dev-vboxnetctl-no-such-file-or-directory)
```
1) sudo modprobe vboxdrv

2) sudo modprobe vboxnetadp - (host only interface)

3) sudo modprobe vboxnetflt - (make vboxnet0 accecible)
```

Afterwards I had to enable AMD-V on my BIOS as it was disabled by default.
On my Gigabyte B450 I AORUS PRO WIFI:
```
Press `Del` key after powering on the computer (while the AORUS logo appears)
"M.I.T" tab > "Advance frequency Settings" > "Advanced Core CPU Settings" > "SVM Mode" - Set to Enabled
Save and restart
```

3. minikube

> Minikube is a tool that makes it easy to run Kubernetes locally.
  Minikube runs a single-node Kubernetes cluster inside a Virtual Machine (VM)
  on your laptop for users looking to try out Kubernetes or develop with it day-to-day.
[Source](https://kubernetes.io/docs/setup/learning-environment/minikube/)

[Alternatives (as of December 5, 2019)](https://brennerm.github.io/posts/minikube-vs-kind-vs-k3s.html)
kind - Kubernetes on docker
k3s - k8s stripped out (without legacy, alpha, non-default, in-tree plugins) and using lightweight components (e.g.: sqlite3 instead of etcd3)

Getting should be as simple as:
```
$ minikube start
```
This should create a new VM on VirtualBox with a k8s cluster inside of it

**Warning**: I actually needed to open VirtualBox and the running VM to enter credentials.
As explained [here](http://www.jppinto.com/2020/01/username-and-password-for-minikube-virtual-machine/) I used
```
minikube login: docker
Password: tcuser
```

`minikube start` should finish successfully shortly after.
You can now verify everything is working, e.g.:
```
$ VBoxManage  list runningvms
"minikube" {710c0626-51ad-4346-a903-30d0f77a2fe9}
$ minikube dashboard
🤔  Verifying dashboard health ...
🚀  Launching proxy ...
🤔  Verifying proxy health ...
🎉  Opening http://127.0.0.1:39947/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
```
This last command should open a browser window with a nice Web Application to monitor your k8s cluster.

4. [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)

> Kubectl is a command line tool for controlling Kubernetes clusters.

It didn't require any further configuration and it was able to automatically control the cluster on the VM.

Check with:
```
$ kubectl get services
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   54m
```

5. [helm](https://helm.sh/)

> The package manager for Kubernetes
  Helm is the best way to find, share, and use software built for Kubernetes.

We will simply use it to get our hands dirty asap for now.

[Install helm from script](https://helm.sh/docs/intro/install/#from-script)
```
$ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
$ chmod 700 get_helm.sh
$ ./get_helm.sh
```

If you already have a kubernetes cluster running
```
$ helm init
Creating /home/pedro/.helm/repository
Creating /home/pedro/.helm/repository/cache
Creating /home/pedro/.helm/repository/local
Creating /home/pedro/.helm/plugins
Creating /home/pedro/.helm/starters
Creating /home/pedro/.helm/cache/archive
Creating /home/pedro/.helm/repository/repositories.yaml
Adding stable repo with URL: https://kubernetes-charts.storage.googleapis.com
Adding local repo with URL: http://127.0.0.1:8879/charts
$HELM_HOME has been configured at /home/pedro/.helm.

Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
To prevent this, run `helm init` with the --tiller-tls-verify flag.
For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation
```

### k8s - Kubernetes

But for real? [What tha hell is k8s?](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)
> Kubernetes is a portable, extensible, open-source platform for managing containerized workloads and services, that facilitates both declarative configuration and automation.

### Lets start!!!

---

### Tips and tricks

[Accessing host resources](https://minikube.sigs.k8s.io/docs/tasks/accessing-host-resources/)
**Warning**: this shouldn't be used but is usefull in development to quickly test stuff
```bash
$ minikube ssh "route -n | grep ^0.0.0.0 | awk '{ print \$2 }'"
```
Validate connectivity:
```bash
host$ minikube ssh
minikube$ telnet <ip> <port>
```

---

[A Developer’s Introduction to Kubernetes - Chris Klug](https://www.youtube.com/watch?v=lAyL9HKx8cQ)
Cool presentation on how K8S work.

[Don't Panic: Kubernetes and Docker](https://kubernetes.io/blog/2020/12/02/dont-panic-kubernetes-and-docker/)
[Discussion](https://www.reddit.com/r/programming/comments/k66cq2/dont_panic_kubernetes_announces_deprecation_of/)

[The Almighty Pause Container - Ian Lewis](https://www.ianlewis.org/en/almighty-pause-container)
> In Kubernetes, the pause container serves as the "parent container" for
  all of the containers in your pod. The pause container has two core
  responsibilities. First, it serves as the basis of Linux namespace sharing
  in the pod. And second, with PID (process ID) namespace sharing enabled,
  it serves as PID 1 for each pod and reaps zombie processes.


## [kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)

Basically, Kubernetes "from scratch".

Using kubeadm, you can create a minimum viable Kubernetes cluster that conforms to best practices.

With that in mind:
> 2 GiB or more of RAM per machine--any less leaves little room for your apps.
Start-up will actually fail if you don't have that available, e.g.:
> [ERROR Mem]: the system RAM (1000 MB) is less than the minimum 1700 MB

You can skip this error with `Installation succeeds if I add "--ignore-preflight-errors=Mem"` but, in practice,
I couldn't set it up in a machine with 1GB of RAM.

enter...

## [K3S](https://k3s.io/)

> Lightweight Kubernetes

Still can't barely use it in 1GB.

[Remote kubectl x509: certificate is valid for 127.0.0.1](https://github.com/k3s-io/k3s/issues/1381)
Install with:
```
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--tls-san <public-ip>" sh -s -
```

[Cluster Access](https://rancher.com/docs/k3s/latest/en/cluster-access/)
[Kubernetes Dashboard](https://rancher.com/docs/k3s/latest/en/installation/kube-dashboard/)
```
# Without address I was able to connect with "curl" inside the container
#  but not from the host browse
kubectl proxy --address="0.0.0.0"
```

## Serverless

> https://serverlessworkflow.io/

### [Knative](https://knative.dev/)

> Kubernetes-based platform to deploy and manage modern serverless workloads.

[Prerequisites](https://knative.dev/development/install/prerequisites/)
> For prototyping purposes, Knative will work on most local deployments of Kubernetes.
> For example, you can use a local, one-node cluster that has 2 CPU and 4GB of memory.

In practice by default it tried to use [Istio](https://istio.io) which requires
 a lot of memory from the get go. In their [`minikube` installation steps](https://istio.io/latest/docs/setup/platform-setup/minikube/#installation-steps)
 they recommend:
> Start minikube with 16384 MB of memory and 4 CPUs.

### [OpenFaaS](https://www.openfaas.com/)

> Serverless Functions Made Simple

Doesn't seem very "open source". GitOps supposedely behind paywall.

### [Kubeless](https://kubeless.io/)

A Bitnami project

> The Kubernetes Native Serverless Framework

_Edit: Abandoned_

### [Apache OpenWhisk](https://openwhisk.apache.org/)

> is a serverless, open source cloud platform

Seems to work with Docker Compose

### [OpenFunction/OpenFunction](https://github.com/OpenFunction/OpenFunction)

> Cloud Native Function-as-a-Service Platform (CNCF Sandbox Project)

[FaaS that can be deployed through GitOps with ease? : kubernetes](https://www.reddit.com/r/kubernetes/comments/1fpw35e/faas_that_can_be_deployed_through_gitops_with_ease/)

> OpenFaaS has GitOps support in the paid version. ...
> OpenFunction is on the other hand open source and made of all open source components
> Implementing all of the parts of OpenFunction independently would be "too hard" for most people.
> OpenFunction is a Helm chart you can install with Flux, it bundles knative and all the other parts ...

## Load Balancing

[MetalLB :: MetalLB, bare metal load-balancer for Kubernetes](https://metallb.io/)

## Security

[Manage your secrets in Git with SOPS & GitLab CI 🦊 - DEV Community](https://dev.to/stack-labs/manage-your-secrets-in-git-with-sops-gitlab-ci-2jnd)

[podman - How to use sops exec-file with docker-compose? - Stack Overflow](https://stackoverflow.com/questions/78211931/how-to-use-sops-exec-file-with-docker-compose)

[Can't use docker compose and sops together · Issue #1470 · getsops/sops](https://github.com/getsops/sops/issues/1470)

[FiloSottile/age: A simple, modern and secure encryption tool (and Go library) with small explicit keys, no config options, and UNIX-style composability.](https://github.com/FiloSottile/age)

[bitnami-labs/sealed-secrets: A Kubernetes controller and tool for one-way encrypted Secrets](https://github.com/bitnami-labs/sealed-secrets)

[Introduction - External Secrets Operator](https://external-secrets.io/latest/)

[Kubernetes Secrets: How to Manage Secrets in Kubernetes? | Cherry Servers](https://www.cherryservers.com/blog/how-to-manage-secrets-in-kubernetes)

[rafi/kubectl-pass: kubectl plugin for integration with pass (the standard unix password manager)](https://github.com/rafi/kubectl-pass)

[Good practices for Kubernetes Secrets | Kubernetes](https://kubernetes.io/docs/concepts/security/secrets-good-practices/)

[Encrypting Confidential Data at Rest | Kubernetes](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/)

[Usage - Secrets Store CSI Driver](https://secrets-store-csi-driver.sigs.k8s.io/getting-started/usage)

[What's the effective difference between Sealed Secrets and SOPs? : r/kubernetes](https://www.reddit.com/r/kubernetes/comments/o4xhm7/whats_the_effective_difference_between_sealed/)

[Mozilla SOPS VS Sealed Secret – fredrkl](https://fredrkl.com/blog/mozilla-sops-vs-sealed-secret/)

## K3D

[Run Kubernetes Cluster Locally with k3d and Helm | Medium](https://medium.com/@munza/local-kubernetes-with-k3d-helm-dashboard-6510d906431b)

wget -q -O - https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

sudo chmod 0755 /usr/local/bin/kubectl
sudo chown root:root /usr/local/bin/kubectl

curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash


> [!TODO]
> WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /var/services/homes/pedro.david/.kube/config

k3d also seems to be failing on Synology:
```
time="2024-12-16T01:00:41Z" level=info msg="Waiting to retrieve agent configuration; server is not ready: \"overlayfs\" snapshotter cannot be enabled for \"/var/lib/rancher/k3s/agent/containerd\", try using \"fuse-overlayfs\" or \"native\": failed to mount overlay: no such device"
Error from server (NotFound): nodes "k3d-local-k8s-server-0" not found
time="2024-12-16T01:00:42Z" level=info msg="Waiting for control-plane node k3d-local-k8s-server-0 startup: nodes \"k3d-local-k8s-server-0\" not found"
```

## K3s Traefik dashboard

[K3s Traefik dashboard](https://qdnqn.com/k3s-traefik-dashboard/)

## K8S Optimized OS

[Elemental - Immutable Linux for Rancher](https://elemental.docs.rancher.com/)

## Simplest (?) Setup

[The Simplest Kubernetes Deployment? K3S, HA, Loadbalancer! Kubernetes At Home: Part 3 - YouTube](https://www.youtube.com/watch?v=6k8BABDXeZI)

## Git Ops

[Flux Documentation | Flux](https://fluxcd.io/flux/)

[Feature branch deployments with Flux and GitLab CI | it_lore](https://6uellerbpanda.gitlab.io/posts/Feature-branch-deployments-flux-gitlab-ci/)

[raffis/gitops-zombies: Identify kubernetes resources which are not managed by GitOps](https://github.com/raffis/gitops-zombies)

### Feature Flags

[fluxcd/flagger: Progressive delivery Kubernetes operator (Canary, A/B Testing and Blue/Green deployments)](https://github.com/fluxcd/flagger)

## Dashboard

[kubernetes/dashboard: General-purpose web UI for Kubernetes clusters](https://github.com/kubernetes/dashboard)
