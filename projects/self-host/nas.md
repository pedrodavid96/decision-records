# NAS

## "DIY" NAS

[Building a Budget Homelab NAS Server (2022 Edition) · mtlynch.io](https://mtlynch.io/budget-nas/)

## Base Configuration

[Removing Google as a Single Point of Failure - Jake Wharton](https://jakewharton.com/removing-google-as-a-single-point-of-failure/)
[Removing Google as a Single Point of Failure – Jake Wharton : programming](https://www.reddit.com/r/programming/comments/f6g3v0/removing_google_as_a_single_point_of_failure_jake/)

Configure Back-ups

https://photostructure.com/faq/how-do-i-safely-store-files/#getting-started-with-your-nas
1. Consider getting a NAS that supports 4 or more drives. More slots for more drives gives you a lot more flexibility in the future, should your data exceed your original drives. If you’re building a box to run Unraid, Fractal makes some nice cases: the new Define 7 and Meshify 2 support 12+ HDDs (!!)
2. Buy 50% more storage than you need right now so you have room to grow in the near future.
3. Enable weekly data scrubbing.
4. Enable snapshotting, if available.
5. Enable monthly S.M.A.R.T. self-tests.
6. Set up your NAS to either apply security patches automatically or notify you to do so.
7. Consider installing a virus scanner and malware detection package on your NAS. Synology has a “security audit” tool as well.
8. Make sure your router has recently-updated firmware
9. Use secure admin and user passwords. Enable 2FA if available. Using a password manager like Bitwarden makes this easy to do.
10. Configure your NAS to tell you if it has any errors: you don’t want any disks dying or backups failing without you knowing it.

TODO: Check SSD triming

## Software / Hardware

[To Synology or not to Synology? : homelab](https://www.reddit.com/r/homelab/comments/1dd3nhp/to_synology_or_not_to_synology/)

## Setup

[agaffney/ansible-synology-dsm: Ansible role for configuring a Synology NAS running DSM](https://github.com/agaffney/ansible-synology-dsm)

### Docker

[How to modify the logging driver of Docker on Synolog NAS](https://en.ccc.tc/notes/how-to-change-docker-logging-driver-on-synology-nas)

[Has anyone been successful in enabling IPv6 for the Docker daemon? | SynoForum.com - The Unofficial Synology Forum](https://www.synoforum.com/threads/has-anyone-been-successful-in-enabling-ipv6-for-the-docker-daemon.435/)

[Docker driver client | Grafana Loki documentation](https://grafana.com/docs/loki/latest/send-data/docker-driver/#upgrade-the-docker-driver-client)

## Backups

[GitHub - awesome-foss/awesome-sysadmin: A curated list of amazingly awesome open-source sysadmin resources.](https://github.com/awesome-foss/awesome-sysadmin#backups)

[alexklibisz/bdv2s3: backup docker volume to s3](https://github.com/alexklibisz/bdv2s3)

[How do I access the data off of the hard drives in my NAS without using Synology hardware? : synology](https://www.reddit.com/r/synology/comments/pzlmxi/how_do_i_access_the_data_off_of_the_hard_drives/)

## SSH

[How do I sign in to DSM with RSA key pairs via SSH? - Synology Knowledge Center](https://kb.synology.com/en-ro/DSM/tutorial/How_to_log_in_to_DSM_with_key_pairs_as_admin_or_root_permission_via_SSH_on_computers)


## Storage

1 Storage Pool / Volume
2 x 4TB HDD (Seagate IronWolf Pro 7200)
Critical Data

1 Storage Pool / Volume
1 x 18TB HDD (Seagate IronWolf Pro 7200)
Data Hoarding

1 Storage Pool / Volume
2 x 1TB M.2 Drive Sandisk WD Black (SN850X)
"Docker" storage (tl;dr: faster applications)
Unfortunately can't use it as 2 different volumes, one for storage and other for cache
 (didn't actually try but it wouldn't be simple and there's a few reports of it not working)

1 Read Cache
1 x 1TB SATA SSD, Samsung EVO 870
Can't R/W with a single drive

### Critical document storage

This includes Photos & Documents, possibly even "personal media" (e.g.: Books, Digital CDs).

### Documents

[Over-engineering my document storage system with Paperless-ngx](https://skerritt.blog/how-i-store-physical-documents/)

## NFS

## K8S

### As Storage device via NFS

[Configuring Your Synology NAS as NFS Storage for Kubernetes Cluster | by Sebastian Ohm | Medium](https://medium.com/@bastian.ohm/configuring-your-synology-nas-as-nfs-storage-for-kubernetes-cluster-5e668169e5a2)
[jparklab/synology-csi: Container Storage Interface (CSI) for Synology](https://github.com/jparklab/synology-csi)
[SynologyOpenSource/synology-csi](https://github.com/SynologyOpenSource/synology-csi)

### As member (to investigate)

* [Has anyone tried to get a Synology nas to participate as a node in a k8s/k3s cluster? : synology](https://www.reddit.com/r/synology/comments/glm8g0/has_anyone_tried_to_get_a_synology_nas_to/)
* [fenio/k3s-synology: k3s (lightweight kubernetes) on Synology DS216+](https://github.com/fenio/k3s-synology)
* [Is the Synology NAS able to run a Kubernetes Cluster ? : synology](https://www.reddit.com/r/synology/comments/t3o4le/is_the_synology_nas_able_to_run_a_kubernetes/?tl=pt-br)
* [Building your home kubernetes cluster with some Rasberry Pi's and a Synology NAS : kubernetes](https://www.reddit.com/r/kubernetes/comments/jgi2ga/building_your_home_kubernetes_cluster_with_some/)
* [Container Runtimes | Kubernetes](https://kubernetes.io/docs/setup/production-environment/container-runtimes/)
* [Run Docker Containers on Different Machines with Kubernetes - Stack Overflow](https://stackoverflow.com/questions/49020582/run-docker-containers-on-different-machines-with-kubernetes)
* [Advanced Options / Configuration | K3s](https://docs.k3s.io/advanced?_highlight=docker#using-docker-as-the-container-runtime)
* [k3s agent with docker, and then mix pods and containers on the same node? : kubernetes](https://www.reddit.com/r/kubernetes/comments/jvprva/k3s_agent_with_docker_and_then_mix_pods_and/)
* [Mirantis/cri-dockerd: dockerd as a compliant Container Runtime Interface for Kubernetes](https://github.com/Mirantis/cri-dockerd)

## Media System

https://www.plex.tv/
https://emby.media/
https://jellyfin.org/

[Synology - TRaSH Guides](https://trash-guides.info/Hardlinks/How-to-setup-for/Synology/#if-you-use-torrents)
[Radarr Docker Installation | Servarr Wiki](https://wiki.servarr.com/radarr/installation/docker)

[[How-to] Export Jellyfin playback stats to Prometheus and Grafana : selfhosted](https://www.reddit.com/r/selfhosted/comments/183yuqe/howto_export_jellyfin_playback_stats_to/)
[Exporting Jellyfin playback stats to Prometheus - Dan Snow](https://sno.ws/writing/2023/11/25/exporting-jellyfin-playback-stats-to-prometheus/)
[prometheus/snmp_exporter: SNMP Exporter for Prometheus](https://github.com/prometheus/snmp_exporter/tree/main)

[Tautulli](https://tautulli.com/#features)
> Monitor your Plex Media Server

[Subgen - Auto-generate Plex or Jellyfin Subtitles using Whisper OpenAI! : selfhosted](https://www.reddit.com/r/selfhosted/comments/17fi80o/subgen_autogenerate_plex_or_jellyfin_subtitles/)
[McCloudS/subgen: Autogenerate subtitles using OpenAI Whisper Model via Jellyfin, Plex, Emby, Tautulli, or Bazarr](https://github.com/McCloudS/subgen)

[I created a tool to remove inactive media from Sonarr and Radarr, introducing Deleterr. : sonarr](https://www.reddit.com/r/sonarr/comments/1632w1e/i_created_a_tool_to_remove_inactive_media_from/)
[rfsbraz/deleterr: Deleterr is a Python script designed to help you manage available disk space in your Plex media server.](https://github.com/rfsbraz/deleterr)
> Honest but probably lazy question, as I need to explore an option to purge old data rather than my powershell script.
  What do you do different than the competition.
  https://github.com/everettsouthwick/Eraserr
  https://github.com/Supergamer1337/media-cleaner
  https://github.com/jorenn92/Maintainerr (has a GUI)
  https://github.com/ngovil21/Plex-Cleaner
  https://github.com/Cleanarr/Cleanarr

[GitHub - evan-buss/openbooks: Search and Download eBooks](https://github.com/evan-buss/openbooks)

[Is there a way that Sonarr will grab a full series torrent rather than just individual seasons? : sonarr](https://www.reddit.com/r/sonarr/comments/1acn6ta/is_there_a_way_that_sonarr_will_grab_a_full/)


[Home - TRaSH Guides](https://trash-guides.info/Bazarr/)

[Radarr doing a full copy instead of move on downloaded movies. : radarr](https://www.reddit.com/r/radarr/comments/b6iksq/radarr_doing_a_full_copy_instead_of_move_on/?share_id=D_C6Abe4oBXYri-xai6cC)
[Hard Link Only Option · Issue #6260 · Sonarr/Sonarr · GitHub](https://github.com/Sonarr/Sonarr/issues/6260)

---

[Tech Independence | Derek Sivers](https://sive.rs/ti)

### Subtitles

> [!TODO]
> Investigate Whisperr integration.

[smacke/ffsubsync: Automagically synchronize subtitles with video.](https://github.com/smacke/ffsubsync?tab=readme-ov-file)
Seems to be what is currently in use by Bazarr.
e.g.:
https://github.com/morpheus65535/bazarr/blob/6ec304d13d5ed8a217f0f6d6ee98ecc98cac87af/bazarr/subtitles/tools/subsyncer.py#L6

[kaegi/alass: "Automatic Language-Agnostic Subtitle Synchronization"](https://github.com/kaegi/alass)
Rust. Might also sync multiple times during media, avoiding drift on "breaks"

### Youtube

[GitHub - nbr23/youtube-dl-server: Web / REST interface for downloading youtube videos onto a server.](https://github.com/nbr23/youtube-dl-server)
[GitHub - tubearchivist/tubearchivist: Your self hosted YouTube media server](https://github.com/tubearchivist/tubearchivist)
[GitHub - meeb/tubesync: Syncs YouTube channels and playlists to a locally hosted media server](https://github.com/meeb/tubesync)
[Mizerka comments on Docker containers, plugins, and VMs roundup - post what you're running](https://old.reddit.com/r/unRAID/comments/iuabr2/docker_containers_plugins_and_vms_roundup_post/g5ky9r5/)


### Photos

[Home | Immich](https://immich.app/)
Self-hosted backup solution for photos and videos on mobile device

[Good way to store a lot of photos - hobby photography : selfhosted](https://www.reddit.com/r/selfhosted/comments/18487u2/good_way_to_store_a_lot_of_photos_hobby/)

[Introducing yet another immich proxy: Proxy for Immich : selfhosted](https://www.reddit.com/r/selfhosted/comments/1gwmlal/introducing_yet_another_immich_proxy_proxy_for/)

[PhotoSync – Photo Transfer and Backup App | For iOS & Android - PhotoSync](https://www.photosync-app.com/home)

[Has anyone moved from Synology Photos to Immich? What is the best practice? : synology](https://www.reddit.com/r/synology/comments/1c5cvkn/has_anyone_moved_from_synology_photos_to_immich/)
[Immich and Synology Photos – Access from outside – Part 1/4 | Planet4](https://www.planet4.se/immich-alongside-synology-photos-with-external-access-part-1-3/)

[OAuth Authentication | Immich](https://immich.app/docs/administration/oauth)
[Quick Start | Immich](https://immich.app/docs/overview/quick-start)

[feat(ml): introduce support of onnxruntime-rocm for AMD GPU by Zelnes · Pull Request #11063 · immich-app/immich](https://github.com/immich-app/immich/pull/11063)

[What is the ultimate backup solution for the Immich DB and files? : immich](https://www.reddit.com/r/immich/comments/1bydti1/what_is_the_ultimate_backup_solution_for_the/)

### Network

Router had network mask 255.255.255.0 and DHCP automatically assigning from 192.168.1.2 to 192.168.1.253 (leaving only 192.168.1.254 available, which was already being used by an unknown "device" (30:b7:d4:dc:f5:e0) - demasiado semelhante com MAC address do router: 30b7d4dcf5d8).
Changed DHCP to assign from 192.168.1.2 to 192.168.1.249.

In Synology network settings, set a static LAN IP instead of 192.168.1.250.

New router actually has 192.168.1.2 - 192.168.1.254.
Changed to  192.168.1.5 - 192.168.1.249 anyway.

---

TODO: For some reason NAS wasn't picking DNS server from DHCP.
Updated manually.
Maybe see:
[DHCP not working | Synology Community](https://community.synology.com/enu/forum/17/post/81997) for more information.
[Synology DHCP does not automatically update Synology DNS server : synology](https://www.reddit.com/r/synology/comments/h8h90f/synology_dhcp_does_not_automatically_update/)


### Speed up via SSD

[[Guide] Use NVME SSD as storage volume instead of cache in DS918 : synology](https://www.reddit.com/r/synology/comments/a7o44l/guide_use_nvme_ssd_as_storage_volume_instead_of/)
[Ric76xxx comments on [Guide] Use NVME SSD as storage volume instead of cache in DS918](https://www.reddit.com/r/synology/comments/a7o44l/guide_use_nvme_ssd_as_storage_volume_instead_of/lwx5bgo/)


### SMB sharing

#### Linux

```
$ sudo apt install cifs-utils -y
$ sudo mkdir /mnt/nas
$ sudo mount -t cifs //[IP_Address]/[share_name] /mnt/winshare -o username=[username]
```

#### Windows

```
net use U: \\Home-NAS\data
U:
net use U: /delete
```

## Upgrading

[DS920+ RAM Upgrade advice. : synology](https://www.reddit.com/r/synology/comments/x2l3q4/ds920_ram_upgrade_advice/)

## SWAP

[Swap file constantly full, not sure what to do : synology](https://www.reddit.com/r/synology/comments/18ta5k6/swap_file_constantly_full_not_sure_what_to_do/)

## Databases

[Upgrading Postgres from 15 to 16 : Paperlessngx](https://www.reddit.com/r/Paperlessngx/comments/16olzcm/upgrading_postgres_from_15_to_16/)
[Upgrading A PostgreSQL Database Running In Docker](https://thomasbandt.com/postgres-docker-major-version-upgrade)
[Change Postgres version from 15 to 16 · paperless-ngx/paperless-ngx · Discussion #6669](https://github.com/paperless-ngx/paperless-ngx/discussions/6669)

## Partition / Disk Management

[mergerfs - Perfect Media Server](https://perfectmediaserver.com/02-tech-stack/mergerfs/)
[ZFS - Perfect Media Server](https://perfectmediaserver.com/02-tech-stack/zfs/)
