# Medium Exporter Sync

The goal of this project curating medium posts to another persistant store.

Why? They might be deleted by either the author or Medium.

Main Goal:
* Store curated list of Medium Posts somewhere owned by me

Nice to have features:
* Present curated list to select, possibly from the newsletter e-mail.
* Download them to check off-line on Mobile (train rides or whatever).
* Aggregate this project with my "decision_records" project.

[TLDR: Writing a Slack bot to Summarize Articles](https://blog.concurlabs.com/how-to-write-a-tldr-chat-bot-ec02d9e1649c)
