# UX - User Experience

[Things end users care about but programmers don't](https://instadeq.com/blog/posts/things-end-users-care-about-but-programmers-dont/)
Very important!!!

[CLUI: Building a Graphical Command Line](https://blog.repl.it/clui)
Discoverable CLIs to be used by non technical users.

[UX patterns for CLI tools](https://lucasfcosta.com/2022/06/01/ux-patterns-cli-tools.html)
* Interactive mode
* Input validation and assisted recovery
* Colours, Emojis, and Layouting
* Loading indicators
* Context-awareness <-
* Exit codes        <-
* Streams           <-
* Consistent commands trees

[3 Commandments for CLI Design](https://medium.com/relay-sh/command-line-ux-in-2020-e537018ebb69)
1. Enable Progressive Discovery
2. Go Upscale, Intelligently
   > they are aware of the affordances that modern terminal programs have and make use of them to provide upgraded experiences
   e.g.: `bat` pretty prints on a terminal but ratains compatibility with `cat` when not printing to a terminal
3. Be a Good CLI Citizen
   - Expect to operate as part of a pipeline and adjust behavior accordingly
   - Closely related to pipeline operation, output should go to stdout and errors to stderr
   - Provide tabular and structured output formats for lists of data (and / or json)
   - A complex command invocation is like a grammatical sentence: it’s usually got a subject, a verb, and an object
   - Observe control conventions
     * The convention is that ^C (control-C) interrupts execution and exits the program, cleanly if possible
     * Similarly, ^Z (control-Z) suspends a program so that it can be paused or put in the background
   - Don’t litter the filesystem
     * Use package managers and follow their conventions for the target operating system wherever possible.
     * Similarly, follow conventions like XDG-Spec for guidance on where to install configuration files, docs, and supporting libraries

## Getting Started

1. Ask for their story
2. Identify key problems they feel they’re solving
3. Identify who the product is helping
4. Find out where the product is at

https://www.hotjar.com/
> The fast & visual way to understand your users
Basically heatmaps over user usage.

### [The Tried and True Laws of UX [with Infographic]](https://uxplanet.org/the-tried-and-true-laws-of-ux-with-infographic-314817e1dbd8)

![UX Laws](./ux_laws.png)

[7 laws of UX design (with illustrations)](https://uxdesign.cc/7-important-laws-of-ux-design-fdda087b4f9d)

### [The ultimate guide to creating personas and how to use them to enhance your business](https://medium.com/@ergomania_UX/the-ultimate-guide-to-creating-personas-and-how-to-use-them-to-enhance-your-business-1066b0d1f50b)

>>>
What is persona in UX?
Personas are commonly used tools in UX design. It’s a representation of the real target audience data

[E-Commerce UX Research Articles](https://baymard.com/blog)
[The Baymard Institute: A glorious, evidence-based trove of UX best practices](https://medium.com/@seandexter1/the-baymard-institute-a-glorious-evidence-based-trove-of-ux-best-practices-189d839b1176)

[Netflix discovery experience Case Study](https://uxdesign.cc/netflix-discovery-experience-a-ux-ui-case-study-7bcd12f74db1)

[Creative loading indicators](https://uxplanet.org/10-creative-loading-indicators-1a15c562b75a)

[Improve your UX with micro-interactions](https://uxdesign.cc/improve-your-ux-with-micro-interactions-bd152445d8e5)

[The ultimate guide to proper use of animation in UX](https://uxdesign.cc/the-ultimate-guide-to-proper-use-of-animation-in-ux-10bd98614fa9)
* Duration and speed of the animation
  Numerous researches have discovered that optimal speed for interface animation is between 200 and 500 ms.
* Avoid using the bouncing effect since it distracts attention
* Animation with easing looks more natural compared to the linear one
* User’s attention should be guided in one fluid direction
* Diagonal appearance for the tabular view of cards
* It is worth animating only one central object and all the rest subjecting to it. Otherwise, a user wouldn’t know what object to follow
* The motion of an object that disproportionately changes its size should be arranged along an arc
* Proportional change of the size is fulfilled in a straight line
* The direction of unfolding/folding the card should coincide with the axis of the interface
* During movement, objects shouldn’t pass through each other, but leave space for the movement of another object
* Objects can rise above other objects and then move

[Introducing Mercury OS](https://uxdesign.cc/introducing-mercury-os-f4de45a04289)

[Why Text Buttons Hurt Mobile Usability](https://medium.com/@uxmovement/why-text-buttons-hurt-mobile-usability-b04c4d465437)

[UI/UX Case Study: Seedly — A Personal Finance App](https://medium.muz.li/ui-ux-case-study-seedly-a-personal-finance-app-63db8229ce32)

[Is UX Design Killing Creativity?](https://uxdesign.cc/is-ux-design-killing-creativity-60f0b9b191dc)

[Will These UX Trends Stick or Fade Away?](https://medium.com/hackernoon/ux-trends-2018-a-retrospective-356bd10615a4)

[Warm your guests up for the wedding day — a UX case study](https://uxdesign.cc/ux-case-study-warm-your-guests-up-for-the-wedding-day-649e0ef896d4)

[UX/UI Case Study — Collaboration in Agile Design](https://medium.com/swlh/ux-ui-case-study-collaboration-in-agile-design-4c8b5f21db6d)

[Case Study: Manuva. UX Design for Gym Fitness App.](https://uxplanet.org/case-study-manuva-ux-design-for-gym-fitness-app-23347aae3596)

[7 simple & effective methods to get better at Visual/UI Design](https://uxdesign.cc/7-simple-methods-to-get-better-at-visual-ui-design-21fec0f417b5)

[How to Design Scannable App Screenshots](https://medium.com/free-code-camp/ux-best-practices-how-to-design-scannable-app-screenshots-89e370bf433e)

[Negative Space in UI Design: Tips and Best Practices](https://uxplanet.org/negative-space-in-ui-design-tips-and-best-practices-98311cb2ad16)
>>>
if everything yells for your viewer’s attention, nothing is heard.

[Case Study: Upper App. UI Design for To-Do List](https://tubikstudio.com/case-study-upper-app-ui-design-for-to-do-list/)

[UX/UI Case Study: Redesigning a Public Transportation App for a Large Touristic City](https://uxplanet.org/ux-ui-case-study-redesigning-a-public-transportation-app-for-a-large-touristic-city-932a2157fa21)
* Sleek and modern design
* Touch reach heat map
* Night theme
* "Multiple calendars" (work, gym, ...)

[Rethinking a calendar by Carlota Antón](https://uxplanet.org/rethinking-a-calendar-def7711c8b02)
* Minimalist
* Together with a to-do list
* Concept of "ranges" in the calendar

[UX Design: Checkbox and Toggle in Forms by Nick Babich](https://uxplanet.org/checkbox-and-toggle-in-forms-f0de6086ac41)

[Building Great Mobile Forms by mobiscroll](https://uxplanet.org/building-great-mobile-forms-2fa8e9a258cc)
* Use segmented controls instead of select dropdowns
* Condense multiple select dropdowns into a single field
* Use switches instead of dropdowns
* Sliders
* Avoid multiple columns
* Use steppers (number input) instead of dropdowns
* Show errors inline instead of grouping them
* Don’t be repetitive with required fields
* Group related fields
* Provide comfortable touch areas
* Stay true to the platform

[Why the GOV.UK Design System team changed the input type for numbers](https://technology.blog.gov.uk/2020/02/24/why-the-gov-uk-design-system-team-changed-the-input-type-for-numbers/)
* Very important!!! Add to web checklist
1. Accessibility
We found that `<input type="number">`:
- cannot be dictated or selected when using Dragon Naturally Speaking
- appears as unlabeled in NVDA's element list
- appears as a spin button in NVDA's object navigation, which has an edit field and two buttons inside- those buttons are unlabeled, but decrease/increase the value
- is reported as unlabeled edit field when using nvda+tab
2. Incrementable numbers
HTML specification states that <input type=”number”> is “not appropriate for input that happens to only consist of numbers but isn't strictly speaking a number”.
Chrome 79.0: type=number displays large numbers in exponential format if user presses the up or down arrows on their keyboard
Safari 6 rounds the last digit on blur
Safari 5.1 attempts to make values with at least 16 digits more readable by inserting commas.
3. Letters
4. Scrolling (can accidentally increase or decrease)
**The solution**
Using `<input type="text" inputmode="numeric" pattern="[0-9]*">` (pattern for backwards compatibility with older iOS devices).

[Designing More Efficient Forms: Structure, Inputs, Labels and Actions](https://uxplanet.org/designing-more-efficient-forms-structure-inputs-labels-and-actions-e3a47007114f)
- Form Structure
    * Only Ask What’s Required
    * Order the Form Logically
    * Group Related Information
    * Forms should never consist of more than one column
- Input Fields
    * Try to minimize the number of fields as much as possible
    * Try to avoid optional fields in forms
    * You should avoid having a static default (exception example: default Country or something that you're "completely sure")
    * Desktop-only: Make Form Keyboard-friendly (easily tab)
    * Desktop-only: Autofocus for Input Field
    * Mobile-only: Match the Keyboard With the Required Text Inputs
- Labels
    * Number of Words: Labels are not help texts. You should use succinct, short and descriptive labels (a word or two) so users can quickly scan your form.
    * Sentence case ("Full name" instead of "Full Name") is slightly easier (and thus faster) for reading than title case. But one thing for sure — you should never use all caps
    * Matteo Penzo’s 2006 [article](https://www.uxmatters.com/mt/archives/2006/07/label-placement-in-forms.php) on label placement implies that forms are completed faster if the labels are on top of the fields.
    * Inline Labels (Placeholder Text)
    Placeholder text works great for a simple username and password form.
    But it’s a poor replacement for a visual label when there is more information.
    Once the user clicks on the text box, the label disappears and thus the user cannot double check that what he/she has written is indeed what was meant to be written. Another thing is that when users see something written inside a text box, they may assume that it has already been prefilled in and may hence ignore it.
- Action Buttons
    * Primary vs Secondary Actions
    * Button Location
    Secondary actions (e.g.: back button in complex form) should be placed *out of sight*
    * Naming Conventions
    Avoid generic words such as “Submit” for actions, because they give the impression that the form itself is generic. Instead state what actions the buttons do when clicked, such as ‘Create my FREE account’ or ‘Send me weekly offers’.
    * Avoid Multiple Action Buttons that may distract users from the goal (e.g.: submit a form)
    * The ‘Reset’ Button Is Pure Evil
    * Visual Appearance
    Make sure action buttons look like buttons. Style them in a way that they lift (thus, indicates that it is possible to click)
    * Visual Feedback
    Provide feedback to user - also avoids double posts

[How to Design Great UX for Sign Up Form](https://uxplanet.org/how-to-design-great-ux-for-sign-up-form-8ce39f84659)
* Allow Sign Up and Log In via social services — Facebook, Google etc.
* Save entered data
* Keep form as short as you can or split it into multiple steps (not for login forms to not mess with password managers browsers extensions)
* Auto-focus the first input field in the form
* Avoid checkboxes for Privacy Police agreement confirmation
* Present fields in a single column layout
* Provide clear heading

* Avoid using placeholders as labels
* Be careful with the password field
    - Let users see their passwords
    - Show the password strength
    - Show the password requirements before submission
    - Warn users that Caps Lock is ON
    - Design a ‘Forgot Your Password?’ flow for log in forms
* Create powerful buttons
    - Name buttons properly ("Submit" is usually bad, "Create account" is good)
    - Make the button disabled until all the required inputs are completed
    - Use primary and secondary buttons properly
    - Avoid Reset and Clear buttons!
* Think about error prevention
    - Input Automation
        1. Auto-fill city and state textfields based on ZIP code or geolocation data.
        2. Select the card type from the entered credit card number.
    - Format Your Fields with Input Masks
    - Field Constraints
* And remember about the errors validation
    - Use inline validation
    Avoid error summaries, place error messages next to inputs. Show error messages one field at a time
    - Use a clear microcopy for the error message
    It should tell users why their information gets rejected and how to fix it. The tone of your error messages should feel polite and professional.
    - Validate fields with multiple requirements before submission
    - Highlight error fields with color, icons and text

[A UX/UI Case Study: Designing a Text-To-Speech App From The Ground Up by Santiago Dimaren](https://blog.prototypr.io/a-ux-ui-case-study-designing-a-text-to-speech-app-from-the-ground-up-1fc95bd04a2b)
Goes very in depth in the "study" and planning process

[Skyscanner — a UX case study by Sanne de Vries](https://medium.muz.li/skyscanner-a-ux-case-study-8a904eabec8c)
Short case study showcasing a clean design

[Designing a CSA food app in 3 days — a UX case study by Paola Ascanio](https://uxdesign.cc/ux-ui-case-study-designing-a-food-app-in-3-days-1e2856680205)
In depth as well

[Screenlife App — a UI/UX case study by Elvis Obanya](https://uxdesign.cc/screenlife-app-ui-ux-design-case-study-4830d364c76f)
In depth as well

[Improving Roommate Communication: a UX Case Study by Katie Stakland](https://blog.prototypr.io/improving-roommate-communication-a-ux-case-study-a05138d0a02f)
Not so in depth but informative

[Designing an e-commerce website for a Homeware store — a UX case study by Zena Zerai](https://blog.usejournal.com/designing-an-e-commerce-website-for-a-homeware-store-a-ux-case-study-16e7375e41fe)
Not so in depth but informative

[Design better buttons by Michal Malewicz](https://uxdesign.cc/design-better-buttons-a5c90a113280)
1. A BUTTON SHOULD LOOK LIKE A BUTTON - By removing elements from a button it’s function starts to dissolve and disappear. It becomes decoration or text, losing it’s actionable qualities.
2. Familiar = good - Unfamiliar can mean elements that can take much longer to be identified as being actionable.
3. Spacing and alignment
4. The right size - The sweet spot is somewhere around 50 for mobile buttons.
5. Important buttons also work well with icons
6. Rounded corners
Rounded buttons are considered more friendly and positive than sharp edges. At the same time however they make it a lot harder to design content around them.
7. Aligning icons
Depending on our corner radius we create a circle or a square the size of the height of our button. Inside it we create another shape to house the icon. It should have a padding inside the larger shape, that’s the same size as our text height. Then we place our icon inside the smaller shape.

[Julie Grundy – The UX of Form Design: Designing an Effective Form](https://www.youtube.com/watch?v=hPS7LUW7SlA)
Very cool talk about UX of forms

[Finding hobbies tailored to your needs — a UX case study](https://uxdesign.cc/hobble-finding-hobbies-tailored-to-your-needs-case-study-f15fad84d329)
Very cool case study, specially the bottom bar discovery

[The Decline of Usability](https://datagubbe.se/decusab/)
Consistency on window title bars, some of them are completely unnoticeable.
On Slack something as simple as resizing and moving the window are limited to special
 new places in the title bar.
Chrome horrendous tab tooltip
Undestinguishable bars in scrollbar.
Missing menu bars with all options cramped into a burger menu.

[The new landing page is to have no landing page at all](https://uxdesign.cc/the-new-landing-page-is-to-have-no-landing-page-at-all-bb57ca1548f1)
Very cool ideas, specially the one that looks like a Q&A site.

Very cool blog 🥇⭐:
[Cleaning up form UI](https://tonsky.me/blog/form-cleanup/)
Define the grid
Normalize the spacing
Buttons
Color contrast
Typography
[Redesigning Github repository page](https://tonsky.me/blog/github-redesign/)
Problems:
1. nested tabs
2. Redundant icons
3. Vanity counters
4. “Watch” button ambiguity
5. Repo description
6. Removing background color
7. Description editing
8. Files description
9. Repository overview
10. Contextualizing buttons
11. Hidden clone link
12. (Bonus) dated look

["Yes or No?" | CSS-Tricks - CSS-Tricks](https://css-tricks.com/yes-or-no/)
tl;dr: Checkbox might not be sufficient to represent mandatory choice without a "default".
