# Website

http://motherfuckingwebsite.com/
http://bettermotherfuckingwebsite.com/
https://thebestmotherfucking.website/
https://thebestmotherfuckingwebsite.co/
[Starter kit for lightweight sites](https://minwiz.com/#h)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/krooqi/a_full_website_in_17_kb_all_assets_included/)
https://interconnected.org/home/2022/10/10/servers
https://ghost.org/

[Viewport meta tag](https://developer.mozilla.org/en-US/docs/Web/HTML/Viewport_meta_tag)

Example:
https://aliabdaal.com/
https://gustavosilva.me/
https://blog.cassidoo.co/post/annoyed-at-react/ (very similar color palette to mine as of 2023)

[prefers-color-scheme: Hello darkness, my old friend](https://web.dev/prefers-color-scheme/)

[Robots.txt Introduction and Guide](https://developers.google.com/search/docs/crawling-indexing/robots/intro)
[What Is a Sitemap](https://developers.google.com/search/docs/crawling-indexing/sitemaps/overview)

https://sre.google/books/

[Monolith – CLI tool for saving complete web pages as a single HTML file | Hacker News](https://news.ycombinator.com/item?id=39810378)
[Show HN: Jampack – Optimizes static websites as a post-processing step | Hacker News](https://news.ycombinator.com/item?id=39816836)

## Router Fiber gateway MEO: Configure service no-ip
https://pplware.sapo.pt/tutoriais/router-fiber-gateway-meo/

## Router Fiber gateway MEO: Port forwarding
https://pplware.sapo.pt/informacao/port-forwarding-no-router-fiber-gateway-da-meo/

## iptables configuration / Allow incoming http traffic
https://www.digitalocean.com/community/tutorials/iptables-essentials-common-firewall-rules-and-commands/#allow-all-incoming-http

## My public ip
https://www.whatismyip.com/

## Themes

https://github.com/OlyaB/CyanTheme

https://github.com/morhetz/gruvbox
https://github.com/diryox/Paper-tmTheme
https://github.com/lkytal/vscode-theme-flatui

https://jonathanmh.com/best-light-atom-editor-themes/

## Static website with node
https://stackoverflow.com/questions/16333790/node-js-quick-file-server-static-files-over-http
answer: https://stackoverflow.com/a/29046869

## Hardware: Building a server
https://www.pcgamer.com/build-the-ultimate-server/


## Design
https://material.io/design/

## Topics to write

https://startbloggingonline.com/perfect-about-me-page/
https://www.thebalancecareers.com/mentioning-fun-facts-about-yourself-in-job-search-4148373
https://www.thebalancecareers.com/good-linkedin-summary-with-examples-4126809
https://www.thebalancecareers.com/how-to-write-about-me-page-examples-4142367

## Cool Portfolio examples

http://gokhanarik.com/#/resume
http://kalynnakano.com/

## Timelines
[What I'm trying to emulate](https://codepen.io/banik/pen/ELpWNJ?editors=1100#0)
[Close and nice color usage, with dates](https://codepen.io/rossmcneil/pen/rcxGb/)
[Simple "labely" with dates concept](https://codepen.io/NilsWe/pen/FemfK)
[Simplist | cool animations!](https://codepen.io/Fischaela/pen/sJdqg/)
[Simple W3 Schools tutorial](https://www.w3schools.com/howto/howto_css_timeline.asp)
[CV Template](https://www.canstockphoto.co.uk/modern-cv-resume-with-detailed-timeline-39666122.html)
[CV Template](https://www.123rf.com/photo_51242263_stock-vector-modern-resume-cv-curriculum-vitae-template-beginning-with-timeline.html)
[Data visualization (image, non css)](https://visual.ly/community/infographic/other/curriculum-vitae)
[Jonas Hietala: A simple timeline using CSS flexbox](https://www.jonashietala.se/blog/2024/08/25/a_simple_timeline_using_css_flexbox/)

## W3 Schools Templates (design inspiration)

[templates](https://www.w3schools.com/w3css/w3css_templates.asp)
Prefered from quick glance:

### Interesting entries, possibly for other projects
* [Analytics!!! Cool for custom monitoring platform](https://www.w3schools.com/w3css/tryw3css_templates_analytics.htm#)
* [Custom Message / Email client?](https://www.w3schools.com/w3css/tryw3css_templates_mail.htm)
* [Seems cool promo for maybe OSS documentation website](https://www.w3schools.com/w3css/tryw3css_templates_black.htm)

### Best looking
* [Grid of Cards](https://www.w3schools.com/w3css/tryw3css_templates_food_blog.htm)
* [CV](https://www.w3schools.com/w3css/tryw3css_templates_cv.htm)
* [Minimalist home page / header, similar to my website](https://www.w3schools.com/w3css/tryw3css_templates_coming_soon.htm)
* [Same + Scroll behavior and interesting footer with links](https://www.w3schools.com/w3css/tryw3css_templates_wedding.htm#rsvp)
* ["Missaligned" grid](https://www.w3schools.com/w3css/tryw3css_templates_photo.htm)
* [Linkedin like / social network](https://www.w3schools.com/w3css/tryw3css_templates_social.htm)

### Can take some ideas
* [Simplist, cool UI with nice header](https://www.w3schools.com/w3css/tryw3css_templates_start_page.htm#)
* [Sidebar and header](https://www.w3schools.com/w3css/tryw3css_templates_webpage.htm#)
* [Photographer Portfolio](https://www.w3schools.com/w3css/tryw3css_templates_photo3.htm#contact)
* [Grid of cards with side bar](https://www.w3schools.com/w3css/tryw3css_templates_portfolio.htm)

---
[HeatMaps to improve UI design / UX](https://uxplanet.org/how-heat-maps-can-improve-ui-design-7e2b8a68bb8a)

## Colors

[Easy to Remember Color Guide for Non-Designers](https://docs.sendwithses.com/random-stuff/easy-to-remember-color-guide-for-non-designers)

[Site to generate color themes](https://coolors.co/)
[Other](https://www.sessions.edu/color-calculator/)

---

[Website linter](https://webhint.io/)

[Web Application bundler](https://github.com/parcel-bundler/parcel)

[HTTP Blog Server in 100 lines off C](https://www.reddit.com/r/programming/comments/gdxh3w/http_blog_server_100_lines_of_c_in_a_closet/)


[How to Create Your Own SSL Certificate Authority for Local HTTPS Development](https://www.reddit.com/r/programming/comments/hjbx29/how_to_create_your_own_ssl_certificate_authority/)
[There is a way to create a valid Let’s Encrypt certificates for local development… sometimes](https://www.reddit.com/r/programming/comments/ghgbud/there_is_a_way_to_create_a_valid_lets_encrypt/fqaq3s9/)
The above link shows a really interesting take on it.

[Creating a blazing fast static website without Gatsby or JavaScript](https://bennetthardwick.com/blog/creating-a-blazingly-fast-blog-without-js-or-gatsby/)
JavaScript costs seconds
SEO Implications
No service worker, no worries
- Bad service workers
  Despite hard-refreshing the page a bad service worker almost ruined everything for Aleks.
  He had to manually open the developer tools and flush the cache - something most users wouldn't - or couldn't - do.
Gatsby speed with static sites
- Prerender your pages
  <link rel="prerender" href="<page url>" />
- Inline styles
  When you inline styles in the head of your website you save another request to the server.
- Self-host your fonts
- Serve your data well - Use a CDN (but measure)
  > I've found the latency introduced by Cloudflare proxying your site often outweighs it's benefits

[How to track and display profile views on GitHub](https://rushter.com/blog/github-profile-markdown/)
A single-pixel image can provide you a lot of information.
This is how the whole AD industry works when it tracks your visits on various websites.

[How to Favicon in 2021: Six files that fit most needs](https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs)
[Reddit discussion (r/programing)](https://www.reddit.com/r/programming/comments/kjchj4/how_to_favicon_in_2021_six_files_that_fit_most/)
[Reddit discussion (r/webdev)](https://www.reddit.com/r/webdev/comments/kiyki7/how_to_favicon_in_2021_six_files_that_fit_most/)
Suggestion from commenter: https://realfavicongenerator.net/
### In article:
The Ultimate Favicon Setup
1. favicon.ico for legacy browsers
2. A single SVG icon with light/dark version for modern browsers
3. 180×180 PNG image for Apple devices
4. Web app manifest with 192×192 and 512×512 PNG icons for Android devices.
Did we forget anyone? (Maybe, it’s time to say farewell to some less successful formats out there.)
* Windows Tile Icon (seems that it's not needed in recent versions)
* Safari Pinned Icon (seems that it's not needed since Safari 12, even apple.com doesn't serve it)
* rel=”shortcut”
  > A lot of (now outdated) tutorials will include a favicon.ico into HTML like this:
    ```html
    <link rel="shortcut icon" href="/favicon.ico">
    ```
    > Be warned that shortcut is not, and never was a valid link relation.
    > Read this [amazing article](https://mathiasbynens.be/notes/rel-shortcut-icon) by Mathias Bynens
    >  from ten years ago that explains why we never needed shortcuts and rel="icon" is just fine.
* Yandex Tableau (Chromium-based browser from the biggest Russian search engine)
  Had a special nice feature however it proved not to be very popular and now Yandex has removed
   the technical documentation for it from its website.
* Opera Coast (experimental browser for iOS, left the App Store in 2017)
[rel="shortcut icon" considered harmful](https://mathiasbynens.be/notes/rel-shortcut-icon)
How to build our Ultimate Favicon Set
1. Prepare the SVG
2. Create an ICO file
3. Create PNG images
4. Optimize PNG and SVG files ([svgo](https://github.com/svg/svgo) - SVG Optimizer)
5. Add icons to HTML
Free Tip: Separate icon for staging (nice way to distinguish from production environment)
6. Create a web app manifest (For static HTML, create a JSON file named manifest.webmanifest)

[CSS testing checklist](https://mobile.twitter.com/b0rk/status/1320352867253932032?s=09)

[No cookie for you](https://github.blog/2020-12-17-no-cookie-for-you/)
Good news: we removed all cookie banners from GitHub 🎉
> EU law requires you to use cookie banners **if** your website contains cookies that are not required for it to work.
> Common examples of such cookies are those used by third-party analytics, tracking, and advertising services.
> These services collect information about people’s behavior across the web, store it in their databases, and can use it to serve personalized ads.
> At GitHub, we want to protect developer privacy, and we find cookie banners quite irritating, so we decided to look for a solution. After a brief search, we found one: just don’t use any non-essential cookies. Pretty simple, really.
[Reddit Discussion](https://www.reddit.com/r/programming/comments/kf3aam/no_cookie_for_you_the_github_blog/)

[Front End Checklist](https://github.com/thedaviddias/Front-End-Checklist)
> The Front-End Checklist is an exhaustive list of all elements you need to have / to test before launching your website / HTML page to production.

[Self-hosted Google Fonts in Hugo | Redowan's Reflections](https://rednafi.com/misc/self_hosted_google_fonts_in_hugo/)

[Holy grail layout  |  Layout patterns  |  web.dev](https://web.dev/patterns/layout/holy-grail)

[Getting Started – Lightning CSS](https://lightningcss.dev/docs.html)
[GitHub - wilsonzlin/minify-html: Extremely fast and smart HTML + JS + CSS minifier, available for Rust, Deno, Java, Node.js, Python, Ruby, and WASM](https://github.com/wilsonzlin/minify-html?tab=readme-ov-file)
(Uses Lightning CSS underlying).
