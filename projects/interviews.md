# Interviews

[My favorite programming interview questions - Engineering Management](https://engineering-management.space/post/my-favorite-programming-interview-questions)
Breaking the ice:
* Asking if it was it easier to get here?
* Offering coffee or water
* Presenting myself (and other interviewers); talk a bit about what we do
* Give a small overview of the agenda of the interview
* Explain that there are no right or wrong questions, we’re just getting to know each other and if we would be a good fit together
Passing the mic to the candidate
* Can you say what was the project that you are most proud of working at and why?
* If you had the power to do anything on that project, what would you do?
* Can you tell us about a complex problem you had to solve, and how you did you do it?
* And what about something you did that was a total failure?
* Imagine that at some point you see yourself with a task that takes 2 weeks, but you have a deadline in one week, what would you do?
* What would be your ideal development process, from getting a feature to deliver it with quality?
Going technical
* Imagine that you have a task to build a HashMap class. How would you do it?
* Now you have this class being used by several projects. But you have a change request: we should be able to obtain the values in ascending order.
* We have a bug report. The class isn’t thread safe. What should we do?
* Now the data on the hash map is starting to be huge. What to do if that data doesn’t fit in the memory we have available?
