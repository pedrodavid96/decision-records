# Mobile

## UX

### Notifications

##### [4 Interesting Mobile Notifications Concepts](https://uxplanet.org/4-concepts-of-mobile-notifications-e028cc97f109)
>>>
1. Lock Screen Notifications and Day schedule
![example](./notifications_lock_screen.gif)
2. Customizing notification settings
3. Dive in details
![example](./notifications_dive_in_details.gif)
4. Request for permissions

#### [Misused mobile UX patterns](https://medium.com/@kollinz/misused-mobile-ux-patterns-84d2b6930570)
>>>
1. Hidden navigation
2. (Unclear) Icons, icons everywhere
3. (Non Standard) Gesture based navigation
4. Tutor overlays as onboarding
5. Creative but unintuitive empty states

[Example step by step Pet Diet App](https://uxdesign.cc/ui-ux-case-study-a-step-by-step-guide-to-the-process-of-designing-a-pet-diet-app-d635b911b648)

[5 Delightful Ways to Onboard New Users](https://medium.com/better-product/5-delightful-ways-to-onboard-new-users-4c0496b30b6a)

[Animated Transitions in Mobile Apps](https://uxplanet.org/animated-transitions-in-mobile-apps-412b8e8478e7)

[Redesigning the New York Times app — a UX case study](https://uxdesign.cc/the-new-york-times-timely-app-concept-27efe88e5d4b)

[Booking.com — UX Case Study](https://medium.muz.li/booking-com-ux-case-study-7ffb39e54791)

[3 Creative Concepts of Mobile Tab Bar Navigation](https://uxplanet.org/3-creative-concepts-of-mobile-tab-bar-navigation-86ec7e3b11ab)