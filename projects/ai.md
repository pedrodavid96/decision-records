# AI (Artificial Intelligence)

[Ollama](https://ollama.com/)

[🏡 Home | Open WebUI](https://docs.openwebui.com/)
[OIDC login with reverse proxy and custom root ca · Issue #6793 · open-webui/open-webui](https://github.com/open-webui/open-webui/issues/6793)

[You've been using AI Wrong - YouTube](https://www.youtube.com/watch?v=UbDyjIIGaxQ)

[danielmiessler/fabric](https://github.com/danielmiessler/fabric?utm_source=danielmiessler.com&utm_medium=referral&utm_campaign=why-i-created-fabric)
fabric is an open-source framework for augmenting humans using AI.
It provides a modular framework for solving specific problems using a crowdsourced set of AI prompts that can be used anywhere.

[Recall - Summarize Anything, Forget Nothing](https://www.getrecall.ai/)
Commercial version?

[YouTube Data API Overview](https://developers.google.com/youtube/v3/getting-started)

[Why I Created Fabric](https://danielmiessler.com/p/fabric-origin-story)

## Summarizing

[Review of best AI summarizers : NoteTaking](https://www.reddit.com/r/NoteTaking/comments/1d2sutz/review_of_best_ai_summarizers/)

[A youtube video summarizer using Ollama : ollama](https://www.reddit.com/r/ollama/comments/1e8iy7t/a_youtube_video_summarizer_using_ollama/)

## AMD

[Use ROCm on Radeon GPUs — Use ROCm on Radeon GPUs](https://rocm.docs.amd.com/projects/radeon/en/latest/index.html)

[My setup for using ROCm with RX 6700XT GPU on Linux : LocalLLaMA](https://www.reddit.com/r/LocalLLaMA/comments/18ourt4/my_setup_for_using_rocm_with_rx_6700xt_gpu_on/)

[ROCm Installation guide on Arch · GitHub](https://gist.github.com/augustin-laurent/d29f026cdb53a4dff50a400c129d3ea7)
Using [Morganamilo/paru - feature packed AUR helper in Rust](https://github.com/Morganamilo/paru).

Output details of GPU (may add to some toolbar).
```bash
watch -n 0.1 /opt/rocm/bin/rocm-smi -tP --showmeminfo vram --showuse --showmemuse --json
```

## Whisper

> [!TODO]
> To setup

Although whisper transcribes from lots of languages it can only translate into english.

### [GitHub - pluja/whishper: Transcribe any audio to text, translate and edit subtitles 100% locally with a web UI. Powered by whisper models!](https://github.com/pluja/whishper)

Currently under a rewrite:
[GitHub - pluja/whishper at v4](https://github.com/pluja/whishper/tree/v4)
> Uses WhisperX backend: better accuracy, speaker diarization, alignment...
> Anysub isn't limited to a single machine! With the worker system, you can set up multiple whisperx-api workers on different servers

Things missing:
User authentication.
AMD GPU support (rocm) -> may be missing from whisper itself [Add support for AMD GPU (ROCm Platform) by yaomingamd · Pull Request #1473 · openai/whisper · GitHub](https://github.com/openai/whisper/pull/1473).

### Misc

The translate functionality and editor is AMAZING (the seek on click on a timestamp, works with my speed up plugin, etc...).
This would be really useful for so many things.

[YouTube transcription not possible: Bot protection · Issue #112 · pluja/whishper](https://github.com/pluja/whishper/issues/112)
`yt-dlp` seems to get outdated frequently hitting bot protection.
Solved by going into the container and doing `yt-dlp -U`.

### [GitHub - schibsted/WAAS: Whisper as a Service (GUI and API with queuing for OpenAI Whisper)](https://github.com/schibsted/WAAS)

### [GitHub - ahmetoner/whisper-asr-webservice: OpenAI Whisper ASR Webservice API](https://github.com/ahmetoner/whisper-asr-webservice)

Usable by Bazarr

## [argosopentech/argos-translate](https://github.com/argosopentech/argos-translate)
> Open-source offline translation library written in Python

Uses [OpenNMT - Open-Source Neural Machine Translation](https://opennmt.net/)

**Examples:**

[LibreTranslate/LibreTranslate](https://github.com/LibreTranslate/LibreTranslate?tab=readme-ov-file)
> Free and Open Source Machine Translation API. Self-hosted, offline capable and easy to setup.
Cool UI on top of argos-translate / OpenNMT.
