# Password Manager

## Plugins

[Use 1Password to authenticate the GitLab CLI with biometrics | 1Password Developer](https://developer.1password.com/docs/cli/shell-plugins/gitlab/)

[Native messaging - Mozilla | MDN](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging)
Native messaging enables an extension to exchange messages with a native application, installed on the user's computer.
[passff/passff: zx2c4 pass manager extension for Firefox, Chrome and Opera](https://github.com/passff/passff?tab=readme-ov-file)

[tadfisher/pass-otp: A pass extension for managing one-time-password (OTP) tokens](https://github.com/tadfisher/pass-otp)


## VaultWarden

[Hardening Guide · dani-garcia/vaultwarden Wiki](https://github.com/dani-garcia/vaultwarden/wiki/Hardening-Guide)

[Bitwarden_rs requires 3 additional proxies · Issue #224 · lucaslorentz/caddy-docker-proxy](https://github.com/lucaslorentz/caddy-docker-proxy/issues/224)

### Backup

[General (not docker) · dani-garcia/vaultwarden Wiki](https://github.com/dani-garcia/vaultwarden/wiki/General-%28not-docker%29)
[How to backup sqlite database? - Stack Overflow](https://stackoverflow.com/questions/25675314/how-to-backup-sqlite-database)
