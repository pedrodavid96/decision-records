# Photography

[How to Remember Your Life - YouTube](https://www.youtube.com/watch?v=GLy4VKeYxD4)

When you take a photo your brain starts taking less from your non-visual aspects of the scene (the smell, the temperature, the sounds, etc...)
Get it "out of your system" by taking your photos and then put your camera away to actually be immersed in the experience and
build the memories that your photos will trigger.
The key to remembering your life is deleting photos.
Do a monthly review or something on your photo and delete the duplicates, the ones that don't resonate with you anymore, etc...
Sync photos / deletions between devices

[20 Essential Photography Tips for Beginners](https://www.bobbooks.co.uk/blog-post/20-essential-photography-tips-for-beginners-1#1-learn-to-hold-your-camera-properly)