# Performance testing / Benchmarking

[sharkdp/hyperfine: A command-line benchmarking tool](https://github.com/sharkdp/hyperfine)

[The Billion Row Challenge (1BRC) - Step-by-step from 71s to 1.7s](https://questdb.io/blog/billion-row-challenge-step-by-step/)

[Let’s Have Some Fun With Load Tests](https://levelup.gitconnected.com/lets-have-some-fun-with-load-tests-c7b6b69e1cc6)
When to Load Test?
- Before you release your product to the masses
- Before you release a highly anticipated update for your product
- Depending on your product, before holidays and events like Black Friday
`npm install -g artillery`
`artillery run -o report.json test.yml`
> You can also export the report as an HTML file with `artillery report report.json`
  and it will generate some fancy charts ready for review
Loadmill is a cloud-based service that is focused on running automated API and load tests.

[A Practical Guide to Load Testing](https://www.freecodecamp.org/news/practical-guide-to-load-testing/)
`npm install -g loadtest`
Apache JMeter
[discussion](https://www.reddit.com/r/programming/comments/h9yk41/a_practical_guide_to_load_testing/)
Gattling
locust

[How to Trace Linux System Calls in Production with Minimal Impact on Performance](https://en.pingcap.com/blog/how-to-trace-linux-system-calls-in-production-with-minimal-impact-on-performance)

Perf-stat
Intel Vtune

[Cachegrind: a cache and branch-prediction profiler](https://valgrind.org/docs/manual/cg-manual.html)
[GNU gprof](https://ftp.gnu.org/old-gnu/Manuals/gprof-2.9.1/html_mono/gprof.html)

[google/benchmark: A microbenchmark support library](https://github.com/google/benchmark)
