# Networking

## nosnet.pt

1. WPS disable
2. Changed networks to WPA 2 only (removed WPA + WPA 2)
3. Change DHCP range to allow some fixed IPs or disable it (and host your own)
   Changed to  192.168.1.5 - 192.168.1.249 (and disabled afterwards to delegate to [PiHole](#pihole))

### Port Forward bugs

[Forum](https://forum.nos.pt/app-nos-net-108/como-configurar-regras-de-port-forward-no-router-em-nosnet-pt-13802/index24.html#:~:text=atencao%20que%20os%20problemas%20sao%20diversos,fica%20aqui%20a%20lista%20dos%20bugs%20reportados%20ha%201%20ano%2C%20ainda%20nao%20resolvidos!).

* Não é possível CRIAR uma regra Port Forwarding com o DHCP desligado
* Não é possível CRIAR uma regra Port Forwarding com o DHCP ligado
  caso o IP do HOST não esteja no intervalo definido na gestão do DHCP
  (devia ser o contrário)
* Não é possível EDITAR UMA regra Port Forwarding
  resulta num UI não responsivo e na REMOÇÃO da referida regra!

## IPv6

[Why IPv6? : r/homelab](https://www.reddit.com/r/homelab/comments/mpvc0s/why_ipv6/)

## Basic Server Security

[Ansible 101 - Episode 9 - First 5 min server security with Ansible](https://www.youtube.com/live/gV_16dU7XjM?si=Et26bEbvppfy_Y01)

[PSA: Your subdomains are not as private as you think : homelab](https://www.reddit.com/r/homelab/comments/10so0i4/psa_your_subdomains_are_not_as_private_as_you/)

## PiHole

Change DHCP range to 192.168.1.5 - 192.168.1.249.

[//]: # "TODO: something"
<!--- TODO: Something else -->

* Persist configuration
* Harden PiHole Security -> Both Pi ("hardware") and WebServices
  Login
  SSH (keys / certificates)
    [How to configure and setup SSH certificates for SSH authentication - DEV Community](https://dev.to/gvelrajan/how-to-configure-and-setup-ssh-certificates-for-ssh-authentication-b52)
    [SSH Should You Open It to the Internet or Keep It Locked Down? : selfhosted](https://www.reddit.com/r/selfhosted/comments/1g6rcng/ssh_should_you_open_it_to_the_internet_or_keep_it/)
    [SSH password automation in Linux with sshpass](https://www.redhat.com/en/blog/ssh-automation-sshpass)
  Firewall
  Services logins
    [fail2ban/fail2ban: Daemon to ban hosts that cause multiple authentication errors](https://github.com/fail2ban/fail2ban)
    [Implementing Port Knocking with knockd < System | The Art of Web](https://www.the-art-of-web.com/system/port-knocking-knockd/)
    [jvinet/knock: A port-knocking daemon](https://github.com/jvinet/knock)
  [Crowdsec](https://www.crowdsec.net/) ?
  [What are you good practices and advices when exposing home services in internet? : selfhosted](https://www.reddit.com/r/selfhosted/comments/19b4ktd/what_are_you_good_practices_and_advices_when/)
* BackUps
  [jlar0che/Pi-hole-Automated-Backup-Solution at selfh.st](https://github.com/jlar0che/Pi-hole-Automated-Backup-Solution?ref=selfh.st)

[Can Pihole act as an mdns repeater? : pihole](https://www.reddit.com/r/pihole/comments/mvhzst/can_pihole_act_as_an_mdns_repeater/)
This would be useful to access machines via their hostname when connecting to home via VPN.

If for some reason you can't use `network: host` for PiHole container you might need a
DHCP relay.
A simple one in Docker is present in [homeall/dhcphelper: DHCP relay for DHCP Server in the docker container.](https://github.com/homeall/dhcphelper).

[Wildcard DNS record in PiHole : selfhosted](https://www.reddit.com/r/selfhosted/comments/1eenxzp/wildcard_dns_record_in_pihole/)
```dnsmasq.d/<05-custom>.conf
address=/example.com/192.168.100.220
```

## Exposing your server to the world

### Dynamic DNS Services

[Duck DNS - about](https://www.duckdns.org/about.jsp) -
[Whats wrong with DuckDNS? : selfhosted](https://www.reddit.com/r/selfhosted/comments/1eiwu9c/whats_wrong_with_duckdns/)
[Free Dynamic DNS - Managed DNS - Managed Email - Domain Registration - No-IP](https://www.noip.com/)

Basically these servers are "plugged" into the router so that when the Public IP changes (usual for non-commercial tiers of ISPs)
it triggers a change in the DNS server to target the new IP.

These free services aren't that reliable and usually require a "not professional" domain.
Another option is to build your own with the (very good) Cloudflare API.

### Port Forward

Open a port to the world... This is not ideal and should be used with great care.
Should only open a port to a very secure service / machine.

Ideally this could also be behind something that protected against DDOS / bot attacks.
Maybe Cloudflare [Private networks | Cloudflare Zero Trust docs](https://developers.cloudflare.com/cloudflare-one/connections/connect-networks/private-net/#create-a-tunnel-to-connect-your-network) / Routes.

#### Jump Box?

Might be worth thinking on having a Jump Box instead to filter out unwanted traffic
straight away, avoiding basic bots / DDOS attacks.
[How do you expose Nextcloud? : selfhosted](https://www.reddit.com/r/selfhosted/comments/15z7h4l/how_do_you_expose_nextcloud/)
Even though similar setups are descbribed here it would still require opening a port an in the end,
even if you firewall non jump box requests you're still processing them and subject to DDOS.
Or is the system described a Reverse Proxy and your Home Network is effectively a VPN client of the
jump box?

#### Checking for open (udp) ports / port forward

https://serverfault.com/a/733921

UDP is "sessionless" so if you just "ping" with `-zv` it will report as sucesful even if it is not
"accepted" by the target instance.

```bash
$ # Listen on the server via `nc -l`, `-u` is to use udp
$ nc -ul <port>
$ # "Ping" from the client
$ nc -u <server> <port>
type whatever you want to send and see on the server
```

### Cloudflare Tunnel

Basically you install a daemon in your target instance that connects directly with
Cloudflare and creates a "tunnel" for communication with the Open World.

The advantage compared to simply opening a port is that this "tunnel" also acts as
a middleware that can block unwanted traffic.
Additionally Cloudflare allows very tight security controls and you can put your
tunnel "behind" a lot of restrictions, even an IdP.

[Do you use cloudflare tunnel? : selfhosted](https://www.reddit.com/r/selfhosted/comments/u4qe6b/do_you_use_cloudflare_tunnel/)
Mentions crowdsec.

[fosrl/pangolin: Tunneled Reverse Proxy Management Server with Identity and Access Control and Dashboard UI](https://github.com/fosrl/pangolin)
SelfHostable Cloudflare Tunnel if you don't want to trust Cloudflare.

## VPN (WireGuard)

[How To Set Up WireGuard Firewall Rules in Linux - nixCraft](https://www.cyberciti.biz/faq/how-to-set-up-wireguard-firewall-rules-in-linux/)
This guide made my setup finally work! (even though I haven't understanded why yet).

I was able to connect to WireGuard already but basically I couldn't access the internet.
Initially this looked like a DNS issue but after some discovery I found I wasn't able to
ping ip addresses directly as well.

> [!TODO]
> Script DNS record for wireguard, so that you don't need to connect through (possibly changing) IP.

> [!FIXME]
> Currently needs to run `sudo resolvectl domain wg0 ""` in order to ping back the internet.

[wg-easy/wg-easy: The easiest way to run WireGuard VPN + Web-based Admin UI.](https://github.com/wg-easy/wg-easy)
[wg-easy/docker-compose.yml at master · wg-easy/wg-easy](https://github.com/wg-easy/wg-easy/blob/master/docker-compose.yml)

## CGNAT

[Cloudflare tunnel alternative? Want to use the right tools for the job. : selfhosted](https://www.reddit.com/r/selfhosted/comments/1gq3pdr/cloudflare_tunnel_alternative_want_to_use_the/)
[Tunnel with rathole • Shell Yeah!](https://blog.mni.li/posts/cgnat-and-rathole/)

## Ansible

[ansible/awx: AWX provides a web-based user interface, REST API, and task engine built on top of Ansible. It is one of the upstream projects for Red Hat Ansible Automation Platform.](https://github.com/ansible/awx)

[agaffney/ansible-synology-dsm: Ansible role for configuring a Synology NAS running DSM](https://github.com/agaffney/ansible-synology-dsm)
