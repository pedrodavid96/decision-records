[AWS Security Best Practices Part 1 : IAM & EC2 Key Pairs](https://www.botmetric.com/blog/aws-security-best-practices-part-1/)
EC2 Key-Pair Do’s
- Rotate SSH keys regularly
- Create Key Pairs Using Passphrase
- Enable Google Authenticator based MFA for SSH
- Change SSH from port 22 to a non standard port
EC2 Key-Pair Don’ts
- Do not keep private keys in temp or home directories
- Do not keep unused EC2 key pairs
Identity and Access Management Do’s
- Create individual IAM users using unique credentials
- Grant least privilege
- Use groups to assign permissions to IAM users
- Configure a strong password policy for your users
- Restrict privileged access further with conditions
- Enable MFA for all users not just privileged users, why take chance
- Use roles for applications that run on Amazon EC2 instances
- Rotate credentials regularly
Identity and Access Management Don’ts
- Do not use your root account access keys
- Do not share access using Access Key & Secret Key
- Do not have single role for all the users
- Do not have too many users with administrative access
- Do not use old access keys. Rotate it.

## Retrieve instance metadata

http://169.254.169.254/latest/meta-data/

The IP address 169.254.169.254 is a link-local address and is valid only from the instance.
For more information, see [Link-local address](https://en.wikipedia.org/wiki/Link-local_address) on Wikipedia.

## Tools

### [awsume](https://github.com/trek10inc/awsume)

A utility for easily assuming AWS IAM roles from the command line.

### [aws-vault](https://github.com/99designs/aws-vault)

A vault for securely storing and accessing AWS credentials in
development environments.
