[Extreme brainstorming questions to trigger new, better ideas](https://longform.asmartbear.com/extreme-questions/)
* 10x prices
  If you were forced to increase your prices by 10x, what would you have to do to justify it?
* No customers
  If all our customers vanished, and we had to earn our growth and brand from scratch, what would we do?
* No tech support
  e.g.: How would on-boarding need to be improved
* Maximize fun
  What would be the most fun thing to build?
* Complete rip-off
  If our biggest competitor copied every feature we have, how would we still win?
* No time
  What if we are forced to ship a full, completed MVP (or actually, [SLC][SLC]) new feature, in just two weeks, that would delight and surprise some fraction of our customers.
* Flipped business model
  What if you were forced to charge customers in a completely different manner?
  e.g.: usage-based -> flat monthly rate;
        monthly with tiers -> measure and charge daily with some formula.
* No website
  how would you still grow your business?
* No meetings
* No customer contact
* Cost is no object
* Sociopathic CEO
  What if you could change anything, regardless of what anyone thinks or feels?
* Mortal wound
  What externality has the potential to kill the entire company?
* Philanthropist
  What if our only goal were to create the most good in the world, personally for our customers?
* Only one thing this year

[SLC]: https://longform.asmartbear.com/slc/ "Simple, Viable and Complete, pronounced as 'Slick'"
