# Music

[Learn music theory in half an hour -  ANDREW HUANG](https://www.youtube.com/watch?v=rgaTLrZGlk0)

## Trumpet

[Mr.B's Covers](https://www.youtube.com/channel/UCBydHCB41h8kE5V6zHKsCuA)
Awesome, funny, "simple" songs (Nothing Else Matters solo wasn't simple). 🥇⭐
Most importantly, shows how someone can enjoy even the simplest pop songs.
* [Shallow from "A Star Is Born" (2018) Trumpet Cover](https://www.youtube.com/watch?v=JC8xZfQLKP8)
* [Always Remember Us This Way (Trumpet cover)](https://www.youtube.com/watch?v=5kALQ79T7Eg)
* [A Thousand Years (Trumpet Cover)](https://www.youtube.com/watch?v=FpMGeLP3TeE)
* [Free Bird (Trumpet Cover)](https://www.youtube.com/watch?v=Brl_dyd-ig4) - Caution for Rick Roll, intro is awesome nonetheless 😂
* [Nothing Else Matters (Trumpet Cover)](https://www.youtube.com/watch?v=hwpdWGRv1rE)
* [Con Te Partiro (Trumpet Cover)](https://www.youtube.com/watch?v=5OE9W-n0FK8)
* [I'm Yours (Trumpet Cover)](https://www.youtube.com/watch?v=lpbBLI7HcyI)

[Expanding my musical knowledge : trumpet](https://www.reddit.com/r/trumpet/comments/9m8fw7/expanding_my_musical_knowledge/)
* Haydn concerto
* Hummel concerto
* Hindemith sonata
* Halsey Stevens sonata
* Ewazen sonata
* Arutunian Concerto
* Neruda Concerto
* Tomasi concerto
* Aaron Copland - Quiet City
* Kent Kennan sonata
* Arbans Variations on the Carnival of Venice
* Enescu- Légende
* Leonard Bernstein- Rondo for Lifey
* Herbert Clarke - any cornet solo - e.g. The Maid of the Mist
* Bruce Broughton - Oliver’s Birthday
* Vincent Persichetti. The Hollow Men
* Vincent Persichetti - Parable XIV for Solo Trumpet
* Arthur Honegger - Intrada
* J. Guy Ropartz - Andante and Allegro
* Eugene Bozza - Caprice
* Eugene Bozza - Rustiques
* Oskar Bohme - Concerto
* Theo Charlier - 1st/2nd Solos de Concours
