# Prolog

[The Power of Prolog](https://www.metalevel.at/prolog)

[TerminusDB](https://github.com/terminusdb/terminusdb)
open-source graph database and document store. It is designed for collaboratively building data-intensive applications and knowledge graphs.
Largely written in Prolog
