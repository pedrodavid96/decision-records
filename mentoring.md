# Mentoring

[The Mentor Playbook - Gabriela Dombrowski](https://www.youtube.com/watch?v=AsT4bCM4tJc)

Formulate good responses:
* What have you considered already?
* What went as planned?
* What do you need to feel supported?
* When X, do Y, because Z... (instead of "just do XYZ")
