# git

[Popular git config options](https://jvns.ca/blog/2024/02/16/popular-git-config-options/)

**Debugging Git:**
```
GIT_TRACE=true git <...>
```
Used to debug git credential helper calls.

## Commands to investigate further:

- [] Sparse - https://www.reddit.com/r/programming/comments/qrmna7/make_your_monorepo_feel_small_with_gits_sparse/
- [] git bisect
- [] git rerere
- [] git credential-manager-core
- [] [GitHub - ewanmellor/git-diff-image: An extension to 'git diff' that provides support for diffing images.](https://github.com/ewanmellor/git-diff-image)
- [] git sync hooks - [Husky - Git hooks](https://typicode.github.io/husky/#/)
- [] git format-patch

## Misc

[Make Git automatically remove trailing white space before committing](https://stackoverflow.com/a/28446440)

## Code Review Tools

Gerrit

# Practical Git / Hands on Git / Git introduction

## Beforeword

A lot of you may think this is just another article on getting started with git,
spoiler alert: it is.

**Why am I making this?**

I remember when I started using Git (around 4 years ago) I struggled a lot on the first weeks despite the huge amount of tutorials already available
(to be honest I was still very inexperienced in the Computer Science world altogether, with just ~1.5 years of my degree).

Official resources (and even most tutorials) use a lot of (correct, but overly complicated for a beginner) jargon
(just checked `git diff --help`, the description contains concepts like `working tree`, `index`, `blob objects`).

While important, specially if you're discussing and trying to explain more complex use cases,
these concepts aren't crucial to get started and taking advantage of the tool.

This guide aims to be that simple "first hands on", one more simple pratical guide shouldn't hurt anyone.

**Why I do this kind of "head first" tutorials?**

1. Brains have limited storage, I like to offload as much as possible to my personal archive - this blog
2. I won't ever reject a candidate for a Software Engineering position **simply because** they don't know \<technology X>
  - They may have used an alternative in the past
  - Nobody knows everything, I usually look for different indicators / qualities when evaluating SE's
3. With that in mind I may find myself with a new colleague that doesn't know how to use \<technology X>
  - I will gladly help them with anything they need
  - **But** context switches hurt productivity
  - Onboarding (TODO: Add Good Onboarding resources)
  - Leading them to these tutorials as part of the onboarding seems like a better approach to equip them with the basic concepts beforehand
3. I will try to mention multiple other resources that you can follow for more in-depth explanations
4. I hope I will mention at least one thing you might haven't known already

## Why should I learn Git?

You really need version control!
In fact, once you try it, you will want it for everything!
No more "work_1.txt", "work_2.txt", "work_final.txt", "work_final_now_for_real.txt"...
Other systems will now look very inadecuate for managing your project (I still remember trying to use Dropbox... 😬)
It gives you the freedom to experiment

## Common missconception

Git is not Github/Gitlab... you don't need the later to use the first and version control your project, they simply provide you a "remote" repository and cool aditional features. TODO: elaborate

## Why should you use Git from the command line?

It's not "that hard"... well... it kinda is, the UX is pretty ifty but well, complex systems have complex UX and to be honest I've yet to find a better UX on a Git GUI
(besides IntelliJ IDEA "merge solver", but I only use it for that and it's my go to choice).

TBH, I've started with Sourcetree (which I highly recommend if you really want one) but I found myself falling back to the terminal multiple times and, in insight, it didn't help me much with getting better with the tool and discovering it's possibilities.
Once I changed my main workstation to Linux (no Sourcetree there) I completely ditched it.
IMO the greatest benefict of these GUIs is the beautifull graph views (seriously, look at Sourcetree and Gitkraken homepages),
which was no longer an issue after I found it's pretty easy to render those on the terminal!

```bash
$ # I alias this as `git adog`, which is a simple mnemonic https://stackoverflow.com/a/35075021
$ git log --all --decorate --oneline --graph
$ # From documentation and just like [this comment](https://stackoverflow.com/questions/1057564/pretty-git-branch-graphs#comment100890942_35075021) mentions `--decorate` seems to be included by default
TODO: Add example output
```

With that out of the way, lets get started:

1. Install git
2. Create a repository
Lets think of the repository as simply your files + their history for now.

```bash
$ git init
```
3. Check your repository status

```bash
$ git status
TODO: add output
```

*Bonus:* From now on, check your repository status after each step to see insights into how you're changing your repository

4. Create a commit
Lets think of a commit as a point in history of your repository

I usually like to start my git repositories with an empty commit, by default you can't make empty commits so we have to use the flag `--allow-empty`
```bash
$ git commit -m "Create repository" --allow-empty
```

Commits are an object (we won't dive into this yet) with a mandatory message, that's what our `-m` argument is.

I rarely (well, not really, but that's mostly because of my personal workflow) use this flag.
Commit messages are very important! and you should strive to [write good ones](TODO), `-m` rarely results in those.

If you don't provide this argument you will be taken to your `$EDITOR` where you will be able to write them,
adittionaly you can pass `-v` (`--verbose`) so your editor shows the [differences](TODO:) commented, usefull to have a quick glance (I actually alias commit so it's verbose by default TODO: check if true)

*Bonus:* Check your git history

```bash
$ git log
```

6. Change your repository (kinda)
by creating a README.md file

```bash
$ echo "Hello, world! I'm getting started with Git" > README.md
```

*Repository status:* Changes are by default [unstaged](TODO), we have to `add` (stage) before commiting them

7. View differences

```bash
$ git diff
```

8. Add changes

```bash
$ git add README.md
```

In this case, you could use `--all` or `.` (TODO: `--all` vs `.`) instead of providing the file name.

*Repository status:* The file went from `unstaged` to `staged` (probably changed from red to green)

*Bonus:* If you view differences (`git diff`) now you won't see anything,
that's because `diff` checks unstaged differences by default,
you can use `--staged` (which is a synonym for the old `--cached`, Git UX is improving) to see only the staged diffs.

9. Commit your changes

```bash
$ git commit -m "Create README.md"
```
*Bonus:* Recheck your git history, you now see both commits

10. Let's go back in history

```bash
$ git checkout <first-commit-hash>
```

Your file disappeared! But no worries, it's finally version controlled, you just went back in history to a different version of your repository.
TODO: How do you get back to latest version?

**You're now a master in Git!!!**

I'm joking, there's still a lot to learn! but this should pretty much enable you to start playing with Git and start to version control your project,
even if at a rudimentary level.

(TODO: follow up)

* `remote`
  If you go to Git homepage the first adjective (as of now at least) is `distributed` but the repository is yours only (and not backed up anywhere 😱).
  This article will allow you to start working with your team and provide a "free backup" (clickbait alert)(well, it's simply a copy somewhere else, redundancy is a form of back ups 😛)
* `HEAD`
* `branch`
* `.gitignore` ([gitignore.io](https://gitignore.io))
* [Git LFS](TODO)
* Tips and tricks
* Improve your productivity with git aliases and bash aliases (TODO: link to my dotfiles)
* Different Git Workflows

## I've just ran multiple commands and I can (kinda) do stuff, but now explain what I've just done for real / Article explanation

## TODO: References

## TODO: Git repository analysis tool

---

[Interactive tutorial](https://learngitbranching.js.org/)

[Becoming a Git pro. Part 1: internal Git architecture](https://indepth.dev/becoming-a-git-pro-part-1-internal-git-architecture/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/fd9zpj/becoming_a_git_pro_part_1_internal_git/)

[Git from the Bottom Up](https://jwiegley.github.io/git-from-the-bottom-up/)
[Git for Computer Scientists](https://eagain.net/articles/git-for-computer-scientists/)
[Git Concepts Simplified](https://gitolite.com/gcs.html#(1))
[Chapter 10 of Pro Git - Git Internals - Plumbing and Porcelain](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain)
[Git from the inside out](https://codewords.recurse.com/issues/two/git-from-the-inside-out)

[Git series 1/3: Understanding git for real by exploring the .git directory](https://www.daolf.com/posts/git-series-part-1/)

[How to explain git in simple words?](https://smusamashah.github.io/blog/2017/10/14/explain-git-in-simple-words)

[15 Git Commands you may not know](https://zaiste.net/15-git-commands-you-may-not-know/)

## Git repository between Multiple Platforms

Have you ever seen diffs with loads of `^M` (`CRLF`), this guide is for you.

If your Git repositories has different OSs as clients you might hit some
 problems due to the way they process "New Lines".

DOS/Windows: CRLF - Carriage Return + Line Feed - '\r'
Unix: LF - '\n'
Mac (prior to Mac OS X): CR
Mac (nowadays): 'LF'

[Source](https://superuser.com/a/439443)

Nowadays this should only be a problem between Windows and Unix based systems.

[git replacing LF with CRLF](https://stackoverflow.com/a/20653073)
Seems incoherent with next article which seems to work, but explanation
 seems pretty good.

[Github recommendations](https://help.github.com/en/github/using-git/configuring-git-to-handle-line-endings)

### Global settings for line endings

For Windows:
```
$ git config --global core.autocrlf true
# Configure Git to ensure line endings in files you checkout are correct for Windows.
# For compatibility, line endings are converted to Unix style when you commit files.
```

### Per-repository settings (recommended!)

Create a `.gitattributes` file with the following contents

```
# Set the default behavior, in case people don't have core.autocrlf set.
* text=auto

# Explicitly declare text files you want to always be normalized and converted
# to native line endings on checkout.
*.c text
*.h text

# Declare files that will always have CRLF line endings on checkout.
*.sln text eol=crlf

# Denote all files that are truly binary and should not be modified.
*.png binary
*.jpg binary
```

`text` [documentation](https://git-scm.com/docs/gitattributes#_code_text_code)

* `text eol=crlf` Git will always convert line endings to CRLF on checkout. You should use this for files that must keep CRLF endings, even on OSX or Linux.

* `text eol=lf` Git will always convert line endings to LF on checkout. You should use this for files that must keep LF endings, even on Windows.

This basically means that when you checkout something from remote your Git Client
 on Windows will automatically convert `LF`(`\n`) to `CRLF`(`\n`), but it won't
 create a diff because your checkin will automatically convert it back.

This is easy to verify using `WSL` (Windows Subsystem for Linux).

I cloned the repository on (normal) Windows and in `WSL`.
Using one of the following commands to find `CRLF`:

```
$ # -I ignore binary files
$ # -U prevents grep to strip CR characters.
$ #    By default it would do it if it decides it's a text file.
$ # Use Ctrl+V, Ctrl+M to enter a literal Carriage Return character into your grep string.
$ grep -IUr --color "^M"
$ # Or equivalent
$ grep -IUr $(printf '\r')
```

I was able to find them in the "Windows Clone" and not in the "Unix Clone",
 although none of them reported any diffs.

### Refreshing a repository after changing line endings

```
$ git add . -u
$ git commit -m "Saving files before refreshing line endings"
$ git add --renormalize .
$ git commit -m "Normalize all the line endings"
```

[another option:](https://stackoverflow.com/questions/1889559/git-diff-to-ignore-m)
```
# Remove everything from the index
$ git rm --cached -r .
# Re-add all the deleted files to the index
# You should get lots of messages like: "warning: CRLF will be replaced by LF in <file>."
$ git diff --cached --name-only -z | xargs -0 git add
$ git commit -m "Fix CRLF"
```

## Git diff stats (files changed)

```
git diff --stat
```

## Git add "parts" (partial...)

```
git add -p
```

## Git pull vs Git fetch

## Ensure file permissions are transmited to git

https://stackoverflow.com/a/40979016

### In short:
By default, when you add a file to a repository, Git will try to honor its filesystem attributes and set the correct filemode accordingly.

Add flag:
`git update-index --chmod=+x path/to/file`
Remove flag:
`git update-index --chmod=-x path/to/file`

Show files stage:
`git ls-files --stage`
* 100644 for regular files
* 100755 for executable ones

Starting with git 2.9 you can stage a file *and* set the flag in one command:
`git add --chmod=+x path/to/file`

## Use `#` in git messages

`git commit --cleanup=scissors`

>>>
from git commit --help

--cleanup=<mode>
  scissors
    Same as whitespace, except that everything from (and including) the line
    "# ------------------------ >8 ------------------------" is truncated if the message
    is to be edited. "#" can be customized with core.commentChar.

In:
https://stackoverflow.com/a/37058066

## Delete remote branches

`git push --delete <branches ...>`

## Clone specific branch

`git clone -b <branch> <remote_repo>`

## Git archive

https://git-scm.com/docs/git-archive

```.gitattributes
*.ignore export-ignore
```

`git archive --format=tar.gz --prefix=git-1.4.0/ v1.4.0 >git-1.4.0.tar.gz`

Create a compressed tarball archive that contains the contents of the v1.4.0 release

### export-subst
If the attribute export-subst is set for a file then Git will expand several placeholders when adding this file to an archive.

```
$ echo 'Last commit date: $Format:%cd$' > LAST_COMMIT
$ echo "LAST_COMMIT export-subst" >> .gitattributes
$ git add LAST_COMMIT .gitattributes
$ git commit -am 'adding LAST_COMMIT file for archives'
```

When you run git archive, the contents of that file when people open the archive file will look like this:

```
$ cat LAST_COMMIT
Last commit date: $Format:Tue Apr 21 08:38:48 2009 -0700$
```

## Signing Your Work

https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work

```
$ git config user.signingKey <key>
$ git tag -s v1.5 -m 'my signed 1.5 tag'
$ git commit -S -m 'my signed commit'
$ git log --show-signature
```

GPG sign commits and annotated tags by default
```
git config --global commit.gpgsign true
git config --global tag.forceSignAnnotated true
```

## Transparent Git Encryption

### git-crypt
https://www.agwa.name/projects/git-crypt/

### "Native" (.gitattributes + .git/config)
https://gist.github.com/sandeepraju/4934282f5f87c83ddd93
https://github.com/arcticicestudio/igloo/pull/69/files

## [Git LFS](https://git-lfs.github.com/)

```
git lfs install
git lfs track "*.psd"
git add .gitattributes

git add file.psd
git commit -m "Add design file"
git push origin master
```

## Git tags

2 types: *lightweight* and *annotated*.

* *lightweight* tags are just a pointer to a specific commit
  Can be created by not specifying an `-a`, `-s` or `-m`.
* *annotated* tags are stored as full objects in the Git database
  They're checksummed, contain the tagger name, email and date, a message and can be signed and verified with GPG.
  Can be created by specifying an `-a`.

`git push --follow-tags` will only push annotated tags


## [Using GitHub HTTPS Credentials in WSL 2](https://blog.anaisbetts.org/using-github-credentials-in-wsl2/)

```bash
$ sudo nano /usr/bin/git-credential-manager
```

```bash
#!/bin/sh
exec /mnt/c/Program\ Files/Git/mingw64/libexec/git-core/git-credential-manager.exe $@
```

```
$ sudo chmod +x /usr/bin/git-credential-manager
```

```.gitconfig
[credential]
helper = manager
```

## Git Hooks

> Doing git add inside a commit hook seems pretty evil to me. What if you are doing partial staging/committing of a file? You don't want the complete file to be committed behind your back, do you?
https://stackoverflow.com/questions/591923/make-git-automatically-remove-trailing-whitespace-before-committing#comment24601678_3516525
a


## Workflows

[Please stop recommending Git Flow!](https://georgestocker.com/2020/03/04/please-stop-recommending-git-flow/)

[On Git commit messages and issue trackers](https://hackernoon.com/on-git-commit-messages-and-issue-trackers-f700f3cbb5a7)
1. Using the issue ID as first-class citizen into our commit message is not the best thing to do.

   We might change it, it might become offline temporarily or permanently

2. The issue ID is wasting precious space that instead could be used to explain what we’ve done in that commit.

3. The linked issue’s nature and its state after the commit is not clear.

   Closed? Referenced? Causes?

4. What about many issues? And no issues at all?

Extra chapter: do you know that you can use emoji in your commit messages? 😁

---

## [8.2 Customizing Git - Git Attributes](https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes)

[Keyword Expansion](https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes#_keyword_expansion)
Looks pretty cool. Maybe for `@since` tags or whatnot.

---

[Git - how do I view the change history of a method/function?](https://stackoverflow.com/questions/4781405/git-how-do-i-view-the-change-history-of-a-method-function)
> Recent versions of git log learned a special form of the -L parameter
[Source](https://stackoverflow.com/a/33953022)


[My favourite Git commit](https://dhwthompson.com/2019/my-favourite-git-commit)
1. It explains the reason for the change
2. It’s searchable
3. It tells a story
4. It makes everyone a little smarter
5. It builds compassion and trust
Good commits matter

[A Branch in Time (a story about revision histories)](https://tekin.co.uk/2019/02/a-talk-about-revision-histories)
Awesome talk on the beneficts of a good Git history!!!

## [Print last commit without indentation](https://stackoverflow.com/a/49429009)

This may be usefull if you need to copy it back to vim, as to avoid
automatic line breaks.

```bash
$ git log --format=%B HEAD
```

## [Git: How to reuse/retain commit messages after 'git reset'?](https://stackoverflow.com/questions/16858069/git-how-to-reuse-retain-commit-messages-after-git-reset)
```bash
$ git commit --reuse-message=HEAD@{1}
$ # Or even shorter
$ git commit -C HEAD@{1}
```

[What's the difference between HEAD^ and HEAD~ in Git?](https://stackoverflow.com/questions/2221658/whats-the-difference-between-head-and-head-in-git)
* Use ~ most of the time — to go back a number of generations, usually what you want
* Use ^ on merge commits — because they have two or more (immediate) parents

[--simplify-by-decoration](https://git-scm.com/docs/git-log#_history_simplification)
> Commits that are referred by some branch or tag are selected.
Which means basically it would only show merges in master.

[Transform a project folder into a package; collaborate on confidential work.](https://statagroup.com/articles/repo-remixing)

```bash
$ git subtree split --prefix=path/to/thatLibrary -b thatLibrary
$ git init a --bare library-name
```
Bare repositories do not have a Working tree, so they can't "add" and "commit".
These are the types of "central repos" we have in GitHub, Gitlab, Gitea, etc...

[git-exfiltrate: rescue large branches](https://statagroup.com/articles/git-exfiltrate-enabling-post-hoc-increments)

# Git Messages

[Telling stories with your Git history](https://www.futurelearn.com/info/blog/telling-stories-with-your-git-history)

[How Git Partial Clone lets you fetch only the large file you need](https://about.gitlab.com/blog/2020/03/13/partial-clone-for-massive-repositories/)
[reddit discussion](https://www.reddit.com/r/programming/comments/fin7sc/git_partial_clone_lets_you_fetch_only_the_large/)

[How to prevent code reviews from slowing down your team](http://www.sheshbabu.com/posts/how-to-prevent-code-reviews-from-slowing-down-your-team/)
- Not having coding guidelines
- Not using automated checks
- Not doing self reviews
- Raising huge PRs
- Raising vague PRs
- Not having deadlines for finishing reviews

[typicode / husky](https://github.com/typicode/husky)
> Git hooks made easy

---

`git bisect`
[Locating a compiler bug with git bisection - discussion](https://www.reddit.com/r/programming/comments/gf65l3/locating_a_compiler_bug_with_git_bisection/)
> At one point the author uses a shell variable containing old or new to pass to git bisect a la git bisect ${OLD_OR_NEW}. I'd like to point out that git has automated facilities for this that are much more ergonomic.
> After beginning a git bisect by some git bisect start command, you can instruct git on how to automatically test each step via a git bisect run <script> <args...> command. For each revision tested, git will run the given script with the given arguments and check the return code $?.
> If $? is 0, then the revision is marked good. If $? is 125, the revision is skipped. If $? is any other value, the revision is marked bad. For example, if you have an automated set of tests you wish to check, you might specify test.sh as something like
```bash
#!/usr/bin/env bash
make || exit 125
make test
```
> It's a simple concept, but this little bit of automation has saved me much manual effort over the years.

[The Biggest Misconception About Git](https://medium.com/@gohberg/the-biggest-misconception-about-git-b2f87d97ed52)
> Git is all about storing differences between files, right?
Wrong. Git does not rely on diffs to do its magic.

The Zen of Git (The 3-Line Version):
1. Git stores the content of your files in objects called 'blobs'
2. Your folders turn into objects called 'trees' containing other 'trees' (subfolders) and 'blobs'
3. A commit is a type of object containing a 'tree'. Once created, objects cannot change.

> 99% of your Git work is just creating those objects and manipulating pointers that reference them.

> So Git doesn’t care about diffs at all?
Not really. Git tries to be very efficient, see zlib and 'delta compression’.

Sum it up:
* Git is not diff-based, it is object-based.
* Git does not apply diffs to show you a version of your project
* Git does traverse object trees to show you a version of your project
* Git does use diffs to minimize disk space for its objects

[git-diff-image](https://github.com/ewanmellor/git-diff-image)

[Git Credential Manager Core](https://github.blog/2020-07-02-git-credential-manager-core-building-a-universal-authentication-experience/)

[How Subversion was built and why Git won | Discussion](https://www.reddit.com/r/programming/comments/hl4gmh/how_subversion_was_built_and_why_git_won/)
Very interesting discussions.

**git rerere**
[Fix conflicts only once with git rerere](https://medium.com/@porteneuve/fix-conflicts-only-once-with-git-rerere-7d116b2cec67)
[The Git Rerere Command — Automate Solutions to Fix Merge Conflicts](https://levelup.gitconnected.com/the-git-rerere-command-automate-solutions-to-fix-merge-conflicts-d501a9ab9007)
```bash
git config --global rerere.enabled true
```

[Better git diffs with fzf](https://medium.com/@GroundControl/better-git-diffs-with-fzf-89083739a9cb)
```bash
fd() {
  preview="git diff $@ --color=always -- {-1}"
  git diff $@ --name-only | fzf -m --ansi --preview $preview
}
```

[Preemptive commit comments](https://arialdomartini.wordpress.com/2012/09/03/pre-emptive-commit-comments/)
Looks pretty cool to test 🥇⭐
tl;dr version:
Rule #1: write commit comments before coding
Rule #2: write what the software should be supposed to do, not what you did

Less cognitive load: I am used to write a failing test before quitting the office. It helps me quickly switch to the work context the next day.

[How to customize git diff to spot changes faster](https://www.reddit.com/r/programming/comments/i748k2/how_to_customize_git_diff_to_spot_changes_faster/)
delta as an alternative to `diff-so-fancy`?
or simply?
```bash
git diff--word-diff
git diff --word-diff-regex=.
```
[Difftastic, a structural diff](https://difftastic.wilfred.me.uk/)
Difftastic is a CLI diff tool that compares files based on their syntax, not line-by-line.
Difftastic produces accurate diffs that are easier for humans to read.

[Oh Shit, Git!?!](https://ohshitgit.com/)
[Discussion](https://www.reddit.com/r/programming/comments/k9hlr5/oh_shit_git_love_this_site_for_git/)
> So here are some bad situations I've gotten myself into, and how I eventually got myself out of them in plain english.
Basically quick tips on how to fix "common git issues"

[Pijul - The Mathematically Sound Version Control System Written in Rust | Discussion](https://www.reddit.com/r/programming/comments/k39td1/pijul_the_mathematically_sound_version_control/)

[Git email flow vs Github flow](https://blog.brixit.nl/git-email-flow-versus-github-flow/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/kn083e/git_email_flow_vs_github_flow/)
Email flow:
* Clone the repository locally
* Make your changes on your local checkout
* Run git send-email with a ref pointing to one or a range of commits
* Get comments as response to the patch as emails, mirrored on the webpage of the mailing list
* Fix up your previous mistakes, run git send-email again with the -v2 argument to send an updated* version
* The maintainer applies the patch from the email.
Main issues:
* You can't se the full state of the "pull request"
  Having the mail lists on Sourcehut, the patch view fixes most of the visibility issues

[Git Commands to Live By](https://medium.com/better-programming/git-commands-to-live-by-349ab1fe3139#e221)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/kmxz65/git_commands_to_live_by_the_cheat_sheet_that_goes/)
* Stash Individual Files
  `git stash push -- <filepath(s)>`
* Work With Multiple Branches Simultaneously
  `git worktree add <path> <branch>`
* Remove Ignored Files From Remote
  `git rm [-r] [-n] --cached <path(s)>`

[Commits are snapshots, not diffs](https://github.blog/2020-12-17-commits-are-snapshots-not-diffs/)

[git-format-patch](https://git-scm.com/docs/git-format-patch)

[How to find the nearest parent of a Git branch? - Stack Overflow](https://stackoverflow.com/questions/3161204/how-to-find-the-nearest-parent-of-a-git-branch)
```bash
git show-branch \
  | sed "s/].*//" \
  | grep "\*" \
  | grep -v "$(git rev-parse --abbrev-ref HEAD)" \
  | head -n1 \
  | sed "s/^.*\[//"
```

[Multiple working directories with Git?](https://stackoverflow.com/questions/6270193/multiple-working-directories-with-git)
[Git subtree: the alternative to Git submodule | Atlassian Git Tutorial](https://www.atlassian.com/git/tutorials/git-subtree)
[Git - git-worktree Documentation](https://git-scm.com/docs/git-worktree)

[Daily Habits to Turn Your Git History Into Valuable Documentation](https://betterprogramming.pub/daily-habits-to-turn-your-git-history-into-valuable-documentation-15113e1bf312)
* Choose a Standardized Format for Git Commit Messages
  - The broken-windows theory
  - Fix broken windows in Git commit messages.
* Capture “Why” in Your Git Commits

[Merging vs Rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)

[SemanticDiff - Language Aware Diff For VS Code](https://semanticdiff.com/)

[Show just the current branch in Git](https://stackoverflow.com/a/1418022)

[The tutorial that made Git click for me : programming](https://www.reddit.com/r/programming/comments/1egtaib/the_tutorial_that_made_git_click_for_me/)

[git-spice: Git branch and PR stacking tool, written in Go : programming](https://www.reddit.com/r/programming/comments/1e9gqc1/gitspice_git_branch_and_pr_stacking_tool_written/)

[Using the slash character in Git branch name](https://stackoverflow.com/questions/2527355/using-the-slash-character-in-git-branch-name)
I had an error trying to create a branch 'pd/TIC-123/custom-feat' when
'pd/TIC-123' already existed. Seems to be a limitation because objects are
"mapped" to the filesystem.
