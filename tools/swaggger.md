# Swaggger

[OpenAPI cop](https://github.com/EXXETA/openapi-cop)
A proxy that validates responses and requests against an OpenAPI document.
[Reddit discussion](https://www.reddit.com/r/programming/comments/fc9tfh/show_reddit_an_openapi_compliance_proxy_that/)
