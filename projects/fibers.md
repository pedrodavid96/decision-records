[OpenJDK Project Loom: JVM Threads With Light-Weight Concurrency](https://ahsensaeed.com/java-openjdk-project-loom-threading-example/)

https://cr.openjdk.java.net/~rpressler/loom/Loom-Proposal.html

[State of Loom](http://cr.openjdk.java.net/~rpressler/loom/loom/sol1_part1.html)
[Reddit discussion](https://www.reddit.com/r/programming/comments/gkgzld/state_of_project_loom_on_jvm/)

[Taking Project Loom for a spin](https://renato.athaydes.com/posts/taking-loom-for-a-spin.html)
[disucssion](https://www.reddit.com/r/programming/comments/glirkj/taking_project_loom_for_a_spin_java_virtualgreen/)
