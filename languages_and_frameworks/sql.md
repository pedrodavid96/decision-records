# SQL

https://www.sqlstyle.guide/#tables
> Use a collective name or, less ideally, a plural form. For example (in order of preference) staff and employees.

https://justinsomnia.org/2003/04/essential-database-naming-conventions-and-style/
> give tables singular names, never plural (update: i still agree with the reasons given for this convention, but most people really like plural table names, so i’ve softened my stance)
>   avoid confusion of english pluralization rules to make database programming easier (e.g. activity becomes activities, box becomes boxes, person becomes people, data remains data, etc.)