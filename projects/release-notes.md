# Release Notes

## OpenAPI Generator for Rust required the following lib

[OpenSSL not found on Ubuntu · Issue #763 · sfackler/rust-openssl](https://github.com/sfackler/rust-openssl/issues/763)

```bash
$ apt install libssl-dev
```

[Calling Purgatory from Heaven: Binding to Rust in Haskell - Well-Typed: The Haskell Consultants](https://well-typed.com/blog/2023/03/purgatory/#example-rust)