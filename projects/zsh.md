# ZSH

Disable aliases
```
zstyle ':omz:*' aliases no
```

[Faster and enjoyable ZSH (maybe) • htr3n's](https://htr3n.github.io/2018/07/faster-zsh/)
[Profiling zsh startup time](https://stevenvanbael.com/profiling-zsh-startup)
[nvm very slow to start · Issue #2724 · nvm-sh/nvm · GitHub](https://github.com/nvm-sh/nvm/issues/2724#issuecomment-1494082026)

[zsh-config/zlogin at master · htr3n/zsh-config](https://github.com/htr3n/zsh-config/blob/master/zlogin)
Compile optimizations on zlogin

[Why do I have duplicates in my zsh history? - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/599641/why-do-i-have-duplicates-in-my-zsh-history)

[Customization · ohmyzsh/ohmyzsh Wiki · GitHub](https://github.com/ohmyzsh/ohmyzsh/wiki/Customization#partially-overriding-an-existing-plugin)

[ZSH lazy loading #zsh #lazy · GitHub](https://gist.github.com/smac89/4b85bd3f9fb902439c0e67e36272832e)
[Speeding up initial zsh startup with lazy loading | Frederic Hemberger](https://frederic-hemberger.de/notes/speeding-up-initial-zsh-startup-with-lazy-loading/)
[zsh lazy loading | Peter Lyons](https://peterlyons.com/problog/2018/01/zsh-lazy-loading/)

[zsh - How to lazy load init code of programs in bash? - Super User](https://superuser.com/questions/1663173/how-to-lazy-load-init-code-of-programs-in-bash)

[Bug: Slow startup/init in ZSH · Issue #977 · sdkman](https://github.com/sdkman/sdkman-cli/issues/977)
