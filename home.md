# Home

[O que compraram e acham que é algo obrigatório na vossa casa e que aconselhavam a quem se está a mudar? : portugal](https://www.reddit.com/r/portugal/comments/qicm18/o_que_compraram_e_acham_que_%C3%A9_algo_obrigat%C3%B3rio_na/)
[Quanto gastam entre agua, luz e gás(caso tenham) ? : portugal](https://www.reddit.com/r/portugal/comments/qlxx8e/quanto_gastam_entre_agua_luz_e_g%C3%A1scaso_tenham/)
[ERSE - Preços de energia](https://www.erse.pt/simuladores/precos-de-energia/)

---
[Que fazer depois da Escritura?](https://www.atrindade.pt/depois-da-escritura)
[Check list: 12 assuntos a tratar depois de mudar de casa – O Jornal Económico](https://jornaleconomico.sapo.pt/noticias/check-list-12-assuntos-a-tratar-depois-de-mudar-de-casa-533250)

https://www.thekitchn.com/kitchen-organizing-ideas-264906
https://www.etsy.com/listing/833436531/wood-hanging-rope-wall-shelves-made-in?ga_order=most_relevant&ga_search_type=all&ga_view_type=gallery&ga_search_query=plant+swing&ref=sr_gallery-1-2

https://hip2save.com/tips/pantry-organization-ideas/?mc_cid=462c99a7ea&mc_eid=b4cc2cb814
https://www.tecnoporta.pt/
https://www.fichet.pt/

[Affordable IKEA Products That Make Your Home Look Expensive - YouTube](https://www.youtube.com/watch?v=8n-d_JfhgS0)
* IKEA Pots / Vases
* Trays to make more items look like one
* Carpet
* Table lamps
* 365 Containers
* Cushions
  - FJADRAR
  - Covers:
    * HEDSAV for texture
    * Plommonros
    * Majbraken
    * Sanela - High End looking sheen
    * More high end: Kustfly | Tofto
    * Bolder patterns are more recognizable so potentially try to avoid IKEA for them
* Langfjall desk chair

[How to Wash the Dishes - Peter Miller Book Excerpt](https://food52.com/blog/25102-peter-miller-how-to-wash-the-dishes-book-review)
1. Think of dishwashing as an opportunity for review: Do you need better pasta bowls? Was the broccoli poorly received? You will literally see how the meal fared and take note for future meals.
2. A bowl filled with hot soapy water is your best piece of equipment: Use it to soak small items and silverware before they’re rinsed off, or to repeatedly dip your sponge into (and save both soap and water in the bargain!).
3. Always, always rinse: Rinsing allows you to prioritize dishes, pots and pans. Also, un-rinsed plates take double the labor—not good—and destroy the strength of the bowl of soapy water
4. Get the smaller pieces out of the way first; start with the glasses, the smaller plates and cups, and then tackle the larger plates and platters. When those are washed and cleared, take on the cookware
5. “That extra-large bowl, we call ours 'Big Bertha,' is always there at the most inconvenient moment. Give it your full attention and get it dispatched,”
6. natural sea sponge: It's better for the environment, but remember to rinse it out with cold water and wring it out every time, or it will smell sour
7. Don’t put “key tools” like your spatula in the dishwasher
8. Dishcloths are not the same as dish towels
9. Consider having two piles of towels: the hardworking ones, distinguished by their war wounds
10. “A dish rack takes too much space.”: If you have a dishwasher, use it as a drying rack to dry dishes for about 20 seconds. Then take 'em out and wipe 'em down. ... singles (peelers and whisks). Singles must be hand-washed and accounted for, one by one.
11. Give thanks

## Home Office

https://effectivehomeoffice.com/
[Setup Zoom for Effective Screen Sharing | Effective Home Office](https://effectivehomeoffice.com/setup-zoom-for-effective-screen-sharing/)
[Setup Zoom as Virtual Team Room | Effective Home Office](https://effectivehomeoffice.com/setup-zoom-as-virtual-team-room/)
[Fast git handover with mob | Tool for smooth git handover.](https://mob.sh/)

[Upgraded my Alex drawer mod to include airflow, fan control and easy access to components : pcmods](https://www.reddit.com/r/pcmods/comments/rm6mfq/upgraded_my_alex_drawer_mod_to_include_airflow/)

https://theultralinx.com/2019/07/5-perfect-workspaces-for-your-inspiration-9/?epik=dj0yJnU9NmxOVnJzZmhZallzS1A1NnVPU1plRHhuZmdJYjI3TU0mcD0wJm49TDI4eGJwdklRSmVtU0dIRmlIY2hUdyZ0PUFBQUFBR094OHlR

## To Buy

- Candles
- [17 Stories Landrienne 62.2'' H x 23.6'' W Steel Standard Bookcase & Reviews | Wayfair](https://www.wayfair.com/furniture/pdp/17-stories-5-tier-landrienne-tall-bookcase-shelf-storage-organizer-modern-book-shelf-for-bedroom-living-room-and-home-office-w005114681.html?piid=1199122809)
- [Trent Austin Design® Augustus 25.62'' H x 60'' W Etagere Bookcase & Reviews | Wayfair](https://www.wayfair.com/furniture/pdp/trent-austin-design-augustus-2562-h-x-60-w-etagere-bookcase-w005629755.html?piid=1959091799)
- [Painel Perfurado](https://www.leroymerlin.pt/pesquisa/Painel%20perfurado)
- [FJÄLLBO Estante, preto, 100x136 cm - IKEA](https://www.ikea.com/pt/pt/p/fjaellbo-estante-preto-70339291/)
- Vinil Autocolante
  * [Autocolante Skin para PS5 Pele preta ps5 - TenStickers](https://www.tenstickers.pt/autocolantes-decorativos/vinil-skin-para-ps5-pele-preta-ps5-A27874)
  * [Vinil originais Recortes de Anúncios Vintage - TenStickers](https://www.tenstickers.pt/autocolantes-decorativos/vinil-originais-recortes-de-anuncios-vintage-A17417)
  * [Vinil autocolante para frigoríficos Canecas de cerveja - TenStickers](https://www.tenstickers.pt/autocolantes-decorativos/autocolante-para-frigorificos-canecas-de-cerveja-A18616)
- plasti dip - https://www.pintadip.pt/pt/tinta-dip-spray/26-spray-dip-preto-matte.html
- TV - https://www.avforums.com/threads/what%E2%80%99s-the-difference-lg-cx5-cx6.2329478/
- [ALEX Bloco de gavetas, preto-castanho, 36x70 cm - IKEA](https://www.ikea.com/pt/pt/p/alex-bloco-de-gavetas-preto-castanho-60473548/)
- [HULTARP Escorredor p/loiça, preto - IKEA](https://www.ikea.com/pt/pt/p/hultarp-escorredor-p-loica-preto-60448766/)

## Fridge

https://blog.kuantokusta.pt/como-escolher-um-frigorifico/

## Standing Desk

https://www.one-tab.com/page/Zp79VafvR2SiSS55q7Nm6Q

[Please stop over paying for your desks! : StandingDesk](https://www.reddit.com/r/StandingDesk/comments/izw3m5/please_stop_over_paying_for_your_desks/)
[Hiya - height adjustable 140x80 » Haworth Store EU & ASIA](https://shopping.haworth.com/product/hiya-height-adjustable-140x80/)
[StandDesk: Build Your Own Electric Sit-Stand Desk for Less](https://standdesk.co/products/build-your-own-standing-desk)
[RODULF Estrut inf sentado/de pé p/tampo, branco, 140x80 cm - IKEA](https://www.ikea.com/pt/pt/p/rodulf-estrut-inf-sentado-de-pe-p-tampo-branco-60464290/)
[BEKANT Underframe sit/stand f table tp, el, black, 160x80 cm - IKEA](https://www.ikea.com/pt/en/p/bekant-underframe-sit-stand-f-table-tp-el-black-50255254/)
[Sierra Standing Desk Pro - Height Adjustable Standing Desk](https://www.fitnest.eu/product/fitness-sierra-electric-standing-desk-pro-model/)
[SmartDesk Pro | The Office Standing Desk with More Power and Range](https://www.autonomous.ai/en-PT/standing-desks/smartdesk-2-business?option1=3&option2=5&option16=37&option17=41)
[Jarvis Bamboo Standing Desk - The #1 Rated Desk - Fully | Fully EU](https://www.fully.com/en-eu/jarvis-adjustable-height-desk-bamboo.html)
[UPLIFT V2 & V2-Commercial Standing Desk #1 Desk | UPLIFT Desk](https://www.upliftdesk.com/uplift-v2-standing-desk-v2-or-v2-commercial/)

[tampos secretaria - Pesquisar - IKEA](https://www.ikea.com/pt/pt/search/products/?q=tampos%20secretaria)

## Soundbar

[Best soundbars 2021: the best TV speakers you can buy | What Hi-Fi?](https://www.whathifi.com/best-buys/home-cinema/best-soundbars)
[Best soundbar for price? : Soundbars](https://www.reddit.com/r/Soundbars/comments/f95z18/best_soundbar_for_price/)
[Best sound bar $150-200 for 55 inch tv : Soundbars](https://www.reddit.com/r/Soundbars/comments/oot4u8/best_sound_bar_150200_for_55_inch_tv/)
[Best soundbar under $500 ? : Soundbars](https://www.reddit.com/r/Soundbars/comments/jc9xux/best_soundbar_under_500/)

## Lightboard

[How to Build your Own LIGHTBOARD / Light board DIY Fast and Easy | Elisa Valkyria - YouTube](https://www.youtube.com/watch?v=L1au1JxMSaA)
[How to Write on Plexiglass | eHow](https://www.ehow.com/how_8715686_write-plexiglass.html)
[LPT: Use Plexiglass to make a cheap, color-matching, dry erase board. : LifeProTips](https://www.reddit.com/r/LifeProTips/comments/ta0l0/lpt_use_plexiglass_to_make_a_cheap_colormatching/)
[Cavalete Com Rodas - OLX Portugal](https://www.olx.pt/ads/q-cavalete-com-rodas/)
[Metacrilato transparente 3 mm, tamanhos diferentes (100x100, 100x70, 100x50, 100x30, etc), folha de metacrilato translúcido, placa de acrílico transparente : Amazon.es: Bricolage e Ferramentas](https://www.amazon.es/-/pt/dp/B0895ZN4QT/ref=sr_1_1?dchild=1&keywords=plexiglass&qid=1634376775&sr=8-1&th=1)
[Amazon.es : neon marcador](https://www.amazon.es/s?k=neon+marcador&ref=nb_sb_noss_2)

## Misc

https://home.by.me/en/ - Design

https://www.lifeandmuse.com/the-home-office-reveal

[Cozinha com "janela" para sala](https://www.idealista.pt/imovel/30982934/foto/6/)
[T4 grande relativamente barato, casa do século passado](https://www.idealista.pt/imovel/31035828/)

[Comprar ou arrendar casa: o que compensa mais atualmente](https://www.doutorfinancas.pt/vida-e-familia/habitacao/comprar-ou-arrendar-casa-o-que-compensa-mais-atualmente/)
**Comprar:**
* Entrada entre 10/20%
---
* Abertura de processo (varia consoante entidade bancária) - e.g.: 550€
* Imposto de selo - 0.8% - e.g.: 1.200€
* Escritura (variavel consoante o valor do imóvel) - e.g.: 751€
* Imposto Municipal de Imóveis (variável consoante o valor do imóvel) - e.g.: 1.860€
Total: 4.361€
**Custos fixos após compra:**
* Prestação: 370€ / mês
* Seguro de vida para casal: 100€ / mês
* Condomínio: 45€ / mês
* Seguro Multirriscos: 30€ / ano
* IMI: 400€/ ano
TOTAL: 6.610€/ ano, cerca de 550€/ mês.
**Arrendar:**
Para arrendar um T1 (de 50m2) em Lisboa, tem de estar pronto para pagar, no mínimo, 800 € mensais.
Ao fim de um ano, terá pago 9.600€, aos quais poderá deduzir 502€ (valor máximo de dedução em IRS do valor das rendas - calculado a 15%).
Contas feitas: no arrendamento deste T1 na capital gastaria 9.098€ anuais, que por mês significa um gasto de 758€.

[Vai comprar casa? Conheça todos os custos, papéis e impostos!](https://www.doutorfinancas.pt/creditos/credito-habitacao/custos-comprar-casa/)

Dish Drying Rack:
https://www.amazon.com/Bamboo-Tier-Dish-Drying-Rack/dp/B07WQY7799/ref=sr_1_35?dchild=1&keywords=Dish+Drying+Cabinet&qid=1613353425&sr=8-35
https://www.amazon.com/Surpahs-Multipurpose-Roll-Up-Drying-Large/dp/B00P8KHRIU/ref=sr_1_22?dchild=1&keywords=Dish+Drying+Cabinet&qid=1613353425&sr=8-22

---

[Biggest Lighting Mistakes and How to Fix Them](https://www.youtube.com/watch?v=M1vX-q69zH4)
1. Selecting the wrong color temperature for your space
   You most likely should use 2600-3000 kelvin light bulbs in bedroom / living room / kitchen.
   Hallway and Bathroom can use brighter lights.
2. Relying on one light bulb to light an entire room
3. Sticking with builder grade lighting fixtures
4. Blinding your guests
5. Not utilizing dimmers in your home


## Buying

- [Como obter a Certidão Permanente online? | ComparaJá.pt](https://www.comparaja.pt/blog/certidao-permanente-online)
- https://www.predialonline.pt/PredialOnline/docs/Pedido_Certidao_Online.pdf
  > Nas  seguintes  imagens  mostra-se  como  identificar/recolher  os  elementos  do  imóvel  em  cada  um
dos documentos
  Canto superior direito: (Nº de Ficha / Prédio)/(data de registo do prédio) - (Fração Autónoma)
- https://www.sobredinheiro.info/imt-conheca-melhor-o-imposto-que-incide-sobre-imoveis-29/
  > 2. O conceito de “sujeito passivo” (quem está sujeito ao pagamento deste imposto):
- https://forumdacasa.com/discussion/65278/guias-imt-e-imposto-selo/#Comment_1563339

[French Oak Parquet](https://www.madeofwood.uk/flooring/v72n-solid-oak-426x70x22)


## Homes

[Moradia em banda à venda na rua Alberto de Noronha, 23, Azambuja — idealista](https://www.idealista.pt/imovel/33554801/)
[Casa ou moradia à venda em Vila Chã de Ourique, Cartaxo — idealista](https://www.idealista.pt/imovel/33530374/)
[Moradia independente à venda , N365-2, 15, Cartaxo e Vale da Pinta, Cartaxo — idealista](https://www.idealista.pt/imovel/33527009/)
[Moradia independente à venda na rua Vasco Ribeiro, Vila Chã de Ourique, Cartaxo — idealista](https://www.idealista.pt/imovel/33199374/foto/32/)
