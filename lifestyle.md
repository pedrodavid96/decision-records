# Lifestyle

[Reducing stress levels](https://www.youtube.com/shorts/Nhm_NCIh8gA)
* Getting enough sleep is probably the best thing
* Secondly is just "remembering to breath deeply" - often when you're stressed, you hold your breath or start breathing in a shallow manner - each hour, when you check on "how am I feeling emotionally", just check your current breathing and do 10 deep breaths for the sake of it anyway

[The Perfectionism Trap, and How to Get Out of It](https://www.youtube.com/watch?v=UYBWMkH4X6I)
Accept yourself, even in failure.
Progress is great, but you need to be fine without progress as well.

[Ali Abdaal Website](https://aliabdaal.com/)
Doctor working in Cambridge / Youtuber

[Tiny Changes with Big Results // Ground Up 083](https://www.youtube.com/watch?v=eK1fXca3-qQ) - author of "Atomic Habits"

[Soft Skills](https://neilonsoftware.com/soft-skills/top-20/)

[Willpower is for Losers](https://www.youtube.com/watch?v=k2Wcu6aGyz8)
Great video 🥇⭐
- Shows studies with proof that willpower is finite

- People using more willpower throughout the day (e.g.: working on a job that they don't like)
  will be more prone to not be able to deny "bad-practices" at the end of the day

- People with healthier habits managed to mostly make it a "non-decision", meaning
  they don't need to "waste willpower" on being healthy

- The more desires a person resits, the more likely the person is to give in to future temptations

- Those exerting more self-control were not more successfull in achieving their goals
  Is the people who plan their lives to use less self that were more successfull.
  e.g.: instead of trying to resist cookies, simply don't buy them

- In practice, healthier people reported less decisions made throughout the day than unhealthy people

- Besides, decisions are usually distractions as well.
  A study showed that when you're trying to focus for a task, the temptation for a single notification
  on your phone might reduce your IQ in 10 points.

- Reducing choices and decisions

---

[11 Common Health Problems of IT Professionals & Prevention Tips](https://codeandhack.com/health-problems-of-it-professionals-prevention-tips/)
1. Carpal Tunnel Syndrome
2. Anxiety, stress, and depression
3. Thrombosis
4. Heart disease
5. Computer vision syndrome
6. Insomnia
7. Lower back pain
8. Neck and eye strain
9. Obesity or overweight
10. Breathing Problems
11. Internet Addiction
[Discussion](https://www.reddit.com/r/programming/comments/gvpl0k/common_health_problems_of_it_professionals/)

## Fix Posture
[Athlean-X](https://www.youtube.com/watch?v=g-7ZWPCWv0U)
[Rounded Shoulders](https://www.youtube.com/watch?v=oLwTC-lAJws)
[One more](https://www.youtube.com/watch?v=l2VQ_WZ8Bto)

---

## Misc

[3 Ways to Rescue a Conversation That’s Going Nowhere](https://forge.medium.com/3-ways-to-rescue-a-conversation-thats-going-nowhere-1e0d954ae371)
* Keep your questions open-ended, but also narrow them so the other person doesn't have an overwhelming free reign
* Try reversals: "Then what?", "Really? Tell me more"... inside buffering phrases so it doesn't have an harsh impact

[How to Stay Fit Physically and Mentally and Keep Coding ](https://dev.to/ilonacodes/how-to-stay-fit-physically-and-mentally-and-keep-coding-5a4p)

[6 Reasons You Wake Up Tired After A Full Night Of Sleep](https://thesleepdoctor.com/2018/07/29/6-reasons-you-wake-up-tired-after-a-full-night-of-sleep/?cn-reloaded=1)

[How to retain more from the books you read |Darius Foroux](https://medium.com/swlh/how-to-retain-more-from-the-books-you-read-7eef814db987)

[11 Tips to become an Efficient, High-Performance Software Developer](https://blog.veamly.com/efficient-software-developer/)

[How to Stop Wasting Time - 5 Useful Time Management Tips](https://www.youtube.com/watch?v=xwsLuxlbY2w)

[How I Doubled My Income By Buying Back My Time](https://medium.com/swlh/how-i-doubled-my-income-by-buying-back-my-time-f743cb994b6f)
Interesting insights into cost savings (specially in terms of time)

## Journal

:goldstar: [Replace Your To-Do List With Interstitial Journaling To Increase Productivity](https://medium.com/better-humans/replace-your-to-do-list-with-interstitial-journaling-to-increase-productivity-4e43109d15ef)
Journal + Pomodoro

[How To Write In Your Journal To Improve Yourself and Achieve Your Goals](https://medium.com/better-humans/how-to-write-in-your-journal-to-improve-yourself-and-achieve-your-goals-7a8171aabad3)
> Your Morning Journaling Ritual to Get Yourself Into a Peak State, Daily

### Bullet Journal

[My 2019 Bullet Journal Setup | AmandaRachLee](https://www.youtube.com/watch?v=aSX0G7fzkBE)

[Bullet Journal Key Ideas](https://sheenaofthejournal.com/productivity/bullet-journal-key-ideas/)

![Bullet Journal Key](./bullet_journal_key.jfif)

[How to Craft a Better To-Do List](https://bulletjournal.com/blogs/bulletjournalist/how-to-craft-a-better-to-do-list)

[Easy bullet journal guide for beginners](https://diaryofajournalplanner.com/bullet-journal-guide/)
>>>
- What is bullet journal?
- Supplies to get started
- Bullet journal key
- Bullet journal index
- Bullet journal future log
- Bullet journal monthly spreads
- Bullet journal weekly spreads
- Bullet journal daily spreads
- Custom bullet journal collections
- The creative side of bullet journaling.

[How to Make Towels Soft Again – Miracle Brand](https://www.miraclebrand.co/blogs/news/how-to-make-towels-soft-again)
Cut back on the amount of detergent you use.
Do you have a hard water system?
Use a liquid detergent rather than a powered one. Powdered detergents attach easily to the minerals in the water, making the detergent less available to clean the actual laundry (your towels).
Add ½ cup of Borax to help soften the water in the load. The Borax will counteract the calcium that absorbs detergent.
**Wash towels, and only towels, together.**
**Don't overload each wash.**
**Avoid using liquid fabric softener.**
  Liquid fabric softeners contain oils and other additives that can create a coating over the fabric of your towels. This filmy coating may also make you feel like washing the towels more often - your towel will become worn-down more quickly.
  An alternative to fabric softeners? Vinegar.
How to make towels soft again?
  Add baking soda
  Use white vinegar instead of fabric softener.
    Bonus - white vinegar also helps combat bacterial growth.

## Productivity

[How to Become a Productivity Master](https://www.youtube.com/watch?v=L8aRN2HgW7U)

### [Full Focus Planner by Michael Hyatt - how to plan your days & weeks to achieve your biggest goals](https://www.youtube.com/watch?v=MGP-gwa5FvA)

### [TODO lists on steroids, ABCDE Method](https://medium.com/personal-growth-lab/forget-a-to-do-list-use-the-abcde-method-instead-5a3a329ff448)

Dont' really think this is required to be "on paper", but the concepts are important

[How to Study with INTENSE Focus - 7 Essential Tips](https://www.youtube.com/watch?v=hP5TNI_2VRs)

## Morning Routines

[Seven things to do every Morning](https://www.youtube.com/watch?v=qAOf9ekaF-c)

>>>
1. Stop snoozing! Get up when the alarm first goes off.
2. Weigh yourself — it’s Alpha’s way of keeping himself honest
3. Make a glass of warm lemon water — use 1/2 lemon with large cup of water to aid digestion, boost your immune system, aid in weight loss, and detoxify your liver
4. Prioritize your day while the coffee is brewing — review the list at the end of the day to shift items over to the next day
5. Get outside first thing in the morning to feel more alert and boost your mood
6. Exercise in the morning — simply MOVE, whether jogging, calisthenics, yoga, or working out
7. Drink coffee — Alpha LOVES his coffee

## Sleeping

[WHY wake up at 4am?](https://www.youtube.com/watch?v=2lkSngu9nSM)
Awesome video from Lefie (Swedish girl - illustrator / minimalist)
1 - if you're not as physically active as you'd like to be
2 - if you find yourself consuming rather than creating
3 - if you work from home
4 - if you live alone
5 - if you absolutely hate mornings

[Stop worrying about sleeping!](https://medium.com/darius-foroux/the-only-sleep-hack-that-actually-works-78e53020d27d)
Stuff he did and didn't notice any improvements (but seem actually useful):

>>>
* “Use a sleep app”
* Read a book
* No screens before bedtime
* Crack a window
* Drink herbal tea
* Sleep with a notepad
* Have a bath
* Change your sheets

Lessons:

>>>
* STOP WORRYING ABOUT SLEEP! The truth is that no one ever died after one bad night of sleep.
* Next time you go to bed late because of a party, or you were working on your dream — tell yourself you’ll feel good in the morning.

### [What I Learned from Six Months of Obsessive Sleep Hacking](https://medium.com/better-humans/what-i-learned-from-six-months-of-obsessive-sleep-hacking-2128b76f042a)
>>>
To increase your total sleep:
- Don’t eat rich or sugary foods—or anything at all, to be honest—a few hours before bedtime.
- Take a break from screens before bedtime, and try to do something dull and less mentally stimulating.
- Exercise a few times per week—but not too close to bedtime.
- Try sleep aids (like nasal strips and eye masks) if breathing or sensory overload is an issue for you.

To increase restorative sleep:
- Don’t drink caffeine after lunchtime.
- Don’t have more than one alcoholic drink per night.
- Consider taking ZMA for a few weeks at a time.
- Have more sex!

[The Neuroscience Behind Fixing Your Sleep Schedule](https://youtu.be/cyKEfejsVps?si=_0LvcfVjtSBeuG76)
* Biggest mistake - Trying to fix things regarding going to bed (at night)
  You have to fix your waking hours
* Missing the window for sleep - You're fatigued enough to go to bed but not fatigued enough to restrain from other activities (e.g.: phone).
  If you go past that window you won't have the will power reserve to retrain your impulses and procrastinate
  going to bed
* 2 types of procrastination
  - Going to bed all-together 30-75 minutes - always existed
  - In-bed procrastination (another 30-75 minutes) - getting worse with the advent of technology
* Emotional flooding
  -  Technology (generally, more specifically social media) generally suppress negative emotion circuitry.
     This is not an elimination, it's a suppression.
     The moment you put your devices away, there's an "experience" of emotional flooding,
     resulting in anxiety, depression, frustration, etc...
  - This was usually deal with throughout the day and processed during idle time (cleaning out the trash)
  - Currently there's so much sensory stimulus that we don't have time to process our minor emotionol things
  - Give yourself time throughout the day to be bored and let your mind take out the trash
    1 hour walk, with no music, no podcast, etc...
    Meditation / Journaling is an option but you just need to give your mind space
* How you spend your waking hours
  - There's a corthisol signal when your brain "knows" there's something that needs to be done and
    you've been prochrastinating it to the last moment (e.g.: studying for a test)
  - Remove corthisol signal
    Taking "meaningless action" is usually sufficient to diminish it.
    e.g.: Taking the trash out.
    While this seems silly it can be seen as similar to brining food to a funeral.
    Even though it's not necessary the act of being able to do something (even though it's basically meaningless) makes you feel better.
* Anabolism
  - Working out - specially weight lifting asks for muscle growth which requires sleep
                  body sends signals accordingly
  - Learning new stuff - similar to above
* Diet
  If you're on a calloric / nutrient defficit, the body will want to replinish them instead of sleeping
  2/3 meals a day
  Before going to bed the dinner should be quite heavy, can have a fair load of carbs (with protein and fibers as well)
  Stimulates insulin which makes us sleepy - induce food coma
  Highly processed meal would result in a big insulin spike but followed by a blood sugar crash (which makes you hungry again) if you don't have enough protein and fiber.
  They slow down the release of carbs to our system since the body has to digest all the other stuff.
  It slowly keeps our insulin levels high which keeps us sleepy for a sustained period of time.
  Big bowl of pasta with a salad and chicken.
* Stress
  Used to be more focused - chortisol
  Currently there's a raise on chronic stress (grades, relationships, morgages, etc...)
  Everyone thinks everything starts with the sleep schedule.
  Sleep schedule is actually the "final thing", you can't tackle "sleep first" because all of these signals should be worked before, then sleep will come more easily.
* Final tip - waking up when you're tired is easier neuroscientifically than going to bed when you're not.
  With that in mind:
  - There's a chortisol peak in the morning
  - Waking up around 5:30 in the morning
  - If you need caffeine, caffeinate yourself
  - Get to work right away (until ~9)
    Your brain will be so trilled you got everything you had to do by that time
  - Have a solid breakfast around 9
  - When you're feeling tired you don't go to sleep - don't stay past your inhibition window
  - Ideally avoid caffeine past 10am
  - Observe dawn and observe dusk
* Conclusion
  Fixing your sleep is about understanding why do we sleep? (what is the function of sleep) and leaning into those biological signals.
  You must regulate your technology but this becomes way easier when you do everything else.

## Weight lost

[7 Genius Weight Loss Hacks I Wish I Had Followed Earlier In Life | by Karthik Rajan | Aha Moments | Apr, 2021 | Medium](https://medium.com/aha-moments/7-genius-weight-loss-hacks-i-wish-i-had-followed-earlier-in-life-b22ad2f3efe2)
1. COVID stay at home miracle — food experiment of epic proportions.
2. Limit credit card foods
3. Feel good — inside out. Limit gassy food. If you must, remember this story
4. Every zoom call at work is different. Switch some to mobile phone and walk around during the call
5. Fuel for faster metabolism : The ice water trick worked for me.
6. Close kitchen at 4 PM — another COVID induced lifestyle miracle
7. Eat what you deem absolute best food often. Nobody died eating their food.

### [7 Habits That Can Help You Lose Weight](https://blog.myfitnesspal.com/7-habits-that-can-help-you-lose-weight/?otm_medium=onespot&otm_source=inbox&otm_campaign=MFP&otm_content=MFP_Intl_NL_UI_Weekly_20190916&otm_click_id=20d32536233fbb64b0873dcad386778d&utm_campaign=MFP_Intl_NL_UI_Weekly_20190916&utm_source=international&utm_medium=email)
>>>
1. Prioritize protein at breakfast
2. Incorporate high-intensity workouts
3. Keep an eating schedule
4. Drink more water
5. Walk for 30 minutes every day
6. Add seeds to your diet
7. Reorganize your plate

### [Business and Productivity Gurus Stack Habits to Stay Fit](https://medium.com/better-humans/business-and-productivity-gurus-stack-habits-to-stay-fit-367751e15b88)
>>>
- Minimize the Number of Decisions You Have To Make
- Focus on a Few Big Wins
- Keep Your Kitchen Clean
- Buy Perishable Ingredients to Force Yourself to Cook (use-it-or-lose-it)
- Meditate Every Day
- Pick the Right Gym for You
- Work Out When Your Energy Level Is Highest
- Engineer Your Environment to Make Healthy Decisions Automatic
- No Matter How Busy You Get, Protect Your Sleep Time
- Figure Out What It Would Take to Guarantee Success, and Do That
- Busier People Than You Have Done It, and You Can Too

[I counted every calorie for 30 days](https://www.youtube.com/watch?v=F2btwUXNXh0)

## Self Improvement

### [How to Live ON PURPOSE and Maximize Every Freaking Day](https://medium.com/better-humans/the-secret-to-making-the-most-of-today-953e472b5b25)
>>>
1. Have a weekly reflection and planning session
2. At the end of each day, make your game plan for tomorrow
3. Focus on today, not tomorrow
4. Three month energy cycles
5. Organize yourself
6. Always choose the harder right or “higher road”

### [The power of doing nothing at all](https://medium.com/swlh/the-power-of-doing-nothing-at-all-73eeea488b8b)
Doing what matters vs. busy-bragging

### [5 Signs You Are Wasting Your Life](https://medium.com/the-post-grad-survival-guide/5-signs-you-are-wasting-your-life-44e474935a53)
>>>
1. You don’t get out of bed quickly upon waking.
2. You spend more than an hour or two a day on aimless activities.
3. You don’t feel ready for bed at the end of each day
4. You spend more of your time planning than doing
5. You worry what others think of you

#### [The 5 Things You Must Sacrifice To Have a Better Future](https://medium.com/the-post-grad-survival-guide/the-5-things-you-must-sacrifice-to-have-a-better-future-4f5a6e960222)
>>>
1. Other People’s Definition of Success
2. Looking Good
3. Busyness
4. Entertainment and Distraction
5. “Kinda-Good” Opportunities

#### [The Top 10 Skills People Want to Learn in 2019 and How to Learn Them](https://medium.com/skilluped/the-top-10-skills-people-want-to-learn-in-2019-and-how-to-learn-them-a37ce262bcd2)
>>>
#10. Drawing
#9. Being More Productive
#8. Playing the guitar
#7. Photography
#6. Meditation
#5. Spanish
#4. Programming
#3. Public Speaking
#2. Learning How to Learn
#1. Writing

[8 Things You Can Learn in 10 Minutes That Will 10x Your Productivity by Thomas Oppong](https://medium.com/kaizen-habits/habits-you-can-learn-in-10-minutes-that-will-make-you-more-productive-for-the-rest-of-your-life-b428015becfb)
1. How to prioritise
2. How to get everything out of your head
3. How to separate urgent from important tasks
4. How to focus on one thing at a time
5. How to live the 80/20 life - roughly 80% of the effects come from 20% of the causes.
To do more in less time, track the time you spend on tasks each hour of each day for a week.
When you force yourself to focus on essential tasks that have a large Return on Investment (ROI)
Pick the 20% of your tasks that yield 80% of the results and outsource or simply discontinue the rest.
!! Choose three Most Important Tasks for each day, and focus completely on gettting them done within a specific time.Any more than that and you might not get them all done. !!
6. How to own and defend your time
7. How to stop being a perfectionist
8. How to measure your inputs and results!

[5 Things Highly Productive People Do Every Sunday That Most Others Don’t](https://medium.com/personal-growth-lab/5-things-highly-productive-people-do-every-sunday-that-most-others-dont-9bf39d175f65)
1. Setting Goals
2. Planning The Upcoming Week
3. Reviewing The Previous Week
4. Journaling
5. Reading

[An Extreme Guide to Simplifying Your Life so You Can Stress Less](https://medium.com/swlh/an-extreme-guide-to-simplifying-your-life-so-you-can-stress-less-ede68cb8cf4c)
1. Make a list of the only requests you will say YES to
2. Introduce automation so you make less decisions
3. Throw away most of your podcast list
4. Book in time for your brain to synthesize new ideas
5. Do a huge cleanout at home
6. Start a clean desk policy
7. Commit to a single social media platform
8. Drastically change your email game
9. Re-prioritise the areas you spend money
10. Be more selfish
11. Disappoint people (say no)
12. Allow white space into your calendar

[9 Habits to Increase Your Energy](https://medium.com/swlh/9-habits-to-increase-your-energy-69d65265beaa)
1. Go to Sleep Early
2. Exercise Every Day
3. Twenty-Minute Naps
4. Do Your Hard Work in the Morning
5. Set Your Intention the Day Before
6. Sell Yourself on Your Goals
7. Get Better Friends
8. Read Better Books
9. Align Your Life

[84 Productivity Killers and How to Avoid Them by Danny Forest](https://medium.com/better-marketing/84-productivity-killers-and-how-to-avoid-them-804d9416fdc5)
Using a Computer
1. Using menus when you can use keyboard shortcuts
2. Not setting your mouse at max speed (:notsureif:, do you even CS bro?)
3. Not watching instructional videos at 2x
4. Searching for files and apps with your eyes instead of the search function
5. Typing with 2 fingers
6. Typing while looking at your keyboard
7. Not taking advantage of auto-suggestions
8. Working for software instead of making it work for you
9. A slow internet connection
10. Buying a subpar computer to save money
11. Waiting on a loading screen
12. Typing on your cellphone when you can type on your computer

At Work
1. Spending too much time at the water cooler or coffee machine
2. Going to meetings that have nothing to do with you
3. Going to meetings that don’t have an agenda
4. Not standing up and stretching your legs
5. Working on your hardest problems when you’re sleepy
6. Working for a company that frowns upon taking naps
7. Starting the day without knowing what you’re going to do
8. Allowing tap-on-the-shoulder interruptions
9. Not taking the time to adjust your chair and posture properly
10. Eating out every day
11. Living really far from where you work
12. Over-snacking

Going Somewhere
1. Walking slowly
2. Waiting at traffic lights when you can take an alternative route
3. Not Jay-walking
4. Not finding and taking diagonals on your path
5. Going in circles when looking for things
6. Waiting in line when you don’t need to
7. Waiting 10 minutes for your Uber or cab when it takes you 10 minutes to walk it anyway
8. Waiting at the drive-through
9. Driving during rush hour
10. Commuting during rush hour
11. Waiting for the elevator when you’re just going up or down the third floor
12. Not doing something during your commute

While Shopping
1. Not using self-checkout
2. Going to the store without a plan or list
3. Going to the groceries when you’re hungry
4. Going in person when you can buy online and get it delivered to you
5. Not using contactless payment
6. Wait in the shortest line, not realizing that people in front have massive shopping carts
7. Not using express checkout lines when you can
8. Going to a further away store to save pennies
9. Going in line with people you know will count their pennies at the checkout
10. Finding the absolute closest parking spot
11. Not using the divide-and-conquer method when going with someone
12. Waiting for someone to move away from the middle of the aisle

At Home
1. Taking long showers just because it feels good
2. Placing things in illogical places
3. Having a TV in front of a couch
4. Cooking bad food
5. Snoozing
6. Not getting the kid next door to mow the lawn
7. Not getting the kid next door to shovel the snow
8. Not cooking extras for leftovers
9. Not using tools to make your cooking faster
10. Waiting next to the coffee machine while it brews
11. Cleaning too frequently
12. Taking the trash out when it’s not full (unless it smells…)

In School/Learning
1. Going to school for a piece of paper
2. Studying for tests
3. Studying by yourself
4. Not changing your studying environment
5. Not recalling what you previously learned
6. Not implementing what you learned
7. Not seeking a tutor or mentor when you don’t understand
8. Taking elective classes because they are easy
9. Doing a master’s when you have no clue what you want to do in life
10. Doing what you went to school for even if you lost your “passion”
11. Going back to school because you are bored
12. Going to school because it’s what society tells you to

In Life
1. Not learning something new every day
2. Not knowing your why
3. Not revisiting your self-awareness regularly
4. Doing something efficiently that shouldn’t be done at all
5. Not acting on things you read
6. Not delegating things other people would do faster and better
7. Not taking a damn break
8. Not connecting with other people
9. Not listening to other people’s constructive feedback
10. Not taking care of your health
11. Only thinking about yourself
12. Reading this article and doing nothing about it


[My Productivity & Life Mega List | 29 Strategies](https://medium.com/swlh/my-productivity-life-mega-list-29-strategies-243f1b017600)
1. Try sleeping at the same time every night
2. Do the most difficult thing first
3. Replace common and recurring decisions with routines
4. Keep airplane mode turned on for the first couple hours in the morning
5. Avoid starting and stopping to eliminate your administrative overhead
6. Check external solutions to your problem before inventing a new one
7. Ask. Ask to create your own options, to uncover solutions, and to learn
8. Write down mistakes and lessons learned
9. Design your environment to serve you
10. Identify and mentally label the credible people around you
11. Be aware of your mental state when making decisions
12. Making life easier for other people almost always ensures life is easier for yourself
13. The right type of meditation makes all the difference
14. If it takes less than a few minutes to do, just do it
15. Put schedules next to your to-do list items
16. Have an overarching goal for the day
17. When reading self help books, case studies, papers and biographies, have a pen and notepad with you
18. Actively listen to the person you’re talking to
19. Spend time refining your diet
20. Thank people for their hard work and acknowledge their efforts
21. Learn to delegate and automate
22. Get the sleeping hours that work for you
23. Use the 3 second rule when you’re procrastinating
24. Split up your regular week and blow off some steam
25. Dress for the occasion
26. Split up your workload and tackle it chunk by chunk
27. Verbalise a problem that you’re struggling with
28. Bundle your social media distractions
29. Make your communications clear and concise

[I Practiced Yoga Every Day for 30 Days](https://www.youtube.com/watch?v=LWe4fV_ctn0)
A lot of explanation about different types of Yoga, good introduction

[10 Questions To Ask To Go Deep In Your Relationship](https://www.jordangrayconsulting.com/questions-to-ask-to-go-deep-in-your-relationship/)
1. Is there anything I can do for you in this moment to help you feel more comfortable or loved?
2. How can I better support you in your life?
3. Is there anything I have done in the past week that may have unknowingly hurt you?
4. When you come home from work, what can I do or say that will make you feel the most loved?
5. Is there any kind of physical touch that I can engage in more that helps you to feel loved?
6. Do you think you will need more closeness or more alone time over the next couple of days?
7. Is there any argument that we had this past week that you feel incomplete about?
8. How do you feel about our sex life lately?
9. What are the main stressors currently in your life, and is there any way I can alleviate that stress for you, if only a small amount?
10. When do you find speaking difficult and how can I best support you through those moments?

[Why Video Games are Good for You](https://www.youtube.com/watch?v=Xg2cmsnvWx8)

[I Woke Up at 5 a.m. for a Week to See if It Would Change My Life](https://medium.com/better-humans/i-woke-up-at-5-a-m-for-a-week-to-see-if-it-would-change-my-life-8205f7645320)
Very cool article.
- 5:00 — 5:10 → Get out of bed, put sweatpants on, go out on the terrace.
- 5:10 — 5:25 → Stretching + 10-minute walk around the park.
- 5:25 — 5:30 → Quick journaling.
- 5:30 — 7:15 → Full focus work, nonstop. Mostly writing, my MIT (Most Important Task) of the day.
- 7:15 — 7:45 → Coffee, breakfast.
- 7:45–8:00 → Shower, get dressed.
- 8:00–8:30 → Prepare lunchbox for my 9–5 job, finish some work on the side if I have a bit of extra- time.
- 8:30 → Leave for the office.
**Learnings:**
- The power of accountability is huge
- A routine is primordial
- The time you gain is insane
If you do everything in the morning, what do you do at night?
- My energy died down very fast in the afternoon, from 3 to 4. When you have less energy, it becomes- harder to get things done.
- I had already done so much in the morning, I actually didn’t have much less to do at the end of the day. The rest was for the day after. All of my most important tasks were done.

[Simple Burnout Triage](https://benmccormick.org/2020/08/31/simple-burnout-triage)
[Discussion](https://www.reddit.com/r/programming/comments/k8lplz/are_you_headed_towards_burnout/)

[9 Quotes by Paulo Coelho That Will Change the Way You Live Your Life | by Sinem Günel | The Ascent | Medium](https://medium.com/the-ascent/9-quotes-by-paulo-coelho-that-will-change-the-way-you-live-your-life-c38760ecc16)
> “You drown not by falling into a river, but by staying submerged in it.”

> “When you want something, all the universe conspires in helping you to achieve it.”

> “You have to take risks. We will only understand the miracle of life fully when we allow the unexpected to happen.”

> “I knew that if I failed I wouldn’t regret that, but I knew the one thing I might regret is not trying.” - Jeff Bezos

> “You can become blind by seeing each day as a similar one. Each day is a different one, each day brings a miracle of its own. It’s just a matter of paying attention to this miracle.”

> “Everyone seems to have a clear idea of how other people should lead their lives, but none about his or her own.”

> “When we love, we always strive to become better than we are. When we strive to become better than we are, everything around us becomes better too.”

> “Wherever your heart is, there you will find your treasure. You’ll never be able to escape from your heart. So it’s better to listen to what it has to say.”

> “When someone leaves, it’s because someone else is about to arrive.”

> “It’s the possibility of having a dream come true that makes life interesting.”

[12 Tiny Changes To Help You Get Your Life Together | by Kirstie Taylor | Better Advice | Jan, 2021 | Medium](https://medium.com/better-advice/12-tiny-changes-to-help-you-get-your-life-together-a82cbfd3fc84)
## Mental health:
* Write things down.
* Stop wasting time on things that don’t add to your life.
* Reflect more.
## Work:
* If you want to make moves, make plans.
* Eat the frog. - When you wake up each morning, start your day with the hardest or most annoying thing on your to-do list.
* Make schedules the night or week before.
## Physical Health:
* Get more sleep.
* Use the Pomodoro Technique. - stand up more often
* See a doctor.
## Relationships
* Talk about your feelings more.
* Learn better conflict skills.
* Listen with intent.

[Create the life you want in 2023](https://youtu.be/traWsNhxYig)
* Setting goals makes me feel more put together
* Cons:
  * Sometimes I care more about the goal than the journey to get there
  * Most people don't achieve the goals they set
  * The problem seems to be most of the times you don't have clarity on what you really want to achieve
  * Society noise
  * Goals are set on what we think we should do or what we see other people doing
  * e.g.: Having the greatest grades on dentist school, then I realised I didn't even like to be a dentist
Reflection:
* Find clarity and stillness in the noise & untagle the chaos, sit through your feelings
Reflect about last year and "What I've accomplished? What I've struggled with? What memories I've made?"
This allows to bring past experiences to new goals, and also reflect about what we want to leave behind.
**Figure out why goals were achieved / not achieved.**
Goals that were achieved:
* had strong intention and emotional connection
* Also, she made them "easy" to themself
* Reading more by joining library to reduce costs and to reduce "pain" in leaving a book behind if it was not resonating
Goals that were not achieved:
* Making more effort with friends? - What does it even mean?
* Running - I don't even enjoy running - other cardio would be better
* Drawing & painting every week was to overwhelming - not enough room for an off week
New goals: DART system
* Intention
* Connection
* Is it in your control? - e.g.: you can't control the number on the scale but you can control you calories intake
* Voids - it's ok to be happy where you are and having areas you don't want to improve
Ideal vs Minimal - e.g.: Daily yoga practice ideal vs 5 minutes of scretch and breathing exercises daily
> "Goals are good for setting a direction, but systems are best for making progress" - James Clear
E.G:
Goal        | Routine | Make it fun       | Make it easy
Write daily | morning | writer's playlist | writing prompts

[Why ‘Don’t Care What Other People Think’ Is Bad Advice](https://youtu.be/AT8T7h2jbY8)
At the end of the day we're social beings, we're built to care a bit and not caring will
 end up as you being less social.
There are opinions that matter, some matter more than others, so, whos opinions do I value?
and how can I manage the discomfort of the opinion of those people and if those people
disapprove what I'm doing in some way.

[How to Do Laundry When You're Depressed | KC Davis | TED - YouTube](https://www.youtube.com/watch?v=kqItMybTKTo)
Mottos:
* Good enough is perfect
* Everything worth doing is worth doing half-assed
Do everything, not with the a perspective of "what does society expect of me" but instead with "what do I deserve".
Replace the inner voice of "I'm failing" with "I'm having a hard time right now"
Some of the examples:
- If it's too hard to shower today, grab baby wipes
  It might not be the normal way to do it, but you deserve to be clean
- If you're too depressed to do your dishes, put them into a ziploc bag and seal it
  It will keep the bugs away and it will be there for you when you're ready, because you deserve a sanitary environment

[Akta - Stop trying so hard in life](https://www.youtube.com/watch?v=Siov3ZYpImE)
The things I wanted came so easily to other people but not to me
Problem was that I was trying so hard
I made friends when I stopped trying
Made more money when stopped micromanaging it
...
The backwards law

Scarcity - the feeling of not having enough
First "seen" in law of attraction
Scarcity results in
• Tunnel vision - the initial focus might be good but long term you can't think outside of the box
• reduces mental bandwidth
• you think in terms of trade offs
These 3 things trap you in a "scarcity" snowball

All this doesn't mean to stop try or not even trying hard

It just means it's a balancing act where you need to try hard enough while keeping authentic

85% rule
Wu wei - trying not to try
Nature does not worry, yet everything is accomplished
Finding ease instead of pushing against friction

[Reinvent Yourself: Get Things Done with this System - YouTube](https://www.youtube.com/watch?v=WOpRipzmres)
"Getting Things Done (GTD) program"
* Capture
  Empty your brain and leave space for more important things (personal note: see building a second brain)
* Clarify
  Actual action
  * Throw it away
  * keep it for reference
  * label it as a project
  * delegate it
  * save it for later
  * next actions
  * do it (less than 2 minutes and cannot be delegated)
* Organize
  1. Subject specific files and information
  2. General info
* Reflect
  Review action list and calendar at least on a weekly basis
  Make it a ritual / self care routine
  Ponder weather your projects are aligned with your values and goals or weather something needs change in your work/life balance
* Engage
  4 criteria model:
  * context
    Certain tasks require a specific location - prioritize first
  * available time
    Select a task that you have time for
  * available energy
    Which ones match your current level of energy
  * priority
    You can use the 6 level model
This system may seem a little overwhelming but it makes sense when applying on a daily basis
Read the rest of the book - it will help consolidate this knowledge and how everything correlates

[The ROUTINE that makes me happy & effective ✔️ - YouTube](https://www.youtube.com/watch?v=h-1icJainvo)
* Time blocking
    * Specially for important tasks that are not urgent
* The planning fallacy
    * Our tendency to underestimate the time, cost, or effort required to complete a task
    * Even when done before
    * We usually plan for the best case scenario
    * Buffer of 20% to not stress out
* Zeigarnik Effect
    * Our tendency to remember uncompleted tasks > completed tasks
    * The brain is a terrible filing system - write things down (personal note: second brain)
    * That way you ran relax
* Negativity bias
    * Our tendency to be more effected by negative experiences instead of positive ones
    * Write things you’re grateful for before sleep - mindful about the actual positive experiences
    * Meditation

[I Journaled Everyday for 90 Days. Here’s What I Learned. - YouTube](https://www.youtube.com/watch?v=JHKiDe8E4hY)
Why bother with journaling in the first place?

It helps calming our monkey mind (the one going all directions and trying everything)
Cognitively evaluate life choices - actively ponder down if you’ve been doing the things that align with your goals

Journaling Digital & Physical

Physical:
Morning pages:
The artist’s way (book)
Write 3 pages by hand of whatever comes to mind - it doesn’t matter if it’s complete garbage
Unclog the brain

Digital
Day 1 App
1 Folder for nice comments
Morning pages sometimes (if I can’t bother to do it in physical)
Scan physical notes into Day 1

Benefits:
Going back to past self we can be grateful for the present moment, how everything has changed and how we’ve grown.
Processing emotions - men tend to get more out of journaling since they’re usually worse at expressing their emotions normally
Example of helpful prompts to “unblock”:
- How am I feeling right now?
- Why am I feeling this way?
- Where do I see my life 5 years from now?
- If I only had 2 hours a day to do work, what would I do?
- How am I currently feeling about <my major project in my life>?
- How am I feeling about my relationships?

It’s almost like a “Personal therapist”

Top tips for getting started:

If it’s too much of an elaborated ritual, you do it for 2 or 3 days, but eventually you stop doing it
Tip #1 - actually, journaling is something that you can do anytime, anywhere
Tip #2 - screw any type of journaling methodologies
rigidity is the ultimate killer of creativity and productivity and consistency
prompts can be helpful but just lower the bar if necessary
Tip #3 - Attach journaling to an existing habit (see Atomic Habits)
Example: Public transportation
Tip #4 - Prompts can sometimes be helpful
Sometimes if you’re stuck - write that out, e.g.: I don’t know want to write right now.
Tip #5 - Do it first thing in the morning
3 Step morning routine:
- Get up to the living room and make myself a coffee
- Bust out laptop / journal
- Check Morning Brew

[How to Find a Career You Actually Love - YouTube](https://www.youtube.com/watch?v=O3m14PVOq_g)
Book “Think Big”

#1 Task over title

You want to be a Doctor, Trader, …
This instead if you want to do the tasks that a Doctor, Trader, …, does.

#2 Visualize your me+

What’s your “ideal me”?

#3 Audit your time

Write down, not only what you do, but also how you feel about how you’re spending your time.
Action item: On your weekly routine - Screenshot your calendar and make notes on top of it

#4 Invest 13 minutes a day

Invest towards your me+.
Aiming for 90 minutes a week - meaning 13 minutes a day - since this should be feasible even in the busiest of weeks.

Career capital theory

#5 You probably don’t need to go back to the university

Note: This advice comes from an university professor
Most of the things can now be easily accessible through internet / books.
University isn’t worthless at all but it’s no longer a “requirement”

#6 Embrace U-Turns

The End of History Illusion:
Experiment of asking:
50%: How much your values will change in the next 10 years?
50%: How much your values have changed in the last 10 years?
Change does slow down as you age, but it slows much slower than you actually expect.

COVID was a big “experiment” on this, you had to embrace being wrong and constantly change your vision according to the current status.

#7 The Grid-Search Mindset

Make a Grid of your latest focus and connect the dots between the “quadrants”.

[Why Video Games Are So Hard To Put Down (And How To Learn To Moderate) - YouTube](https://www.youtube.com/watch?v=T8tq0xiOwKI)
Former Alcoholics often said “I don’t even think about it”
They actually desire to not drink

Behavioural Reinforcement

nucleus accumbens (neural interface between motivation and action).
When dopamine is released in the nucleus accumbens it reinforces a behaviour.
The subjective feeling of the reinforcement in the nucleus accumbens is actually enjoyment / pleasure.

Basic problem with videogames - they’re fun
Every time we play it we get a spurt of dopamine and the behaviour gets reinforced. (e.g.: the next morning the brain is craving dopamine and tells you to actually  play a game).

If you activate your front lobes and use or will power to actually be productive, e.g.: workout.After 20 minutes you’re exhausted and get back home.

What are you reinforcing here?
* You feel terrible because you’re exhausted
* You feel bad because you’re out of breath
* You feel ashamed because you could only last 20 minutes

Are you actually reinforcing working out?
Of course not!

There’s no reward, only punishment.Your brain will actually start avoiding this new behaviour you wanted.

Each time you kick the habit it becomes harder because you’re actually just paying the cost without seeing any benefit (personal note: your brain is not programmed for this long term goals, see “Atomic Habits”).

Then how do we reinforce the right type of behaviours?

Catch your creative impulses.
There’s times around the day when you actually get excited towards the right behaviour.
These impulses come naturally.
Capture your impulses (write it down, either in Physical or Digital).
This trains us in doing something else.
It also allows to reflect upon them.
By reflecting, we’re actually already using what we wrote, and your mind will be more inspired into writing even more.
Even in the case of exercise, which you might only “feel bad” about, think critically. Even if you did only 20 minutes, that’s a huge Win, it’s infinitely better than yesterday.

Practicing gratitude
Even as a psychiatrist, Dr. K thought this was BS before.
Actually, this is actually a huge part on cultivating the right type of desire.
“Did I do the right thing?” - off yes, it was better than staying at home “just playing games”.
You can’t let your mind punish you for making progress in life.


These 3 things - **catch your impulses, reflect upon them & practicing gratitude** - will reinforce your behaviours.

[Why your habits never stick - YouTube](https://www.youtube.com/watch?v=xWrH3chqkBE)
Brains prioritise immediate rewards
Hook habits that you desire to develop with stuff that you already like
- Only drink favourite coffee while working
- Only listen to favourite podcast when doing household shores

Most surprising finding - consistency - randomly assigned people to exercise groups, one consistent and another more flexible.
The ones that went more to the gym were the ones with flexible schedule as the others wouldn’t come at all if something happened that didn’t allow to go in the “fixed schedule” while the others still managed to go in a different day.

[The Top 5 Regrets of the Dying #shorts - YouTube](https://www.youtube.com/watch?v=1gS5v6EMaSc)
* Live a life true to myself, not the life other expected of me
* Didn't work so hard
* I wish I'd had courage to epress my feelings
* Stay in touch with my friends
* Let myself be happier

[You Don't Need To Be Perfect To Start A Relationship - YouTube](https://www.youtube.com/watch?v=AhXOE4ETKiY)
Like picking a "finished product" in the store from a checklist - relations don't work like that.

[The Parable of the Pottery Class #shorts - YouTube](https://www.youtube.com/watch?v=fDm1KLlQ4wM)
2 groups - 1st -> quality group of 1 pot for 30 days vs 2nd -> quantity group doing as many pots as possible in 30 days
Without exception, every 1 of the highest quality pots came from the quantity group.
When learning something, the most predictable path to quality is in fact quantity.
Doing something over and over again helps overcoming the fear of imperfection, if you focus on quality from the start you get
 paralyzed by perfection. It's also more fun
This story comes from the book Art & Fear by David Bales, Ted Orland


[If this is the only thing I accomplish today, will I be satisfied with my day? / Tim Ferriss on X](https://twitter.com/tferriss/status/1725914309677514999/?rw_tt_thread=True)
> Don’t ever arrive at the office or in front of your computer without a clear list of priorities.
  You’ll just read unassociated e-mail and scramble your brain for the day.
  Compile your to-do list for tomorrow no later than this evening.
  I don’t recommend using digital to-do lists, because it is possible to add an infinite number of items.
  I use a standard piece of paper folded in half three times, which fits perfectly in the pocket and limits you to noting only a few items.
  There should never be more than two mission-critical items to complete each day. Never. It just isn’t necessary if they’re actually high-impact.
  If you are stuck trying to decide between multiple items that all seem crucial, as happens to all of us, look at each in turn and ask yourself, If this is the only thing I accomplish today, will I be satisfied with my day?
  To counter the seemingly urgent, ask yourself: What will happen if I don’t do this, and is it worth putting off the important to do it?

[Change Your Life by Journalling - 10 Powerful Questions](https://youtu.be/MoGTXsusX6Q?si=HaizZT6GvUpscfqQ)
* What would I do if money was no object
* What would I want people (family, friends, ...) to say at your funeral?
  - e.g.: His content inspired and educated me, ultimately helped me change trajectory of my life
  - No one cares about your performance, they care about how you make them feel
* If I were to repeat this weeks actions for 10 years, where does it lead and is it where I want to be?
  You can change frequencies but "today" kinda varies too much
  Build ideal week
* What activities in the last 2 weeks have energised me and drained me?
* How is your wheel of life?
  Work - Growth, Money, Mission
  Health - Body (Physical), Mind (Mental), Soul (Spiritual)
  Relationships - Romance, Family, Friends
  \* Bonus - Joy
* What is your Odyssey Path?
  Current Path, Alternative Path and Radical Path
  What will life look like 5 years from now in each
* What is the goal and what is the bottleneck?
  What are we optimizing for?
* Which goal will have the greatest impact on your life?
* Do you work for your business or does your business work for you?
* If you knew you would die in 2 years, how would you spend your life?

## A little happier
A Little Happier: Stoic Principles for Modern Life 💭
Ali Abdaal August, 8, 2024

> **What are the activities that allow us the most peace, productivity, pleasure?**
  Are there things we require to do those things well? Do we need to bother quite so much with the others?

> The Productivity List
  MacBook, a backpack, two cameras, two microphones for video and podcasts, a load of USB-C cables, and a big soft light for filming stuff.
  Let’s add in a small action camera or vlog camera for the rare occasions when I feel like vlogging.
  Essentially, that's all I need for productivity.
  I'd also include a phone and, if we're being adventurous, a Kindle and an iPad.
  All of this fits into a single backpack, and the light takes up only a small part of a suitcase.

> The Pleasure List
  Audible subscription, some AirPods, Amazon book credits for buying books, a guitar, and an electronic keyboard or piano.
  A couple of board games and a space to invite friends for co-working, dinner, and games would be nice too.
  And probably a gym membership that ideally includes squash / badminton / padel courts would be lovely.
  Maybe I’d add a PlayStation and a couple of games to the mix, but beyond that, I don't need or want much more for pleasure…

> The Peace List
  A relatively free calendar, the ability to take walks in the local park while listening to an audiobook, good physical health, and good mental health.
  It also comes from doing work that I enjoy and find meaningful, preferably with friends or coworkers hanging out in person. And of course, it comes from having strong relationships.

## 5 Secret Factors For Any Successful Change
Eric Partaker <eric@ericpartaker.com> August 11

The Lippitt-Knoster Model says that for change to work well, we need five things:

1. Vision: A clear idea of where we’re going.
2. Skills: The abilities we need to work in the new way.
3. Incentives: Reasons for people to want the change.
4. Resources: The tools and time we need to complete the change.
5. Action Plan: A step-by-step guide for how to change.

• No clear vision leads to confusion.​
• Missing skills cause anxiety.​
• Lack of incentives can lead to resistance.
• Insufficient resources lead to frustration.​
• A missing action plan results in false starts.

## Quotes

> Caring about everything is a disaster.
> Caring about nothing is also a disaster.
> Nurture the small pocket of things that truly matter to you.
James Clear 3-2-1 - Jan 16, 2025
