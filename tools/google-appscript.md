# Google Apps Script

[Top 30 Useful Google Apps Script Snippets | by tanabee | Google Developer Experts | Medium](https://medium.com/google-developer-experts/top-30-useful-google-apps-script-snippets-8ad13077530f)
Contains multiple "low level snippets" that could be good abstractions (e.g.: Create file on drive, etc...)

[GitHub - contributorpw/google-apps-script-awesome-list: The usual list of links to interesting resources for Google Apps Script](https://github.com/contributorpw/google-apps-script-awesome-list)

[How to Receive Files in your Google Drive from Anyone - Digital Inspiration](https://www.labnol.org/internet/receive-files-in-google-drive/19697/)
[How to Save your Gmail to Google Drive Automatically - Digital Inspiration](https://www.labnol.org/internet/send-gmail-to-google-drive/21236/)
[Convert and Email Google Spreadsheets as PDF Files - Digital Inspiration](https://www.labnol.org/code/19869-email-google-spreadsheets-pdf)
[Creating a ‘full fat’ RSS feed for Google Gmail labels (enabling some dark social judo) – By @mhawksey](https://hawksey.info/blog/2013/05/gmail-label-to-rss-feed/)
[A Lynx-like Text Browser that Runs on Google Servers - Digital Inspiration](https://www.labnol.org/internet/google-text-browser/26553/)
[Using HTMLService with Google Apps Script - Digital Inspiration](https://www.labnol.org/code/19348-htmlservice-google-apps-script)
[Send Self-Destructing Messages with Google Docs - Digital Inspiration](https://www.labnol.org/internet/send-self-destructing-messages/26125/)
[How to Set Expiration Dates for Shared Google Drive Files - Digital Inspiration](https://www.labnol.org/internet/auto-expire-google-drive-links/27509/)
[How to Scrape Reddit with Google Scripts - Digital Inspiration](https://www.labnol.org/internet/web-scraping-reddit/28369/)


## Utils

[GitHub - glotlabs/gdrive: Google Drive CLI Client](https://github.com/glotlabs/gdrive)
