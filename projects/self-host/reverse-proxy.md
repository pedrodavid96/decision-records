# Reverse Proxy

[Best reverse proxy approach? (Cloudflare, Tailscale, NextDNS, Oracle Cloud, Caddy) : selfhosted](https://www.reddit.com/r/selfhosted/comments/14mcgz9/best_reverse_proxy_approach_cloudflare_tailscale/)
To read yet.

[How to use DNS provider modules in Caddy 2 - Wiki - Caddy Community](https://caddy.community/t/how-to-use-dns-provider-modules-in-caddy-2/8148)
[caddy-dns/cloudflare: Caddy module: dns.providers.cloudflare](https://github.com/caddy-dns/cloudflare)
[DNS challenge with Cloudflare · Issue #650 · lucaslorentz/caddy-docker-proxy](https://github.com/lucaslorentz/caddy-docker-proxy/issues/650)
[homeall/caddy-reverse-proxy-cloudflare: Docker image with Caddy server and Cloudflare plugin installed](https://github.com/homeall/caddy-reverse-proxy-cloudflare)
e.g.:
```
{
	"module": "acme",
	"challenges": {
        "dns": {
            "provider": {
                "name": "cloudflare",
                "api_token": "YOUR_CLOUDFLARE_API_TOKEN"
            }
        }
    }
}
```
I'm not sure if this effectively does the same as the setup described here but seems very likely.
Also see [Automatic TLS](#automatic-tls)
https://web.archive.org/web/20230402090448/https://wiki.kasad.com/books/kasadcom/page/dns
> DNS as proof of ownership
> A temporary DNS record is used to prove ownership of the kasad.com domain when
  obtaining TLS certificates from Let's Encrypt.
  Certbot, the program used to request new certificates, can do this automatically
  using a Cloudflare API key that has the Zone > DNS > Edit permission for the kasad.com zone.


[Caddy with Cloudflare Tunnel - Wiki - Caddy Community](https://caddy.community/t/caddy-with-cloudflare-tunnel/18569)


* With internal HTTPS:
  1. Change the “Origin Server Name” to the domain Caddy is expecting.
  2. Change the “HTTP Host Header” to the same domain name.
  3. If you are using a self-signed certificate, you also need to enable the option “No TLS Verify”


[Trying to host Caddy behind Cloudflare Tunnel on a subdomain : selfhosted](https://www.reddit.com/r/selfhosted/comments/18pd2su/trying_to_host_caddy_behind_cloudflare_tunnel_on/)
Doesn't seem possible.

How [Automatic HTTPS](https://caddyserver.com/docs/automatic-https#local-https) works in Caddy.
tl;dr: It generates valid certificates and adds them to the local certificate store.

[Single Sign On (SSO) with subdomains using Caddy v2](https://blog.sjain.dev/caddy-sso/)
[reverse_proxy | Defaults](https://caddyserver.com/docs/caddyfile/directives/reverse_proxy#defaults)
Note: To make this work with keycloak PROXY_HEADERS were set to xforwarded
[Using a reverse proxy - Keycloak](https://www.keycloak.org/server/reverseproxy#_configure_the_reverse_proxy_headers)
[Security 101: X-Forwarded-For vs. Forwarded vs PROXY | System Overlord](https://systemoverlord.com/2020/03/25/security-101-x-forwarded-for-vs-forwarded-vs-proxy.html)
[Add support for Forwarded header (RFC 7239) · Issue #3262 · caddyserver/caddy](https://github.com/caddyserver/caddy/issues/3262)
[Deprecated Proxy Option | Upgrading Guide](https://www.keycloak.org/docs/24.0.5/upgrading/index.html#deprecated-proxy-option)

## Auth

### Keycloak

[Configuring TLS](https://www.keycloak.org/server/enabletls)
[Release Notes | Proxy Option Removed](https://www.keycloak.org/docs/latest/release_notes/index.html#proxy-option-removed)
[Configuring Keycloak for production | Reverse Proxy](https://www.keycloak.org/server/configuration-production#_reverse_proxy_in_a_distributed_environment)
[Configuring the hostname (v2)](https://www.keycloak.org/server/hostname)

[Enabling Tracing](https://www.keycloak.org/server/tracing)
Via OpenTelemetry.

**Aside:**

[Automatic redirect from root to relative path](https://www.keycloak.org/docs/latest/release_notes/index.html#automatic-redirect-from-root-to-relative-path)
I think previously this wasn't working correctly and
ended up as one of the biggest reasons for me to favor
subdomains to subpaths in my whole setup
(as this was common accross multiple open source tools).

[Managing Organizations](https://www.keycloak.org/docs/26.0.5/server_admin/#_managing_organizations)
Doesn't seem relevant for "home usage" but interesting.

### oauth2-proxy

(in case the app itself doesn't support OpenID Connect, e.g.: arrs)

[Why is nobody talking about using oauth2-proxy to secure one's services? : r/selfhosted](https://www.reddit.com/r/selfhosted/comments/1g21ue4/why_is_nobody_talking_about_using_oauth2proxy_to/)
