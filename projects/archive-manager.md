[GitHub - wabarc/wayback: A self-hosted archiving service integrated with Internet Archive, archive.today, IPFS and beyond. : programming](https://www.reddit.com/r/programming/comments/12n21wq/github_wabarcwayback_a_selfhosted_archiving/)
[Wayback: Self-hosted archiving service integrated with Internet Archive | Hacker News](https://news.ycombinator.com/item?id=35586845)

[GitHub - dosyago/DiskerNet: 💾 DiskerNet - An internet on yer disk. Full text search archive from your browsing and bookmarks. Weclome! to the Diskernet: Your preferred backup solution. It's like you're still online! Disconnect with Diskernet, an internet for the post-online apocalypse. Or the airplane WiFi. Or the site goes down. Or ... You get the picture. Get DiskerNet](https://github.com/dosyago/DiskerNet)
[ArchiveBox | 🗃 Open source self-hosted web archiving. Takes URLs/browser history/bookmarks/Pocket/Pinboard/etc., saves HTML, JS, PDFs, media, and more…](https://archivebox.io/)
[Monolith – CLI tool for saving complete web pages as a single HTML file | Hacker News](https://news.ycombinator.com/item?id=39810378)
[GitHub - go-shiori/shiori: Simple bookmark manager built with Go](https://github.com/go-shiori/shiori)

[GitHub - derfenix/webarchive: Own webarchive service](https://github.com/derfenix/webarchive)

[GitHub - ndom91/briefkasten: 📮 Self hosted bookmarking app](https://github.com/ndom91/briefkasten)

[GitHub - paperless-ngx/paperless-ngx: A community-supported supercharged version of paperless: scan, index and archive all your physical documents](https://github.com/paperless-ngx/paperless-ngx)
[Chat with Paperless-ngx documents using AI : selfhosted](https://www.reddit.com/r/selfhosted/comments/18rn3gr/chat_with_paperlessngx_documents_using_ai/)
[SecureAI Tools now integrates with Paperless-ngx! - YouTube](https://www.youtube.com/watch?v=dSAZefKnINc)

[PaddlePaddle/PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR/tree/main)
[Using a Reverse Proxy with Paperless ngx · paperless-ngx/paperless-ngx Wiki](https://github.com/paperless-ngx/paperless-ngx/wiki/Using-a-Reverse-Proxy-with-Paperless-ngx#nginx)
[[Feature Request] Alternative OCR engines (Azure AI Vision, Google Cloud Vision etc.) · paperless-ngx/paperless-ngx · Discussion #5128](https://github.com/paperless-ngx/paperless-ngx/discussions/5128)
[tony-xlh/opencvjs-document-scanner: A document scanner implemented with opencv.js](https://github.com/tony-xlh/opencvjs-document-scanner)

---

[Overview of the Hypothesis System : Hypothesis](https://web.hypothes.is/help/overview-of-the-hypothesis-system/)
[Self hosting docker-compose.yml · Issue #4899 · hypothesis/h · GitHub](https://github.com/hypothesis/h/issues/4899)
[Can you share a working Hypothes.is docker compose file? : selfhosted](https://www.reddit.com/r/selfhosted/comments/18x1gja/can_you_share_a_working_hypothesis_docker_compose/)

[Rich Text Editor · Issue #1514 · hypothesis/product-backlog · GitHub](https://github.com/hypothesis/product-backlog/issues/1514)

[Using Hypothes.is : Professors](https://www.reddit.com/r/Professors/comments/vrjllj/using_hypothesis/)

[GitHub - omnivore-app/omnivore: Omnivore is a complete, open source read-it-later solution for people who like reading.](https://github.com/omnivore-app/omnivore?tab=readme-ov-file)
[GitHub - WorldBrain/Memex: Browser extension to curate, annotate, and discuss the most valuable content and ideas on the web. As individuals, teams and communities.](https://github.com/WorldBrain/Memex)
