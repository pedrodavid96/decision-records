# Arch Linux (btw)

* Silent Boot
* Terminal Sexy (http://terminal.sexy/)
* Figure out the emitted keycode for Fn+F6: https://unix.stackexchange.com/questions/125476/figure-out-the-emitted-keycode-for-fnf6


[DNS (Domain Name Resolution)](https://wiki.archlinux.org/index.php/Domain_name_resolution)
[Alternative DNS Services](https://wiki.archlinux.org/index.php/Alternative_DNS_services)

## Default partitions

```
/          # 20GiB
/boot      # 1GiB
/home      # Everything else!
```

/
> The / partition or root partition is necessary and it is the most important.
  traditionally contains the /usr directory, which can grow significantly depending upon how much software is installed.
  15–20 GiB should be sufficient for most users with modern hard disks.

/boot
[/boot](https://wiki.archlinux.org/title/Partitioning#/boot)
> In both cases the suggested size for the partition is 1 GiB, which should give enough space to house multiple kernels.

/home
A different partition for home that ideally can be moved across distros / OSs.
Also, although not impossible it makes OS reinstall simpler.
See [/home](https://wiki.archlinux.org/title/Partitioning#/home)

## MISC

[[SOLVED]Boot failed, missing libssl.so.1.1 and libcrypto.so.1.1 / Newbie Corner / Arch Linux Forums](https://bbs.archlinux.org/viewtopic.php?id=281027)

## hDPI

[Mouse Cursor Size](https://bbs.archlinux.org/viewtopic.php?id=164935)

[Experiences using 4khidpi monitors with tiling wms](https://www.reddit.com/r/archlinux/comments/6aqkuf/experiences_using_4khidpi_monitors_and_tiling_wms/)
  [This specific comment](https://www.reddit.com/r/archlinux/comments/6aqkuf/experiences_using_4khidpi_monitors_and_tiling_wms/dhgx2dp/)

[Kernel mode setting: Setting Display resultion in kernel space rather than user space](https://wiki.archlinux.org/index.php/Kernel_mode_setting#Forcing_modes_and_EDID)
The Linux kernel's implementation of KMS enables native resolution in the framebuffer and allows for instant console (tty) switching. KMS also enables newer technologies (such as DRI2) which will help reduce artifacts and increase 3D performance, even kernel space power-saving.

## Lock Screen
https://github.com/pavanjadhaw/betterlockscreen

## Desktop inspiration

Minimalist (both visually and applications) retro: https://www.reddit.com/r/unixporn/comments/cag47x/dwm_meditations_in_the_suckless_void/

Animated background: https://www.reddit.com/r/unixporn/comments/cae6q8/bspwm_animeted_wallpaper_pywal/

[[i3-gaps] a bit more refined xx](https://www.reddit.com/r/unixporn/comments/6k31u9/i3gaps_a_bit_more_refined_xx/)
Very cool!

### Windows

RainMeter: https://www.youtube.com/watch?v=2M81qFqdf6k

Translucent Windows taskbar: https://github.com/TranslucentTB/TranslucentTB

Sidenote: tiled Windows: https://github.com/microsoft/PowerToys/blob/master/src/modules/fancyzones/README.md

## Dual Boot with Windows, sync clocks

In windows:
```
;RealTimeIsUniversal.reg
Windows Registry Editor Version 5.00

;Enable UTC support so clock doesn't get out of sync when dual booting
;https://superuser.com/questions/975717/does-windows-10-support-utc-as-bios-time

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation]
     "RealTimeIsUniversal"=hex(b):01,00,00,00,00,00,00,00
```

---

Open the Application launcher using the Super key
https://github.com/hanschen/ksuperkey

[How to Install Archlinux](https://www.youtube.com/watch?v=fX9dn0aCQfc)

## Fonts

https://www.codingfont.com/

* match available fonts - `fc-match`
* list available fonts - `fc-list`

[What is the default font in Arch Linux for Terminal? : archlinux](https://www.reddit.com/r/archlinux/comments/19ear75/what_is_the_default_font_in_arch_linux_for/)
[Linux console - ArchWiki](https://wiki.archlinux.org/title/Linux_console)
[Fonts - ArchWiki](https://wiki.archlinux.org/title/Fonts)
[How to change the font size of GRUB booting : archlinux](https://www.reddit.com/r/archlinux/comments/qlg6n6/how_to_change_the_font_size_of_grub_booting/)

## Disable Mouse Acceleration

```conf
# /etc/X11/xorg.conf.d/50-mouse-acceleration.conf
#
# Disables mouse acceleration

Section "InputClass"
	Identifier "Mouse"
	Driver "libinput"
	MatchIsPointer "yes"
	Option "AccelProfile" "flat"
EndSection
```


Configure arch sad periodic trim

## Sound

[sound - How do you set a default audio output device in Ubuntu? - Ask Ubuntu](https://askubuntu.com/questions/1038490/how-do-you-set-a-default-audio-output-device-in-ubuntu)
[How to Set Default Audio in Ubuntu | how.wtf](https://how.wtf/how-to-set-default-audio-in-ubuntu.html)
[A more robust version of https://github.com/LarsBekhof/scripts/blob/master/switch-audio-sink · GitHub](https://gist.github.com/pdonadeo/aaf5f3f164ef23c9db4ac8458137c2c8)

[sound - What is the relation between PipeWire and PulseAudio on Ubuntu 22.04? - Ask Ubuntu](https://askubuntu.com/questions/1432040/what-is-the-relation-between-pipewire-and-pulseaudio-on-ubuntu-22-04)
[PipeWire](https://pipewire.org/)

[How to Use PipeWire to replace PulseAudio in Ubuntu 22.04 | UbuntuHandbook](https://ubuntuhandbook.org/index.php/2022/04/pipewire-replace-pulseaudio-ubuntu-2204/)

## Window System

[Bspwm on an ultrawide monitor : r/bspwm](https://www.reddit.com/r/bspwm/comments/vx1p3z/bspwm_on_an_ultrawide_monitor/)

### Wayland

[Wayland - ArchWiki](https://wiki.archlinux.org/title/Wayland)
[river/river: A dynamic tiling Wayland compositor - Codeberg.org](https://codeberg.org/river/river)
(bspwm clone for wayland).
Discovered in [On Wayland · Issue #199 · baskerville/bspwm](https://github.com/baskerville/bspwm/issues/199).

## Pacman

[pacman - ArchWiki](https://wiki.archlinux.org/title/pacman)

* Install
  `pacman -S package_name1`
* Remove
  `pacman -R package_name`
  - Remove and its dependencies which are not required by any other installed package
    `pacman -Rs package_name`
  - Pacman saves important configuration files when removing certain applications and names them with the
    extension: .pacsave.
    To prevent the creation of these backup files use the -n option:
    `pacman -Rn package_name`
* Upgrading all system packages
  `pacman -Syu`

[pacman/Tips and tricks - ArchWiki](https://wiki.archlinux.org/title/pacman/Tips_and_tricks)
Cool fzf tips & tricks

## Troubleshoot

Refresh keys:
```
pacman-key --refresh-keys
```

[[SOLVED] Vmlinuz-linux not found after kernel update / Newbie Corner / Arch Linux Forums](https://bbs.archlinux.org/viewtopic.php?id=250486)
It seems this was because the files on /boot were not updated with the kernel update for some reason...
Re-Installing it "just worked", meaning:
```
pacman -S linux
pacman -S mkinitcpio
```

[14.04 - Change boot order using efibootmgr - Ask Ubuntu](https://askubuntu.com/questions/485261/change-boot-order-using-efibootmgr)

[how to fix missing libcrypto.so.1.1?](https://unix.stackexchange.com/a/726271)
* use live CD/USB and boot it
* run lsblk command and mount / and /boot as I explain (see picture)
* run mount /dev/sda3 /mnt and mount /dev/sda1 /mnt/boot/efi
* pacman --root /mnt --cachedir /mnt/var/cache/pacman/pkg --config /mnt/etc/pacman.conf -Syu