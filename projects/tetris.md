# Tetris

[Code-It-Yourself! Tetris - Programming from Scratch](https://www.youtube.com/watch?v=8OK8_tHeCIA)

[A Game of Tetris](https://alex-hhh.github.io/2020/03/a-game-of-tetris.html)
Using Racket (Lisp / Scheme like language)

[Cool simplistic tetris](https://jstris.jezevec10.com/)
https://clips.twitch.tv/AlluringGlamorousUdonJebaited
