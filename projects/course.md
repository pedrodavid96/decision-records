# Programming Intro

I just graduated!!! (party emoji)
First of all that means I have to start looking for a job.
But it also means for now I have a lot of free time, and a course to wrap up.
Because of that I decided to clean and save some of my previous work,
to also check if there is something worth saving, continue working on and puting on resume.

So why not teaching you some of what I learned in this (squized up) three years?
That's what I thought. Starting today, intro to programming!
I will take a similar but a lot different (wow...) approach to the one my teacher took in this one.

My course was "Engenharia Informática e Computadores" literally translated to "Informatic engennering and computers",
Computer Science. In this course I learned how computers work, from the transistors to the high level languages that can be used to control it.
I learned algorithms, problem solving and a plethora of commonly used paradigms to solve specific problems as well as languages, libraries, frameworks...

In summary (and like I already said), I learned how computers work and how someone can tell it what to do (and it can do a lot!).
Someone with this power is called a programmer and exercises this power by programming in a specific language.
Despite knowing only some languages like Java, C#, C, C++, Javascript, SQL, Kotlin, Assembly... what I know likely translates to most other languages,
despite some different paradigms and some really different syntax they're really similar after all.

In intro to programming I learned the basic concepts of imperative programming, for that I used Java but I could have used almost any other language.
My take in this will be a different alternative. One of my objectives with this blog is also learning (not just teaching you... sorry to break it :( )
so I will probably give examples in a lot of different languages.

Pros:
    - I (and you) will learn and try different languages
    - Discussing about different languages approaches to some problems
    - Seeing pros and cons of each language
    - Teaching basic concepts can be clearer in a language where that concept is "more important"

Cons:
    - Downloading and Installing more stuff
    - For a beginner it can be overwhelming, I hope not
    - If it works: Jack of all trades, master of none

All in all the Pros seem to outweigh the cons, specially taking into consideration that
Con1, the programs shouldn't be that big
Con3, I'm just showing you hypothesis, I urge you to investigate more and become a master of the language that you like more
Con2 is a special one. I hope I'm good enough so I don't overwhelm you and that you can reach the end of post knowing about what you just read.
