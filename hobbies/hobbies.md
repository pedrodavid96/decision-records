# Hobbies

[What new hobbies have you picked up this year and would you recommend them to others?](https://www.reddit.com/r/AskReddit/comments/ia8xfz/what_new_hobbies_have_you_picked_up_this_year_and/)
Awesome awesome thread 🥇⭐
Some ideas:
* Calligraphy
* Reading
* Free online classes
* Archery
* Cooking
* Lock picking
* Learning languages
* Sewing
* Gardening
* Meditating
* Bob Ross "The Joy of Painting"
* Walks
* Crocheting
* 3D printing
    - [Ender 3 Pro 3D Printer](https://www.creality3dofficial.com/products/creality-ender-3-pro-3d-printer)
    - https://www.reddit.com/r/3Dprinting/comments/r3gypn/whats_a_good_3d_printer_for_a_beginner_under_200/hmb06sz/?context=3
* Yoga - [Yoga With Adriene](https://www.youtube.com/watch?v=oBu-pQG6sTY)
* Chess
* Ceramics
