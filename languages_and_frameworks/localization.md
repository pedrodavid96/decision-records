# Localization

## Open Source

[Weblate - web-based localization](https://weblate.org/en/)
[Weblate 5.3 documentation](https://docs.weblate.org/en/latest/index.html)
[Installing using Docker - Weblate 5.3 documentation](https://docs.weblate.org/en/latest/admin/install/docker.html)


[tolgee/tolgee-platform: Developer & translator friendly web-based localization platform](https://github.com/tolgee/tolgee-platform)

## Licensed

[Localization Management Platform for agile teams | Crowdin](https://crowdin.com/)
[A Localization and Translation Software Tool | Lokalise](https://lokalise.com/)
