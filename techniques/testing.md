# Testing

[Selfie JVM Snapshot Testing](https://selfie.dev/jvm)

[Reasons not to add messages when you assert](https://www.mikedeluna.com/reasons-not-to-add-messages-when-you-assert/)

[9 Excuses Why Programmers Don’t Test Their Code](https://medium.com/better-programming/9-excuses-why-programmers-dont-test-their-code-8860a616b1b5)
1. “My Code Works Fine — Why Should I Even Bother Testing It?”
2. “This Piece of Code Is Untestable”
3. “I Don’t Know What to Test”
4. “Testing Increases the Development Time, and We’re Running Out of Time”
5. “The Requirements Are No Good”
6. “This Piece of Code Doesn’t Change”
7. “I Can Test This Way Faster If I do It Manually”
8. “The Client Only Wants to Pay for Deliverables”
9. “This Piece of Code Is So Small … It Won’t Break Anything”

[How SQLite Is Tested](https://www.sqlite.org/testing.html)
SQLite test coverage is huge
> By comparison, the project has 644 times as much test code and test scripts - 91900.0 KSLOC.

## Test data Generation

[F# Test data: generators, shrinkers and arbitrary instances](https://fscheck.github.io/FsCheck/TestData.html#Test-data-generators-shrinkers-and-Arbitrary-instances)

---

[PropEr Testing](https://propertesting.com/toc.html)

[Property Based testing](https://www.joecolantonio.com/property-based-testing/)
[The sad state of property-based testing libraries](https://stevana.github.io/the_sad_state_of_property-based_testing_libraries.html)
[The sad state of property-based testing libraries : programming](https://www.reddit.com/r/programming/comments/1duamq2/the_sad_state_of_propertybased_testing_libraries/)
[jqwik | Property-Based Testing in Java](https://jqwik.net/)

[Property-Based Testing for everyone by Romeu Moura](https://www.youtube.com/watch?v=5pwv3cuo3Qk)

[Property-Based Testing in a Screencast Editor: Introduction](https://wickstrom.tech/programming/2019/03/02/property-based-testing-in-a-screencast-editor-introduction.html)

[Property Testing Example with Vavr](https://www.baeldung.com/vavr-property-testing)

[Effective Property-Based Testing | Blog – Auxon Corporation](https://blog.auxon.io/2021/02/01/effective-property-based-testing/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/lba30v/effective_propertybased_testing/)

---

[Mockaroo - Random Data Generator and API Mocking Tool | JSON / CSV / SQL / Excel](https://www.mockaroo.com/)

[Testing Without Mocks](https://www.jamesshore.com/Blog/Testing-Without-Mocks.html)

["Mocking" HttpClient in unit tests](https://stackoverflow.com/questions/36425008/mocking-httpclient-in-unit-tests)

[Clean Coder Blog](https://blog.cleancoder.com/uncle-bob/2014/05/14/TheLittleMocker.html)

[Using a Jetty Server with JUnit tests](https://stackoverflow.com/questions/5267423/using-a-jetty-server-with-junit-tests/18207653)

[Rethinking Unit Test Assertions](https://medium.com/javascript-scene/rethinking-unit-test-assertions-55f59358253f)

[Lean Testing or Why Unit Tests are Worse than You Think | Eugen Kiss](https://blog.usejournal.com/lean-testing-or-why-unit-tests-are-worse-than-you-think-b6500139a009)

[Write tests. Not too many. Mostly integration. | Kent C. Dodds](https://kentcdodds.com/blog/write-tests)

## TDD

[Why Most Unit Testing is Waste](https://www.reddit.com/r/programming/comments/ca7f2m/why_most_unit_testing_is_waste/)

[🚀 DevTernity 2017: Ian Cooper - TDD, Where Did It All Go Wrong](https://www.youtube.com/watch?v=EZ05e7EMOLM)

[TDD is a lot easier with the right tools](https://i-kh.net/2019/11/11/tdd-is-easier-with-right-tools/)
Winner: .Net with NCrunch
NCrunch runs continuously as you type.
Infinitest on Java

## Fuzzy (Mutation?) Testing

[libFuzzer – a library for coverage-guided fuzz testing](https://llvm.org/docs/LibFuzzer.html)

## OpenGL

[Testing OpenGL - TDD Unit testing](https://stackoverflow.com/questions/3221451/learning-opengl-while-practicing-tdd-unit-testing)

[When is it OK not to TDD?](http://gamesfromwithin.com/when-is-it-ok-not-to-tdd)

---

[3 steps to add tests on existing code when you have short deadlines](https://understandlegacycode.com/blog/3-steps-to-add-tests-on-existing-code-when-you-have-short-deadlines/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/eunf24/how_to_add_tests_on_existing_code_when_you_have/)

[Kent Beck youtube playlist on tests](https://www.youtube.com/watch?v=5LOdKDqdWYU&list=PLlmVY7qtgT_lkbrk9iZNizp978mVzpBKl)

[The key points of Working Effectively with Legacy Code](https://understandlegacycode.com/blog/key-points-of-working-effectively-with-legacy-code/)
> “Legacy Code is code without tests”

1. First, add tests, then do your changes

2. Adding tests: the Legacy Code dilemma (the paradox of Legacy Code)

   Before you change code, you should have tests in place.
   But to put tests in place, you have to change code.

   1. Identify change points (Seams)
   2. Break dependencies
   3. Write the tests
   4. Make your changes
   5. Refactor

3. Identify Seams to break your code dependencies

4. Unit tests are fast and reliable

   In short, your test is not unit if:
   * it doesn’t run fast (< 100ms / test)
   * it talks to the Infrastructure (e.g. a database, the network, the file system, environment variables…)

5. Characterization tests

   > “A characterization test is a test that characterizes the actual behavior of a piece of code.”

    Instead of writing comprehensive unit tests, you capture the current behavior of the code. You take a snapshot of what it does.

    The test ensures that this behavior doesn’t change!

    This is very powerful because:
    * With most systems, what the code actually does is more important than what it should do.
    * You can quickly cover Legacy Code with these tests, giving you a safety net to refactor.

6. Use Sprout & Wrap techniques to add code when you don’t have time to refactor
   * The Sprout technique
     1. Create your code somewhere else.
     2. Unit test it.
     3. Identify where you should call that code from the existing code: the insertion point.
     4. Call your code from the Legacy Code.
   * The Wrap technique
     1. Rename the old method you want to wrap.
     2. Create a new method with the same name and signature as the old method.
     3. Call the old method from the new method.
     4. Put the new logic before/after the other method call.

7. Use scratch refactoring to get familiar with the code

   The only rule is: revert your changes when you’re done.

8. Don’t make your code depend on libraries’ implementation

   > “Avoid littering direct calls to library classes in your code. You might think that you’ll never change them, but that can become a self-fulfilling prophecy.”

[The Practical Test Pyramid](https://martinfowler.com/articles/practical-test-pyramid.html)

[Data Driven Testing with Spock Framework](http://spockframework.org/spock/docs/1.3/data_driven_testing.html)

[Unexpected Lessons From 100% Test Coverage](https://medium.com/better-programming/unexpected-lessons-from-100-test-coverage-eebeee211b7a)
Log Tests?
Code Golf? `if (case) { Log(...); }` turns into `warnIf(case, ...)`
Neutral: Clarifying assertions versus errors
Opportunity to Simplify Code

---

[Comment](https://medium.com/@hubert.jarema/the-purpose-of-a-unit-test-is-not-to-test-if-inputs-produce-an-expected-output-fb216518912c)
> The purpose of a unit test is not to test if inputs produce an expected output. The output may be a 200 HTTP code response, but there might be dozen of different states that are produced by your inputs.
> Consider testing a function that calls five different services. Your input is what determines what is being called. You want to write tests to verify that appropriate service has been called, and appropriate data has been returned, and that your code reacted to the various return values. In what you are proposing, you would have to store all those data points in the database, for each possible variation. Wouldn’t that be more maintenance than verifying it in code?

No! That leads to tight coupling with the implementation which will actually make refactoring harder as you'll always break the tests. This makes your developers angry and actually goes against the TDD loop (red -> green -> refactor -> red ...)

[Puppeteer](https://pptr.dev/)
Selenium (npm) alternative


[Opportunistic Testing](https://itnext.io/opportunistic-testing-28bfd2ca5572)
Very cool article with funny illustrations / comics 🥇⭐

[When I lost a few hundred leads](https://stitcher.io/blog/when-i-lost-a-few-hundred-leads)
> I'd like to make clear that this post isn't about how to deal with or prevent bugs. It's about how I felt during a rather bad and stressful time, to talk about emotions and how to self-reflect and improve. With that out of the way, I hope you find value in this post.
[Discussion](https://www.reddit.com/r/programming/comments/k34s17/when_i_lost_a_few_hundred_leads/)

[Writing Good Unit Tests; Don’t Mock Database Connections](https://qvault.io/2020/11/23/writing-good-unit-tests-dont-mock-database-connections/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/kjzmi4/writing_good_unit_tests_dont_mock_database/)
Made a [comment](https://www.reddit.com/r/programming/comments/kjzmi4/writing_good_unit_tests_dont_mock_database/ggzmyze/)
 specially regarding the last topic (Don’t Test Your Dependencies, Ensure They Pass Their Own Tests) which I didn't agree on.

[Cost of Testing](https://testing.googleblog.com/2009/10/cost-of-testing.html)
> A lot of people have been asking me lately, what is the cost of testing, so I decided, that I will try to measure it, to dispel the myth that testing takes twice as long.
Very interesting article with the metrics used to measure it.
This should be very valuable information when estimating a task.
> The magic number is about **10% of time spent on writing tests**.
Found on [StackOverflow answer](https://stackoverflow.com/a/1595387)

[Testing in Production, the safe way](https://copyconstruct.medium.com/testing-in-production-the-safe-way-18ca102d0ef1)
The Three Phases of “Production” - deploy | release | post-release

[Prodfiler, a continuous profiler that "just works" -- for C/C++/Rust/Go/JVM/Python/Perl/PHP -- no code change required, no symbols on the machine required, no service restart required. : programming](https://www.reddit.com/r/programming/comments/pfeia5/prodfiler_a_continuous_profiler_that_just_works/)

[TDD for databases](http://dbfit.github.io/dbfit/docs/writing-tests.html)

[Going overboard with unit tests](https://www.tedinski.com/2018/11/27/contradictory-tdd.html)

[GitHub - Devskiller/jfairy: Java fake data generator](https://github.com/Devskiller/jfairy)

[Javadoc in JUnit test classes?](https://stackoverflow.com/a/9127841)
The main objective here is to make the test understandable for other developers contributing to your project. And for that we don't even need to generate the actual javadoc.
```
/**
 * Create a valid account.
 * @result Account will be persisted without any errors,
 *         and Account.getId() will no longer be <code>null</code>
 */
@Test
public void createValidAccount() {
    accountService.create(account);
    assertNotNull(account.getId());
}
```
```
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-javadoc-plugin</artifactId>
            <version>2.8</version>
            <configuration>
                <tags>
                    <tag>
                        <name>result</name>
                        <placement>a</placement>
                        <head>Test assertion :</head>
                    </tag>
                </tags>
            </configuration>
        </plugin>
    </plugins>
</build>
```
```
javadoc:test-javadoc (or javadoc:test-aggregate for multi-module projects)
```
