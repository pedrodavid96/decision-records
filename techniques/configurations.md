[The Configuration Complexity Curse](https://blog.cedriccharly.com/post/20191109-the-configuration-complexity-curse/) - [discussion](https://www.reddit.com/r/programming/comments/dwt9gj/the_configuration_complexity_curse_dont_be_a_yaml/)
> Don’t be a YAML Engineer

[Configurability is the root of all evil by fishshell](https://fishshell.com/docs/current/design.html#design-configurability)
> Every configuration option in a program is a place where the program is too stupid to figure out for itself what the user really wants, and should be considered a failure of both the program and the programmer who implemented it.

[INTERCAL, YAML, And Other Horrible Programming Languages](https://blog.earthly.dev/intercal-yaml-and-other-horrible-programming-languages/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/ls6tgm/intercal_yaml_and_other_horrible_programming/)
