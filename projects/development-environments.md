# Development Environments

## NixOS

[Nix & NixOS | Reproducible builds and deployments](https://nixos.org/)

## DevPod

[DevPod - Open Source Dev-Environments-As-Code](https://devpod.sh/)
[Reuse local credentials | DevPod docs | DevContainers everywhere](https://devpod.sh/docs/developing-in-workspaces/credentials)

## devenv

[Fast, Declarative, Reproducible, and Composable Developer Environments - devenv](https://devenv.sh/)

## [GitHub Codespaces](https://github.com/features/codespaces)

Non Open Source SASS

## [Gitpod: Always ready-to-code.](https://www.gitpod.io/)

Non Open Source SASS (direct competitor to GitHub Codespaces)

[We’re leaving Kubernetes - Blog](https://www.gitpod.io/blog/we-are-leaving-kubernetes)
[We're Leaving Kubernetes | Hacker News](https://news.ycombinator.com/item?id=42041917)

One of the big reasons is build artifacts (lack of) caching and resource allocation / distribution.
