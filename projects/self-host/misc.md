# Self-Host (misc)

https://www.reddit.com/r/selfhosted/comments/1ge7s07/there_is_an_opensource_alternative_to_almost_any/lu9l8m4/
```
Notion → AppFlowy, Outline
Salesforce CRM → EspoCRM, SuiteCRM
Zendesk → Chatwoot, Zammad
HubSpot → EspoCRM, Crust CRM
Slack → Mattermost, Rocket.Chat, Zulip
Google Analytics → Matomo, Plausible
Zapier → n8n, Huginn
Google Docs → Etherpad, Collabora Online, CryptPad
Shopify → Saleor, Medusa, Reaction Commerce
SurveyMonkey → LimeSurvey, SurveyJS
Trello → WeKan, Focalboard
Airtable → NocoDB, Baserow
Calendly → Calendso, EasyAppointments
Mailchimp → Mautic, Listmonk
Algolia → Typesense, **MeiliSearch**
Zoom → Jitsi Meet, BigBlueButton
Dropbox → Nextcloud, Seafile
AWS Lambda → OpenFaaS, Kubeless
Sentry → Self-hosted Sentry, GlitchTip
Google Tag Manager → Open Tag Manager, RudderStack
```

## Inspiration

[What's on my Home Server?? MUST HAVE Services 2023! - YouTube](https://www.youtube.com/watch?v=5YgWaeq07As)

[SELF-HOSTED Podcast Grabber in Docker! - Podgrab - YouTube](https://www.youtube.com/watch?v=tQgGTI8wfqw)
[Introduction - PodFetch Documentation](https://www.samtv.fyi/PodFetch/Introduction.html)
[madeofpendletonwool/PinePods: Pinepods is a complete podcast management system and allows you to play, download, and keep track of podcasts you enjoy. All self hosted and enjoyed on your own server!](https://github.com/madeofpendletonwool/PinePods)
[advplyr/audiobookshelf: Self-hosted audiobook and podcast server](https://github.com/advplyr/audiobookshelf)
[Alternative to Podgrab? : selfhosted](https://www.reddit.com/r/selfhosted/comments/16gulhn/alternative_to_podgrab/)

[My Homelab, September 2024 (TrueNAS, Proxmox, Tailscale, a 2014 Mac Mini, and more) - Alex Klibisz](https://alexklibisz.com/2024/09/27/homelab-september-2024)
[Reddit Discussion](https://www.reddit.com/r/selfhosted/comments/1frvnlr/my_homelab_september_2024_truenas_proxmox/)

[I Control EVERYTHING with this Raspberry Pi - YouTube](https://www.youtube.com/watch?v=gpyYCTgJO88)
> I set up a Raspberry Pi as a Home Assistant control board.
> ...
> Thanks to Raspberry Pi for sending the Touch Display 2 for testing.

[How to Turn a USB Printer Into a Wireless Printer With Raspberry Pi Zero W  | Raspberry Pi | Maker Pro](https://maker.pro/raspberry-pi/projects/how-to-turn-a-usb-printer-into-a-wireless-printer-with-raspberry-pi-zero-w)

[Bandwidth Monitor : 7 Steps (with Pictures) - Instructables](https://www.instructables.com/Bandwidth-Monitor/)
> Shows Internet speed in e-ink display

[Alexa Assistant With a $10 Raspberry Pi Zero W and Lights : 6 Steps - Instructables](https://www.instructables.com/Alexa-Assistant-With-a-10-Raspberry-Pi-Zero-W-and-/)

[Off-The-Shelf 10" Gear Guide : minilab](https://www.reddit.com/r/minilab/comments/1g1nto6/offtheshelf_10_gear_guide/)
(Cool subreddit)
[it grows : minilab](https://www.reddit.com/r/minilab/comments/1gqdty5/it_grows/)
[My new mini lab 2.0 - remember WAF 🥺 : minilab](https://www.reddit.com/r/minilab/comments/1gqko06/my_new_mini_lab_20_remember_waf/) (four IKEA VARIERA)


[Idk what to host anymore... : selfhosted](https://www.reddit.com/r/selfhosted/comments/1g6p4fa/idk_what_to_host_anymore/)

[Your Upcoming Projects 2025 : selfhosted](https://www.reddit.com/r/selfhosted/comments/1hmf6ey/your_upcoming_projects_2025/)

## Tools

[Composerize](https://www.composerize.com/)

## List of Self Hostable projects that seem interest to check out somewhen

[There is an open-source alternative to almost any SaaS, what do you use? : selfhosted](https://www.reddit.com/r/selfhosted/comments/1ge7s07/there_is_an_opensource_alternative_to_almost_any/)

[rishikanthc/Scriberr: Self-hosted AI audio transcription](https://github.com/rishikanthc/Scriberr?tab=readme-ov-file)
[Introducing Scriberr - Self-hosted AI Transcription : selfhosted](https://www.reddit.com/r/selfhosted/comments/1fwcgvr/introducing_scriberr_selfhosted_ai_transcription/)

[What's the self-hosted alternative to the Google Maps timeline? : selfhosted](https://www.reddit.com/r/selfhosted/comments/1b4jr97/whats_the_selfhosted_alternative_to_the_google/)

[Migrating from Google Location History to OwnTracks](https://www.technowizardry.net/2024/01/migrating-from-google-location-history-to-owntracks/)

[jaypyles/open-spots](https://github.com/jaypyles/open-spots?tab=readme-ov-file)
Open Spots is a fork of Spots that is designed to help organizations deliver real-time building availability data to staff, employees, customers, or students.

[getwud/wud: WUD (aka What's up Docker?)](https://github.com/getwud/wud)
gets you notified when a new version of your Docker Container is available.

[Yooooomi/your_spotify: Self hosted Spotify tracking dashboard](https://github.com/Yooooomi/your_spotify?tab=readme-ov-file)

[dani-garcia/vaultwarden: Unofficial Bitwarden compatible server written in Rust, formerly known as bitwarden_rs](https://github.com/dani-garcia/vaultwarden)

[Juspay Hyperswitch | Open-Source Payment Orchestrator](https://hyperswitch.io/)

[Open Streaming Platform](https://openstreamingplatform.com/)
[Owncast - Free and Open Source Livestreaming](https://owncast.online/)

[Shelf - Open-Source Asset Management Software | Simplify Inventory Tracking](https://www.shelf.nu/)
The stack looks absurd as it needs Supabase which might be self-hostable but it's imense,
 trying to fully emulate a PAAS on Docker.
[Self-Hosting with Docker | Supabase Docs](https://supabase.com/docs/guides/self-hosting/docker#before-you-begin)
There doesn't seem to exist working examples of this either
[maarteNNNN/shelf-docker-compose](https://github.com/maarteNNNN/shelf-docker-compose/tree/main)
This looks promising but at first glance there's lot of undocummented changes required and
I gave up when "analytics" tried to connect to Postgres with a non-existing user.

[Docspell – Simple document organizer](https://docspell.org/)

[DocuSeal | Open Source Document Signing](https://www.docuseal.com/)

[plugNmeet - Open source web conferencing system](https://www.plugnmeet.org/)
(WebRTC)

[BigBlueButton Developers | Integrate BigBlueButton With Your LMS](https://bigbluebutton.org/developers/)
> Meet your customizable virtual classroom

[Firefly III - A free and open source personal finance manager](https://www.firefly-iii.org/)

[Bandwidth Monitor : 7 Steps (with Pictures) - Instructables](https://www.instructables.com/Bandwidth-Monitor/)

[Collaboration mode - Self-hosting vs Collab on top of <Excalidraw/> · excalidraw/excalidraw · Discussion #3879](https://github.com/excalidraw/excalidraw/discussions/3879#discussioncomment-1110524)

[shlinkio/shlink: The definitive self-hosted URL shortener](https://github.com/shlinkio/shlink)

[danielbrendel/hortusfox-web: Self-hosted collaborative plant management and tracking system for plant enthusiasts](https://github.com/danielbrendel/hortusfox-web?tab=readme-ov-file)

[GPU Support](https://whishper.net/guides/gpu/)

[tolgee/tolgee-platform: Developer & translator friendly web-based localization platform](https://github.com/tolgee/tolgee-platform)

[Home - Readeck](https://readeck.org/en/)

[Hoarder](https://hoarder.app/)

[Build a Tiny Certificate Authority For Your Homelab](https://smallstep.com/blog/build-a-tiny-ca-with-raspberry-pi-yubikey/)
