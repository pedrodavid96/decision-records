# Recipes Project

Example:
[I created an extension for automatically scrolling to the recipe part of a food post | Reddit discussion](https://www.reddit.com/r/programming/comments/kko8ae/i_created_an_extension_for_automatically/)

Comments talk about https://schema.org/Recipe which seems awesome 🥇⭐

[GitHub - kcal-app/kcal: the personal food nutrition journal](https://github.com/kcal-app/kcal)
[KitchenOwl Docs](https://docs.kitchenowl.org/)

## [Home - Mealie](https://nightly.mealie.io/)

A self-hosted recipe manager and meal planner with a RestAPI backend and a reactive frontend application built in Vue for a pleasant user experience for the whole family.

[Meal planner printing · mealie-recipes/mealie · Discussion #3574](https://github.com/mealie-recipes/mealie/discussions/3574)
[OAuth setup with Keycloak · mealie-recipes/mealie · Discussion #3428](https://github.com/mealie-recipes/mealie/discussions/3428)
[Disable password login when using OIDC or LDAP · mealie-recipes/mealie · Discussion #4581](https://github.com/mealie-recipes/mealie/discussions/4581)

### Tesseract integration

https://github.com/mealie-recipes/mealie/blob/mealie-next/mealie/services/ocr/pytesseract.py
https://github.com/mealie-recipes/mealie/blob/mealie-next/mealie/routes/ocr/pytesseract.py

### Macros integration

#### AI for macros (?)

[GitHub - stratospark/food-101-keras: Food Classification with Deep Learning in Keras / Tensorflow](https://github.com/stratospark/food-101-keras)

### Competition

[READ THIS FIRST: MacroFactor Setup and FAQs : MacroFactor](https://www.reddit.com/r/MacroFactor/comments/pp6lpr/read_this_first_macrofactor_setup_and_faqs/)
