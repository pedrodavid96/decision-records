# Server

https://www.cyberciti.biz/tips/linux-security.html
https://github.com/mikeroyal/Self-Hosting-Guide

[Tech Independence | Derek Sivers](https://sive.rs/ti)
[SSD VPS Servers, Cloud Servers and Cloud Hosting - Vultr.com](https://www.vultr.com/)

[Caddy - The Ultimate Server with Automatic HTTPS](https://caddyserver.com/)
(nginx alternative)
[DNS server (pihole) with reverse proxy (caddy) : homelab](https://www.reddit.com/r/homelab/comments/14yvt40/dns_server_pihole_with_reverse_proxy_caddy/)

[How to use Cloudflare Tunnel in your Homelab (even with Traefik) - YouTube](https://www.youtube.com/watch?v=yMmxw-DZ5Ec)
[You Need to Learn This! Cloudflare Tunnel Easy Tutorial - YouTube](https://www.youtube.com/watch?v=ZvIdFs3M5ic)

[Docker restart on CLI? | SynoForum.com - The Unofficial Synology Forum](https://www.synoforum.com/threads/docker-restart-on-cli.4678/)

[How to run Docker commands without sudo on a Synology NAS](https://andyyang.co.uk/synology-nas-how-to-run-docker-commands-without-sudo/)
[Manage docker without needing sudo on your Synology NAS](https://davejansen.com/manage-docker-without-needing-sudo-on-your-synology-nas/)

[Resolving a .local domain name works like this. $ dig -p 5353 raspberrypi.local ... | Hacker News](https://news.ycombinator.com/item?id=17094138)

[Single Sign On (SSO) with subdomains using Caddy v2](https://blog.sjain.dev/caddy-sso/)
[Security flaws in an SSO plugin for Caddy | Trail of Bits Blog](https://blog.trailofbits.com/2023/09/18/security-flaws-in-an-sso-plugin-for-caddy/)

[Let's Encrypt](https://letsencrypt.org/)

[Getting started](https://edgeshark.siemens.io/#/getting-started?id=service-deployment)

[Keycloak | Caddy Security](https://authp.github.io/docs/authenticate/oauth/backend-oauth2-0011-keycloak)
[GitHub - lucaslorentz/caddy-docker-proxy: Caddy as a reverse proxy for Docker](https://github.com/lucaslorentz/caddy-docker-proxy#with-docker-compose-file)
[Using a reverse proxy - Keycloak](https://www.keycloak.org/server/reverseproxy)
[Running Keycloak in a container - Keycloak](https://www.keycloak.org/server/containers)
[Configuring Keycloak for production - Keycloak](https://www.keycloak.org/server/configuration-production)

[Using Caddy as a reverse proxy in a home network - Wiki - Caddy Community](https://caddy.community/t/using-caddy-as-a-reverse-proxy-in-a-home-network/9427)

paperless csrf verification failed on subdomain - Pesquisa Google
[caddy localhost subdomain invalid certificate - Pesquisa Google](https://www.google.com/search?client=firefox-b-d&q=caddy+localhost+subdomain+invalid+certificate)
[How to use caddy to create a local LAN domain for a personal dashboard - Help - Caddy Community](https://caddy.community/t/how-to-use-caddy-to-create-a-local-lan-domain-for-a-personal-dashboard/6377)
[Https on local network - Help - Caddy Community](https://caddy.community/t/https-on-local-network/14745)
[Help: Caddy setup for internal LAN access only : selfhosted](https://www.reddit.com/r/selfhosted/comments/11u3bsp/help_caddy_setup_for_internal_lan_access_only/)
[Is it possible to automate reverse proxy entries? · Issue #551 · lucaslorentz/caddy-docker-proxy](https://github.com/lucaslorentz/caddy-docker-proxy/issues/551)

[swag - LinuxServer.io](https://docs.linuxserver.io/images/docker-swag/)
> sets up an Nginx webserver and reverse proxy with php support and
> a built-in certbot client that automates free SSL server certificate generation and renewal processes
>  (Let's Encrypt and ZeroSSL).
> It also contains fail2ban for intrusion prevention.

[Pihole works as DHCP server over ethernet but not wifi : pihole](https://www.reddit.com/r/pihole/comments/vjuaa3/pihole_works_as_dhcp_server_over_ethernet_but_not/)
[Docker DHCP and Network Modes - Pi-hole documentation](https://docs.pi-hole.net/docker/dhcp/)

[What's On My Home Server? Storage, OS, Media, Provisioning, Automation](https://youtu.be/f5jNJDaztqk?si=lBTWeR2GMwzzJzBs)
* Homer https://github.com/bastienwirtz/homer
* My custom CSS for Homer https://github.com/notthebee/infra/blob/ca87f0daf211919ff19ce678523f48f2a6d25ed7/roles/homer/files/custom.css
* Nord colors for Homer https://pastebin.com/AH9NWmSL
* Jellyfin https://github.com/linuxserver/docker-jellyfin
* arch-delugevpn https://github.com/binhex/arch-delugevpn
* Radarr https://hub.docker.com/r/linuxserver/radarr
* Sonarr https://hub.docker.com/r/linuxserver/sonarr
* OpenBooks https://github.com/evan-buss/openbooks
* Nextcloud https://hub.docker.com/_/nextcloud
* PhotoPrism https://hub.docker.com/r/photoprism/photoprism
* Vaultwarden https://github.com/dani-garcia/vaultwarden
* UniFi Controller https://hub.docker.com/r/linuxserver/unifi-controller
* Jackett https://hub.docker.com/r/linuxserver/jackett
* PiKVM https://github.com/pikvm/pikvm
* PiHole + Unbound https://github.com/chriscrowe/docker-pihole-unbound
* Deconz https://github.com/deconz-community/deconz-docker
* Home Assistant https://hub.docker.com/r/linuxserver/homeassistant

[Home - homepage](https://gethomepage.dev/v0.8.0/)
> A modern, fully static, fast, secure fully proxied, highly customizable application dashboard with integrations for over 100 services and translations into multiple languages. Easily configured via YAML files or through docker label discovery.

[Tool to manage multiple linux machines : selfhosted](https://www.reddit.com/r/selfhosted/comments/18rzcpk/tool_to_manage_multiple_linux_machines/)

[Ubuntu server hardening on Racknerd : selfhosted](https://www.reddit.com/r/selfhosted/comments/18ryvip/ubuntu_server_hardening_on_racknerd/)

[GitHub - evan-buss/openbooks: Search and Download eBooks](https://github.com/evan-buss/openbooks)

Allow configuration of Prometheus storage.tsdb.retention.time #175
https://github.com/geerlingguy/internet-pi/issues/175

[linuxserver/Heimdall: An Application dashboard and launcher](https://github.com/linuxserver/Heimdall)

[Why are ports 80 (http) and 443 (https) automatically redirected to the DSM ports? : synology](https://www.reddit.com/r/synology/comments/j3h6o9/why_are_ports_80_http_and_443_https_automatically/)
[Prevent DSM Listening on Port 80/443 : synology](https://www.reddit.com/r/synology/comments/ahs3xh/prevent_dsm_listening_on_port_80443/)

[inotify limit in docker container : jellyfin](https://www.reddit.com/r/jellyfin/comments/ntis6u/inotify_limit_in_docker_container/)
`sh -c '(sleep 90 && echo 204800 > /proc/sys/fs/inotify/max_user_watches)&'`

## Security

https://github.com/9p4/jellyfin-plugin-sso
[Web authentication for reverse proxy : selfhosted](https://www.reddit.com/r/selfhosted/comments/132ouo9/web_authentication_for_reverse_proxy/)
[How to Configure Microsoft Azure Active Directory as Keycloak Identity Provider to Enable Single Sign-On for HCL Compass](https://www.hcl-software.com/blog/versionvault/how-to-configure-microsoft-azure-active-directory-as-keycloak-identity-provider-to-enable-single-sign-on-for-hcl-compass)

[Google Sign In](https://keycloakthemes.com/blog/how-to-setup-sign-in-with-google-using-keycloak)
https://stackoverflow.com/questions/36109708/google-oauth2-0-web-applications-authorized-redirect-uris-must-end-with-a-pub
Google Identity Provider doesn't seem to be working with `*.localhost` domains

[Questions about Jellyseerr. : jellyfin](https://www.reddit.com/r/jellyfin/comments/108dd2f/questions_about_jellyseerr/)
[GitHub - Ombi-app/Ombi: Want a Movie or TV Show on Plex/Emby/Jellyfin? Use Ombi!](https://github.com/Ombi-app/Ombi)

[Authelia | The Single Sign-On Multi-Factor portal for web apps](https://www.authelia.com/)
[Keycloak vs. Authentik vs. Authelia, help choose SSO : selfhosted](https://www.reddit.com/r/selfhosted/comments/13rr6i7/keycloak_vs_authentik_vs_authelia_help_choose_sso/)

[KeyCloak vs Authelia - Gary's Blog](https://www.awaimai.com/en/3111.html)

[I recently went down this road for my home lab and went with Authelia Keycloak w... | Hacker News](https://news.ycombinator.com/item?id=39335096)

[Authentik - Identity &... | Kasad BookStack](https://web.archive.org/web/20230402081530/https://wiki.kasad.com/books/kasadcom/page/authentik-identity-sso-provider)

[Lightweight keycloak alternative : selfhosted](https://www.reddit.com/r/selfhosted/comments/13sa28x/lightweight_keycloak_alternative/)

[casdoor/casdoor](https://github.com/casdoor/casdoor)
An open-source UI-first Identity and Access Management (IAM) / Single-Sign-On (SSO) platform with web UI supporting OAuth 2.0, OIDC, SAML, CAS, LDAP, SCIM, WebAuthn, TOTP, MFA, Face ID, RADIUS, Google Workspace, Active Directory and Kerberos
> what sketched you out about Casdoor? looking for similar capability to you, so curious why you pulled the rip cord on it
> Among other things: their blog. Chat GPT drivel with a sales orientation and fake profile pictures. That’s not who I want running my auth. Overall, I just couldn’t quite get comfortable with the org behind the product.

[lldap/lldap: Light LDAP implementation](https://github.com/lldap/lldap)
> It mostly targets self-hosting servers, with open-source components like Nextcloud, Airsonic and so on that only support LDAP as a source of external authentication.
> For more features (OAuth/OpenID support, reverse proxy, ...) you can install other components (KeyCloak, Authelia, ...) using this server as the source of truth for users, via LDAP.

[Allow two way syncing with other Authentication front ends (SCIM/LDAP edits) · Issue #301 · lldap/lldap](https://github.com/lldap/lldap/issues/301)

[Authelia vs Keycloak in home lab for SSO and authentication : selfhosted](https://www.reddit.com/r/selfhosted/comments/prz2gz/authelia_vs_keycloak_in_home_lab_for_sso_and/)

[Docs overview | mrparkers/keycloak | Terraform | Terraform Registry](https://registry.terraform.io/providers/mrparkers/keycloak/latest/docs)

## Self Hosting

[Cloud vs. Data Center vs. Basement: The Programmers’ Self Hosting Delusion With The Cloud | by Jan Kammerath | Oct, 2024 | Medium](https://medium.com/@jankammerath/cloud-vs-data-center-vs-basement-the-programmers-self-hosting-delusion-with-the-cloud-2274dc3ccb58)
[Reddit discussion](https://www.reddit.com/r/programming/comments/1fv4vnf/cloud_vs_data_center_vs_basement_the_programmers/)
> I mean, I don't know about yall, but I don't run any businesses out of my basement.
  I do run a home network primarily to learn skills.
  I have no SLAs, but I do hear complaints from a user I share a bed with
  when I take the NAS offline and Plex is down.

[dustinkirkland/ssh-import-id: git mirror of the official upstream at http://launchpad.net/ssh-import-id](https://github.com/dustinkirkland/ssh-import-id)
Nice utility to add public keys to servers.
