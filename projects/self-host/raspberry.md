# Raspberry PI

[Memory Usage always zero · Issue #3469 · google/cadvisor](https://github.com/google/cadvisor/issues/3469#issuecomment-2005749081)
```
Potential solution: append cgroup_memory=1 cgroup_enable=memory to /boot/cmdline.txt. I have not tried it yet as I can't restart my Pi right now.

[stefanprodan/dockprom#228 (comment)](https://github.com/stefanprodan/dockprom/issues/228#issuecomment-897086352)

[docker/for-linux#1112](https://github.com/docker/for-linux/issues/1112)
```

[Make your very own Kubernetes cluster from Raspberry PI](https://medium.com/nycdev/k8s-on-pi-9cc14843d43)
[Building a microcloud with a few Raspberry Pis and Kubernetes (Part 1)](https://mirailabs.io/blog/building-a-microcloud/)
[I broke my Kubernetes cluster running on Raspberry Pi](https://itnext.io/i-broke-my-kubernetes-cluster-running-on-raspberry-pi-355234a24d)
[Building a kubernetes cluster on Raspberry Pi and low-end equipment. Part 1](https://itnext.io/building-a-kubernetes-cluster-on-raspberry-pi-and-low-end-equipment-part-1-a768359fbba3)
Looks freaking awesome!!! 🥇⭐
[Walk-through — install Kubernetes to your Raspberry Pi in 15 minutes | by Alex Ellis | Medium](https://alexellisuk.medium.com/walk-through-install-kubernetes-to-your-raspberry-pi-in-15-minutes-84a8492dc95a)
Deploys OpenFaaS
[Five years of Raspberry Pi Clusters](https://medium.com/@alexellisuk/five-years-of-raspberry-pi-clusters-77e56e547875)

[Raspberry Pi: Projects, Models, Prices, How to Get Started](https://www.tomshardware.com/reviews/raspberry-pi,6308.html)

[Pi-hole](https://pi-hole.net/)
> Network-wide Ad Blocking
> A black hole for Internet advertisements

[How to easily configure WireGuard](https://www.stavros.io/posts/how-to-configure-wireguard/)
[Build your own private WireGuard VPN with PiVPN : programming](https://www.reddit.com/r/programming/comments/138l8kq/build_your_own_private_wireguard_vpn_with_pivpn/)
[Build your own private WireGuard VPN with PiVPN | Jeff Geerling](https://www.jeffgeerling.com/blog/2023/build-your-own-private-wireguard-vpn-pivpn)

---

[Raspberry Pi UPS? Do you need this? - YouTube](https://www.youtube.com/watch?v=oyp-m4VzxwQ)
> SunFounder PiPower 2 is a purpose-built UPS for the Raspberry Pi

## Live TV Tuner

[SC0054 Raspberry Pi | Mouser Portugal](https://pt.mouser.com/ProductDetail/Raspberry-Pi/SC0054?qs=T%252BzbugeAwjgxHF68DckjwA%3D%3D)
SC0054
RF TV HAT

[TV HAT Case for Raspberry Pi 4 (v2.0) | The Pi Hut](https://thepihut.com/products/tv-hat-case-for-raspberry-pi-4)

## Tablet

[SC1635 Raspberry Pi | Mouser Portugal](https://pt.mouser.com/ProductDetail/Raspberry-Pi/SC1635?qs=iLKYxzqNS77haif4zd6Z7w%3D%3D)
SC1635
Touch Display 2

## SSDs

[Drives de Estadi Sólido - SSD – Mouser Portugal](https://pt.mouser.com/c/?marcom=172363405)

## Gigabit Ethernet

[TG-3468 TP-LINK Placa de Rede Gigabit PCI Express](https://mauser.pt/catalog/product_info.php?products_id=047-2427)

## Unattended install

[raspberry pi unattended install - Pesquisa Google](https://www.google.com/search?client=firefox-b-d&q=raspberry+pi+unattended+install)
[debian-pi/raspbian-ua-netinst: Raspbian (minimal) unattended netinstaller](https://github.com/debian-pi/raspbian-ua-netinst)
[AutomatedRaspbianInstaller/preseed.cfg at master · aaronmelton/AutomatedRaspbianInstaller](https://github.com/aaronmelton/AutomatedRaspbianInstaller/blob/master/preseed.cfg)

[Raspberry Pi PXE Boot - Network booting a Pi 4 without an SD card - Linuxhit](https://linuxhit.com/raspberry-pi-pxe-boot-netbooting-a-pi-4-without-an-sd-card/#9-configure-the-rasperry-pi-4-bootloader-to-pxe-boot)
Seems super hackish and with lots of caveats.

## PiHole issues with DNS over UDP instead of TCP

`dig +notcp` doesn't work.
In practice this was affecting wireguard DNS setting.

[Pi-Hole on Network only resolves over TCP, not UDP - Help - Pi-hole Userspace](https://discourse.pi-hole.net/t/pi-hole-on-network-only-resolves-over-tcp-not-udp/26173)
