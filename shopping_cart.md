# Shopping Cart

[Tea stuff](https://www.one-tab.com/page/E1uIiquUQeKo0AuioHbCeQ)

[Mesh Routers](https://www.tomsguide.com/us/best-mesh-router,review-5191.html)

NAS and / or [Raspberry PI 4](https://mauser.pt/catalog/product_info.php?products_id=096-7400&src=raspberrypi)

[Aqara HomeKit Câmara de vigilância para interior, hub de câmara G2H, visão noturna, áudio bidirecional, câmara HD 1080p, Plug In Smart Home Bridge para sistema de alarme, funciona com IFTTT : Amazon.es: Bricolage e Ferramentas](https://www.amazon.es/-/pt/dp/B08X1PSG8K/ref=sr_1_6?crid=14X3U5YN4192K&keywords=Aqara&qid=1647899095&sprefix=aqara%2Caps%2C108&sr=8-6)

## Arrumação

[Amazon Basics - Cestas de metal empilháveis para armazenamento, para cozinha ou casa de banho : Amazon.es: Casa e Cozinha](https://www.amazon.es/-/pt/dp/B07XYM1LG3/ref=sr_1_1?crid=39187RADV3LH6&keywords=stackable+basket&qid=1662622054&sprefix=stackable+basket%2Caps%2C101&sr=8-1)
[mDesign Cesta de armazenamento multiusos - cesta organizadora alta com alças, cesta metálica de arame e compacta para cozinha, casa de banho, escritório e outros quartos, branco fosco : Amazon.es: Casa e Cozinha](https://www.amazon.es/-/pt/dp/B071W9L3LB/ref=sr_1_11?crid=39187RADV3LH6&keywords=stackable+basket&qid=1662622054&sprefix=stackable+basket%2Caps%2C101&sr=8-11)
[mDesign Conjunto de 4 caixas de plástico organizadoras pequenas, organizador prático de despensa sem tampa, organizador de frigorífico com ranhuras laterais de ventilação, transparente : Amazon.es: Casa e Cozinha](https://www.amazon.es/-/pt/dp/B07PJ21B2X/ref=sr_1_40?crid=39187RADV3LH6&keywords=stackable%2Bbasket&qid=1662622054&sprefix=stackable%2Bbasket%2Caps%2C101&sr=8-40&th=1)
[mDesign Conjunto de 2 prateleiras empilháveis para armazenamento de cozinha, prateleira metálica de cozinha com pernas dobráveis, organizador moderno de armários para louça, conservas e especiarias - prata : Amazon.es: Casa e Cozinha](https://www.amazon.es/-/pt/dp/B07F18KWTC/ref=sr_1_55?crid=39187RADV3LH6&keywords=stackable%2Bbasket&qid=1662622117&sprefix=stackable%2Bbasket%2Caps%2C101&sr=8-55&th=1)
[Evance 10 caixas organizadores de gaveta de plástico, organizadores transparentes para gavetas, bandejas de maquilhagem, estojos de artigos de papelaria para casa de banho, cozinha, escritório, mesa (transparente) : Amazon.es: Casa e Cozinha](https://www.amazon.es/-/pt/dp/B08B8593QL/ref=sr_1_7?crid=2FENEEC3WNNXP&keywords=pp%2Bmakeup%2Bbox&qid=1662622468&sprefix=pp%2Bmakeup%2Bbox%2Caps%2C86&sr=8-7&th=1)
[Small Stackable Basket | Target Australia](https://www.target.com.au/p/small-stackable-basket/65059395)

## Speakers

https://www.reddit.com/r/AverageJoeAudiophile/comments/3uoksp/i_have_xxxxx_to_spend_what_should_i_buy_bookshelf/

1. Audioengine A2+ Wireless (white)
2. Kanto YU2 Powered Bookshelf Speakers

[Micca MB42X Bookshelf Speakers](https://www.amazon.com/Micca-MB42X-Bookshelf-Speakers-Tweeter/dp/B00E7H8GG2) - "cheap" amazon choice
[Polk Signature Series S15 Bookshelf Speakers](https://www.amazon.com/dp/B01LVWWZS0)
Bowers & Wilkins MM-1
[Polk Audio T15 100 Watt Home Theater Bookshelf Speakers](https://www.amazon.com/dp/B002RJLHB8)
[Edifier R1280T Powered Bookshelf Speakers](https://www.amazon.com/dp/B016P9HJIA/) - sexy wood

[Edifier R1850DB Active Bookshelf Speakers with Bluetooth and Optical Input - 2.0 Studio Monitor Speaker - Built-in Amplifier with Subwoofer Line Out](https://www.amazon.com/Edifier-R1850DB-Bookshelf-Speakers-Bluetooth/dp/B073W1R4XQ)
[Wharfedale Diamond 9.1](https://www.amazon.co.uk/dp/B002WJAQY2/ref=psdc_199618031_t1_B073XQJG7K)
Mackie Studio Monitor, Black w/green trim, 3-inch (CR3) - That green 😬

## Mugs

[Cup<T>](https://www.zazzle.pt/caneca_de_cafe_em_dois_tons_copo_generico_t-168953054069593498)

## Old PC

✅ Watch space
Kindle
DIY
Raspberry PI
Xiaomi Mi Band 2

Steam
Home Office
https://about.gitlab.com/handbook/marketing/readmes/dmurph/#work-from-home-office-setup
https://remote.com/blog/maximum-productivity-home-office
https://twitter.com/andreasklinger/status/1238325463300202497
Webcam / Camera - Alpha 6400 APS-C Interchangeable Lens Mirrorless Camera - https://electronics.sony.com/imaging/interchangeable-lens-cameras/aps-c/p/ilce6400-b (can be found cheaper 2nd hand)
https://www.lifeandmuse.com/the-home-office-reveal
https://twitter.com/zack415/status/1249741767814959104/photo/1
https://twitter.com/mg/status/1242441484294635521/photo/1
https://www.amazon.com/dp/B000BNG4VU

[The Remote Work Starter Kit — Desk Stuff Edition | by Gant Laborde | Red Shift](https://shift.infinite.red/the-remote-work-starter-kit-desk-stuff-edition-6b841b8c745)
* An uninterruptable power supply (UPS)
* Two Computers
* Meeting key light
* Meeting sound dampener
* A quality microphone / camera setup
* All the ergonomics
* Blue-blocker glasses
* Coding slippers
* Amazing desk notepad
* Sit/Stand Desk
[The Remote Work Starter Kit — Office Stuff Edition | by Gant Laborde | Red Shift](https://shift.infinite.red/the-remote-work-starter-kit-office-stuff-edition-5f1d360bfe70)
* A second uninterrupted power supply UPS
* Door draft guard + weather stripping
* Modem cable surge protection
* A Door lock
* Good coffee & vitamins
* Air plant or purifier
* EZ cleaning stuffs
* Backup Internet
* Light dimming stickers / Slide stopping stickers
* A high-quality voice activated assistant
[Improve Your Virtual Setup — Video | by Gant Laborde | Google Developers Experts | Medium](https://medium.com/google-developer-experts/improve-your-virtual-setup-video-3b0b9dc15833)
1. Webcams
2. Lighting
3. Backdrops

Computer SetUp ([Working From Home Setup 3.0. New Home, New Office, New Setup. | by Paul Alvarez | Techuisite | Jan, 2021 | Medium](https://medium.com/techuisite/working-from-home-setup-3-0-7bef964b6b4b))
    - Standing Desk (e.g.: https://www.thehumansolution.com/adjustable-height-desks/)
    - ✅ Chair
    - UltraWide Monitor
    - Printer
    - Computer
    - ✅ Duster
    - Drawable Walls!!!
    - Phone Docking
    - Virtual Reality
    - Kinnect
    - Cable Management
    - ✅ Steering Wheel
    - ✅ Gamepad
    - Sound System - Audio
        * Phones
            - ✅ Sennheeiser HD 6XX (Massdrop)
        * Microphone
            - ✅ Antlion Modmic wireless
            -  Shure SM7B Vocal Dynamic Microphone, Cardioid
        * Audio Interface
            - Focusrite SCARLETT-2I2-3RD-GEN 2x2 USB 2.0 Audio Interface
            - MOTU M4 - https://motu.com/en-us/products/m-series/m4/
        * Speakers
        * Wireless headphones:
            - ❌ Sennheiser Momentum 2.0 Wireless with Active Noise Cancellation- Black
            - ❌ Sony MDR-1000x
            - ❌ Bose QuietComfort 35 wireless headphones II
            - ✅ Sennheiser PXC 550
Smart Home (Alexa / Dimmable lights (program with sensor and computer))

Drums (China / Splash / Cowbell / Acoustic / Midi to USB)
✅ Trumpet
Gym Stuff (Sneakers football, running, low weight dumbbell, matching weight, bench, elastico)
Kitchen Stuff
Acessories / Everyday Carry(https://www.youtube.com/watch?v=BJzQJ3zo7Zk):
    - Cap
    - ✅ Sunglasses
    - ✅ Watch
    - Knife
    - Pen
    - Rotring 1904255
Sleep Mask
Dog
Lego
✅ Orient Bambino

HTPC
NAS

projector
Bluetooth stealth fones

✅ Shaver
http://www.dorcousa.com/pace-6-cartridges-24-refills-save-20/
http://www.dorcousa.com/pace-6-combo-set/

https://www.artifactpuzzles.com/collections/really-hard-puzzles/products/artifact-puzzles-erin-hanson-crystal-grove

## Cooking

https://sardelkitchen.com/collections/frontpage
https://www.amazon.com/shop/notanothercookingshow


## Music

Synth / MIDI Controller:

https://www.thomann.de/pt/akai_mpk_mini_mk3.htm
https://www.thomann.de/pt/m_audio_oxygen_pro_mini.htm <---
