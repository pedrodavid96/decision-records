# mini-pc

[Why ed25519 Key is a Good Idea](https://www.unixtutorial.org/how-to-generate-ed25519-ssh-key/#:~:text=Why%20ed25519%20Key,copy%2Fpaste%20them)
Compared to the most common type of SSH key – RSA – ed25519 brings a number of cool improvements:
* it’s faster: to generate and to verify
* it’s more secure
* collision resilience – this means that it’s more resilient against hash-function collision attacks (types of attacks where large numbers of keys are generated with the hope of getting two different keys have matching hashes)
* keys are smaller – this, for instance, means that it’s easier to transfer and to copy/paste them

```bash
ssh-keygen -t ed25519
```

[`ssh-import-id`](https://github.com/dustinkirkland/ssh-import-id) is already included in Ubuntu (server at least).

So you can public keys to ~/.ssh/authorized_keys automatically by running the following command (replace with your own username):

```bash
ssh-import-id gh:PedDavid
```

## Thermals

It seems I went with one of the worst ^[1][ak2-review] mini-pc's available (probably why it was one of the cheapest as well).
[ak2-review]: https://www.youtube.com/watch?v=FlIenneQyYI "Budget Performance? Kamrui AK2 Plus (FIREBAT) Mini PC Review"

Noticed that when Flux was reconciliating the system became very unresponsive (grafana, running on it, would basically not load).
"Luckily" I've a few different Grafana's on my homelab (seriously, I need to decide which one to make official and bullet-proof it...).

With it I was able to stitch up the following panels:
![thermal-throttling](./thermal-throttling.png)

These might not be super accurate but I had to stitch them asap to decide if I was to go get some thermal paste since the store was closing
in half an hour...

And actually, I'm not entirely sure what happened because as you can see, after a reboot the temperatures normalized a bit lower,
but with some pressure on it it would start throttling.

After badly cleaning it up and applying a (randomly bought) thermal paste (first time doing it... elaborate),
here's the high level overview:

![thermal-paste-applied](./thermal-paste-applied.png)

Here my grafana skills might be undermining the accomplishment
but I wanted to give the full view with how things were specially
before the restart.

![in-detail](./stress-tests.png)

Even though the line looks close it's almost a 10ºC (TODO:)
difference and those circles actually highlight stress tests
(sadly different ones from "Flux" reconciliation running)
and we can see at max temperatures get to basically the same max as the "Max Idle" before.

Additionally, although almost non-existent there were actually
a few throttles even "on Idle" before.
Now even with the stress test there are no throttles!

A few more references:
[NiPoGi AK2 Plus Mini PC cooling? : MiniPCs](https://www.reddit.com/r/MiniPCs/comments/1awjjhe/nipogi_ak2_plus_mini_pc_cooling/)
> he factory thermal paste is low grade and becomes thermal clay.
> consider reaching out to NiPoGi for the latest BIOS.
> Some of our accounts with the AK2 Plus and its clones stated they saw better performance and thermals with the revised firmware.
> Apparently some of the fan curves and processor voltages have been updated after Intel's August(?) release.

## Future Work

Well, currently it's winter here and my house is rather cold...
Maybe I'll have to think about something like [alternative Case for AK2 Plus](https://www.printables.com/model/926938-alternative-case-for-ak2-plus-intel-n100-mini-pc-f)

Other references:
[Topton N100 fan : homelab](https://www.reddit.com/r/homelab/comments/1hl6xlm/topton_n100_fan/)

## Expansion / Upgradability

[Amazon.com: 2 Pack NGFF M2 to PCI-E 4X 1X Slot Riser Card, M Key M.2 2260 2280 SSD Port to PCI-e Adapter Converter Multiplier for ETH GPU BTC Mining with LED Voltage Indicator : Electronics](https://www.amazon.com/EXPLOMOS-NGFF-Adapter-Power-Cable/dp/B074Z5YKXJ?keywords=m.2+to+pci+riser&qid=1570509593&sr=8-13)

## GMKtec G3 Mini PC

### BIOS Configuration

#### Chipset

PCIE Lan Configuration -> Wake on LAN Enable (already enabled)

#### Power

Power Limit Select -> increase to 15w

#### Advance

* Power & Performance -> CPU Power Management Control
	C States - Enable ^[[1]](#c-states)

* Network Stack Configuration -> Enable PXE Boot
	Restart and go to bios again to enable it in the boot override options
 	necessary since disk already has windows installer

_Note:_ You can also see the MAC address in this tab

[1]: [Ryzen 1700 or Dual Xeon E5-2680 V2 : r/homelab][c-states]

> Same here, I also use my 1700x since I upgraded my gaming rig.
> Don't forget to disable C-States in bios or you will have a bad experience running the 1700 headless!
> https://forum.proxmox.com/threads/proxmox-ve-5-0-ryzen-7-1700x-crashes-daily.36123/

[c-states]: https://www.reddit.com/r/homelab/comments/10tjgpg/ryzen_1700_or_dual_xeon_e52680_v2/

## AMD AM4 (?)

[Unexpected benefit with Ryzen - reducing power for home server : r/Amd](https://www.reddit.com/r/Amd/comments/9cixzt/unexpected_benefit_with_ryzen_reducing_power_for/)

>  if this is you, you can also undervolt 50-100mv,
> add a slight drop in soc voltage(for 2-4w savings there) and
> push that TDC/EDC even further down at no performance cost.

> i've gone further and have my 2700x switch to a 2.2ghz max power plan in windows(process lasso's idlesaver)
> so that it aggressively idles as much as possible...
> resulting in even a 16thread cinebench run sitting in the high 30w range(package, not wall) for ~800pts. > typical idle w firefox/vm's etc running drops from 55w or so to 22-24w just from capping max freq when time to completion doesnt matter.
> in a 70f room this makes my 2700x essentially passively cooled(~300rpm on prism)
> with just a gentle breeze across vrm from rear exhaust.
