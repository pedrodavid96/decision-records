# Gradle

./gradlew help --task :wrapper
./gradlew wrapper --gradle-version 7.6 --distribution-type ALL
./gradlew run --args "Test arguments"

[GitHub - jjohannes/gradle-project-setup-howto: How to structure a growing Gradle project with smart dependency management?](https://github.com/jjohannes/gradle-project-setup-howto)
[GitHub - jjohannes/idiomatic-gradle: How do I idiomatically structure a large build with Gradle 7.2+?](https://github.com/jjohannes/idiomatic-gradle)
[GitHub - liutikas/gradle-best-practices](https://github.com/liutikas/gradle-best-practices)
[GitHub - jjohannes/understanding-gradle: The Understanding Gradle video series introduces the concepts of the Gradle Build Tool one-by-one in short videos.](https://github.com/jjohannes/understanding-gradle#readme)

[The Problem with Gradle](https://www.bruceeckel.com/2021/01/02/the-problem-with-gradle/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/kykj74/the_problem_with_gradle/)
[Reddit discussion](https://www.reddit.com/r/java/comments/ksf3ku/the_problem_with_gradle/)
**Note:** Most of the issues I haven't faced yet and / or simply do not agree.
          Besides, I don't know other build system that deal with them better.
          People in reddit discussions are suggesting gradle as an alternative 😬
1. You’re not Configuring, You’re Programming
2. Groovy is Not Java
3. Gradle Uses a Domain-Specific Language
4. There are Many Ways to do the Same Thing
5. Magic
6. The Lifecycle
[Rebuttal: The problem with Gradle: really?](https://melix.github.io/blog/2021/01/the-problem-with-gradle.html)

[The java library plugin](https://docs.gradle.org/current/userguide/java_library_plugin.html)

Contains info on **API and implementation separation**,
the key difference between `api` and `implementation` definitions as well as
showing the details of other dependency configurations.

Investigate `compileOnly` dependencies: [search](https://www.google.com/search?client=firefox-b-d&channel=crow&q=gradle+compileOnly+example)

[Introduction to Dependency Management](https://docs.gradle.org/current/userguide/introduction_dependency_management.html)

[Managing Transitive Dependencies](https://docs.gradle.org/current/userguide/managing_transitive_dependencies.html)

[Java 9 modules tutorial](https://guides.gradle.org/building-java-9-modules/#what_you_ll_need)

[Kotlin DSL Primer](https://docs.gradle.org/current/userguide/kotlin_dsl.html)

[Writing Build Scripts: Extra properties](https://docs.gradle.org/current/userguide/writing_build_scripts.html#sec:extra_properties)

[Java testing: using JUnit 5](https://docs.gradle.org/current/userguide/java_testing.html#using_junit5)

[Execute specific test classes](https://stackoverflow.com/questions/25125036/how-to-execute-multiple-tests-classes-in-gradle-that-are-not-in-the-same-package)

[Minimizing copy and paste build.gradle code between projects](https://discuss.gradle.org/t/minimizing-copy-and-paste-build-gradle-code-between-projects/32010)
Create a new Plugin that simply calls the other plugins.

[Gradle Maven settings plugin](https://github.com/mark-vieira/gradle-maven-settings-plugin)
Use maven `settings.xml` file, with encrypted properties.

## [Stop using Gradle buildSrc. Use composite builds instead](https://proandroiddev.com/stop-using-gradle-buildsrc-use-composite-builds-instead-3c38ac7a2ab3)
You are invalidating the whole project for any change within buildSrc, usually without there being a valid reason. We can keep the sweetness of Kotlin whilst avoiding this.

This has become very popular in recent years and it is a common practice to define project-specific tasks or **storing dependencies in buildSrc**.

[From the docs:](https://docs.gradle.org/current/userguide/organizing_gradle_projects.html#sec:build_sources)
A change in buildSrc causes the whole project to become out-of-date so this means we would be rebuilding the whole project.

Alternative: Use Composite builds — `includeBuild`

[Open issue to "Convert `buildSrc` to be an implicit, included build"](https://github.com/gradle/gradle/issues/2531)

---

[Locking dependency versions](https://docs.gradle.org/6.4.1/userguide/dependency_locking.html)
Use of dynamic dependency versions (e.g. 1.+ or [1.0,2.0)) makes builds non-deterministic.

[Building, testing and running Java Modules](https://docs.gradle.org/6.4.1/release-notes.html#java-modules)

[GradleGitDependenciesPlugin](https://github.com/alexvasilkov/GradleGitDependenciesPlugin)

[Faster builds with highly par­al­lel GitHub Actions](https://rnorth.org/faster-parallel-github-builds/)

[Introducing file system watching](https://blog.gradle.org/introducing-file-system-watching)
```bash
$ ./gradlew --watch-fs :core:testClasses
```

[How to add Checkstyle and FindBugs plugins in a gradle based project](https://medium.com/@raveensr/how-to-add-checkstyle-and-findbugs-plugins-in-a-gradle-based-project-51759aa843be)

[Top Android tools libraries in 2021 | by Houssein Ouerghemmi | CodeShake | Jan, 2021 | Medium](https://medium.com/codeshake/top-android-tools-libraries-in-2021-3374d0b48368)
* ben-manes/gradle-versions-plugin
* radarsh/gradle-test-logger-plugin - beautiful console logs to your unit test execution
* JakeWharton/diffuse
* passy/build-time-tracker-plugin - this could be cool with GitLab MR metrics

[Gradle Goodness: Shared Configuration With Conventions Plugin - Messages from mrhaki](https://blog.mrhaki.com/2021/02/gradle-goodness-shared-configuration.html)
Shows how to declare "global" dependencies versions

[How to automatically check dependency licenses in Gradle projects using Gradle License Report](https://sivrikayafirat.medium.com/how-to-automatically-check-dependency-licenses-in-gradle-projects-using-gradle-license-report-fad5cc581452)

[Gradle Doctor](https://runningcode.github.io/gradle-doctor/)
> The right prescription for your Gradle build.

[10 ideas to improve your Gradle build times [Part II]](https://medium.com/dipien/10-ideas-to-improve-your-gradle-build-times-part-ii-9551bb10e9bd#61cc)
> 5. Filters on Gradle Repositories declarations

[Improve your Gradle build times by disabling plugins](https://medium.com/dipien/improve-your-gradle-build-times-by-only-applying-needed-plugins-5cbe78319e17)
Putting plugins behind flags / Environment Variables

[3 tips to configure the Gradle Wrapper & Distribution](https://medium.com/dipien/3-tips-to-configure-the-gradle-wrapper-distribution-a46f9b20ddde)
> 2. Switch to Gradle Binary Distribution on your CI environment

[Popular Gradle mistakes (and how to avoid them) | blog.allegro.tech](https://blog.allegro.tech/2024/11/popular-gradle-mistakes-and-how-to-avoid-them.html)
[The Ultimate Gradle Kotlin Beginner's Crash Course For 2025 - YouTube](https://www.youtube.com/watch?v=RCRQlz78wCg)
