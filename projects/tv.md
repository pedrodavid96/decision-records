# TV

[The Ultimate Guide to Configuring Live TV & DVR with Plex! - YouTube](https://www.youtube.com/watch?v=Q5okoyPewyU)
[Amazon.es : cable guide](https://www.amazon.es/s?k=cable+guide&crid=2GZYY8E8NEY2X&sprefix=cable+guide%2Caps%2C139&ref=nb_sb_noss_1)
[Live TV | Jellyfin](https://jellyfin.org/docs/general/server/live-tv/)
[Raspberry pi 5 tvheadend with TV hat on libreelec? : libreELEC](https://www.reddit.com/r/libreELEC/comments/1eh1nrh/raspberry_pi_5_tvheadend_with_tv_hat_on_libreelec/)
[RASPBERRY PI Sintonizador TDT para Raspberry Pi - DVB-T TV uHAT](https://mauser.pt/catalog/product_info.php?products_id=096-6495)
[SC0054 Raspberry Pi | Mouser Portugal](https://pt.mouser.com/ProductDetail/Raspberry-Pi/SC0054?qs=T%252BzbugeAwjgxHF68DckjwA%3D%3D)
[HAT Add-On Boards - Raspberry Pi | Mouser](https://pt.mouser.com/new/raspberry-pi/raspberry-pi-hat-add-on-boards/)
[Hauppauge UK | WinTV-dualHD Product Description](https://www.hauppauge.co.uk/site/products/data_dualhd.html)
[TV HAT Raspberry Pi 5 - Page 2 - Raspberry Pi Forums](https://forums.raspberrypi.com/viewtopic.php?t=359876&sid=48a654310a868326f0c11b9a62bda1d3&start=25)
[DTT Deployment Data - DVB](https://dvb.org/solutions/dtt-deployment-data/)
[Newbie question: I want to split the coax cable coming from my antenna to 2 TVs. What kind of splitter do I need, and where can I buy it? And does it need power? Thanks! : ota](https://www.reddit.com/r/ota/comments/13ysw16/newbie_question_i_want_to_split_the_coax_cable/)
[2-way coaxial splitter - Pesquisa Google](https://www.google.com/search?client=ubuntu-sn&hs=ajH&sca_esv=d4a5450862dcd9df&channel=fs&sxsrf=ADLYWIIPFFbsjnH8svtImO0EIxY4A6cgUw:1734363183037&q=2-way+coaxial+splitter&spell=1&sa=X&ved=2ahUKEwjWxr6rzqyKAxVAQPEDHcRRI3sQBSgAegQICxAB&biw=2165&bih=1488&dpr=1)

[Open source PVR thoughts: Tvheadend vs. MythTV vs. Jellyfin vs. others? : r/cordcutters](https://www.reddit.com/r/cordcutters/comments/yjjhwr/open_source_pvr_thoughts_tvheadend_vs_mythtv_vs/)

[Tvheadend Docs](https://docs.tvheadend.org/documentation)

[Raspberry Pi portable TV Recorder. Pi TV HAT - YouTube](https://www.youtube.com/watch?v=dZPsOG0sOpw)

[Cutting the cord: setting up a DVB-T2 server with a Raspberry Pi TV-Hat - YouTube](https://www.youtube.com/watch?v=eTSd8klU_yE)

[Watch Live TV on a Raspberry Pi 4 with the Raspberry Pi TV-Hat Add On - YouTube](https://www.youtube.com/watch?v=VQsAFhHAjoE)
