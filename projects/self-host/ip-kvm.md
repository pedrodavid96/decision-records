# IP KVM

Known as "KVM over IP" is a solution to control the PC over "IP", meaning remotely controlling it.

This is specially useful to power on the system and configure the BIOS.

For other functionalities there's usually better approaches.
* Setup new a new OS -> I'd rather just PXE boot
* Control remote system with OS installed -> Dedicated software (e.g.: SSH, rustdesk)
  (I guess this could still be useful if we have a good reason to not have that software installed)

## Options

### [PiKVM - Open and inexpensive IP-KVM on Raspberry Pi](https://pikvm.org/)

Fully open source.
Can be diy'ed.

### [JetKVM - Control any computer remotely](https://jetkvm.com/)

Pre-Built
Incredible design.
"Looks polished" (although its still on kickstarter).
Supposedely it will be open sourced.

### [sipeed/NanoKVM: Affordable, Multifunctional, Nano RISC-V IP-KVM](https://github.com/sipeed/NanoKVM)

Interesting that it "leverages" RISC-V.
Probably the smaller footprint.

Saw some threads regarding not being fully open-sourced & vulnerabilities...
This might not be relevant anymore but need to investigate further.

### [TinyPilot: Build a KVM Over IP for Under $100 · mtlynch.io](https://mtlynch.io/tinypilot/)

[Video Capture Card HDMI to USB 3.0 Audio 1080P HD Game Recording Live Streaming | eBay](https://www.ebay.com/itm/396212285182?_skw=hdmi+capture+card&itmmeta=01JMHZEER7DKTAR68R2439VT54&hash=item5c4017aefe:g:osgAAOSwQf5nsBdG&itmprp=enc%3AAQAKAAAA8FkggFvd1GGDu0w3yXCmi1fR7rqglmYK2vcJjMNSOWr07h%2FRmnD88pC4ZLMuOCP9Zu2mDQGh6xRksaF6n6L%2Fq3bn%2BfbNcK1MFZTgzbQUnq57t3KWWyfNkbcghMJqdH7yRUa4aeJ1WotM3c02K8ttKmgQSWJW4ENFpRf45yi%2F4FKLUo%2B6o4rEhgF3%2FCpdGxWll8tpiAEyrGUZV8Vb0xS1IS1IYWGyuI18kl8CZ2O4NvDcvJof6VH279msv81S4Zy3%2FnNpTNLKHVnrYijV9nXeNEQXQHmnaUyeN%2FPa2b%2BPOCYiDTaaovoEtLVrqTR0Uv2jOw%3D%3D%7Ctkp%3ABk9SR6Tsub-kZQ)

