# Game Engine

[Interesting discussion regards modern C++ usage in game engine development](https://www.reddit.com/r/programming/comments/d2nry0/3d_game_tutorial_in_c_from_scratch_part_12/)

[Lag Compensation - Fair Play for all Pings](https://vercidium.com/blog/lag-compensation/)

[The Poor Man's Netcode](http://etodd.io/2018/02/20/poor-mans-netcode/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/7ywahe/the_poor_mans_netcode/)

[Space Game: A std::variant-Based State Machine by Example](https://www.bfilipek.com/2019/06/fsm-variant-game.html)

[Entity-Component-System (ECS) back and forth: the talk - Reddit Thread](https://www.reddit.com/r/gamedev/comments/c1ty13/entitycomponentsystem_ecs_back_and_forth_the_talk/)
[Overwatch uses an ECS (Entity/Component/System) update model! (can't wait to test it in Unity) : gamedev](https://www.reddit.com/r/gamedev/comments/apegca/overwatch_uses_an_ecs_entitycomponentsystem/)
[Cowboy Programming » Evolve Your Hierarchy](https://cowboyprogramming.com/2007/01/05/evolve-your-heirachy/)

[ECS back and forth](https://skypjack.github.io/2019-03-21-ecs-baf-part-2-insights/)https://www.reddit.com/r/gamedev/comments/csythi/ecs_back_and_forth_part_4_insights_hierarchies/
https://skypjack.github.io/2019-08-20-ecs-baf-part-4-insights/

[Data Structures for Entity Systems: Contiguous memory – T-machine.org](https://t-machine.org/index.php/2014/03/08/data-structures-for-entity-systems-contiguous-memory/)

[Comparison of Modern Graphics APIs](https://alain.xyz/blog/comparison-of-modern-graphics-apis)
Very cool article in a very cool blog 🥇⭐
1. Initialize API - Entry Point
2. Physical Device
3. Logical Device
4. Queue
5. Command Pool
6. Frame Backings
   * Window Surface
   * Swapchain
   * Frame Buffers
7. Initialize Resources
   * Texture
   * Buffer
   * Shader
   * Shader Bindings
   * Pipeline
   * Command Buffer
   * Command List
   * Fence
   * Semaphore

[Fix Your Timestep!](https://gafferongames.com/post/fix_your_timestep/)

[CppCon 2017: Guy Somberg “Game Audio Programming in C++”](https://www.youtube.com/watch?v=M8Bd7uHH4Yg)
Awesome awesome awesome talk about game audio programming! 🥇⭐

[8 Frames in 16ms: Rollback Networking in Mortal Kombat and Injustice 2](https://www.youtube.com/watch?v=7jb0FOcImdg)

[Lights and Shadows](https://ciechanow.ski/lights-and-shadows/)
Haven't read yet but the [comments](https://www.reddit.com/r/programming/comments/k9qsrd/lights_and_shadows/) **highly praise it**.

[Rust GPU](https://github.com/EmbarkStudios/rust-gpu/releases)
> This is a very early stage project to make Rust a first-class language and ecosystem for building GPU code
Writing shaders in Rust
[Discussion | It's now possible to write GPU shaders in Rust, because why not?](https://www.reddit.com/r/programming/comments/k7x45d/its_now_possible_to_write_gpu_shaders_in_rust/)

[Fast Inverse Square Root — A Quake III Algorithm](https://www.youtube.com/watch?v=p8u_k2LIZyo)
[Reddit discussion](https://www.reddit.com/r/programming/comments/kmcntc/quake_iiis_fast_inverse_square_root_explained_20/) - very important discussion that hardware now has faster ways to compute than the described software implementation
Resources:
* [IEEE 754](https://en.wikipedia.org/wiki/IEEE_754) - Standard for Floating-Point Arithmetic
* [Newton's Method](https://en.wikipedia.org/wiki/Newton%27s_method) -  root-finding algorithm which produces successively better approximations to the roots

[The Rendering of Rise of the Tomb Raider](https://www.elopezr.com/the-rendering-of-rise-of-the-tomb-raider/)

[Computer Graphics from Scratch: now as a real book! : programming](https://www.reddit.com/r/programming/comments/lbmda2/computer_graphics_from_scratch_now_as_a_real_book/)
[Eric Arnebäck](https://erkaman.github.io/posts/beginner_computer_graphics.html)

[Naughty Dog at SIGGRAPH 2020 || Naughty Dog](https://www.naughtydog.com/blog/naughty_dog_at_siggraph_2020)

[NVIDIA Nsight Graphics | NVIDIA Developer](https://developer.nvidia.com/nsight-graphics)

Culling via Compute Shaders
