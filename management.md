# Management

[The 11 Best Project Management Tools Used by Top Technical Teams from Netflix... | Planio](https://plan.io/blog/best-project-management-tools-used-by-top-technical-teams)

[Mythical Man Month: Why more manpower doesn’t mean faster work](https://www.youtube.com/watch?v=Xsd7rJMmZHg)

[Three drawings I use to explain agile](https://medium.com/swlh/three-drawings-i-use-to-explain-agile-9c0ef15b64b8)

[Technical Debt | Martin Fawler](https://martinfowler.com/bliki/TechnicalDebt.html)

[5 arguments to make managers care about technical debt](https://understandlegacycode.com/blog/5-arguments-to-make-managers-care-about-technical-debt/)
1. Refactoring will reduce the volatility in the marginal cost of features
2. In the last 3 months, we spent 63% of development budget in fixing quality issues
3. We took technical loans to ship faster, we need to pay back some debt to keep a low time-to-market
4. We can reduce our turnover by investing 10% of our time in our code quality
5. Investing 20% of budget in refactoring will cut FRT in half with a positive ROI on devs productivity
**Refactor as you go.**

[TechnicalDebtQuadrant | Martin Fawler](https://martinfowler.com/bliki/TechnicalDebtQuadrant.html)

[When developers disagree](https://www.reddit.com/r/programming/comments/c3bjuo/when_developers_disagree/)

[Maybe Agile Is the Problem](https://www.reddit.com/r/programming/comments/c2s81g/maybe_agile_is_the_problem/)

[How to stop wasting engineering time on technical debt](https://blog.stepsize.com/how-to-stop-wasting-engineering-time-on-technical-debt/)

[A Taxonomy of Tech Debt by Riot Games](https://technology.riotgames.com/news/taxonomy-tech-debt)

[https://codingsans.com/blog/technical-debt](https://codingsans.com/blog/technical-debt)

[Yes, You Should Estimate Software Projects](https://blog.pragmaticengineer.com/yes-you-should-estimate/)

[Most Meetings Are Pointless (But They Don’t Have To Be)](https://www.7pace.com/blog/meetings-are-pointless)
* Meetings Distract People from Meaningful Work
* Meetings Don't Stay On Topic
* Meetings Include People Who Don’t Need To Be There
* Meetings Multiply Wasted Time
* Meetings Aren’t Work

Reasons We Need Meetings Anyway:
* Meetings Help Us Communicate Better
* Meetings Help Us Make Decisions
* Meetings Keep Projects On Track
* Meetings Provide an Open Forum for Organizations

How to Stop Having Pointless Meetings:
1. Decide If You Actually Need a Meeting
2. Create a Meeting Agenda
3. Set an End Time and Stick To It
4. Have a Leader for Every Meeting
5. Only Invite People Who Really Need to Be There
6. Don’t Be Afraid To Decline Meeting Invites


[Advantages of the “As a user, I want” user story template.](https://www.mountaingoatsoftware.com/blog/advantages-of-the-as-a-user-i-want-user-story-template)
1. Requirements are put in the first person so people more closely indetify with them.

   Paul McCartney has said this has been one of the reasons Beatles songs were so amazingly popular (use a lot of personal pronouns). [Source](https://bigthink.com/robby-berman/the-beatles-hit-making-secret-ingredient-personal-pronouns)
2. Having a structure to the stories actually helps the product owner prioritize
3. If that text is unnecessary, why do we mentally mouth the words to ourselves or even glance at the heading while reading the row?

   I've heard an argument that writing stories with this template actually suppresses the information content of the story because there is so much boilerplate in the text.

   With that in mind, we still mentally mouth the words when they're not present

[How to Write Good User Stories in Agile Software Development](https://blog.easyagile.com/how-to-write-good-user-stories-in-agile-software-development-d4b25356b604)

1. Write it as: As a <type of user>, I want <some feature> so that <some reason>
2. Why not simply use Tasks (the how)?

   The fact is, it’s easy to get buried in a contextless, feature developing cycle.
3. Acceptance Criteria

   Shouldn't include stuff like: Code Review, Performance testing, Acceptance and functional testing
   Because this should already be on the DoD (Definition of Done) e.g.:
   * unit/integrated testing
   * ready for acceptance test
   * deployed on demo server
   * releasable

[As a developer, I want this user story template to stop...](https://www.storyhub.io/blog/as-a-developer-i-want-this-user-template-to-stop/)
The main argument against is that the "Defaultness" leads to "Laziness" which in turn leads to
 generic an not useful user stories.
Proposed solution:
* Don't use it by default for every single story. Consider visual alternatives (diagrams, drawings, screenshots)
* Make the user stories visually appealing - Self advertising, but the platform actually looks cool.

[“As a, I want, So that” Considered Harmful](https://blog.crisp.se/2014/09/25/david-evans/as-a-i-want-so-that-considered-harmful)
* Arguably the most important is the reason to change.

  From implementation point of view, doing nothing is cheapest, there needs to be good reason to change

* The reason (usually in the "So that" clause) is last

  "Chris Matts" is credited for the alternative

  > “In order to <achieve some business value>,
    As a <stakeholder type>
    I want <some new system feature>”

* A story does not always need to be told from the user’s point of view.

  > Good software is like good service in a restaurant – often we are delighted by the little things that happen without us asking for them.

* Try a “Whereas currently…” clause (or "Instead of…”)

  Technique to help assessing Story scope and complexity and how much of a difference it
  represents compared to the current behaviour.

* Don’t obsess about the format!

[9 Programmer Life Lessons You Must Experience Yourself to Truly Understand](https://medium.com/better-programming/9-programmer-life-lessons-you-must-experience-yourself-to-truly-understand-686daad49128)
1. The Cheapest, Fastest, and Most Reliable Components Are Those That Aren’t There
2. Voodoo Coding
Sometimes, you manage to fix a bug but don’t understand why
3. Code Never Lies, Comments Sometimes Do
4. Regular Expressions
When confronted with a problem, some people think, “I know, I’ll use regular expressions!” Now they have two problems.
5. Software Is Like Cathedrals: First, We Build Them — Then We Pray
>“The Cathedral model, in which source code is available with each software release, but code developed between releases is restricted to an exclusive group of software developers.”
> “The Bazaar model, in which the code is developed over the internet in plain view of the public. Linus Torvalds, leader of the Linux kernel project, is seen as the inventor of this process.”
6. Cheap, Fast, Reliable: Pick Two
7. There Are Two Difficult Things in Software Engineering
    0. Naming things
    1. Cache invalidation
    2. Off-by-one errors
8. A Good Programmer Looks Both Ways Before Crossing a One-Way Street
Most software is written to carry out the “happy flow” in which everything works as expected and the user doesn’t do weird stuff.
9. Measuring Programming Progress by Lines of Code Is Like Measuring Aircraft Building Progress by Weight
The best code uses the least amount of lines to get the job done and is also the hardest to write.

[Why You Keep Bumping That Task](https://forge.medium.com/why-you-keep-bumping-that-task-a67e748e7c65)
1. You aren’t sure what to do
2. You’re choosing the wrong time
3. You don’t really want to do it


[Momentum > Urgency](https://testobsessed.com/2020/02/momentum-urgency/)
Great article! 🥇⭐
> “Come on! You know programmers. You have to light a fire under them to get anything done!”
>
> Granted, pressure can expedite delivery. However it comes at a cost, and it’s a cost that’s difficult to measure.
> Pressure and urgency typically lead people to cut corners.
> That, in turn, can lead to an illusion of speed that comes crashing down later with delays or poor results.

> While I don’t know if J left a trail of destruction in his high-pressure-wake before,
>  I have seen the mess that others who used similar tactics to J left behind.
> In fact, I’ve spent a good part of my career cleaning up after those messes.

> What I’ve learned is that if we want things to go fast,
>  a sense of momentum is much more effective than a sense of urgency.

> Every time I’ve seen a team achieve a sense of momentum, several things are true:
>
> * Everyone on the team has a shared understanding of the big picture
>
> * The team breaks the work into small pieces (a couple days of work at most)
>    that still represent incremental business value (even if not user-facing features).
>
> * The team embraces the technical practices necessary to ensure that the code they
>    deliver does what they intended it to do.
>
> * When the work is “accepted,” it’s done. The entire team can stop thinking about it.
>
> * The work, and the status of the work, is visible to everyone.
>
> * The team as a whole has a strong sense of partnership and trust,
>    so the visibility never becomes a mechanism for micromanagement.

[Engineering Management Space](https://engineering-management.space/)
Pedro Pereira Santos Blog - met him at Feedzai, blog with great content 🥇⭐
[Github](https://github.com/donbonifacio)

[How to make an accurate estimate? You don't.](https://michalpietraszko.com/how-to-make-an-accurate-estimate-you-dont/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/kkie98/how_to_make_an_accurate_estimate_you_dont/)
Reddit discussion has a great point:
Developers are good at estimating the _mode_ but not the _median_ and / or _mean_
 (because it's very hard to **estimate** worst case scenarios, best case scenarios
 and how they balance out).
Also the distribution can't be normal, it's very rare to get all things going completely
 right, while having things going wrong is much more likely due to a snowball effect.
In the comenter experience, it was usually a "log-normal distribution", with the
 _mode_, _median_ and _mean_ being all in different positions (in that exact order).
Besides, running more tasks in parallel has a larger variance and risk.

[Deadlines are bullshit.](https://contrariantruth.substack.com/p/deadlines-are-bullshit)
When are deadlines necessary?
* Contractual obligations
* Technical liabilities
* Compliance, government, investors, and other external stakeholders
They are all important. They are all deadlines that cannot be missed. They are all external.
Author advocates that all other deadlines are internal and thus "evil", which I don't really agree
 but it's interesting nonetheless.
[Reddit discussion](https://www.reddit.com/r/programming/comments/korf49/deadlines_are_bullshit/) - very interesting

[Goodhart's law](https://en.wikipedia.org/wiki/Goodhart%27s_law)
> Any observed statistical regularity will tend to collapse once pressure is placed upon it for control purposes.

[How To Manage Your Time & Get More Done](https://www.youtube.com/watch?v=Gxdk27u9UwM)
Use your time as if you're using your money (don't throw it away)
Time is a non renewable resource.
[How To Run A Profitable Business & Make Money](https://www.youtube.com/watch?v=aNu000cQZmU)

[Groom Your Backlog Like a Boss - Atlassian Summit U.S. 2017](https://www.youtube.com/watch?v=i3GSw1-zpJM)
Let's agree on the amount of time we are going to dedicate on each specific category, e.g.:
* 70% new features
* 15% bug fixing
* 15% technical debt
Then:
* Balance expectations - be clear with all the parties
* Use Relative sizing to make planning faster - get the best of your team's time
Categorize you backlog in these different categories
"Backlog" only for Features
Planning session:
* Print cards and distribute them with the team
* Display them on the table from easy -> hard (same difficulty on same line), you'll probably see a few harder tickets and a lot of easy ones
* Draw lines on specific distinction of efforts
* "measure" - relative, for example: fibonacci

[GOTO 2020 • Prioritizing Technical Debt as if Time and Money Matters • Adam Tornhill](https://www.youtube.com/watch?v=A2N58uQxclI)
How to prioritize tech debt?
* Identify hotspots - most "touched" files and methods

[Github Stale Bots: A False Economy](https://blog.benwinding.com/github-stale-bots/)
Well the stale bot has had the following effects on the repository:
1. Reduce the metric of Open Issues in github
2. Made duplicate issues far more likely
3. Increased friction of users reporting that the issue still exists
4. Ultimately decreased the quality of the software, as the issues don’t accurately reflect reality
> Well questions that don’t get consistent interaction are inevitably duplicated, or not updated with correct information, or **people get frustrated and don’t ask the question again.**
[Reddit discussion](https://www.reddit.com/r/programming/comments/kzvryq/github_stale_bots_a_false_economy/)
Alternative:
    Do not lock issues (allow commenting) -> tag them "stale" and close them a bit later.
    Stale issues can be "re-opened".

[PMI: Please Get Out of Scrum](https://medium.com/serious-scrum/pmi-please-get-out-of-the-agile-space-9056b0a555df)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/l0zg3n/the_project_management_institute_needs_to_get_out/)
> Asking a development team to estimate the effort in something they’ve never done just doesn’t make sense.

> Add to that the timing of this effort — not one line of code has been written but developers are expected
>  to understand every nuance of the project and accurately estimate it. The benefits of iterative
>  development will never be recognized.

[Ideal Days vs. Story Points: Which Is Better and Why?](https://www.7pace.com/blog/ideal-days-vs-story-points)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/l5gtkm/ideal_days_vs_story_points_which_is_better_and_why/)
* Ideal Days
    + Pros:
        * It’s concrete and intuitive
        * Clients and outside stakeholders understand it immediately (_personal note:_ kinda...)
    - Cons:
        * Human beings are scientifically proven to be outrageously bad at estimating how long things will take
        * Ideal days are often calculated based on the ideal case scenario, not a data-backed review of actual work completed on similar projects
        * In practice, software teams often miss their promise date when using the ideal days method
* Story Points
    + Pros:
        * After you’ve been through three sprints, you will have a very strong understanding of how fast the team is moving—its velocity
        * The velocity will increase over time as the team finds new ways to complete the work faster and with less waste
    - Cons:
        * Story points are abstract and difficult to understand
        * You might not be able to make a good timeline estimate right at the outset of a new project
        * You’ll have to work hard to educate clients about the method and communicate with them often about the team’s progress

    ### How to Create an Initial Delivery Estimate Using Story Points
    Try to be data driven with the client. Go through a previous project with similar scope and use it as baseline.

[What is the best time for stand-up meetings?](https://piechowski.io/post/what-is-the-best-time-for-standup-meetings/)
> The morning is when most teams hold their meetings because their meeting starts out their day.
>
> The Afternoon stand-up is my favorite time as it allows for a block of deep work before and after stand-up, enabling developers maximum productivity.
>  prevents teams from avoiding the start of the work day until stand-up starts.
>
> The end of the day meeting works similarly to the start of day meeting.

[GOTO 2020 • Common Retrospectives Traps & Solutions • Aino Vonge Corry](https://www.youtube.com/watch?v=j3Diza_x9gI)
[The Prime Directive](https://retrospectivewiki.org/index.php?title=The_Prime_Directive)
> Regardless of what we discover, we understand and truly believe that everyone did the best job they could,
>  given what they knew at the time, their skills and abilities, the resources available, and the situation at hand.

[No Jira, No Cry. For those of you lucky enough not to… | by Zvonimir Spajic | The Startup | Dec, 2020 | Medium](https://medium.com/swlh/no-jira-no-cry-b8ccfdb55fc9)

---

## Definition of Done

[The Definition of Done](https://www.leadingagile.com/2017/02/definition-of-done/)
User Story DoD Examples:
* Unit tests passed
* Code reviewed
* Acceptance criteria met
* Functional tests passed
* Non-Functional requirements met
* Product Owner accepts the User Story

[5 Steps to Find Your Definition of Done (With Examples and Workflows) | Planio](https://plan.io/blog/definition-of-done/#1-decide-on-your-definition-of-done-as-a-team)
1. Decide on your definition of done as a team
   * Code is written.
   * Code is documented.
   * Code review has been completed.
   * Build has been made and deployed on a testing environment.
   * Tests have been passed.
2. Create a checklist template for your definition of done
3. Don’t obsess over the list of criteria
4. Make sure each individual task has its own specific acceptance criteria
5. Check your DoD against organizational needs
   A definition of done will occasionally need to be checked against your core values and principles to make sure no glaring issues are being missed.

[How to run 1:1 meetings that work for 2 | by David Lynch | Inside Intercom | Medium](https://medium.com/intercom-inside/how-to-run-1-1-meetings-that-work-for-2-a412206fe28e)
the time belongs first and foremost to the person you’re managing. Regardless of what you want to talk about, always give your report first choice on the agenda. This means they should turn up with an agenda — I think that’s also important.
As with much management work, identifying ownership and expectations up front is a useful exercise in avoiding misspent time.
things that are legitimately in scope for somebody to raise with you, but are out of scope for you to deal with directly. If there’s a difficult personal problem, it’s worth understanding where you can help, where the limits of your remit and ability to help are and what options you might have to get additional forms of support 
One is lack of preparation, which I genuinely don’t understand. There’s no excuse: Get your shit together and get it done on time. If for a genuine reason you’re not prepared, cancel the meeting. Don’t go through the motions.
To help us get in sync, at the end of each 1:1 I jot down the key points of the discussion as I understood them, note down the key next steps and the owners of those and send that email to my team member.
    I crystalise my understanding of what was important about that conversation by writing it down.
    I sanity check this against the other person’s understanding.
    I give the other person the opportunity to clarify my misunderstanding or to identify their own.
    What’s expected of everyone next is crystal clear.
    Over time I build a great set of useful notes.
Let people talk, in whatever manner they want, using whatever natural tone or language that allows them to express themselves. This might not be enjoyable for you, but, remember, it’s not about you.
pay really close attention and step back at this point. Anything talked about from here on will be low value until you’re both feeling safe again.
what’s said in 1:1s should remain in 1:1s.
By default I schedule 30 minute meetings with five minutes either side for prep, notes and follow up
I like to open each 1:1 with “Is now still a good time?”. It’s really important to create a predictable schedule but it’s even more important not to be beholden to it.
The key point here is be present, use your judgement and adapt your style to the person, the context and the general mood.
Concurrent note taking and active listening is challenging.
A “1:1 day” sounds like a good idea, but sometimes I’m lacking in energy at the end of the day, which is bad news for the team member in the 4.30pm slot.