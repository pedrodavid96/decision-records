# Architecture Decision Records

[Lightweight Architecture Decision Records | ThoughtWorks technology radar](https://www.thoughtworks.com/radar/techniques/lightweight-architecture-decision-records)
Much documentation can be replaced with highly readable code and tests.
In a world of evolutionary architecture, however, it's important to record certain design decisions for the benefit of future team members as well as for external oversight.
Lightweight Architecture Decision Records is a technique for capturing important architectural decisions along with their context and consequences.
We recommend storing these details in source control, instead of a wiki or website, as then they can provide a record that remains in sync with the code itself.
**For most projects, we see no reason why you wouldn't want to use this technique..**

[The Company](https://www.thoughtworks.com/pt/)

[Github](https://github.com/joelparkerhenderson/architecture_decision_record)
[Github example](https://github.com/joelparkerhenderson/architecture_decision_record/blob/master/examples/metrics-monitors-alerts.md)

## Cool visualizations

[History](https://www.thoughtworks.com/pt/radar/techniques/lightweight-architecture-decision-records)

---

[A Simple but Powerful Tool to Record Your Architectural Decisions](https://medium.com/better-programming/here-is-a-simple-yet-powerful-tool-to-record-your-architectural-decisions-5fb31367a7da)

[Architectural Decision Records](https://adr.github.io/)

[Code Diary](https://www.youtube.com/watch?v=tarmCEHfGa0)
Types of Code Diary Entries
1. Pros & Cons of X
2. Screw up story
3. Everything you need to know about X
4. Join me as I poke around at X and understand how it works
5. Workflows & best practice checklists I prepare for myself
6. Annotated code walkthroughs
[Discussion](https://www.reddit.com/r/programming/comments/k8lg4r/code_diary_how_and_why_to_keep_one/)

[GNU Recutils - GNU Project - Free Software Foundation](https://www.gnu.org/software/recutils/)

[CNCF Landscape](https://landscape.cncf.io/?view-mode=grid)
Similar to a radar... of CNCF (Cloud Native Computing Foundation) Projects.
Really useful.
