# Monitoring

## Instrumenting your application

[Grafana Example](https://github.com/grafana/grafana/blob/121a9162ffbf08ec21e52f11b7990062eba3bea7/pkg/services/screenshot/cache.go#L26)
Should be relevant being Grafana 😂

[Instrumenting a Go application | Prometheus](https://prometheus.io/docs/guides/go-application/)

[promauto package - github.com/prometheus/client_golang/prometheus/promauto - Go Packages](https://pkg.go.dev/github.com/prometheus/client_golang/prometheus/promauto)

---

[What is Prometheus?](https://prometheus.io/docs/introduction/overview/#what-is-prometheus)

https://apex.sh/ping/

https://itnext.io/istio-observability-with-go-grpc-and-protocol-buffers-based-microservices-d09e34c1255a

[Dashboard Design: Key Performance Indicators and Metrics](https://towardsdatascience.com/dashboard-design-key-performance-indicators-and-metrics-2b13745f5b2f)

[Dashboards are Dead](https://towardsdatascience.com/dashboards-are-dead-b9f12eeb2ad2)
Product advertisement (the product itself looks good)
To avoid infinite filters just create a "platform" to generate them and share them.
Notebooks

[Ward](https://github.com/B-Software/Ward)
> Ward is a simple and and minimalistic server monitoring tool.

[prometheus-rsocket-proxy](https://github.com/micrometer-metrics/prometheus-rsocket-proxy)

[fullstory](https://www.fullstory.com/)
[sentry](https://sentry.io/welcome/)
Monitors user platform usage in order to give usefull analytics and comprehensive and reproduceable bug reports.
[Ackee](https://github.com/electerious/Ackee)
Self-hosted, Node.js based analytics tool for those who care about privacy.

[Why I Self-Host My Website Analytics](https://mbuffett.com/posts/why-i-self-host-my-analytics/)
Article where I found about Ackee
[Discussion](https://www.reddit.com/r/programming/comments/i3dw49/why_i_selfhost_my_website_analytics/)

[Aptabase • Analytics for Mobile, Desktop and Web apps](https://aptabase.com/)
[umami](https://umami.is/)
Umami makes it easy to collect, analyze, and understand your web data — while maintaining visitor privacy and data ownership.

[ChatOps for Production Access Control](https://medium.com/policygenius-stories/chatops-for-production-access-control-b4feafbe9449)
1. Slack user interface for users and approvers.
2. Google Cloud Functions written in Python for the backend.
3. GCP’s IAM Conditions to handle access provisioning.
```chat
@access <resource> for <time duration>. Request related to <jira ticket> and the reason is <description>.
Button for approve or deny
```
[Policygenius 2019 Winter Hackathon](https://medium.com/policygenius-stories/policygenius-2019-winter-hackathon-aa6dc59451c3)
geniusbot — ChatOps
Who's on call?

~[Spotify Gimme](https://github.com/spotify/gimme)~
See [Hashicorp boundary](https://www.boundaryproject.io/) instead

[Error Tracking & Monitoring Part I/II](https://www.youtube.com/watch?v=YpxniTQdML8)
Five Pillars
1. Exception reporting - e.g.: Sentry, rollbar
2. Logging, log retention, and log search-ability
3. Downtime monitoring and alerting
4. System monitoring
5. Financial tracking

[Creating Monitoring Dashboards. Guidelines for developers | by Nikos Katirtzis | Expedia Group Technology | Medium](https://medium.com/expedia-group-tech/creating-monitoring-dashboards-1f3fbe0ae1ac)

Building a metrics dashboard with modern open-source tools: Apache Superset and Cube (spoiler: it will load in sub-second time)
https://www.reddit.com/r/programming/comments/r5t7th/building_a_metrics_dashboard_with_modern/

https://response.pagerduty.com/training/courses/incident_response/
[Crafting sustainable on-call rotations – Increment: On-Call](https://increment.com/on-call/crafting-sustainable-on-call-rotations/)
[Operating a Large, Distributed System in a Reliable Way: Practices I Learned - The Pragmatic Engineer](https://blog.pragmaticengineer.com/operating-a-high-scale-distributed-system#monitoring)

https://github.com/ddosify/ddosify
Cool UI

[GitHub - louislam/uptime-kuma: A fancy self-hosted monitoring tool](https://github.com/louislam/uptime-kuma)