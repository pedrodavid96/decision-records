# Visual Studio

## "Minimal" Setup

Microsoft Visual Studio Community 2017 
Version 15.6.2
VisualStudio.15.Release/15.6.2+27428.2005
Microsoft .NET Framework
Version 4.7.02556

Installed Version: Community

Visual C++ 2017   00369-60000-00001-AA176
Microsoft Visual C++ 2017

ASP.NET and Web Tools 2017   15.0.40301.0
ASP.NET and Web Tools 2017

C# Tools   2.7.0-beta3-62707-11. Commit Hash: 75dfc9b33ed624dff3985c7435c902c3c58c0e5c
C# components used in the IDE. Depending on your project type and settings, a different version of the compiler may be used.

JavaScript Language Service   2.0
JavaScript Language Service

Microsoft JVM Debugger   1.0
Provides support for connecting the Visual Studio debugger to JDWP compatible Java Virtual Machines

Microsoft MI-Based Debugger   1.0
Provides support for connecting Visual Studio to MI compatible debuggers

Microsoft Visual C++ Wizards   1.0
Microsoft Visual C++ Wizards

Microsoft Visual Studio VC Package   1.0
Microsoft Visual Studio VC Package

ProjectServicesPackage Extension   1.0
ProjectServicesPackage Visual Studio Extension Detailed Info

Visual Basic Tools   2.7.0-beta3-62707-11. Commit Hash: 75dfc9b33ed624dff3985c7435c902c3c58c0e5c
Visual Basic components used in the IDE. Depending on your project type and settings, a different version of the compiler may be used.

Visual Studio Code Debug Adapter Host Package   1.0
Interop layer for hosting Visual Studio Code debug adapters in Visual Studio

Visual Studio Tools for CMake   1.0
Visual Studio Tools for CMake

## SQL Operations Studio