# Cooking resources

[How to Keep Your Produce Fresh for Weeks | Wirecutter](https://www.nytimes.com/wirecutter/blog/keep-your-produce-fresh/)
* Start fresh
  Selecting the freshest fruits and veggies is the first step to getting the longest storage life in your kitchen.
* Consider the conditions
  “temperature, ethylene, and airflow—the big three,”
  Even if the bananas, potatoes, or onions you bought came in a perforated plastic bag, they’ll last longer if you take them out and let them breathe.
  Most refrigerated produce stays fresh longer when sealed ... These containers hold in moisture, preventing produce from dehydrating, and they help protect sensitive produce from the effects of ethylene gas.
  When in doubt, throw it out.
* Potatoes and sweet potatoes
  Don’t refrigerate.
  Store in a cool, dark place with relatively high humidity.
  Allow air circulation.
  Keep separate from onions, bananas, and other ethylene-producing items.
* Other roots and tubers
  Remove any leafy green tops.
  Refrigerate in a plastic bag for the longest life.
  For a shorter term (up to two weeks), store loose in your crisper drawer.
* Onions and garlic
  Don’t refrigerate.
  Store in a cool, dark place with low humidity.
  Allow some air circulation.
  Keep separate from potatoes and sweet potatoes.
* Cabbage and its cousins
  Refrigerate in sealed containers.
  Uncut heads can be refrigerated without a bag.
  Once cut, seal in an airtight container.
* Leafy greens
  Refrigerate unwashed.
  Seal in zip-top plastic bags.
* Apples and pears
  Refrigerate in a plastic bag.
  Ideally, use a crisper drawer that you’ve designated for non-ethylene-sensitive fruit, such as strawberries, blueberries, navel oranges, and raspberries.
* Mangoes
  Store unripe at room temperature.
  Once ripe, refrigerate loose, and separate from apples and pears.
* Citrus fruit
  Store on the countertop for up to a week.
  Refrigerate loose for longer storage.

[Parmesan Roasted Carrot Fries]()

[Doctor Reveals "Healthiest" Grocery Store Items - YouTube](https://www.youtube.com/watch?v=lUwUVVqJt1c)
* Berries (with yogurt for breakfast)
  If you see mold in berries, just throw that one out
  Freeze them for smoothies if needed
* Greens
  * Green beans
    Low sodium | Vitamin K | Good fiber
  * Leafy greens (spinash, kale, arugula, romaine)
    Iceberg Lettuces (a bit less nutricional value but still good)
  * Tomatoes
  * Sweet Potatos
* Kiwis
  Maybe they help falling asleep
* Before going foodshopping, make a list, stick to that list
* Sauce:
  Tabasco has a lot of flavor (and 0 callories)
  Salad dressing:
  * Extra virgin olive oild
  * Balsamic vinegar
* Manuka (?) honey
* Oats
* Rare treats won't make or break your lifestyle - general philosophy is most important
* Spices (0 or low calories)
* Bread
  First ingredient whole wheat (if you're not alergic to glutten...)
  With seeds for more vitamins
* Canned Sardines
  Protein | Omega 3 | Vitamin D
* Snacks
  Popcorn (salt and black pepper)
* Turkey
  High protein | Low fat
* Greek yogurt (with granola or berries)
  If mixed afterwards usually it has:
  Less preservatives | More antioxidants
  Good bacteria

https://www.notanothercookingshow.tv/
https://sardelkitchen.com/collections/frontpage
https://www.amazon.com/shop/notanothercookingshow
https://www.anitahealthy.com/


# Recipes

https://www.abeautifulplate.com/garlicky-herbed-goat-cheese-spread/
https://thewanderlustkitchen.com/honey-walnut-and-goat-cheese-crostini/

https://www.anitahealthy.com/caril-de-grao-com-manga-vegan/

[CHORIZO HUEVOS RANCHEROS with Creamy AVOCADO SALSA](https://www.notanothercookingshow.tv/post/chorizo-huevos-rancheros-with-creamy-avocado-salsa)

[Overnight Oats With up to 21 Grams of Protein](https://blog.myfitnesspal.com/overnight-oats-with-up-to-21-grams-of-protein/?otm_medium=onespot&otm_source=inbox&otm_campaign=MFP&otm_content=MFP_Intl_NL_UI_Weekly_20190916&otm_click_id=20d32536233fbb64b0873dcad386778d&utm_campaign=MFP_Intl_NL_UI_Weekly_20190916&utm_source=international&utm_medium=email)

[9 High-Protein, Egg-Free Breakfasts Under 450 Calories](https://blog.myfitnesspal.com/10-high-protein-egg-free-breakfasts-under-450-calories/?otm_medium=onespot&otm_source=inbox&otm_campaign=MFP&otm_content=MFP_Intl_NL_UI_Weekly_20190916&otm_click_id=20d32536233fbb64b0873dcad386778d&utm_campaign=MFP_Intl_NL_UI_Weekly_20190916&utm_source=international&utm_medium=email)

[Vegetable Noodle Stir-fry with Peanut Lime Sauce](https://www.pickuplimes.com/single-post/2019/07/25/Vegetable-Noodle-Stir-fry-with-Peanut-Lime-Sauce)

[What 1,500 Calories Looks Like (High-Protein Edition)](https://blog.myfitnesspal.com/what-1500-calories-looks-like-high-protein-edition/?utm_source=international&utm_medium=email&utm_campaign=MFP_Intl_Extra_Popular_2019930)

* https://www.eatthismuch.com/recipe/nutrition/creamed-peas-with-bacon,36612/
* https://www.eatthismuch.com/recipe/nutrition/penne-with-black-beans,906205/
* https://www.eatthismuch.com/recipe/nutrition/brown-sugar-salmon,34750/
* https://www.eatthismuch.com/recipe/nutrition/chicken-wraps,905689/
* https://www.eatthismuch.com/recipe/nutrition/taco-salad,924528/
* https://www.eatthismuch.com/recipe/nutrition/spaghetti-with-garlic-and-basil,45542/
* https://www.eatthismuch.com/recipe/nutrition/power-chicken-hummus-bowl,906554/
* https://www.eatthismuch.com/recipe/nutrition/mozzarella-sticks,950236/

[Gordon Ramsay's perfect burger tutorial | GMA](https://www.youtube.com/watch?v=iM_KMYulI_s)
https://dinnerthendessert.com/a1-steak-sauce-copycat/

## Old PC

French Toast

Wrap (egg cheese corn)
Burrito(Black Bean, sweet potato, egg)

Pancakes(Chocolate, and peanut butter Toping)
Waffle

Bacon and Jalapeño Egg Sandwich
Bacon-and-Egg Sandwiches with Greens (English Muffins)

Eggs
Greek Yogurt
Peanut butter
Low-fat cottage cheese
Quinoa

http://www.cookingclassy.com/baked-lemon-salmon-creamy-dill-sauce/

salsicha toscana

Healthy Waffles
http://www.geniuskitchen.com/recipe/spinach-garlic-pizza-90229

-----------------------------------------------------------------------

tuna / caesar salad

pineapple

egg

rice / pasta / sweet potatoes / beans / peas / carrots / cocumber / broccoli / green beans / corn / spinash / (cherry) tomato / letuce / (red) onnion / cheese / quinoa / mushrooms

grilled chicken / turkey  // meatballs / chili / burger / sausages / salmon / shrimps / tuna // pork

mayo / pepper / tabasco / lemon / salt / garlic / salsa / bell pepper / chili powder / cilantro(coentros)


https://darebee.com/recipes/caesar-salad.html
https://darebee.com/recipes/pineapple-pork.html


[Pick Up Limes | Delicious curries » 3 recipes + homemade naan](https://www.youtube.com/watch?v=rWfwPTv5jy0)
Homemade naan looks delicious

[Chef Alvin Cailan | The Fairfax Sandwich at Eggslut](https://www.youtube.com/watch?v=96sd2rfSJSQ)

[J. Kenji López-Alt | How to Make Eggs Benedict the Classic Way; Kenji's Cooking Show](https://www.youtube.com/watch?v=pkzRzrxYtMo)

[Binging with Babish: Eggs Florentine from Frasier](https://www.youtube.com/watch?v=34j9_tbM7og)
Similar to eggs benedit

[Healthier Versions of Unhealthy Foods | Basics with Babish](https://www.youtube.com/watch?v=yYUUobrsFDs)
Cauliflower pizza (looks pretty nice!)
Cauliflower "fried rice"
Cauliflower Mac & cheese

[Binging with Babish: Nachos from The Good Place (plus Naco Redemption)](https://www.youtube.com/watch?v=LgzudBvmd08)

[Not another cooking show | is this the BEST CHOCOLATE CHIP COOKIE recipe??](https://www.youtube.com/watch?v=Jy0pSxEkAbc)
Looks mega tasty 🤤

## Breakfast

[5 QUICK HEALTHY BREAKFASTS FOR WEEKDAYS - less than 5 min, easy recipe ideas!](https://www.youtube.com/watch?v=LMLSISU0GFc)
- Yogurt with fruit and nuts
  Optional: Honey, cynamon, toast
- Scrambled eggs on toast
- Avocado with lemon juice and pepper
- Overnight oats

[Why Americans Eat Dessert for Breakfast](https://www.youtube.com/watch?v=kNovwPIWr3Q)
[How I Stopped Hating Breakfast](https://www.youtube.com/watch?v=XVvFRE6yNPk)
japanese: rice / miso soup / salmon / raw egg
israel: humus
mexico: migas tacos - flower tortilla

[Warm Quinoa Sweet Potato Kale Salad with Goji Berries - Tastefulventure](https://tastefulventure.com/warm-quinoa-sweet-potato-kale-salad-goji-berries/)
[Blueberry Power Smoothie Bowl - Recipe Runner](https://reciperunner.com/blueberry-power-smoothie-bowl/)
[Raw Chocolate Peanut Butter Crunch Bars | Natalie's Health](https://www.natalieshealth.com/raw-chocolate-peanut-butter-crunch-bars/)
[Couscous Salad with Lime Basil Vinaigrette](https://www.pinterest.pt/pin/452611831306825993/)
[Sesame Chicken Cabbage Crunch Salad](https://www.pinterest.pt/pin/45599014973654086/)
[High-Protein  Wild Rice Vegan Salad](https://www.pinterest.pt/pin/730005420859310537/)
[Sesame Ginger Chickpea Salad](https://www.pinterest.pt/pin/775604367078305346/)

[Cilantro Lime Crema – A Couple Cooks](https://www.acouplecooks.com/cilantro-lime-crema/)
