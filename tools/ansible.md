# Ansible

[Ansible - an absolute basic overview](https://www.youtube.com/watch?v=MfoAb50Br94)
Very cool video explaining the basics of Ansible.

[ericsysmin's DevOps Blog](https://ericsysmin.com/)
Ansible contributor blog with very nice tricks.

[Ansible vs bash shell](https://devops.stackexchange.com/questions/342/how-is-ansible-different-from-simply-running-a-provisioning-bash-shell-in-vagran)

[Simple tips and tricks with ansible](https://pt.slideshare.net/KeithResar/simple-tips-and-tricks-with-ansible)

[Starting with Ansible in Raspberry Pi](https://dev.to/project42/starting-with-ansible-in-raspberry-pi-2mhm)
Weird idea with Docker in my opinion but with some cool tricks:
[`authorized_key`](https://docs.ansible.com/ansible/latest/modules/authorized_key_module.html)
Add/Remove authorized keys
[`sshpass`](https://www.cyberciti.biz/faq/noninteractive-shell-script-ssh-password-provider/)
SSH with password without prompt

[ansible-playbook-grapher](https://github.com/haidaraM/ansible-playbook-grapher)
A command line tool to create a graph representing your Ansible playbook tasks and roles

[separate group_vars being overwritten when deploying to same host](https://github.com/ansible/ansible/issues/9065)
Variable precedence might not be intuitive

## Cloud

[AWS Guide](https://docs.ansible.com/ansible/latest/scenario_guides/guide_aws.html)
[When using dynamic inventory what are good practices for public cloud in provisioning](https://github.com/ansible/ansible/issues/69227)

## Testing

[Testing Ansible Roles with Docker](https://www.ansible.com/blog/testing-ansible-roles-with-docker)
[Testing Strategies](https://docs.ansible.com/ansible/latest/reference_appendices/test_strategies.html)

## Ansible Collections / Galaxy

[Install multiple collections with a requirements file](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#install-multiple-collections-with-a-requirements-file)
[Installing a Collection from a git repository](https://docs.ansible.com/ansible/devel/user_guide/collections_using.html#installing-a-collection-from-a-git-repository)
[Developing COllections - Roles directory](https://docs.ansible.com/ansible/devel/dev_guide/developing_collections.html#roles-directory)

### Authentication

[ansible-galaxy might need to use credentials for Git](https://github.com/ansible/ansible/issues/17194)
[Ansible Galaxy (requirements file feature): Cloning git repositories via SSH failed](https://github.com/ansible/ansible/issues/8937)

### Misc

[ANSIBLE_PIPELINING | Ansible Config Reference](https://docs.ansible.com/ansible/2.10/reference_appendices/config.html#ansible-pipelining)
> Pipelining, if supported by the connection plugin, reduces the number of network operations
>  required to execute a module on the remote server
> This can result in a very significant performance improvement when enabled.
> However this conflicts with privilege escalation (become).

[What's the most useful trick you learned using ansible?](https://www.reddit.com/r/devops/comments/8r49j6/whats_the_most_useful_trick_you_learned_using/)
It's possible to do things without an inventory file if you use -i hostname,.
It's also possible to use the standard unix #! shebang trick to make little tools
 that use ansible-playbook as an "interpreter".

## TODO

- [ ] [Handlers running operations on change](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html#handlers-running-operations-on-change)
- [ ] [Meta Module](https://docs.ansible.com/ansible/latest/modules/meta_module.html)
  Can be used to refresh environment.
- [ ] [Ansible: check if a package is installed on a remote system](https://dev.to/setevoy/ansible-check-if-a-package-installed-on-a-remote-system-4402)
- [ ] [Local facts (facts.d)](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#local-facts-facts-d)
  Example of them being used: https://github.com/debops/debops/blob/master/ansible/roles/java/tasks/main.yml
- [ ] [AWX](https://github.com/ansible/awx)
  AWX provides a web-based user interface, REST API, and task engine built on top of Ansible.
  It is the upstream project for Tower, a commercial derivative of AWX.
- [ ] [Tags — Ansible Documentation](https://docs.ansible.com/ansible/2.9/user_guide/playbooks_tags.html#tag-inheritance)
