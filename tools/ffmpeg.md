# ffmpeg

Remove "hard" subtitles

```
ffmpeg -i video.mkv -vcodec copy -acodec copy -sn video-no-subs.mkv
```

From https://gist.github.com/innat/7d5511941970e8b448453980f47f9ec3


```sh
/usr/lib/jellyfin-ffmpeg/ffmpeg \
    -analyzeduration 200M \
    -init_hw_device vaapi=va:/dev/dri/renderD128 \
    -filter_hw_device va \
    -hwaccel vaapi \
    -hwaccel_output_format vaapi \
    -autorotate 0 \
    -i file:"/data/media/tv/Friends/Season 3/Friends S03E06 The One With The Flashback  (1080p x265 10bit Joy).mkv" \
    -autoscale 0 \
    -map_metadata -1 \
    -map_chapters -1 \
    -threads 0 \
    -map 0:0 \
    -map 0:1 \
    -map -0:0 \
    -codec:v:0 h264_vaapi \
    -rc_mode VBR \
    -b:v 3725202 
    -maxrate 3725202 \
    -bufsize 7450404 \
    -force_key_frames:0 "expr:gte(t,0+n_forced*3)" \
    -filter_complex "[0:9]scale=s=1920x1080:flags=fast_bilinear[sub];[0:0]setparams=color_primaries=bt709:color_trc=bt709:colorspace=bt709,scale_vaapi=format=nv12:extra_hw_frames=24,hwdownload,format=nv12[main];[main][sub]overlay=eof_action=pass:shortest=1:repeatlast=0,hwupload_vaapi" \
    -start_at_zero \
    -codec:a:0 copy \
    -copyts \
    -avoid_negative_ts disabled \
    -max_muxing_queue_size 2048 \
    -f hls \
    -max_delay 5000000 \
    -hls_time 3 \
    -hls_segment_type mpegts \
    -start_number 0 \
    -hls_segment_filename "/config/data/transcodes/a7d5664e8324b697aae01cc18ce53c0b%d.ts" \
    -hls_playlist_type vod \
    -hls_list_size 0 \
    -y "/config/data/transcodes/a7d5664e8324b697aae01cc18ce53c0b.m3u8"
```