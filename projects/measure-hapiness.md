# Happiness

Joke project / prototype.

Bot to measure happiness in a meeting.

Possible key factors:
* Laugh
* Sarcasm
* Angry

Seems like an interesting POC.
Machine Learning might be one of the best approaches to
 detect laughter and the other key factors in the conversation.

References:
[laughter-detection](https://github.com/jrgillick/laughter-detection)
