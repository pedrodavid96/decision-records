# WebApps frameworks

[Web Application Bundler](https://github.com/parcel-bundler/parcel)

## [elm](https://elm-lang.org/)

[Intro](https://guide.elm-lang.org/)

>
Elm is a functional language that compiles to JavaScript. It competes with projects like React as a tool for creating websites and web apps. Elm has a very strong emphasis on simplicity, ease-of-use, and quality tooling.

Visually very similar with Haskell

* Enforced Semantic Versioning
  Type System detects API changes and enforces semantic versioning
* JavaScript Interop

## [Reasonml](https://reasonml.github.io/)

Reason lets you write simple, fast and quality type safe code while leveraging both the JavaScript & OCaml ecosystems.

Visually very similar with OCaml and F#