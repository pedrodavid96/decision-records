[How one code review rule turned my team into a dream team](https://medium.com/inside-league/how-one-code-review-rule-turned-my-team-into-a-dream-team-fdb172799d11)
Actually 4 rules...:
- Each PR review must have at least 2 same-team developer approvals. Manager approval does not count.

- Each PR must have a good description.

  From reading the description, the reviewer should be able to understand what the code is meant to do. This has to be true even if there is a Jira ticket or a requirements page.

- PR must have sufficient unit test and integration test coverage.

- If the PR is a bug fix, it must contain a test such that, should the bug fix be reverted, this test would fail.

[Code Review Best Practices - Lessons from the Trenches](https://blogboard.io/blog/code-review-best-practices)
What's in this article?
1. Why do code reviews?
2. Code reviews as quality assurance
3. Code reviews as a team improvement tool
4. Preparing a pull request for review
5. Reviewing code - Be human!

[How to Make Your Code Reviewer Fall in Love with You](https://mtlynch.io/code-review-love/)
Why improve your code reviews?
* Learn faster
* Make others better
* Minimize team conflicts:
The golden rule: value your reviewer’s time
Techniques:
1. Review your own code first
2. Write a clear changelist description
3. Automate the easy stuff
4. Answer questions with the code itself
5. Narrowly scope changes
6. Separate functional and non-functional changes
7. Break up large changelists
8. Respond graciously to critiques
9. Be patient when your reviewer is wrong
10. Communicate your responses explicitly
11. Artfully solicit missing information
12. Award all ties to your reviewer
13. Minimize lag between rounds of review
[Discussion](https://www.reddit.com/r/programming/comments/k5cbln/how_to_make_your_code_reviewer_fall_in_love_with/)

[How Google does code review](https://graphite.dev/blog/how-google-does-code-review)
[How Google does code review : programming](https://www.reddit.com/r/programming/comments/1ctqrq2/how_google_does_code_review/)
