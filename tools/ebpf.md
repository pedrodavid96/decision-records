# eBPF

[eBPF - Introduction, Tutorials & Community Resources](https://ebpf.io/)
> eBPF is a revolutionary technology that can run sandboxed programs in the Linux kernel without changing kernel source code or loading a kernel module.

[Introducing Flora: Newly launched eBPF observability solution with near-zero resource overhead, for optimal performance in modern cloud-native environments : programming](https://www.reddit.com/r/programming/comments/12sridb/introducing_flora_newly_launched_ebpf/)
This seems like an ad for a paid product.
Interesting nonetheless.