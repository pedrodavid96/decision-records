# UI Testing

[Your all-in-one visual review platform](https://percy.io/) - not open source </3

[pixels-catcher](https://www.npmjs.com/package/pixels-catcher)

[Detecting the unexpected in (Web) UI | by Nicolas Dubien | Criteo R&D Blog | Medium](https://medium.com/criteo-engineering/detecting-the-unexpected-in-web-ui-fuzzing-1f3822c8a3a5)
[More fuzzy tests](https://medium.com/@atroche/in-my-last-post-i-talked-about-automating-automated-testing-by-taking-a-black-box-approach-34437e5c35ba)

[First Steps Frontend Testing with TDD/BDD](https://medium.com/@aeh.herman/first-steps-in-frontend-testing-with-tdd-bdd-7ddab8796ad6)

[Let's TDD front-end](https://dev.to/shiling/comment/4fb6)

[A guide to GUI testing](https://medium.com/@Apiumhub/a-guide-to-gui-testing-5ea86281fbd6)

[TDD in Android / UI testing using Espresso](https://medium.com/mobility/how-to-do-tdd-in-android-part-4-ui-testing-using-espresso-b381ebede191)

[Chromatic 1.0 — continuous UI testing for React](https://blog.hichroma.com/chromatic-1-0-continuous-ui-testing-for-react-2ad10fd01cfe)
This looks awesome^

[Cypress | A complete end-to-end testing experience](https://www.cypress.io/)
[Cypress vs Playwright : r/QualityAssurance](https://www.reddit.com/r/QualityAssurance/comments/srhafv/cypress_vs_playwright/)
[Playwright enables reliable end-to-end testing for modern web apps.](https://playwright.dev/)
https://www.reddit.com/r/programming/comments/x8bj7w/rewriting_tests_from_cypress_to_playwright_using/
TLDR (Paylwright vs Cypress)
* about 10x faster (at least 7x) in our tests
* a lot easier to parallelize tests
* a lot easier to debug tests in Playwright
* a lot easier to write tests
* a lot easier to multiplayer applications (e.g. chat)

[Loki | Visual Regression Testing](https://loki.js.org/)

[Puppeteer](https://pptr.dev/)
Selenium (npm) alternative

[endtest](https://endtest.io/)
Codeless Automated Testing
Record Web Tests and edit them (add assertions)
