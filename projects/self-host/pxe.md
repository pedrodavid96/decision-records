# PXE

> **Important Note:**
> In order to allow PXE booting, in mini-pc I had to go to bios and enable `uefi network stack`.

A standardized client–server environment that boots a software assembly, retrieved from a network, on PXE-enabled clients.

[Looking for PXE solution : selfhosted](https://www.reddit.com/r/selfhosted/comments/rn0bl7/looking_for_pxe_solution/)
[Your favorite operating systems in one place! | netboot.xyz](https://netboot.xyz/)
[Meet netboot.xyz - Network Boot Any Operating System - YouTube](https://www.youtube.com/watch?v=4btW5x_clpg)

```local-vars.ipxe
# Set live_endpoint to own network instead of fetching live from github.
set live_endpoint http://192.168.1.250:8085
```

Changing menu depending on host (MAC address):

```menu.ipxe
# Already in templates: Ensure it already has network setup, either via set IP or call DHCP before trying to bootload
isset ${ip} || dhcp

# Bootload for specific MAC
iseq ${net0/mac} 68:1d:ef:39:86:f6 && chain proxmox-auto.ipxe || goto error
```

## Issues

Seems that tftp doesn't work well in Synology (and maybe other platforms).
[Can't get any files from TFTP · Issue #1297 · netbootxyz/netboot.xyz](https://github.com/netbootxyz/netboot.xyz/issues/1297)
[Netboot does not work due to UDP-mapping · Issue #11 · linuxserver/docker-netbootxyz](https://github.com/linuxserver/docker-netbootxyz/issues/11)

In practice I saw the TFTP server "acknowledging" the request but then trying to route it
back to the docker network gateway instead of the client.
The client effectively timed out on its request (tested via `tftp` on laptop).
Switching network to host (and moving ports around to avoid clashes) "solved" the issue.
