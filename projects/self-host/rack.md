


[10 inch 2U Mini-ITX case, short depth and rack-mountable - MyElectronics](https://www.myelectronics.nl/us/10-inch-2u-mini-itx-case.html)

[Kasa powerstrip as PDU - XtremeOwnage.com](https://static.xtremeownage.com/blog/2022/kasa-powerstrip-as-pdu/)

## 3d printing

[10'' rack mini itx pc case by Karel | Download free STL model | Printables.com](https://www.printables.com/model/144166-10-rack-mini-itx-pc-case)

[Mini ITX PC case by DaliaDesign | Download free STL model | Printables.com](https://www.printables.com/model/1117121-mini-itx-pc-case)


## Inspiration

[Almost done with my do-everything 10" mostly-printed homelab! : minilab](https://www.reddit.com/r/minilab/comments/1ixo9xm/almost_done_with_my_doeverything_10_mostlyprinted/)

[My home K8s cluster : minilab](https://www.reddit.com/r/minilab/comments/1ix9re6/my_home_k8s_cluster/)

[Minilab for LAN-Party : minilab](https://www.reddit.com/r/minilab/comments/1ivkhxb/minilab_for_lanparty/)

