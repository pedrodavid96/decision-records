[Anurag Saksena](https://www.youtube.com/channel/UCo_aAQ5SK8uEZeOzzIBeKOg/videos)
This channel is a great resource!!! 🥇⭐

## [Better Daily Scrum](https://www.youtube.com/watch?v=RmAJE-cEB_8)

1. Do NOT direct
   It's a ceremony for the team, not a status meeting.
   Supposed to be a facilitator, not someone who directs people.
   Keep a positive team
2. Huddle / Collaborate
3. Scrum Lead starts
   Sets a very positive tone
   Gives a segway to team member items
4. Always have it
5. Encourage break-outs
   Do not resolve issues on the daily
   Resolve time to resolve them afterwards with the relevant parties
6. Stand-up
   More focused
   Usually shorter

## [INVEST (mnemonic)](https://en.wikipedia.org/wiki/INVEST_(mnemonic))

Characteristics of a **good quality** Product Backlog Item (usually user story) or **PBI** for short.

| Letter    | 	Meaning     | 	Description                                                                                                     |
|-----------+---------------+-------------------------------------------------------------------------------------------------------------------|
| I         | 	Independent | 	The PBI should be self-contained, in a way that there is no inherent dependency on another PBI.                 |
| N         | 	Negotiable  | 	PBIs are not explicit contracts and should leave space for discussion.                                          |
| V         | 	Valuable    | 	A PBI must deliver value to the stakeholders.                                                                   |
| E         | 	Estimable   | 	You must always be able to estimate the size of a PBI.                                                          |
| S         | 	Small       | 	PBIs should not be so big as to become impossible to plan/task/prioritize within a level of accuracy.           |
| T         | 	Testable    | 	The PBI or its related description must provide the necessary information to make test development possible.    |

## Sprint Planning

Product Owner (PO) describes **highest priority stories** in the Product Backlog.
Team (including PO) "drives clarity" to decide which stories they can **commit** to in a given sprint.

| Inputs          | Tools & Techniques        | Output          |
-----------------+---------------------------+-----------------|
| Product Backlog | Prioritization Techniques | Sprint Backlog  |
|                 | Estimation Techniques     |                 |
|                 | Velocity                  |                 |
|                 | **Team Calendar**         |                 |


### 3 "C"s of User Stories

Card (As a ... I need to ... So that ...)
Conversation
Confirm

### Acceptance Criterion (Accrit)

```
Given <Precondition> ...
When <Actor + Action> ...
Then <Observable Result>.
```

_Note:_ Accrit is **NOT** a replacement for test cases.

Why this template?
1. Thought Provoking
2. Encourages conversation (with the right people)
3. Documents pre-requisites / assumptions
4. Risk Avoidance


## Retro

1. What is working well (continue doing)?
2. What is not working (stop doing)?
3. What we should start doing?

**Idea:** Estimates Review

## Estimation

Estimation != **Deadline**

[Agile Estimation](https://www.youtube.com/watch?v=f7NVU2WKul4)

Relative measure is faster and should be as accurate as an absolute measure (although it may be less precise).
Relative measure shouldn't be used to compare velocity across different teams.

### Story Points

1. Relative Size
2. Factor in - Review, Unit Tests, Communications
3. Fibonacci - 1, 2, 3, 5, 8, 13
4. Translations
   1 = <9 hours
   2 = 10-20 hours
   3 = 3-4 days
   5 = 5-7 days
   ...
5. Velocity - Story Points that can be completed in a Sprint

[Zenful Estimates: Ditching Task Estimates to Build a Faster Team : programming](https://www.reddit.com/r/programming/comments/1boonq2/zenful_estimates_ditching_task_estimates_to_build/)
