# Plants

https://www.google.com/search?client=firefox-b-d&q=ficus+microcarpa
https://www.jardineriaon.com/pt/ficus-microcarpa.html#Cuidados_del_Ficus_microcarpa
https://guiadassuculentas.com/ficus-microcarpa-um-guia-completo-desta-planta/

https://www.blog-flores.pt/flores-de-interior/lirio-da-paz/

https://www.fiskars.com/en-us/gardening-and-yard-care/ideas-and-how-tos/planting-and-prep/growing-basil-planting-and-harvesting


https://www.amazon.es/-/pt/dp/B088PD28MH/ref=sr_1_15?crid=3GMGOL1XTIPWE&keywords=regadera&qid=1658588941&sprefix=regador%2Caps%2C118&sr=8-15


https://www.google.com/search?client=firefox-b-d&q=plant+window+support
https://www.google.com/search?q=plant+window+support&client=firefox-b-d&sxsrf=ALiCzsYbAj3zdXH5285Cys4ZxSm2CRrQDw:1658589147639&tbm=isch&source=iu&ictx=1&vet=1&fir=q8R3lHt1qgMMJM%252CkZiz0gf0_CLYnM%252C_%253BiKxVEozvvI7s3M%252C9fQaYEtVtRIQ0M%252C_%253BEQBqGWSizU3T9M%252CPzFJSDIfL1ZqMM%252C_%253ByK8iqoz1XGo7mM%252CWlekt4DLD9Vw_M%252C_%253BO-zmUfmrcsZX0M%252CvfXwtVwnGbcJ0M%252C_%253BwF8E4OaHsQKSOM%252CYMLhXO5FZcoLEM%252C_%253BRlQfkDozvcrfPM%252CrNF0z7tKqrPI5M%252C_%253BuwDZMCuxNw5_jM%252Cp2bKCPwyTGfcOM%252C_%253BvB4pnKG3b9Na5M%252CismlTSp0e95_LM%252C_%253BB4ebHaljx0nzsM%252C2wg8dd9MP84G_M%252C_&usg=AI4_-kR5k4CBB_1UtetQS9lmAmMLmyfeHA&sa=X&ved=2ahUKEwj6peSHpo_5AhVP3RoKHd7gAGcQ9QF6BAgZEAE&biw=1440&bih=728&dpr=2#imgrc=O-zmUfmrcsZX0M
https://www.etsy.com/market/window_plant_shelf
https://platthillnursery.com/3-ways-to-maximize-space-around-windows/

---

## Plant care

[Coffee Grounds: How To Use Them To Repel Gnats - Pest Pointers](https://pestpointers.com/coffee-grounds-how-to-use-them-to-repel-gnats/)
[Peace Lily With Brown Tips: Causes Of Brown Tips On Peace Lily Leaves](https://www.gardeningknowhow.com/houseplants/peace-lily/peace-lilies-getting-brown-tips.htm)
[Common Reasons Why Peace Lily Leaf Tips Turn Brown | Gardener’s Path](https://gardenerspath.com/plants/houseplants/brown-leaf-tips-peace-lily/)

[How Do I Get Rid Of Houseplant Flies (Fungus Gnats)? – Bloombox Club](https://bloomboxclub.com/blogs/news/how-do-i-get-rid-of-houseplant-flies-fungus-gnats)
* Avoid Overwatering
* Make Sure Your Plant Has Good Drainage
* Dry Out The Soil
* Sticky Traps
* Vinegar Traps
* Carnivorous Plants

[How To Get Rid Of Gnats On Houseplants | WallyGro – WallyGrow](https://wallygrow.com/blogs/feature/how-to-get-rid-of-gnats-in-houseplants)
4. PRUNE, PRUNE, PRUNE
   ... and remove shed plant material from the soil’s surface as soon as you notice it. The less decaying organic material there is, the less food for flies and gnats there will be!

