# Equals Verifier

EqualsVerifier can be used in Java unit tests to verify whether the contract for the equals and hashCode methods is met.

[Error messages explained](https://jqno.nl/equalsverifier/errormessages/)
