# Home Office

## Camera

[Can I use an Android phone as webcam for an Ubuntu device? - Ask Ubuntu](https://askubuntu.com/questions/1235731/can-i-use-an-android-phone-as-webcam-for-an-ubuntu-device)
[Linux | DroidCam](https://www.dev47apps.com/droidcam/linux/)

## Network

### Routers

https://www.reddit.com/r/HomeServer/comments/183bbdu/a_cheap_router/
https://www.reddit.com/r/HomeServer/comments/17l5a0f/routermodem_recommendations/
[MikroTik Routers and Wireless - Products: hAP ax²](https://mikrotik.com/product/hap_ax2)
[Flint 2 (GL-MT6000) | High-Performance VPN Router - GL.iNet](https://www.gl-inet.com/products/gl-mt6000/)
https://www.gl-inet.com/products/gl-ax1800/
[Alternatives](https://www.reddit.com/r/selfhosted/comments/1c6xfj9/alternatives_to_the_glinet_flint_2_as_a_vpn/#:~:text=Personally%20I%20use,with%205GHz%20too.)
> Personally I use my router as a dedicated wifi access point, while my router is a pfsense vm (in proxmox with dedicated 2.5gbit nics in passthrough).
> The Flint 2 is great but it isn't perfect yet. For instance it has troubles with 2.4GHz reception. And many cheaper commercial solutions offer better performance with 5GHz too.

Going deeper, it seems Flint 2 uses a fork of OpenWRT that lags behind
but it's possible to flash with OpenWRT. Note that this is not possible in every GL.iNet device
and it is not "fully supported" (may loose ability in the future).

MikoTik uses it's own RouterOS
https://www.reddit.com/r/homelab/comments/1ba2tn0/microtik_routeros_experiences/
> The user interface is non-standard -- some would call it user-hostile.
It may support OpenWRT https://openwrt.org/toh/mikrotik/start

#### DIY

Tested Mini-PC with Proxmox + opnsense.
Made it work but was limited to 100mbps.
Could've been the (usb) network adapter or maybe ISP block?

[Giga Router 5.0 limitado em modo Bridge | Forum NOS](https://forum.nos.pt/internet-nos-4/giga-router-5-0-limitado-em-modo-bridge-13150)
[GiGA Router (5.0) - Versão FTTH | Página 29 | ZWAME Fórum](https://forum.zwame.pt/threads/giga-router-5-0-versao-ftth.1045022/page-29)
[Trocar router da nos | Forum NOS](https://forum.nos.pt/internet-nos-4/trocar-router-da-nos-8619/index2.html)
[[AJUDA] Como substituir router/modem NOS? | ZWAME Fórum](https://forum.zwame.pt/threads/ajuda-como-substituir-router-modem-nos.1045190/)

Those threads highligthed that
**NOS Router** might be [Broadcom BCM4708](https://techinfodepot.shoutwiki.com/wiki/Broadcom_BCM4708).

##### OPNsense

[OPNsense automated install/setup, cloud-init, ansible, terraform, etc.](https://forum.opnsense.org/index.php?topic=42517.0)

[Is pfSense/OPNSense over-hyped? : homelab](https://www.reddit.com/r/homelab/comments/1bk1956/is_pfsenseopnsense_overhyped/)

> They aren't over hyped.
> OpenWRT's primary design focus is routing.
> OPNsense's primary design focus is firewall.
> I use both in my homelab and they both have their place but are tailored for a specific purpose and thus are used for just.

### Switches

#### Mikrotik

[RB260GSP](https://mikrotik.com/product/RB260GSP) -> SwOS - People say this sucks. Look for Mikrotik hardware that supports their RouterOS, which seems super cool (good API / terraform integration).
[CRS106-1C-5S](https://mikrotik.com/product/CRS106-1C-5S) -> RouterOS
[CRS305-1G-4S+IN](https://mikrotik.com/product/crs305_1g_4s_in) -> Seems way more enterprise than what's needed.

#### Misc

* [TL-SG105E v5](https://www.tp-link.com/pt/business-networking/easy-smart-switch/tl-sg105e/)
  Managed Gigabit, cheap. Missing PoE.

  > [Not So Smart: TP-Link TL-SG105E V3.0 5-Port Gigabit Easy Smart Switch | Gough's Tech Zone](https://goughlui.com/2018/11/03/not-so-smart-tp-link-tl-sg105e-v3-0-5-port-gigabit-easy-smart-switch/)
  Hopefully only applies to v3 but would be good to check.

  V4 says "unmanaged" on the box but seems as managed as the flex-mini I have.

## "Mesh" Network

https://openwrt.org/docs/guide-user/network/wifi/dawn

This can be useful for improved wifi performance when you have a network with multiple APs and 802.11r-based Wifi roaming enabled.

## Storage

[HDD 18TB Ironwolf Pro for sale | eBay](https://www.ebay.com/sch/i.html?_from=R40&_trksid=p2334524.m570.l1313&_nkw=hdd+18tb+ironwolf+pro&_sacat=0&_odkw=hdd+28tb+ironwolf+pro&_osacat=0)
Second hand doesn't feel that bad if reconditioned by brand and taking into account they will be part of RAID.
