# Unix

[Where should an app save working files](https://unix.stackexchange.com/questions/189693/where-should-an-app-save-working-files)

[How fast are Linux pipes anyway?](https://mazzo.li/posts/fast-pipes.html)

[Pesky little scripts | Redowan's Reflections](https://rednafi.com/misc/pesky_little_scripts/#fn:3)
> All your scripts should start with a character as a prefix that doesn’t have any special meaning in the shell environment. Another requirement is that no other system command should start with your chosen character.
[Activation Energy, by Brandon Rhodes - YouTube](https://www.youtube.com/watch?v=pybtvFFRYFs)

## Managing your .dotfiles
https://www.freecodecamp.org/news/dive-into-dotfiles-part-2-6321b4a73608/

## Suckless software, patch strategies

## Clipboard management
https://github.com/erebe/greenclip

## [Altbox.dev](https://altbox.dev/)
Alternative tools for Linux/Unix/macOS command line

Examples:
* find - fzf
* grep - ripgrep

## Tools

newsbeuter
mutt

## '\n' at end of file

https://stackoverflow.com/a/5813359
> Note that it is a good style to always put the newline as a last character if it is allowed by the file format. Furthermore, for example, for C and C++ header files it is required by the language standard.

---

[fork() can fail: this is important](https://rachelbythebay.com/w/2014/08/19/fork/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/fcewia/fork_can_fail_this_is_important/)
`fork()` can fail (return -1), `kill(-1)` sends sig to every process for which the calling process has permission to send signals, except for process 1 (`init`).
If you're `root`, that's probably everything but you and `init`.

[How to Trace Linux System Calls in Production with Minimal Impact on Performance](https://en.pingcap.com/blog/how-to-trace-linux-system-calls-in-production-with-minimal-impact-on-performance)

[Unix Permissions the easy way](https://towardsdatascience.com/unix-permissions-the-easy-way-98cc19979b3e)
Octal representation:
0 | 000 | - - - | No permissions
1 | 001 | - - x | Only execute
2 | 010 | - w - | Only write
3 | 011 | - w x | Write and Execute
4 | 100 | r - - | Only read
5 | 101 | r - x | Read and Execute
6 | 110 | r w - | Read and Write
7 | 111 | r w x | Read, Write and Execute

Unix permission is the set of 3 octals, which represent the 3 entities: `user`, `group` and `others` (i.e.: the world)
e.g.: `777` Gives all permissions to the 3 groups

[VISUAL vs. EDITOR – what’s the difference?](https://unix.stackexchange.com/questions/4859/visual-vs-editor-what-s-the-difference)
> The `EDITOR` editor should be able to work without use of "advanced" terminal functionality
   (like old `ed` or `ex` mode of `vi`). It was used on teletype terminals.
> A `VISUAL` editor could be a full screen editor as vi or emacs.

> (Citation needed: I'd love to get the proper documentation, perhaps a man page or POSIX spec?)


[The Linux Filesystem Explained](https://www.linux.com/training-tutorials/linux-filesystem-explained/)
> all their exotic names like /etc (not for miscellaneous files), /usr (not for user files), and /bin (not a trash can) meant.

![Standard unix filesystem hierarchy](./standard-unix-filesystem-hierarchy.png)

[Getting better at Linux with 10 mini-projects](https://carltheperson.com/posts/10-things-linux)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/lbccwu/getting_better_at_linux_with_miniprojects/)
1. UNIX - Recat
2. What is a shell? - SeaShell
3. Ownership and permissions - Tellaccess
4. Grep - Grep detective
5. Awk and sed - Passwdinfo
6. Find - Find treasure hunt
7. File system - Root tour
8. Processes - Stranger danger
9. Systemd services - Createservice
10. Bash scripting - Penguin cipher

[Everything you never wanted to know about ANSI escape codes](https://notes.burke.libbey.me/ansi-escape-codes/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/lbmbnm/everything_you_never_wanted_to_know_about_ansi/)

[Filenames with hyphens - UNIX style?](https://www.unix.com/what-is-on-your-mind-/183781-filenames-hyphens-unix-style.html)
I just checked on a Solaris 11 fresh install and got these statistics analyzing 168522 filenames:
* 27.57% contains at least an hyphen
* 23.13% contains at least an underscore

[Of Spaces, Underscores and Dashes](https://blog.codinghorror.com/of-spaces-underscores-and-dashes/)
So it behooves us to use something other than a space in file and folder names.
Historically, I've used underscore, but I recently discovered that the correct character to substitute for space is the dash. Why?

The short answer is, that's [what Google expects](http://weblog.philringnalda.com/2004/04/22/underscores-are-bad-mmkay):

> If you use an underscore `_` character, then Google will combine the two words on either side into one word.
> So bla.com/kw1_kw2.html wouldn't show up by itself for kw1 or kw2. You'd have to search for kw1_kw2 as a query term to bring up that page.

The slightly longer answer is, [the underscore is traditionally considered a word character by the w regex operator](http://www.regular-expressions.info/charclass.html).

## Misc

https://www.cyberciti.biz/tips/how-do-i-find-out-what-shell-im-using.html

## Firewall

[ufw - Pesquisa Google](https://www.google.com/search?channel=fs&client=ubuntu-sn&q=ufw)
Uncomplicated Firewall (Ubuntu)

---

[When should I use /dev/shm/ and when should I use /tmp/?](https://superuser.com/a/45509)

First found when inspecting `pass` codebase and it almost looks like a
"secure" IPC channel (since its volatile).
But its main goal is actually to be faster:

> Since RAM is significantly faster than disk storage, you can use /dev/shm
  instead of /tmp for the performance boost, if your process is I/O
  intensive and extensively uses temporary files.

## Network

[Difference between eth0 and enp0s3? : linuxquestions](https://www.reddit.com/r/linuxquestions/comments/zt2f2q/difference_between_eth0_and_enp0s3/)
[PredictableNetworkInterfaceNames](https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/)

## Ubuntu

### Packaging (not specific to ubuntu)

[fpm - packaging made simple — fpm](https://fpm.readthedocs.io/en/v1.15.1/)

### apt

[How to unhold a package?](https://askubuntu.com/questions/164587/how-can-you-unhold-remove-a-hold-on-a-package#:~:text=sudo%20apt-mark%20unhold%20package_name)

[Can I rollback an apt-get upgrade if something goes wrong?](https://unix.stackexchange.com/a/236711)
[`apt-history`](https://web.archive.org/web/20161203192746/http://redclay.altervista.org/wiki/doku.php?id=projects:old-projects)
Cool little script to find latest `apt` changes:
```bash
function apt-history(){
      case "$1" in
        install)
              cat /var/log/dpkg.log | grep 'install '
              ;;
        upgrade|remove)
              cat /var/log/dpkg.log | grep $1
              ;;
        rollback)
              cat /var/log/dpkg.log | grep upgrade | \
                  grep "$2" -A10000000 | \
                  grep "$3" -B10000000 | \
                  awk '{print $4"="$5}'
              ;;
        *)
              cat /var/log/dpkg.log
              ;;
      esac
}
```

```bash
apt-get -s install $(apt-history rollback | tr '\n' ' ')
```
> if it does what you want remove the `-s` (aka `--dry-run`) and run it again.
