# C++

[Improve Print Logging with Line Pos Info & Modern C++](https://www.bfilipek.com/2019/04/file-pos-log.html)

[Detecting Problems With -fsanitize — Derick Rethans](https://derickrethans.nl/sanitize.html)

[rr debugger](https://rr-project.org/)
> rr aspires to be your primary C/C++ debugging tool for Linux,
 replacing — well, enhancing — gdb. You record a failure once,
 then debug the recording, deterministically, as many times as
 you want. The same execution is replayed every time.


[Vittorio Romeo on Twitter](https://twitter.com/supahvee1234/status/1160256508648677377?s=09)

> Is this the shortest possible way of perfectly propagating the result of a function invocation
  avoiding unnecessary copies/moves and allowing copy elision? #cpp I can't find a way to extract
  the `if constexpr` to a function (only a macro).
  https://t.co/VBAp1z3vy1 https://t.co/VSr60TRCC3
```cpp
template <typename F>
decltype(auto) invoke_log_return(F&& f)
{
    decltype(auto) result{std::forward<F>(f)()};
    std::printf("    ...logging here...\n");

    if constexpr(std::is_reference_v<decltype(result)>)
    {
        return static_cast<decltype(result)>(result);
    }
    else
    {
        return result;
    }
}
```

[c++ - Why can't argument be forwarded inside lambda without mutable? - Stack Overflow](https://stackoverflow.com/q/55992834)

## Test C++ lambda destructor
Capture an object with a destructor that outputs something.
The current solutions seems to work because of UB, a solution to that would be storing an std::function for each window

To test:
[ ] - Template deduction constness (sys1 : sys2<C1, const C2>)
[ ] - Best option seems to be <Args...> and function parameters Args& (value type is a copy so it doesn't pass intent on constness, rvalue don't seem useful in our use case)
[ ] System destructor
[ ] • check assembly output of systems vtable
[ ] • sizeof(system) and system container
[ ] • noexcept CTR and DTR
[ ] - Check standard or implement STD::array_size
[ ] - Check insertion of reference in array of values (copy??)

## Package managers

### vcpkg

[Get started with vcpkg](https://vcpkg.io/en/getting-started.html)
> It is recommended to clone vcpkg as a submodule to an existing project if possible for greater flexibility.
[VCVARSALL.BAT for Visual studio 2019 - Stack Overflow](https://stackoverflow.com/questions/55097222/vcvarsall-bat-for-visual-studio-2019)
[c - How to build x86 and/or x64 on Windows from command line with CMAKE? - Stack Overflow](https://stackoverflow.com/questions/28350214/how-to-build-x86-and-or-x64-on-windows-from-command-line-with-cmake)
[CMake: Visual Studio 15 2017 could not find any instance of Visual Studio - Stack Overflow](https://stackoverflow.com/questions/51668676/cmake-visual-studio-15-2017-could-not-find-any-instance-of-visual-studio)
[View more than one project/solution in Visual Studio - Stack Overflow](https://stackoverflow.com/questions/4079278/view-more-than-one-project-solution-in-visual-studio)
[Tutorial: Install a dependency from a manifest file | Microsoft Learn](https://learn.microsoft.com/en-us/vcpkg/consume/manifest-mode?tabs=cmake%2Cbuild-cmake)
[Install and use packages with CMake in Visual Studio | Microsoft Learn](https://learn.microsoft.com/en-us/vcpkg/get_started/get-started-vs?pivots=shell-cmd)
[c++ - cmake cannot find libraries installed with vcpkg - Stack Overflow](https://stackoverflow.com/questions/55496611/cmake-cannot-find-libraries-installed-with-vcpkg)
[cmake-presets(7) — CMake 3.28.1 Documentation](https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html)
[vcpkg Crash Course | Visual Studio 2022 | C++ libraries simplified! - YouTube](https://www.youtube.com/watch?v=0h1lC3QHLHU)
[Handmade Hero Day 001 - Setting Up the Windows Build - YouTube](https://www.youtube.com/watch?v=Ee3EtYb8d1o)

### conan

---

https://github.com/AmrDeveloper/ClangQL
> ClangQL is a tool that allow you to run SQL-like query on C/C++ Code instead of database files using the GitQL SDK
