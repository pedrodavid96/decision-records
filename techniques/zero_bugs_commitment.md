# The Zero Bugs Commitment

https://github.com/classgraph/classgraph/blob/master/Zero-Bugs-Commitment.md

(1) We will prioritize fixing bugs over implementing new features.
(2) We will take responsibility for code we have written or contributed to.
(3) We will be responsive during the bugfixing process.
(4) We will be respectful and inclusive.