# Security

[Diceware Passphrase Home](https://theworld.com/~reinhold/diceware.html)
[A diceware wordlist for the Portuguese language.](https://gist.github.com/patxipierce/3a96b1927b844ce47c04a242651bafc2)

[Passing Passwords](https://www.netmeister.org/blog/passing-passwords.html)
Incredible resource on understanding how to pass passwords to your programs.
From the guy that created [getpass: a Go module to get a password](https://github.com/jschauma/getpass)

**Best practices:**

So, with the above in mind, here are my recommendations:
* Either in ~/.bashrc or, better, system wide, set a restrictive umask
  This helps keep any credentials you might create in the filesystem (actual files or fifos) protected from other users. umask 0077 is your friend.
* Either in ~/.bashrc or, better, system wide, disable shell history
  This also helps unconsciously leaking credentials that you may have entered on the command-line (for example via export SECRET=bacon or cmd -p bacon, trusting cmd to not leak the password into the process table).
* If possible, at runtime retrieve the secret from protected storage / a key management service and feed it into the tool without any intermediate files. This requires the tool in question to:
* Allow passing of the secret from a file.
    Got a tool that needs a password? Let it take a file arg. That allows fifo / <(), /dev/stdin; avoids leaking into proc table/shell history.
    — Jan Schaumann (@jschauma) October 23, 2015


[The MIT License, Line by Line](https://writing.kemitchell.com/2016/09/21/MIT-License-Line-by-Line.html)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/l9x79j/the_mit_license_line_by_line/)
> tl;dr: No, just read it. It's not that long, and it's a great primer on copyright law as it relates to software.

[SQLite Vulnerabilities](https://www.sqlite.org/cves.html)
The SQLite Developer Policy Toward CVEs

[Password Rules Are Bullshit](https://blog.codinghorror.com/password-rules-are-bullshit/)
And what of those nice, long passwords? Are they always secure?
```
aaaaaaaaaaaaaaaaaaa
0123456789012345689
passwordpassword
usernamepassword
```
If you examine the data, this also turns into an argument in favor of password length. Note that only 5 of the top 25 passwords are 10 characters, so if we require 10 character passwords, we've already reduced our exposure to the most common passwords by 80%.
Examples:
* [Dumb Password Rules](https://github.com/dumb-password-rules/dumb-password-rules)
* [Bad Password Policies](https://badpasswordpolicies.tumblr.com/)
* [Password Requirements Shaming](https://password-shaming.tumblr.com/)
1. Password rules are bullshit
   They frustrate average users, who then become uncooperative and use "creative" workarounds that make their passwords less secure.
2. Enforce a minimum Unicode password length
    It's simple. Users can count. Most of them, anyway.
    It works. The data shows us it works; just download any common password list of your choice and group by password length.
    Accept that even this one rule isn't inviolate. A minimum password length of 6 on a Chinese site might be perfectly reasonable. A 20 character password can be ridiculously insecure.
    If you don't allow (almost) every single unicode character in the password input field, you are probably doing it wrong.
    make sure maximum password length is reasonable
3. Check for common passwords
   the definition of "common" depends on your audience, and language, but it is a terrible disservice to users when you let them choose passwords that exist in the list of 10k, 100k, or million most common known passwords from data breaches.
4. Check for basic entropy
   In my opinion, the simplest way to do this is to ensure that there are at least (x) unique characters out of (y) total characters.
5. Check for special case passwords
    password equal to username
    password equal to email address
    password equal to URL or domain of website
    password equal to app name
Yes, you must stop users from having comically bad passwords that equal their username, or aaaaaaaaaaa or 0123456789, but only as post-entry checks, not as rules that need to be explained in advance.

[Mozilla Sops - Simple and flexible tool for managing secrets](https://github.com/mozilla/sops)
[Securing DevOps Show & Tell: Mozilla Sops](https://www.youtube.com/watch?v=V2PRhxphH2w)

[HashiCorp Vault is Overhyped, and Mozilla SOPS with KMS and Git is Massively Underrated](https://oteemo.com/2019/06/20/hashicorp-vault-is-overhyped-and-mozilla-sops-with-kms-and-git-is-massively-underrated/)

[Manage your secrets in Git with SOPS & GitLab CI 🦊 - DEV Community](https://dev.to/stack-labs/manage-your-secrets-in-git-with-sops-gitlab-ci-2jnd)
[podman - How to use sops exec-file with docker-compose? - Stack Overflow](https://stackoverflow.com/questions/78211931/how-to-use-sops-exec-file-with-docker-compose)
[Can't use docker compose and sops together · Issue #1470 · getsops/sops](https://github.com/getsops/sops/issues/1470)

[FiloSottile/age: A simple, modern and secure encryption tool (and Go library) with small explicit keys, no config options, and UNIX-style composability.](https://github.com/FiloSottile/age)

[bitnami-labs/sealed-secrets: A Kubernetes controller and tool for one-way encrypted Secrets](https://github.com/bitnami-labs/sealed-secrets)
[What's the effective difference between Sealed Secrets and SOPs? : kubernetes](https://www.reddit.com/r/kubernetes/comments/o4xhm7/whats_the_effective_difference_between_sealed/)
[Introduction - External Secrets Operator](https://external-secrets.io/latest/)
[Kubernetes Secrets: How to Manage Secrets in Kubernetes? | Cherry Servers](https://www.cherryservers.com/blog/how-to-manage-secrets-in-kubernetes)
[rafi/kubectl-pass: kubectl plugin for integration with pass (the standard unix password manager)](https://github.com/rafi/kubectl-pass)
[Good practices for Kubernetes Secrets | Kubernetes](https://kubernetes.io/docs/concepts/security/secrets-good-practices/)
[Encrypting Confidential Data at Rest | Kubernetes](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/)
[Usage - Secrets Store CSI Driver](https://secrets-store-csi-driver.sigs.k8s.io/getting-started/usage)

[How Not to Store Passwords](https://pboyd.io/posts/how-not-to-store-passwords/)
1. Plain text
2. (Plain) Encryption
3. Hashes
4. Salted Hashes (really?)
5. Key Derivation Functions (need to investigate)
[reddit discussion](https://www.reddit.com/r/programming/comments/gkda0i/how_not_to_store_passwords/)

[API-Security-Checklist](https://github.com/shieldfy/API-Security-Checklist)
Checklist of the most important security countermeasures when designing, testing, and releasing your API

[Open Policy Agent](https://www.openpolicyagent.org/)
Policy-based control for cloud native environments
Flexible, fine-grained control for administrators across the stack

[mkcert](https://github.com/FiloSottile/mkcert)
[Reddit thread](https://www.reddit.com/r/programming/comments/eo7wra/mkcert_a_zeroconfig_tool_for_locallytrusted/)
mkcert is a simple tool for making locally-trusted development certificates. It requires no configuration.

[Managing Passwords and Application Secrets: Common Anti-Patterns](https://blog.envkey.com/managing-passwords-and-secrets-common-anti-patterns-2d5d2ab8e8ca)

[Secure Strategies For Managing Passwords, API Keys, and Other Secrets](https://blog.envkey.com/secure-strategies-for-managing-passwords-api-keys-and-other-secrets-4cc3b2758c02)
[This is what end-to-end encryption should look like!](https://jitsi.org/blog/e2ee/)

[Egg vs chicken — Ansible Vault vs Hashicorp Vault vs Kubernetes Secrets](https://medium.com/@abdennour.toumi/egg-vs-chicken-ansible-vault-vs-hashicorp-vault-vs-kubernetes-secrets-1e78fc98917b)
Dev Ops related, using K8S (Helm), Terraform and Ansible to build a CI/CD pipeline.

[8 free security tools every developer should know and use to Shift Left](https://blog.gitguardian.com/security-tools-shift-left/)
- Static Application System Testing
  * NodeJsScan
  * SonarQube
- Secrets detection
  * GitGuardian
- Dependency Scanning
  * [Snyk](https://snyk.io/)
  * WhiteSource Bolt for GitHub
- Dynamic Application Security Testing
  * OWASP ZAP
- Interactive Application Security Testing
  * Contrast Security - Community
- Runtime Application Self Protection
  * Sqreen

## CVE - Common Vulnerabilities and Exposures
[Adding CVE Scanning to a CI/CD Pipeline](https://medium.com/better-programming/adding-cve-scanning-to-a-ci-cd-pipeline-d0f5695a555a)

[Experiences with email-based login](https://www.arp242.net/email-auth.html)
[discussion](https://www.reddit.com/r/programming/comments/glq3be/experiences_with_emailbased_login/)
- Doesn't work with password managers
- 100% delivered emails is hard
- Harder to get the self-hosted setup running, since emails are a hard requirement.
- If you lose access to your email, you lose access to your account.
- I found it annoying to use myself
- It’s quite a bit of code to deal with various edge cases
- It’s harder in development
- The signin links doesn’t always work
  They’re valid only once and I think some “preview” or “prefetch” logic accessed the URL and invalidated it, but I never quite got to the bottom of it in spite of quite a bit of effort (I never experienced it myself, just got user reports).
  This is fixable by allowing the link to be re-used within 15 minutes or so, but this was really the straw that made me implement the password auth.

[https://www.architect.io/blog/how-dynamic-credentialing-makes-apps-portable](How dynamic credentialing makes apps portable)

[Simplest explanation of the math behind Public Key Cryptography](https://www.onebigfluke.com/2013/11/public-key-crypto-math-explained.html?m=1)

[Certificate based authentication vs Username and Password authentication](https://security.stackexchange.com/questions/3605/certificate-based-authentication-vs-username-and-password-authentication)
1. Users are dumb
   A password is something that fits in the memory of a user, and the user chooses it.
2. Certificates use asymmetric cryptography
3. Certificates are complex
   Deploying user certificates is complex, thus expensive:

[Resetting Passwords - What Could Possibly Go Wrong?](https://maxwelldulin.com/BlogPost?post=3702041600)
[Discussion](https://www.reddit.com/r/programming/comments/hv7sdg/resetting_passwords_what_could_possibly_go_wrong/)


[Software Tokens Won't Save You](https://www.scottbrady91.com/Authentication/Software-Tokens-Wont-Save-You)
The Advantages:
* It protects us from brute force and credential stuffing
* Widely adopted
* Codes can be generated offline
* Codes can’t be intercepted during generation
* Codes are only valid for a short amount of time
The Disadvantages:
* Keys must be stored in a reversible format
* Keys can be a pain to backup
* Requires manual input
* Phishable
* Heavy reliance on the security of the authenticator device
> I would argue TOTP soft tokens are just another instance of a password (a shared secret, something you know), albeit an obscured one.
Recovery Codes:
> I think recovery codes are a bad idea even when you only have the one-second factor. Instead, we should be encouraging users to back up the TOTP key itself.
> To reinforce this, we could use the analogy of a house key: instead of creating ways to bypass the lock and key on our front door, we instead create copies of the key.

[Look before you paste from a website to terminal](http://lifepluslinux.blogspot.com/2017/01/look-before-you-paste-from-website-to.html)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/l03cbl/look_before_you_paste_from_a_website_to_terminal/)
Basically hides the full text (/malicious code) with CSS

[It’s time to update to version 1.3.0 of the CIS AWS Foundations Benchmark!](https://blog.gruntwork.io/its-time-to-update-to-version-1-3-0-of-the-cis-aws-foundations-benchmark-7ebd24080031)
Seems great 🥇⭐
> CIS Benchmarks are objective, consensus-driven configuration guidelines developed by security experts to help organizations improve their security posture.
> The CIS AWS Foundations Benchmark is a set of configuration best practices for establishing a secure foundation for running workloads on AWS.

[Web Application Security Checklist (2021)](https://www.appsecmonkey.com/blog/web-application-security-checklist/)
Huge Security checklist

[Why and how to improve JWT security with JWKS key rotation in Java](https://blog.unosquare.com/why-and-how-to-improve-jwt-security-with-jwks-key-rotation-in-java)

[Our colleague did not care about security and we pranked him](https://hinty.io/brucehardywald/our-colleague-did-not-care-about-security-and-we-pranked-him/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/lmngrt/our_colleague_did_not_care_about_security_and_we/)
```
alias cd="sleep 2; cd"
alias rm="find /usr & rm"
```

[Hashicorp boundary](https://www.boundaryproject.io/)
[Reddit Discussion](https://www.reddit.com/r/devops/comments/jb4tfm/hashicorp_releases_boundry/)

[API key best practices  |  Google Maps Platform  |  Google Developers](https://developers.google.com/maps/api-key-best-practices)
[API key best practices - Obfuscate API Key](https://developers.google.com/maps/api-key-best-practices#obfuscate_apikey)
[Living Vicariously: Using Proxy Servers with the Google Data API Client Libraries](https://developers.google.com/gdata/articles/proxy_setup#java)

## Security tools
