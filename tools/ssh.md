# SSH

[How SSH port became 22](https://www.ssh.com/ssh/port)
SSH was designed to replace both telnet (port 23) and ftp (port 21).
Port 22 was free.

Port numbers were allocated by IANA (Internet Assigned Numbers Authority).
At the time, that meant an esteemed Internet pioneer called Jon Postel
and Joyce K. Reynolds.
To the author Jon felt outright scary, having authored all the main
Internet RFCs!

Response:
> We have assigned port number 22 to ssh, with you as the point of
contact.

## SSH Config alias

Given the following usage of `ssh` to connect to a server

`ssh -i ~/.ssh/<rsa_key> <username>@<hostname / ip>`

using the following config
```
host <alias>
  User <username>
  HostName <hostname / ip>
  IdentityFile <rsa key>
```

will allow you to use the following command
`ssh website`


## Add rsa-key to ssh-agent (avoiding ssh asking for pass phrase every time)
Given a running ssh-agent
`ssh-agent bash`

>>>
An **agent** is a program that keeps your keys in memory so that you only need
to unlock them once, instead of every time. ssh-agent does this for SSH keys.

The usual methods for **starting ssh-agent** are:

    * `eval `ssh-agent`` – this runs the agent in background, and sets the
      apropriate environment variables for the current shell instance.

      (ssh-agent, when started with no arguments, outputs commands to be
      interpreted by your shell.)

    * `exec ssh-agent bash` – starts a new instance of the bash shell,
      replacing the current one.

      (With one or more arguments, ssh-agent doesn't output anything, but
      starts the specified command: in this case, the bash shell, but
      technically it could be anything.)

The second method is sometimes preferred, since it automatically kills
ssh-agent when you close the terminal window. (When starting it with eval,
the agent would remain running, but inaccessible.)

However, this only starts an empty agent. To actually make it useful, you need
to use `ssh-add`, which unlocks your keys (usually ~/.ssh/id_*) and loads them
into the agent, making them accessible to ssh or sftp connections.

In:
https://superuser.com/a/284397


[If you’re not using SSH certificates you’re doing SSH wrong](https://smallstep.com/blog/use-ssh-certificates/)
[DIY Single Sign-On for SSH](https://smallstep.com/blog/diy-single-sign-on-for-ssh/)
[SSH Certificates: a way to scale SSH access](https://www.youtube.com/watch?v=Lqbtn0Gjnho)

## SSH Agent (forwarding and alternatives)

[The Problem with SSH Agent Forwarding](https://defn.io/2019/04/12/ssh-forwarding/)
Simply put: if your jump box is compromised and you use SSH agent forwarding to connect to another machine through it, then you risk also compromising the target machine!
Instead, you should use either ProxyCommand or ProxyJump (added in OpenSSH 7.3).

[SSH Agent Forwarding considered harmful](https://heipei.io/2015/02/26/SSH-Agent-Forwarding-considered-harmful/)
The problem is that while you’re connected to host A, a forwarding socket will be set up so that the SSH client on host A can connect to the ssh-agent on your workstation to perform authentication on its behalf. This means that anyone with sufficient permission on host A will be able to use that socket to connect to and use your local ssh-agent.
The alternative: ProxyCommand to the rescue

[SSH Agent Explained](https://smallstep.com/blog/ssh-agent-explained/)
Use ProxyJump: a safer alternative
If ProxyJump doesn't work… ProxyCommand

---

[How To Set Up Multi-Factor Authentication for SSH on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-multi-factor-authentication-for-ssh-on-ubuntu-16-04)

[The Black Magic Of SSH / SSH Can Do That?](https://www.reddit.com/r/programming/comments/25xe3p/the_black_magic_of_ssh_ssh_can_do_that/)
* Can be used for remote port forwarding. Port from local machine will be exposed as port on remote machine so that all connections make on that remote machine port will be forwarded to your local machine. e.g. expose local Apache port to remote machine through ssh tunnel for testing non-deployed web apps. Share tmux session with many user with 0 configuration.
* You can change ssh settings of a live connection through the ssh console without having to reconnect.
* Can also do local port forwarding. i.e. connect to remote port through a local port. e.g. initiate VNC connection on a server which does not expose the VNC port to the outside world.
* Can do dynamic port forwarding. Creates a SOCKS 5 proxy on your local machine. Makes all your network connections tunnel through ssh and go outbound from a remote machine. e.g. Use Netflix as if you are from another geographic location.
* X11 forwarding
* Agent forwarding. Forwards your private key to remote machine so that any connections made from the remote machine can authenticate you using your credentials on your local machine. e.g. ssh to remote machine, push code from there to your github using the credentials on the local machine.
* Run a remote command (rather than a shell). Pipe data from the remote command through the ssh connection. e.g. use it in local scripts.
* Can force remote command to execute for particular user when ssh connection created with the authorization_keys file.
* Make your favorite options the default on a per server address basis.
* Create host aliases. e.g. ssh mygit connects to git.myserver.com
* ProxyCommand: configure how remote stdin/stdout is connect. e.g. automatically have an ssh connection to remote server connect to another ssh server on an internal private network that is not accessible from the outside.
* Mutliplex connection: have multiple ssh connections all go through a single TCP/IP connection.
* Compress data going through ssh connection
* keepalive
* SSHFS. Mount remote directories as if they were filesystems.

[Why putting SSH on another port than 22 is bad idea](https://www.adayinthelifeof.nl/2012/03/12/why-putting-ssh-on-another-port-than-22-is-bad-idea/)

[SSH attacks : synology](https://www.reddit.com/r/synology/comments/bk90hj/ssh_attacks/)

[Fearless SSH: short-lived certificates bring Zero Trust to infrastructure](https://blog.cloudflare.com/intro-access-for-infrastructure-ssh/)
[Fearless SSH: Short-lived certificates bring Zero Trust to infrastructure | Hacker News](https://news.ycombinator.com/item?id=41923429)
[notion/bastion: Trove's SSH Bastion](https://github.com/notion/bastion)

[Get passphrase for an SSH key from password store, securely](https://gist.github.com/sinewalker/d055fe92f20151ee5921cb7562a20806)
[ssh-pass | Milosophical Me](https://milosophical.me/blog/2018/ssh-pass.html)
[Tell git which SSH config file to use - Stack Overflow](https://stackoverflow.com/questions/41219524/tell-git-which-ssh-config-file-to-use)
