# Chocolatey

## New computer

```
choco install -yr ^
    jdk9 ^
    7zip ^
    androidstudio ^
    discord ^
    docker-for-windows ^
    f.lux ^
    Firefox ^
    git ^
    gpg4win ^
    gradle ^
    intellijidea-community ^
    kotlinc ^
    nodejs ^
    qbittorrent ^
    qtpass ^
    rust ^
    spotify ^
    steam ^
    virtualbox ^
    visualstudio2017-installer ^
    visualstudio2017community ^
    visualstudiocode ^
    dotnetcore-sdk ^
    yarn
```

visualstudio2017buildtools
Docker Toolbox
vim? (nvim?)
Boinc
.NET Core
cmder
AMD Drivers
git (lfs)
vlc media player
emsdk

## Steps

Uninstall Start Menu Applications
XBox Games removal
Hour format