# Markdown

[Markdown Include](https://github.com/cmacmackin/markdown-include)
Preprocessor to include markdown inside markdown.

[Create impeccable MIME email from markdown](https://begriffs.com/posts/2020-07-16-generating-mime-email.html)
https://github.com/begriffs/mimedown
Attachments
Mutt without HTML render: Content-Disposition: inline attachments for each code snippet.

[markdownlint-cli2 is a new kind of command-line interface for markdownlint](https://dlaa.me/blog/post/markdownlintcli2)
Explains motivation between a completely different project from `markdownlintcli`
https://github.com/markdownlint/markdownlint
https://github.com/DavidAnson/markdownlint (node.js port)

[GitHub - charmbracelet/glamour: Stylesheet-based markdown rendering for your CLI apps 💇🏻‍♀️](https://github.com/charmbracelet/glamour#styles)

[wooorm/markdown-rs: CommonMark compliant markdown parser in Rust with ASTs and extensions](https://github.com/wooorm/markdown-rs)
