# CMake

[Step 1: A Basic Starting Point — CMake 3.28.0-rc5 Documentation](https://cmake.org/cmake/help/latest/guide/tutorial/A%20Basic%20Starting%20Point.html#exercise-1-building-a-basic-project)

[Modules — Mastering CMake](https://cmake.org/cmake/help/book/mastering-cmake/chapter/Modules.html)

[GitHub - nunofachada/cmake-git-semver: CMake module which gets project version from git tags](https://github.com/nunofachada/cmake-git-semver/tree/master)