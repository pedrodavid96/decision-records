# SpellChecker CI

Allow projects to have custom glossary, to be used by CI and to be included in Documentation - unmeasurable usefulness for acronyms.

[yaspeller-ci](https://github.com/ai/yaspeller-ci)
Fast spelling check for Travis CI and AppVeyor.
