# F#

[F# for fun and profit](https://fsharpforfunandprofit.com)

[low-risk-ways-to-use-fsharp-at-work](https://fsharpforfunandprofit.com/posts/low-risk-ways-to-use-fsharp-at-work/)
1. [5. Use FAKE for build and CI scripts](https://fsharpforfunandprofit.com/posts/low-risk-ways-to-use-fsharp-at-work-2/#fake)
2. [Using F# for testing](https://fsharpforfunandprofit.com/posts/low-risk-ways-to-use-fsharp-at-work-3/)
3. [sql-etl](https://fsharpforfunandprofit.com/posts/low-risk-ways-to-use-fsharp-at-work-4/#sql-etl)

[Writing high performance F# Code](https://bartoszsypytkowski.com/writing-high-performance-f-code/)
Looks awesome 🥇⭐

Surprisingly in-depth article, which should be pretty useful not only for F# but for basically any language (including taking ideas to build an high performance language).

Goes in depth on the following topics:
* Understand memory layout of value types
* Readonly and ref-like structs
* Padding
* CPU cache lines
* False sharing
* Using structs and collections together
* Discriminated unions as value types (comparison with Rust)
* Learn to work with virtual calls
* Make use of vectorization
* Immutable code and atomic updates
* Atomic Compare and Swap
* Inlining
