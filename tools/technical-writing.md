# Technical Writing

https://developers.google.com/tech-writing

## Technical Writing One

### Just enough Grammar

| Part of Speech |                               Definition                               |                           Example                             |
|:--------------:|:----------------------------------------------------------------------:|:-------------------------------------------------------------:|
| Noun           | a person, place, concept, or thing                                     | **Sam** runs **races**.                                       |
| Pronoun        | a noun that replaces another noun (or larger structure)                | Sam runs races. **He** likes to compete.                      |
| Adjective      | a word or phrase that modifies a noun                                  | Sam wears **blue** shoes.                                     |
| Verb           | an action word or phrase                                               | Sam **runs** races.                                           |
| Adverb         | a word or phrase that modifies a verb, an adjective, or another adverb | Sam runs **slowly**.                                          |
| Preposition    | a word or phrase specifying the positional relationship of two nouns   | Sam's sneakers are seldom **on** his shelf.                   |
| Conjunction    | a word that connects two nouns or phrases                              | Sam's trophies **and** ribbons live only in his imagination.  |
| Transition     | a word or phrase that connects two sentences                           | Sam runs races weekly. **However**, he finishes races weakly. |

### Words

#### Define new or unfamiliar terms

When writing or editing, learn to recognize terms that might be unfamiliar to some or all of your target audience.
* If the term already exists, link to a good existing explanation. (Don't reinvent the wheel.)
* If your document is introducing the term, define the term. If your document is introducing many terms, collect the definitions into a glossary.

#### Use terms consistently

example, the following paragraph mistakenly renames Protocol Buffers to protobufs:
> Protocol Buffers provide their own definition language. Blah, blah, blah. And that's why protobufs have won so many county fairs.

However, the following paragraph is fine:
> Protocol Buffers (or protobufs for short) provide their own definition language. Blah, blah, blah. And that's why protobufs have won so many county fairs.

#### Use acronyms properly

On the initial use of an unfamiliar acronym within a document or a section, spell out the full term, and then put the acronym in parentheses.
**Put both the spelled-out version and the acronym in boldface**. For example:

> This document is for engineers who are new to the **Telekinetic Tactile Network (TTN)** or need to understand how to order TTN replacement parts through finger motions.

You may then use the acronym going forward.
Do not cycle back-and-forth between the acronym and the expanded version in the same document.

##### Use the acronym or the full term?

readers convert TTN to Telekinetic Tactile Network in their heads,
so the "shorter" acronym actually takes a little longer to process than the full term.

Heavily used acronyms develop their own identity.
After a number of occurrences, readers generally stop expanding acronyms into the full term.
e.g.: HTML

Here are the guidelines for acronyms:
* Don't define acronyms that would only be used a few times.
* Do define acronyms that meet both of the following criteria:
    - The acronym is significantly shorter than the full term.
    - The acronym appears many times in the document.

#### Disambiguate pronouns

pronouns tend to introduce errors ...
Using pronouns improperly causes the cognitive equivalent of a null pointer error in your readers’ heads.
In many cases, you should simply avoid the pronoun and just reuse the noun.
However, the utility of a pronoun sometimes outweighs its risk (as in this sentence).

Consider the following pronoun guidelines:

* Only use a pronoun after you've introduced the noun;
  never use the pronoun before you've introduced the noun.
* Place the pronoun as close as possible to the referring noun.
  In general, if more than five words separate your noun from your pronoun,
  consider repeating the noun instead of using the pronoun.
* If you introduce a second noun between your noun and your pronoun,
  reuse your noun instead of using a pronoun.

##### It and they

The following pronouns cause the most confusion in technical documentation:

* it
* they, them, and their

For example, in the following sentence, does It refer to Python or to C++?

> Python is interpreted, while C++ is compiled. **It** has an almost cult-like following.

As another example, what does their refer to in the following sentence?

> Be careful when using Frambus or Carambola with HoobyScooby or BoiseFram because a bug in **their** core may cause accidental mass unfriending.

#### This and that

Consider two additional problem pronouns:

* this
* that

For example, in the following ambiguous sentence, This could refer to Frambus, to Foo, or to both:

> You may use either Frambus or Foo to calculate derivatives. **This** is not optimal.

Use either of the following tactics to disambiguate this and that:

* Replace **this** or **that** with the appropriate noun.
* Place a noun immediately after **this** or **that**.

For example, either of the following sentences disambiguate the previous example:

> Overlapping functionality is not optimal.

> This overlapping functionality is not optimal.

#### Active voice vs. passive voice

The vast majority of sentences in technical writing should be in active voice.

In an active voice sentence, an actor acts on a target. That is, an active voice sentence follows this formula:

> Active Voice Sentence = actor + verb + target

e.g.:

> The cat sat on the mat

A passive voice sentence reverses the formula. That is, a passive voice sentence typically follows the following formula:

> Passive Voice Sentence = target + verb + actor

e.g.:

> The mat was sat on by the cat.

Some passive voice sentences omit an actor. For example:

> The mat was sat on.

Who or what sat on the mat? A cat? A dog? A T-Rex?
Readers can only guess.
Good sentences in technical documentation identify who is doing what to whom.

##### Recognize passive verbs

Passive verbs typically have the following formula:

> passive verb = form of be + past participle verb

Although the preceding formula looks daunting, it is actually pretty simple:
* A form of be in a passive verb is typically one of the following words:
  - is/are
  - was/were
* A past participle verb is typically a plain verb plus the suffix ed.
  For example, the following are past participle verbs:
  - interpreted
  - generated
  - formed

Unfortunately, some past participle verbs are irregular; that is,
the past participle form does not end with the suffix ed. For example:
* sat
* known
* frozen

Putting the form of be and the past participle together yields passive verbs, such as the following:
* was interpreted
* is generated
* was formed
* is frozen

If the phrase contains an actor, a preposition ordinarily follows the passive verb.
(That preposition is often a key clue to help you spot passive voice.)
The following examples combine the passive verb and the preposition:
* was interpreted as
* is generated by
* was formed by
* is frozen by

##### Imperative verbs are typically active

It is easy to mistakenly classify sentences starting with an imperative verb as passive.
An **imperative verb** is a command.
Many items in numbered lists start with imperative verbs. For example, Open and Set in the following list are both imperative verbs:

* Open the configuration file.
* Set the Frombus variable to False.

Sentences that start with an imperative verb are typically in active voice,
even though they do not explicitly mention an actor.
Instead, sentences that start with an imperative verb imply an actor.
The implied actor is **you**.

##### Prefer active voice to passive voice

Use the active voice most of the time.
Use the passive voice sparingly.
Active voice provides the following advantages:

* Most readers mentally convert passive voice to active voice.
  Why subject your readers to extra processing time?
  By sticking to active voice, readers can skip the preprocessor stage and go straight to compilation.
* Passive voice obfuscates your ideas, turning sentences on their head.
  Passive voice reports action indirectly.
* Some passive voice sentences omit an actor altogether, which forces the reader to guess the actor's identity.
* Active voice is generally shorter than passive voice.

##### Scientific research reports (optional material)

Passive voice runs rampant through certain scientific research reports.
In those research reports, experimenters and their equipment often disappear,
leading to passive sentences that start off as follows:
* It has been suggested that...
* Data was taken...
* Statistics were calculated...
* Results were evaluated.

Many scientific journals have embraced active voice. We encourage the remainder to join the quest for clarity.

#### Clear sentences

Comedy writers seek funniest results, horror strive for the scariest, technical writes aim for **the clearest**.

##### Choose strong verbs

Reduce imprecise, weak, or generic verbs, such as the following:
* forms of be: is, are, am, was, were, etc.
* occur
* happen

| Weak Verb                                             | Strong Verb                                           |
|:-----------------------------------------------------:|:-----------------------------------------------------:|
| The error **occurs** when clicking the Submit button. | Clicking the Submit button **triggers** the error.    |
| This error message **happens** when...                | The system **generates** this error message when...   |
| We **are** very careful to ensure... 	                | We carefully **ensure**...                            |

Generic verbs often signal other ailments, such as:
* an imprecise or missing actor in a sentence
* a passive voice sentence

##### Reduce there is/there are

Generic.

In the best case scenario, you may simply delete it.

You can sometimes repair a **There is** or **There are** sentence by moving
the true subject and true verb from the end of the sentence to the beginning.

In still other situations, writers start sentences with **There is** or **There are**
to avoid the hassle of creating true subjects or verbs.
If no subject exists, consider creating one.

> There is no guarantee that the updates will be received in sequential order.

Replacing "There is" with a meaningful subject (such as **clients**) creates
a clearer experience for the reader:

> Clients might not receive the updates in sequential order.

##### Minimize certain adjectives and adverbs

Adjectives and adverbs perform amazingly well in fiction and poetry.

Unfortunately, adjectives and adverbs sometimes make technical readers bark
_loudly_ and _ferociously_. 😂

> Setting this flag makes the application run screamingly fast.

Granted, screamingly fast gets readers attention but not necessarily in a good way. Feed your technical readers factual data instead of marketing speak. Refactor amorphous adverbs and adjectives into objective numerical information. For example:

> Setting this flag makes the application run 225-250% faster.

#### Short sentences

Same rules as lines of code:

* Shorter documentation reads faster than longer documentation.
* Shorter documentation is typically easier to maintain than longer documentation.
* Extra lines of documentation introduce additional points of failure.

##### Focus each sentence on a single idea

In the example, a full stop / end of sentence is preferred to multiple `and`s and `,`.

##### Convert some long sentences to lists

Consider a bulleted list instead of the conjunction `or` in a long sentence.

Consider a bulleted or numbered list when you see an embedded list of items or tasks
within a long sentence.

##### Eliminate or reduce extraneous words

Many sentences contain filler—textual junk food that consumes space without nourishing the reader.

The following table suggests replacements for a few common bloated phrases:

| Wordy                     | Concise   |
|:-------------------------:|:---------:|
| at this point in time 	| now       |
| determine the location of | find      |
| is able to 	            | can       |

##### Reduce subordinate clauses

A **clause** is an independent logical fragment of a sentence, which contains an actor and an action.
Every sentence contains the following:
* a main clause
* zero or more subordinate clauses

You can usually identify subordinate clauses by the words that introduce them.
The following list (by no means complete) shows common words that introduce subordinate clauses:
* which
* that
* because
* whose
* until
* unless
* since
Some subordinate clauses begin with a comma and some don't.

When editing, scrutinize subordinate clauses.
Keep the `one sentence = one idea` formula in mind.
Do the subordinate clauses in a sentence extend the single idea or do they branch off into a separate idea?
If the latter, consider dividing the offending subordinate clause(s) into separate sentences.

##### Distinguish that from which

In the United States, reserve **which** for nonessential subordinate clauses,
and use **that** for an essential subordinate clause that the sentence can't live without.

If you read a sentence aloud and hear a pause just before the subordinate clause, then use **which**.
If you don't hear a pause, use **that**.

Place a comma before **which**; do not place a comma before **that**.

#### Lists and tables

Good lists can transform technical chaos into something orderly.
Technical readers generally love lists.
Therefore, when writing, seek opportunities to convert prose into lists.

##### Choose the correct type of list

* bulleted lists
* numbered lists
* embedded lists

Use a **bulleted list** for unordered items; use a **numbered list** for ordered items

An **embedded list** (sometimes called a **run-in** list) contains items stuffed within a sentence.
For example, the following sentence contains an embedded list with four items.

> The llamacatcher API enables callers to create and query llamas, analyze alpacas, delete vicugnas, and track dromedaries.

Generally speaking, embedded lists are a poor way to present technical information.
Try to transform embedded lists into either bulleted lists or numbered lists.

##### Keep list items parallel

All items in a parallel list look like they "belong" together.
That is, all items in a parallel list match along the following parameters:
* grammar
* logical category
* capitalization
* punctuation

Conversely, at least one item in a nonparallel list fails at least one of the preceding consistency checks.

The first item in a list establishes a pattern that readers expect to see repeated in subsequent items.

##### Start numbered list items with imperative verbs

##### Punctuate items appropriately

If the list item is a sentence, use sentence capitalization (capitalize first letter) and punctuation.
Otherwise, do not use sentence capitalization and punctuation.

##### Create useful tables

Given a page containing multiple paragraphs and a single table, engineers' eyes zoom towards the table.

Consider the following guidelines when creating tables:

* Label each column with a meaningful header.
  Don't make readers guess what each column holds.
* Avoid putting too much text into a table cell.
  If a table cell holds more than two sentences, ask yourself whether that information belongs in some other format.
* Although different columns can hold different types of data, strive for parallelism within individual columns.
  For instance, the cells within a particular table column should not be a mixture of numerical data and famous circus elephants.

**Note:** Some tables don't render well across all form factors.
For example, a table that looks great on your laptop may look awful on your phone.

##### Introduce each list and table

We recommend introducing each list and table with a sentence that tells readers what the list or table represents.

Although not a requirement, we recommend putting the word **following** into the introductory sentence.

#### Paragraphs

##### Write a great opening sentence

The opening sentence is the most important sentence of any paragraph.
Busy readers focus on opening sentences and sometimes skip over subsequent sentences.

##### Focus each paragraph on a single topic

When revising, ruthlessly delete (or move to another paragraph) any sentence
that doesn't directly relate to the current topic.

##### Don't make paragraphs too long or too short

Readers generally welcome paragraphs containing **three to five sentences**,
but will avoid paragraphs containing more than about **seven sentences**.

Conversely, **don't make paragraphs too short.**
If your document contains plenty of one-sentence paragraphs, your organization is faulty.
Seek ways to **combine** those one-sentence paragraphs **into cohesive multi-sentence paragraphs**
or possibly into lists.

##### Answer what, why, and how

Good paragraphs answer the following three questions:
1. What are you trying to tell your reader?
2. Why is it important for the reader to know this?
3. How should the reader use this knowledge?
   Alternatively, how should the reader know your point to be true?

#### Audience

Make sure your document provides the information your audience needs that your audience doesn't already have.

* Define your audience.
* Determine what your audience needs to learn.
* Fit documentation to your audience.

##### Define your audience

Serious efforts might involve:
* Surveys
* User experience studies
* Focus groups
* Documentation testing

You probably don't have that much time, so a simpler approach might be:
Begin by identifying your audience's role(s).
Sample roles include:
* software engineers
* technical, non-engineer roles (such as technical program managers)
* scientists
* professionals in scientific fields (for example, physicians)
* undergraduate engineering students
* graduate engineering students
* non-technical positions

With that in mind, not everyone in the same role shares exactly the same knowledge.
For example, you will have Software Engineers with different experiences in different programming languages.

Roles, by themselves, are insufficient for defining an audience.
That is, you must also consider your audience's proximity to the knowledge.
The software engineers in Project Frombus know something about related Project Dingus but nothing about unrelated Project Carambola.

Time also affects proximity. Almost all software engineers, for example, studied calculus.
However, most software engineers don't use calculus in their jobs, so their knowledge of calculus gradually fades.

##### Determine what your audience needs to learn

Write down a list of everything your target audience needs to learn to accomplish goals.
In some cases, the list should hold tasks that the target audience needs to perform.
For example:
> After reading the documentation, the audience will know how to do the following tasks:
> * Use the Zylmon API to list hotels by price.
> ...

If you are writing a design spec, then your list should focus on information your target audience should learn rather than on mastering specific tasks:
For example:
> After reading the design spec, the audience will learn the following:...
> * Three reasons why Zylmon outperforms Zyljeune.
> * Five reasons why Zylmon consumed 5.25 engineering years to develop.

##### Fit documentation to your audience

You must create explanations that satisfy your audience's curiosity rather than your own.
No easy answers, but a few parameters to focus on:

###### Vocabulary and concepts

Match your vocabulary to your audience.

Be mindful of proximity. - As your target audience widens, assume that you must explain more.

Unless you are writing specifically for other experienced members of your team, you typically must explain more than you expect (e.g.: new members in your team that don't yet have context).

###### Curse of knowledge

Expert understanding of a topic ruins explanations to newcomers.
It is easy to forget that novices don’t know what you already know.

###### Simple words

English is not the native language of a significant percentage of technical readers.
Therefore, prefer simple words over complex words.

###### Cultural neutrality and idioms

Idioms are phrases whose overall meaning differs from the literal meaning of the individual words in that phrase. For example, the following phrases are idioms:

* a piece of cake
* Bob's your uncle

#### Documents

##### State your document's scope

A good document begins by defining its scope. For example:
> This document describes the overall design of Project Frambus.

A better document additionally defines its non-scope--the topics not covered
that the target audience might expect your document to cover.
For example:
> This document does not describe the design for the related technology, Project Froobus.

##### State your audience

A good document explicitly specifies its audience. For example:
> I wrote this document for the test engineers supporting Project Frambus.

Beyond the audience's role, a good audience declaration might also specify
any prerequisite knowledge or experience.
For example:
> This document assumes that you understand matrix multiplication and how to brew a really good cup of tea.

In some cases, the audience declaration must also specify prerequisite documents.
For example:
> You must read "Project Froobus: A New Hope" prior to reading this document.

##### Establish your key points up front

Ensure that the start of your document answers your readers' essential questions.

##### Write for your audience

###### Define the audience

* Who is your target audience?
* What do your readers already know before they read your document?
* What should your readers know or be able to do after they read your document?

###### Organize for your audience's needs

###### Break your topic into sections

Top-level section headers > low-level section headers > paragraphs and lists

#### Punctuation

##### Commas

Programming languages enforce clear rules about punctuation.
In English, by contrast, the rules regarding commas are somewhat hazier.
As a guideline, insert a comma wherever a reader would naturally pause somewhere within a sentence.
For the musically inclined, if a period is a whole note rest, then a comma is perhaps a half-note or quarter-note rest.

Some situations require a comma. For example, use commas to separate items in an embedded list like the following:
> Our company uses C++, Python, Java, and JavaScript.
You might be wondering about a list's final comma, the one inserted between items N-1 and N.
This comma—known as the **serial comma** or **Oxford comma**—is controversial. We recommend supplying that final comma simply because technical writing requires picking the least ambiguous solution.

In sentences that express a condition, place a comma between the condition and the consequence.
For example:
> If the program runs slowly, try the --perf flag.
> If the program runs slowly, then try the --perf flag.

You can also wedge a quick definition or digression between a pair of commas as in the following example:
> Python, an easy-to-use language, has gained significant momentum in recent years.

Finally, **avoid** using a comma to paste together two independent thoughts.
For example, the comma in the following sentence is guilty of a punctuation felony called a **comma splice**:
> Samantha is a wonderful coder, she writes abundant tests.

##### Semicolons

A period separates distinct thoughts; a semicolon unites highly related thoughts.
For example, notice how the semicolon in the following sentence unites the first and second thoughts:
> Rerun Frambus after updating your configuration file; don't rerun Frambus after updating existing source code.

The thoughts preceding and following the semicolon must each be grammatically complete sentences.
For example, the following semicolon is **incorrect** because the passage following the semicolon is not a complete sentence:
> Rerun Frambus after updating your configuration file; not after updating existing source code.

Before using a semicolon, ask yourself whether the sentence would still make sense
if you flipped the thoughts to opposite sides of the semicolon.

You should almost always use commas, not semicolons, to separate items in an embedded list.
For example, the following use of semicolons is incorrect:

Many sentences place a transition word or phrase immediately after the semicolon.
In this situation, place a comma after the transition.

##### Em-Dashes

An em-dash represents a longer pause—a bigger break—than a comma.
If a comma is a quarter note rest, then an em-dash is a half-note rest.
For example:
> C++ is a rich language—one requiring extensive experience to master.

Writers sometimes use a pair of em-dashes to block off a digression, as in the following example:
> Protocol Buffers—often nicknamed protobufs—encode structured data in an efficient yet extensible format.

Could we have used commas instead of em-dashes in the preceding examples? Sure.
Why did we choose an em-dash instead of a comma? Feel. Art. Experience.
Remember—punctuation in English is squishy and malleable.

##### Parentheses

Use parentheses to hold minor points and digressions.
Parentheses inform readers that **the enclosed text isn't critical**.
Some editors feel that text that deserves parentheses doesn't deserve to be in the document.
As a compromise, keep parentheses to a minimum in technical writing.

The rules regarding periods and parentheses aren't always clear. Here are the standard rules:
* If a pair of parentheses holds an entire sentence, the period goes inside the closing parenthesis.
* If a pair of parentheses ends a sentence but does not hold the entire sentence, the period goes just outside the closing parenthesis.

## Technical Writing Two

### Self-editing

Transforming a blank page into a first draft is often the hardest step.
After you write a first draft, make sure you set aside plenty of time to refine your document.

#### Adopt a style guide

e.g.: https://developers.google.com/style
You might prefer to start by adopting the [style-guide highlights](https://developers.google.com/style/highlights)
**Note:** For smaller projects, such as team documentation or a small open source project, you might find the highlights are all you need.

#### Think like your audience

#### Read it out loud

Alternatively, consider using a screen reader to voice the content for you.

#### Come back to it later

#### Change the context

Some writers like to print their documentation and review a paper copy, red pencil in hand.
For a modern take on this classic tip, copy your draft into a different document and change the font, size, and color.

#### Find a peer editor

### Organizing large documents

The following tactics can help:
* Choosing to write a single, large document or a set of documents
* Organizing a document
* Adding navigation
* Disclosing information progressively

#### When to write large documents

You can organize a collection of information into longer standalone documents or a set of shorter interconnected documents.
A set of shorter interconnected documents is often published as a website, wiki, or similar structured format.

Some readers respond more positively than others to longer documents.
Consider the following guidelines:
* How-to guides, introductory overviews, and conceptual guides
  often work better as shorter documents when aimed at readers who are new to the subject matter.
  Readers will often be completely new to your subject and just wants to gain a quick and
  general overview of the topic.
* In-depth tutorials, best practice guides, and command-line reference pages can work well as lengthier documents,
  especially when aimed at readers who already have some experience with the tools and subject matter.
* A great tutorial can rely on a narrative to lead the reader through a series of related tasks in a longer document.
  However, even large tutorials can sometimes benefit from being broken up into smaller parts.
* Many longer documents aren't designed to be read in one sitting. For example, users typically scan through a
  reference page to search for an explanation of a command or flag.

The remainder of this unit covers techniques that can be useful for writing longer documents,
such as tutorials and some conceptual guides.

#### Organize a document

Techniques for planning a longer document.

##### Outline a document

Starting with a structured, high-level outline can help you group topics and determine where more detail is needed.

Some practical tips:
* Before you ask your reader to perform a task, explain to them why they are doing it.
* Limit each step of your outline to describing a concept or completing a specific task.
* Structure your outline so that your document introduces information when it's most relevant to your reader.
* Consider explaining a concept and then demonstrating how the reader can apply it either in a sample project or in their own work.
  Documents that alternate between conceptual information and practical steps can be a particularly engaging way to learn.
* Before you start drafting, share the outline with your contributors.
  Outlines are especially useful if you're working with a team of contributors who are going to review and test your document.

##### Introduce a document

If readers of your documentation can't find relevance in the subject, they are likely to ignore it.
Provide an introduction that includes the following information:

* What the document covers.
* What prior knowledge you expect readers to have.
* What the document doesn't cover.

Remember that you want to keep your documentation easy to maintain,
so don't try to cover everything in the introduction.

##### Add navigation

Clear navigation includes:

* introduction and summary sections
* a clear, logical development of the subject
* headings and subheadings that help users understand the subject
* a table of contents menu that shows users where they are in the document
* links to related resources or more in-depth information
* links to what to learn next

The tips in the following sections can help you plan the headings in your documentation.

###### Prefer task-based headings

Choose a heading that describes the task your reader is working on.
Avoid headings that rely on unfamiliar terminology or tools.

###### Provide text under each heading

Most readers appreciate at least a brief introduction under each heading to provide some context.
Avoid placing a level three heading immediately after a level two heading, as in the following example:

##### Disclose information progressively

Readers are more likely to be receptive to longer documents that progressively disclose new information to them when they need it.
The following techniques can help you incorporate progressive disclosure in your documents:
* Where possible, try introducing new terminology and concepts near to the instructions that rely on them.
* Break up large walls of text. To avoid multiple large paragraphs on a single page, aim to introduce tables, diagrams, lists, and headings where appropriate.
* Break up large series of steps. If you have a particularly long list of complicated steps, try to re-arrange them into shorter lists that explain how to complete sub-tasks.
* Start with simple examples and instructions, and add progressively more interesting and complicated techniques.
  For example, in a tutorial for creating forms, start by explaining how to handle text responses,
  and then introduce other techniques to handle multiple choice, images, and other response types.

### Illustrating

According to research by [Sung and Mayer (2012)](https://www.sciencedirect.com/science/article/abs/pii/S0747563212000921),
providing any graphics—good or bad—makes readers like the document more;
however, only instructive graphics help readers learn.
This unit suggests a few ways to help you create figures truly worth a thousand words.

#### Write the caption first

It is often helpful to write the caption before creating the illustration.
Then, create the illustration that best represents the caption.
This process helps you to check that the illustration matches the goal.

Good captions have the following characteristics:

* They are **brief**.
  Typically, a caption is just a few words.
* They explain the **takeaway**.
  After viewing this graphic, what should the reader remember?
* They **focus** the reader's attention.
  Focus is particularly important when a photograph or diagram contains a lot of detail.

As a rule of thumb, don't put more than one paragraph's worth of information in a single diagram.
(An alternative rule of thumb is to avoid illustrations that require more than five bulleted items to explain.)

After showing the "big picture," provide separate illustrations of each subsystem.

Alternatively, start with a simple "big picture" and then gradually expand detail in each subsequent illustration.

##### Focus the reader's attention

Adding a visual cue, for example, a red oval.

**Callouts** provide another way to focus the reader's attention.

##### Illustrating is re-illustrating

Revise illustrations to clarify the content.
As you revise, ask yourself the following questions:
* How can I simplify the illustration?
* Should I split this illustration into two or more simpler illustrations?
* Is the text in the illustration easy to read? Does the text contrast sufficiently with its background?
* What's the takeaway?

### Creating sample code

Good sample code is often the best documentation.
After all, text and code are different languages, and it is code that the reader ultimately cares about.
Good samples are **correct** and **concise** code that your readers can
**quickly understand** and **easily reuse** with **minimal side effects**.

#### Correct

Sample code should meet the following criteria:
* Build without errors.
* Perform the task it claims to perform.
* Be as production-ready as possible. For example, the code shouldn't contain any security vulnerabilities.
* Follow language-specific conventions.

Sample code is an opportunity to directly influence how your users write code.
Therefore, sample code should set the best way to use your product.

Always test your sample code.
Over time, systems change and your sample code may break.
Be prepared to test and maintain sample code as you would any other code.

Many teams reuse their unit tests as sample programs, which is sometimes a bad idea.
The primary goal of a unit test is to test; the only goal of a sample program is to educate.

A **snippet** is a piece of a sample program, possibly only one or a few lines long.
Snippet-heavy documentation often degrades over time because teams tend not to test snippets
as rigorously as full sample programs.

#### Running sample code

Good documents explain how to run sample code.
For example, your document might need to tell users to perform activities
such as the following prior to running the samples:
* Install a certain library.
* Adjust the values assigned to certain environment variables.
* Adjust something in the integrated development environment (IDE).

Users don't always perform the preceding activities properly.
In some situations, users prefer to run or (experiment with) sample code directly in the documentation.
("Click here to run this code.")

Writers should consider describing the expected output or result of sample code,
especially for sample code that is difficult to run.

#### Concise

* Pick descriptive class, method, and variable names.
* Avoid confusing your readers with hard-to-decipher programming tricks.
* Avoid deeply nested code.
* Optional: Use bold or colored font to draw the reader's attention to a specific section of your sample code
  However, use highlighting judiciously—too much highlighting means the reader won't focus on anything in particular.

Although it is tempting to keep sample code as short as possible,
omitting parameter names (example in Go, with named parameters) makes it harder for novices to learn.

#### Commented

Consider the following recommendations about comments in sample code:
* Keep comments short, but always prefer clarity over brevity.
* Avoid writing comments about obvious code,
  but remember that what is obvious to you (the expert) might not be obvious to newcomers.
* Focus your commenting energy on anything non-intuitive in the code.
* When your readers are very experienced with a technology,
  don't explain what the code is doing, explain why the code is doing it.

Should you place descriptions of code inside code comments or in text (paragraphs or lists) outside of the sample code?
Note that readers who copy-and-paste a snippet gather not only the code but also any embedded comments.
So, put any descriptions that belong in the pasted code into the code comments.
By contrast, when you must explain a lengthy or tricky concept, you should typically place the text before the sample program.

#### Reusable

For your reader to easily reuse your sample code, provide the following:

* All information necessary to run the sample code, including any dependencies and setup.
* Code that can be extended or customized in useful ways.

#### The example and the anti-example

In addition to showing readers what to do, it is sometimes wise to show readers what not to do.

#### Sequenced

A good sample code set demonstrates a range of complexity.

Readers completely unfamiliar typically crave "Hello World", only wanting more complex programs afterwards.
