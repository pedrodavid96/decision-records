# Android

[Modern Android development: Android Jetpack, Kotlin, and more (Google I/O 2018)](https://www.youtube.com/watch?v=IrMw7MEgADk)

https://developer.android.com/jetpack/compose/touch-input/gestures
https://developer.android.com/jetpack/compose/graphics/images/customize

[Quick Access Wallet  |  Android Open Source Project](https://source.android.com/docs/core/connect/quick-access-wallet)

[Fundamentals of Compose Layouts and Modifiers - MAD Skills - YouTube](https://www.youtube.com/watch?v=xc8nAcVvpxY)

[Android Studio Gradle Version Catalogs With Examples of Plugin and Dependency | Huawei Developers](https://medium.com/huawei-developers/android-studio-gradle-version-catalogs-72c1bf2912de)

[The elephant in the room: How to update Gradle in your Android project correctly | by Ed Holloway-George | May, 2024 | Medium](https://sp4ghetticode.medium.com/the-elephant-in-the-room-how-to-update-gradle-in-your-android-project-correctly-09154fe3d47b)
> If you normally update Gradle via the gradlew wrapper directly, make sure you run it twice!
  https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:upgrading_wrapper
  > If you want all the wrapper files to be completely up-to-date, you will need to run the wrapper task a second time.
