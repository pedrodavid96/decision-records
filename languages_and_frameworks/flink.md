[Here’s How Apache Flink Stores Your State data by Kartik Khare](https://towardsdatascience.com/heres-how-flink-stores-your-state-7b37fbb60e1a)

In short:
1. Memory state backend
2. File System (FS) state backend
3. RocksDB state backend

[DataStream API](https://ci.apache.org/projects/flink/flink-docs-stable/getting-started/walkthroughs/datastream_api.html)
Simple fraud detection

[A Journey to Beating Flink's SQL Performance](https://www.ververica.com/blog/a-journey-to-beating-flinks-sql-performance)
[Replayable Process Functions: Time, Ordering, and Timers](https://www.ververica.com/blog/replayable-process-functions-time-ordering-and-timers)

[Advanced Flink Application Patterns Vol.1: Case Study of a Fraud Detection System](https://flink.apache.org/news/2020/01/15/demo-fraud-detection.html)
[Github repo](https://github.com/afedulov/fraud-detection-demo)
Looks Awesome

[Monitoring Apache Flink Applications 101](https://flink.apache.org/news/2019/02/25/monitoring-best-practices.html)

[State Unlocked: Interacting with State in Apache Flink](https://flink.apache.org/news/2020/01/29/state-unlocked-interacting-with-state-in-apache-flink.html)

Temporary storage on "Submit New Job"
/tmp/flink-web-00dab2e3-757c-4e5f-8aa0-9faaefef8ed4/flink-web-upload/095e9821-ba0e-45aa-919e-e80164fc5f0b_dynamic-fraud-detection-demo-all.jar

[An Overview of End-to-End Exactly-Once Processing in Apache Flink (with Apache Kafka, too!)](https://flink.apache.org/features/2018/03/01/end-to-end-exactly-once-apache-flink.html)

[Kubernetes Setup](https://ci.apache.org/projects/flink/flink-docs-release-1.10/ops/deployment/kubernetes.html)
[Native Kubernetes Setup Beta](https://ci.apache.org/projects/flink/flink-docs-release-1.10/ops/deployment/native_kubernetes.html)

## Creating Job specific image

[Kubernetes Setup](https://ci.apache.org/projects/flink/flink-docs-stable/ops/deployment/kubernetes.html)
contains a specific section on [creating job specific image](https://ci.apache.org/projects/flink/flink-docs-stable/ops/deployment/kubernetes.html#creating-the-job-specific-image)
which links to these [instructions](https://github.com/apache/flink/blob/release-1.10/flink-container/docker/README.md).

These instructions didn't really work, we created a much bigger image
which wasn't really "compatible" with the [official flink Docker Image](https://hub.docker.com/_/flink).

By inspecting both repositories ([broken instructions](https://github.com/apache/flink/tree/release-1.10/flink-container/docker) and [official image repository](https://github.com/docker-flink/docker-flink))
and a running flink container I was able to create a Docker Image `my-flink`
which runs a standalone job and is almost oob (out of the box) compatible with
the helm chart (needed to change the command but I could, in fact, reuse it).

Create new version of the Helm Chart
https://github.com/docker-flink/examples

```bash
helm package helm/flink/
helm upgrade my-flink --set image=my-flink --set imageTag=0.1.0 --set imagePullPolicy=IfNotPresent flink-1.4.1.tgz
```
