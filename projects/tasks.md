[Tired of online Kanban tools, I created this vscode ui to manage project tasks](https://www.reddit.com/r/programming/comments/fmi7lw/tired_of_online_kanban_tools_i_created_this/)
Persists tasklist as markdown

https://github.com/Marceliny/Taskboard

[vim-bujo](https://github.com/vuciv/vim-bujo)
[blogpost](https://jerseyfonseca.com/projects/bujo)

### Competition

[TickTick:Todo list, checklist and task manager app for Android, iPhone and Web](https://ticktick.com/)
[Taskwarrior](https://taskwarrior.org/)
[Choose the plan that's right for you. Free, Premium, or teams and businesses.](https://www.any.do/pricing)
[Any.DO Moment on Vimeo](https://vimeo.com/59882957)

[GitHub - msoultanidis/quillnote: Take beautiful markdown notes and stay organized with task lists.](https://github.com/msoultanidis/quillnote)
Super similar to Google Keep but mentions "Markdown notes". 100% written in Kotlin.
Looks promising!

[quillpad/quillpad: Take beautiful markdown notes and stay organized with task lists. Fork of Quillnote](https://github.com/quillpad/quillpad)
Fork of quillnote
