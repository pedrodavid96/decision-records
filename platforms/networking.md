# Networking

## Basics

[You Suck at Subnetting by NetworkChuck](https://youtube.com/playlist?list=PLIhvC56v63IKrRHh3gvZZBAGvsvOhwrRF&si=lwaTbK1Z7vVgMIZ9)
[we’re out of IP Addresses….but this saved us (Private IP Addresses) - YouTube](https://www.youtube.com/watch?v=8bhvn9tQk8o&list=PLIhvC56v63IKrRHh3gvZZBAGvsvOhwrRF&index=5)

[Developers Need to Learn Basic Network Engineering](https://medium.com/better-programming/developers-need-to-learn-basic-network-engineering-c67767969cd5)
> Applications usually function perfectly inside of a vacuum, but the game changes once large networks with real users are involved.
- The OSI model
- IP addressing and subnetting
- TCP and UDP
- DNS
- NAT
- Basic types of hardware
    * Routers
    * Layer-2/3 switches
    * Access points
    * Firewalls/security appliances
    * Modems
    * NICs

_The Scenario Quiz_

> “You sit down at your desk and open your laptop. You open your browser and in the address bar type www.microsoft.com. Describe in detail everything that happens once you hit enter.”


[How to easily configure WireGuard](https://www.stavros.io/posts/how-to-configure-wireguard/)
> WireGuard is a very simple VPN that uses state-of-the-art cryptography, and the buzz comes from both the fact that it’s simple and good at what it does, and the fact that it’s so good that it’s going to be included in the Linux kernel by default. Linus Torvalds himself said that he loves it, which took the software world by storm, as we weren’t aware that Linus was capable of love or any emotion other than perkele.

[Create Your Own Network Namespace](https://codeburst.io/create-your-own-network-namespace-90aaebc745d)
> Linux ip command. In this post, I will show you how to use the command to connect processes in two different network namespaces, on different subnets, over a pair of veth interfaces.

[DNS Resolution and Records for Developers](https://www.arbazsiddiqui.me/dns-resolution-and-records-for-developers/)
> The Domain Name System (DNS) is the phonebook of the internet.
- DNS Resolver
  Computer that responds to a request from a client
  It will query all other subsequent servers on your behalf and after finding the IP address will respond with it.
- Root nameserver
  A root nameserver has the IP address of TLD nameservers.
- TLD nameserver
  TLD nameservers contain the IP addresses of all the subdomains which are the part of their TLD.
- Authoritative nameserver
  An authoritative DNS server is a server that actually holds, and is responsible for, DNS resource records.

- A records
  An A record maps a domain name to an IPv4 address
  Type 	Host 	Value 	        TTL
  A 	@ 	    104.198.14.52 	14400
  The @ here indicates that this is a record for the root domain
  The AAAA record does the same for IPv6
- CNAME
  A CNAME record is used when a domain or subdomain is an alias of another domain.
  The CNAME record's value is always another domain and NOT the IP address.
  Type 	    Host 	Value 	            TTL
  CNAME 	www 	arbazsiddiqui.me 	14400
- TXT
  The text record let’s a domain administrator enter text into the DNS record. (some sort of metadata)
  Type 	Host 	Value 	TTL
  TXT 	@ 	google-site-verification=xyz 	14400
- MX
  Type 	Host 	Value 	                Priority 	TTL
  MX 	@ 	    mailhost.example.com 	3 	        14400
  MX 	@ 	    mailhost2.example.com 	4 	        14400
  The mail exchange record directs email to a mail server. The value of MX record should be a domain.
  The new field priority here indicates the the order in which the message will be tried.
Caching
Hosts File

[How DNS Works Visually](https://www.youtube.com/watch?v=vrxwXXytEuI)
[Reddit discussion](https://www.reddit.com/r/programming/comments/klaffg/dns_explained_visually_in_10_minutes/)
2 - DNS Resolver (/ Recursive Resolver) (By default it usually uses the ISP resolver, you can use third parties instead, e.g.: cloudflare, google...)
3 and 4 - Root nameservers and TLD nameservers "simply" answer back to the DNS Resolver (/ Recursive Resolver) and that's the one making all the requests to the different nameservers (including the "terminal" authoritative nameserver)
  This can be quite expensive and that's why this server heavily utilizes caching as well

[security - Expose remote desktop directly to the internet - Super User](https://superuser.com/questions/1381315/expose-remote-desktop-directly-to-the-internet)
[network - What risks are involved in exposing our home computers over the public internet? - Information Security Stack Exchange](https://security.stackexchange.com/questions/41983/what-risks-are-involved-in-exposing-our-home-computers-over-the-public-internet)
[Access your home network from anywhere with WireGuard VPN - David's Homelab](https://davidshomelab.com/access-your-home-network-from-anywhere-with-wireguard-vpn/)
[Quick Start - WireGuard](https://www.wireguard.com/quickstart/)
[Guide: Install Wireguard On Raspberry latest releases - Raspberry Pi Forums](https://www.raspberrypi.org/forums/viewtopic.php?t=277111)
[Setting up a WireGuard VPN on the Raspberry Pi - Pi My Life Up](https://pimylifeup.com/raspberry-pi-wireguard/)
[WireGuard Basic Setup](https://youtu.be/UUyxS51OGNA?si=_6Th8EBuECMn6_wx)

[PIVPN: Simplest way to setup a VPN](https://pivpn.io/)
[Pi-hole – Network-wide protection](https://pi-hole.net/)
[AdGuard — World's most advanced adblocker!](https://adguard.com/en/welcome.html)

[WSL 2 Networking](https://www.youtube.com/watch?v=yCK3easuYm4)

[Computer Networking Introduction - Ethernet and IP (Heavily Illustrated)](https://iximiuz.com/en/posts/computer-networking-101/) | [Comments](https://www.reddit.com/r/programming/comments/r5j2lt/computer_networking_basics_for_developers/)

Why did I need to set DNS server manually on Synology NAS (should've used DHCP one automatically).
