# Expense Tracker

---
tags:
- expense-tracker
- finance-tracker
- budget
---

## Open Banking

[What is open banking? A comprehensive guide](https://nordigen.com/en/blog/open-banking-explained/)
https://www.santander.pt/open-banking/developers
https://www.pay.sibs.com/solucoes/api-market/api-contas/

https://play.google.com/store/apps/details?id=com.mobilecreditcards&hl=en_US&gl=US&pli=1
https://www.youneedabudget.com/

1. It is useful to have a 1->N relation between expenses / account movements and actual items.
2. It is useful to individually tag items with a specific category.
   e.g.: Buying a playstation in Continente instead of Worten, alongside with multiple daily consumables, I don't want to
         tag all with the same category
3. A lot of users (maybe even me included), want to interact with the data via Spreadsheet (/Excel)
4. Hooking to phone notifications seems like the best approach (while we don't have permission for PSD2 integration)

https://play.google.com/store/apps/details?id=com.mint&hl=pt_PT&gl=US

https://stackoverflow.com/questions/13587314/sqlite3-import-csv-exclude-skip-header
```sqlite
.import --csv --skip 1 artist_t.csv artist_t
```

https://dba.stackexchange.com/questions/40656/how-to-properly-format-sqlite-shell-output
```sqlite
-- Show "column"
.mode column
```

```sql
UPDATE expenses SET receipt_date_fmt = date(SUBSTR(receipt_date, -4) || '-' || CASE SUBSTR(receipt_date, 1, 3) WHEN 'Jan' THEN '01' WHEN 'Feb' THEN '02' WHEN 'Mar' THEN '03' WHEN 'Apr' THEN '04' WHEN 'May' THEN '05' WHEN 'Jun' THEN '06' WHEN 'Jul' THEN '07' WHEN 'Aug' THEN '08' WHEN 'Sep' THEN '09' WHEN 'Oct' THEN '10' WHEN 'Nov' THEN '11' WHEN 'Dec' THEN '12' END || '-' || SUBSTR(receipt_date, 5, 2));
```

```sql
UPDATE bank_movements_oct SET operation_date = DATE(SUBSTR("Data Operação", -4) || '-' || SUBSTR("Data Operação", 4, 2) || '-' || SUBSTR("Data Operação", 1, 2));
```

Check overlap
```sql
SELECT receipt_date, bank.operation_date, bank.value, category, vendor, bank.bank_description FROM expenses INNER JOIN (SELECT "Data Operação", "Data valor", "Descrição" as bank_description, "Montante( EUR )" as value, operation_date FROM bank_movements_oct ) AS bank ON expenses.value = -bank.value and bank.operation_date BETWEEN receipt_date_fmt and DATE(receipt_date_fmt, '+5 days');
```

Enable Gmail API on Google Cloud Console
Create Credentials
      Permissions for User Data
Go to "OAuth consent screen" to add myself as a Test User

https://googleapis.github.io/google-api-java-client/batching.html

Notification Listener Service
https://developer.android.com/reference/kotlin/android/service/notification/NotificationListenerService
https://github.com/Chagall/notification-listener-service-example/blob/master/app/src/main/java/com/github/chagall/notificationlistenerexample/MainActivity.java

[danschultzer/receipt-scanner: Receipt scanner extracts information from your PDF or image receipts - built in NodeJS](https://github.com/danschultzer/receipt-scanner)
[ReceiptManager/receipt-manager-app: Receipt parser application written in dart.](https://github.com/ReceiptManager/receipt-manager-app)
[vadiole/ReceiptsKeeper: Scan and view your e-receipts](https://github.com/vadiole/ReceiptsKeeper)

## Self Hosting

[Free Accounting Software for Small Businesses - Akaunting](https://akaunting.com/)
[Actual | Actual Budget Documentation](https://actualbudget.org/)
[GitHub - firefly-iii/firefly-iii: Firefly III: a personal finances manager](https://github.com/firefly-iii/firefly-iii)
PHP. Probably just for features reference

[maybe-finance/maybe: The OS for your personal finances](https://github.com/maybe-finance/maybe)
Ruby
Still in early stage but "target" look seems beautiful.

### References

[How I Track My Daily Expenses in 2 Minutes](https://hulry.com/how-to-track-expenses/)
