# Quotes

>  “We are what we pretend to be, so we must be careful about what we pretend to be.”

>  “I had hoped, as a broadcaster, to be merely ludicrous, but this is a hard world to be ludicrous in, with so many human beings so reluctant to laugh, so incapable of thought, so eager to believe and snarl and hate. So many people wanted to believe me!
> Say what you will about the sweet miracle of unquestioning faith, I consider a capacity for it terrifying and absolutely vile.”
― Kurt Vonnegut, Mother Night

> “Until you make the unconscious conscious, it will direct your life and you will call it fate.”
- Carl Gustav Jung


> “The less effort, the faster and more powerful you will be.”
- Bruce Lee

> “By failing to plan, you are preparing to fail”
- Benjamin Franklin's

Failure is a precondition for success - me

> “You never want to fail because you didn't work hard enough”
- Arnold Schwarzenegger

> Je n’ai fait celle-ci plus longue que parce que je n’ai pas eu le loisir de la faire plus courte.
As Blaise Pascal said.
(often miss-attributed to Twain as “I apologise for such a long letter - I didn't have time to write a short one.”)

It's basically a variation of (or vice-versa)
> Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away. Antoine de Saint-Exupery

> Never attribute to malice that which is adequately explained by stupidity.
Hanlon's razor - probably Robert J. Hanlon, who submitted the statement to Murphy's Law Book Two (1980).

> “[...] throughout the whole history of science most of the really great discoveries which had ultimately proved to be beneficial to mankind had been made by men and women who were driven not by the desire to be useful but merely teh desire to satisfy their curiosity”
― Abraham Flexner, The Usefulness of Useless Knowledge

III.
"Don't worry about being the most interesting person in the room, just try to be the most interested person in the room.
    The interested person asks about others and leaves a good impression because people like talking about themselves.
    The interested person is genuinely curious about someone's craft and learns a lot about how things work.
    The interested person engages with more people and—because opportunities come through people—is more likely to catch a lucky break.
In general, the interested person learns more and tends to be well-liked. And in the long run, it's hard to keep down someone who is well-learned and well-liked."
- James Clear

read on JamesClear.com | May 30, 2024
I.
"It's never the right time, but right now is usually the best time."
III.
"Three ways to learn something new:
    Reflect on what you have already tried.
    Attempt something you have not tried.
    Read about what someone else has tried."

read on JamesClear.com | May 23, 2024
I.
"Clarity isn't about knowing what you want to do with your life, it's
about knowing what you want to do this week.
You don't need to have it all figured out. You just need to know your next
step."

Ali Abdaal | 30 Lessons I've Learned in my 20s
18. If it’s not a “hell yeah”, it should probably be a “no thank you” (ht
Derek Sivers).
19. Be the lead organiser of social events for your friends. They all want
to hang out, they just suck at organising things.

read on JamesClear.com | May 16, 2024
II.
"You'll probably surprise yourself with what you can accomplish—if you're
focused on one thing.
You'll probably frustrate yourself with what you fail to accomplish—if
you're doing 5 or 7 or 10 things.
