# Xrandr

```
xrandr --output DP-1-2 --primary --mode 2560x1440 --pos 0x0 --rate 59.95 --output eDP-1 --pos 2560x360
```

## Screen position

In https://superuser.com/a/485165

> Basically, --pos specifies the position of the upper left corner of the screen in the virtual screen space.
> The virtual screen is a screen that spans your entire physical screens.
> This is a very generic way of specifying screen positions.

```
(virtual screen coordinates)
     0       1366                 1366+1920

   0           A-----------------------
               |                      |
               |                      |
               |                      |
  x? B---------|         HDMI         |
     |         |                      |
     |  LVDS   |       1920x1080      |
     |1366x768 |                      |
1080 ----------------------------------
```