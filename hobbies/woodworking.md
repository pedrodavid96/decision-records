# Woodworking

Seems like there's a "fight" between 2 big segments, "hand tools" vs "power tools".
Hands tools seem essential and a smaller initial investment and more than enough for small projects.

I'd already thought about started playing with it but seems like a found a few (seemingly) easy
projects to start working on:

[The Pinch – Frictitious Climbing](https://frictitiousclimbing.com/products/pinch-block-the-pinch)
[The Block – Tension Climbing](https://tensionclimbing.com/products/the-block-2)
[Flash Board – Tension Climbing](https://tensionclimbing.com/products/flash-board-2?pr_prod_strat=e5_desc&pr_rec_id=7e4c6f155&pr_rec_pid=8010355998901&pr_ref_pid=8010356195509&pr_seq=uniform)
[Easy DIY Homemade Pinch Block (2x4 and cord) : climbharder](https://www.reddit.com/r/climbharder/comments/fqsu8q/easy_diy_homemade_pinch_block_2x4_and_cord/)
[How-to-Tie-a-Double-Fisherman’s-Knot-1-2527387843.jpg (JPEG Image, 650 × 628 pixels)](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.101knots.com%2Fwp-content%2Fuploads%2F2016%2F08%2FHow-to-Tie-a-Double-Fisherman%25E2%2580%2599s-Knot-1.jpg&f=1&nofb=1)
[How to make a pinch block to train for climbing 💪🏻 #climbing - YouTube](https://www.youtube.com/shorts/iD2muwqYg6Y)

Wrist roller

[Crafting Picture Frames - Hand Tools Only - YouTube](https://www.youtube.com/watch?v=D0US_-t4SXY)

[Now Playing (wood)](https://www.amazon.es/-/pt/dp/B0978PSL94/?coliid=I477CY6MWICKO&colid=3QFH38RKJPSPX&psc=0&ref_=list_c_wl_lv_ov_lig_dp_it_im)

## Power tool

[This is the easiest method for rounding corners on tables - YouTube](https://www.youtube.com/watch?v=gzTe_PfyBcc)

## Hand tool wood working

[20 Woodworking Hand Tools List For Beginners](https://woodandshop.com/which-hand-tools-do-you-need-for-traditional-woodworking/)

[Handpowered Sawmill - Log to Timber with HandTools - YouTube](https://www.youtube.com/watch?v=ikseNeMddyE)
What is this? ASMR? Incredible setup and good show of hand tool wood working

[Hand tool woodworking is easier than you think - YouTube](https://www.youtube.com/watch?v=Tc6_pTs2_eQ)

[What can you do with a chisel? - YouTube](https://www.youtube.com/watch?v=cVoQtSWZ2QM)


[How To Create A Roundover With A Handplane - YouTube](https://www.youtube.com/watch?v=sN3bxnSoZMM)
