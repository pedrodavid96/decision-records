# GPG

[How Pretty Good Privacy works, and how you can use it for secure communication](https://www.freecodecamp.org/news/how-does-pretty-good-privacy-work-3f5f75ecea97)
PGP generates a public key (to encrypt messages) and a private key (to decrypt messages)
OpenPGP is an e-mail encryption standard
GPG is an open-source implementation of OpenPGP

## Renew GPG key

https://gist.github.com/krisleech/760213ed287ea9da85521c7c9aac1df0

```
$ gpg --edit-key <key-id>
gpg> key 1
gpg> key 2
gpg> expire
gpg> trust
gpg> quit
```

## Reload agent
`gpg-connect-agent reloadagent /bye`

## Encrypt
`gpg --encrypt <file>`

## Decrypt
`gpg -d <file>`

[Creating the perfect GPG keypair](https://alexcabal.com/creating-the-perfect-gpg-keypair)

[The Definitive Guide to password-store](https://medium.com/@chasinglogic/the-definitive-guide-to-password-store-c337a8f023a1)

## Import key from keyserver

```
gpg --keyserver hkps://pgp.random-name.com --batch --search-keys "randomemail@mail.com" 2>&1 | grep -Po '\d+\s*bit\s*\S+\s*key\s*[^,]+' | cut -d' ' -f5
```

[gnupg - How do you safely backup a GPG private key? - Super User](https://superuser.com/questions/1110877/how-do-you-safely-backup-a-gpg-private-key?utm_source=pocket_saves)

[GPG Change Passphrase Secret Key Password Command - nixCraft](https://www.cyberciti.biz/faq/linux-unix-gpg-change-passphrase-command/?utm_source=pocket_reader)
1. Get a list of GPG keys by running the `gpg --list-keys` command.
2. Run `gpg --edit-key <your-key-id>` command.
3. At the `gpg>` prompt enter the `passwd` to change the passphrase.
4. First enter the current passphrase when prompted.
5. Then type the new passphrase twice to confirm it.
6. Finally type the `save` command at the `gpg>` prompt.

[gnupg - How to share one pgp-key on multiple machines? - Ask Ubuntu](https://askubuntu.com/questions/32438/how-to-share-one-pgp-key-on-multiple-machines)
1. `gpg --export-secret-key -a > secretkey.asc`
2. `gpg --import secretkey.asc`
3. `shred --remove secretkey.asc`

[privacy - Where do you store your personal private GPG key? - Information Security Stack Exchange](https://security.stackexchange.com/questions/51771/where-do-you-store-your-personal-private-gpg-key?utm_source=pocket_saves)
I like to store mine on paper - using a JavaScript (read: offline) QR code generator, I create an image of my private key in ASCII armoured form, then print this off.
