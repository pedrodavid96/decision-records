# Knowledge Base

PKMS

Well, this repository is a sort of knowledge base / archive of everything I find
 (/ might find) useful.

While it (kinda) works, it is not something "easy" to use 😅.

## recutils

Sounds like a very cool tool for this kind of information.

[A Database that Follows the Unix Philosophy GNU recutils](https://www.youtube.com/watch?v=qnlkr3mCqW8)
This video shows some basic functionality and capabilities.


## Decision Logs

[We should track our decisions in a decision log](https://engineering-management.space/post/decision-logs/)

https://www.fleetingnotes.app/

## Drawings

https://forum.obsidian.md/t/drawing-sketching-support-for-users-who-use-styluses-tablets/3090/84
[Obsidian-Excalidraw 1.8.8 - Custom Pen support](https://www.youtube.com/watch?v=uZz5MgzWXiM)
https://github.com/excalidraw/excalidraw/issues/6264


## PKMS

[Show HN: Eidos – Offline alternative to Notion | Hacker News](https://news.ycombinator.com/item?id=40746773)
[GitHub - mayneyao/eidos: Offline alternative to Notion. Eidos is an extensible framework for managing your personal data throughout your lifetime in one place.](https://github.com/mayneyao/eidos)

[Lazy • A capture tool for knowledge](https://lazy.so/)

## Try Outs

Joplin
* First impression is that the "notebooks" organization might not be great for a "Second Brain" as it doesn't map well to the PARA method.

## Joplin

[Plugin: Note Link System - Plugins - Joplin Forum](https://discourse.joplinapp.org/t/plugin-note-link-system/21768)
[maxnegro/joplin-plugin-admonition: Joplin markdown plugin for custom containers](https://github.com/maxnegro/joplin-plugin-admonition)
[Creating a Markdown editor plugin | Joplin](https://joplinapp.org/help/api/tutorials/cm6_plugin/)
[catppuccin/joplin: 📔️ Soothing pastel theme for Joplin](https://github.com/catppuccin/joplin)

## Alternatives

[Outline – Team knowledge base & wiki](https://www.getoutline.com/)
[WizNote](https://www.wiz.cn/)
> cloud notes, personal knowledge management, teamwork, database, knowledge management,
> notepad, encrypted notes, alternatives to Evernote, meeting minutes, diary, cognitive offloading
[Other great note takers and points for improvement for Joplin - Lounge - Joplin Forum](https://discourse.joplinapp.org/t/other-great-note-takers-and-points-for-improvement-for-joplin/20486/38?page=3)
[Any opinions on SiYuan (an apparently very good Notion alternative) : selfhosted](https://www.reddit.com/r/selfhosted/comments/1c0i2lq/any_opinions_on_siyuan_an_apparently_very_good/)
