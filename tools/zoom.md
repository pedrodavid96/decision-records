# Zoom


[Setup Zoom for Effective Screen Sharing | Effective Home Office](https://effectivehomeoffice.com/setup-zoom-for-effective-screen-sharing/)
* General -> Disable Use dual monitors. Put both participants and shared screen and on your primary screen to have eye contact.
* Share Screen -> Enable Maximize Zoom window when a participant shares their screen. Focus.
* Share Screen -> Disable Side-by-side Mode. This increases your screen estate.
* Video -> Enable Always display participant name on their videos
* Video -> Enable Display up to 49 participants per screen in Gallery View (personal note: Doesn't seem available in Linux)

[Setup Zoom as Virtual Team Room | Effective Home Office](https://effectivehomeoffice.com/setup-zoom-as-virtual-team-room/)
* Consider to upgrade to Pro ($14.99/mo) to bypass the 40 mins limit
* Enable Video for Host and Participant to have fewer clicks when starting the meeting.
* Select Enable join before host
* Disable Enable waiting room

* Settings -> In Meeting (Advanced) -> Enable Show a “Join from your browser” link to enable plain browser participants


https://www.remotemobprogramming.org/
