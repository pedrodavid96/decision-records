# Read it later app

## Pocket

Pros:
* Beautiful UI
* Save it offline to read it later
* Text-to-speech is quite good
* Alexa integration (had to change Amazon region default to US)

Cons:
* Some saved articles are actually just linking to original content for some reason
* Gathering highlights is super convoluted, requires API that is only accessible with the app key
  of their web app, meaning, registering your own app doesn't have access to that
* Can't really take notes, only highlights
* Clicking on "go to original" changes the link to add tracking information

## Omnivore (shutting down)

[Welcome to Omnivore](https://omnivore.app/login?errorCodes=AUTH_FAILED)
[Details on Omnivore shutting down - Omnivore Blog](https://blog.omnivore.app/p/details-on-omnivore-shutting-down)
