# [fzf](https://github.com/junegunn/fzf)

It's an interactive Unix filter for command-line that can be used with any list; files, command history, processes, hostnames, bookmarks, git commits, etc.

[Examples](https://github.com/junegunn/fzf/wiki/examples)
[Usage example](https://medium.com/@sidneyliebrand/how-fzf-and-ripgrep-improved-my-workflow-61c7ca212861)

[So you've installed `fzf`. Now what?](https://andrew-quinn.me/fzf/)

[A Practical Guide to fzf: Building a File Explorer](https://thevaluable.dev/practical-guide-fzf-example/)

[Browsing git commit history with fzf · GitHub](https://gist.github.com/junegunn/f4fca918e937e6bf5bad)

[Reddit Discussion](https://www.reddit.com/r/programming/comments/11y9zlq/so_youve_installed_fzf_now_what/)

## Fuzzy file search with preview window, open in vim
vim $(rg --files | fzf --height=70% --preview="cat {}" --preview-window=right:60%:wrap)

[idea taken from](https://medium.com/@_ahmed_ab/crazy-super-fast-fuzzy-search-9d44c29e14f)
source /usr/share/fzf/key-bindings.bash

[fzf - a terminal fuzzy finder](https://briansunter.com/blog/fzf/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/l90f3r/how_to_use_fzf_a_terminal_fuzzy_finder/)
Interactive file finder using bat to show a preview side by side
```
list() { fzf -m --preview '[[ $(file --mime {}) =~ binary ]] && echo {} is a binary file || bat --style=numbers --color=always {}' | xargs ls -lha }
```

Interactive kill command using space to highlight multiple files.
```
fkill() { local pid pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}') if [ "x$pid" != "x" ] then echo $pid | xargs kill -${1:-9} fi }
```

Git commit browser showing previews in the right hand panel.
```
fshow() {
  git log --graph --color=always \
      --format="%C(auto)%h%d %s %C(black)%C(bold)%cr" "$@" |
  fzf --ansi --no-sort --reverse --tiebreak=index --bind=ctrl-s:toggle-sort \
      --bind "ctrl-m:execute:
                (grep -o '[a-f0-9]\{7\}' | head -1 |
                xargs -I % sh -c 'git show --color=always % | less -R') << 'FZF-EOF'
                {}
FZF-EOF"
}
```

[Improving shell workflows with fzf - Sebastian Jambor's blog](https://seb.jambor.dev/posts/improving-shell-workflows-with-fzf/)

[Tracking hours with Clockify, fzf, jq and awk](https://marcel.is/tracking-hours/)

[Autocompletion of files with bare git repo : zsh](https://www.reddit.com/r/zsh/comments/kcgtfx/autocompletion_of_files_with_bare_git_repo/)

[Sharing some Custom Commands · Issue #455 · go-jira/jira · GitHub](https://github.com/go-jira/jira/issues/455)
```
...
    {{jira}} ls -n {{args.query}} | \
      fzf --preview="echo {} | cut -d : -f 1 | xargs -I % sh -c 'jira view %'"
```

[pacman/Tips and tricks - ArchWiki](https://wiki.archlinux.org/title/pacman/Tips_and_tricks)
