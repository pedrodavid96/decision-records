# Difftastic

https://difftastic.wilfred.me.uk/

Git config:
```
[diff]
        tool = difftastic
[difftool]
        prompt = false
[difftool "difftastic"]
        cmd = difft "$LOCAL" "$REMOTE"
[pager]
        difftool = true
[alias]
        dft = difftool
```