# Document Storage

## [Paperless-ngx](https://docs.paperless-ngx.com/)

[File Name handling](https://docs.paperless-ngx.com/advanced_usage/#file-name-handling)
[Utilities Renamer](https://docs.paperless-ngx.com/administration/#utilities-renamer)

[paperless-ngx-postprocessor: Automatically retitle your documents and more! · paperless-ngx/paperless-ngx · Discussion #1935](https://github.com/paperless-ngx/paperless-ngx/discussions/1935)

### Tracking

[Over-engineering my document storage system with Paperless-ngx](https://skerritt.blog/how-i-store-physical-documents/)

[[Draft] Feature: automatic document translation by bjesus · Pull Request #6386 · paperless-ngx/paperless-ngx](https://github.com/paperless-ngx/paperless-ngx/pull/6386)
[Improved functionality for receipts · paperless-ngx/paperless-ngx · Discussion #2870](https://github.com/paperless-ngx/paperless-ngx/discussions/2870)

[Paperless-ngx extract date from filename : selfhosted](https://www.reddit.com/r/selfhosted/comments/1et9e0u/paperlessngx_extract_date_from_filename/)
> Just write a batch script and restructure / rename your files.

### Email Setup

[OAuth Email Setup](https://docs.paperless-ngx.com/usage/#oauth-email-setup)
Needs configuration of Developer App that I'm trying to automate with Pulumi in the scope of keycloak.

### AI

#### Mature

[icereed/paperless-gpt: Use LLMs and LLM Vision to handle paperless-ngx](https://github.com/icereed/paperless-gpt?tab=readme-ov-file)

#### Incubating

* [B-urb/doclytics: A document analyzer for paperless-ngx using ollama](https://github.com/B-urb/doclytics)
* [hendkai/paperless_sort_low_quality_ollama: Find and tag files with ollama](https://github.com/hendkai/paperless_sort_low_quality_ollama)
