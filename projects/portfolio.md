[How I designed and built my UX design portfolio from scratch](https://uxdesign.cc/how-i-designed-and-built-my-ux-design-portfolio-from-scratch-f1f9b5261029)

[10 Coding Prompts for Your Portfolio | Aphinya Dechalert](https://medium.com/better-programming/10-coding-prompts-for-your-portfolio-4a5266d86ede)

## Cool Portfolio examples

http://gokhanarik.com/#/resume
http://kalynnakano.com/

* [Photographer Portfolio](https://www.w3schools.com/w3css/tryw3css_templates_photo3.htm#contact)

# Brittany Chiang
https://brittanychiang.com/
https://bchiang7.github.io/
https://bchiang7.github.io/v2/
https://bchiang7.github.io/react-profile/

[Create your portfolio for free](https://techstax.dev/)
