# Proxmox

[What do people use Proxmox on a Pi for? : Proxmox](https://www.reddit.com/r/Proxmox/comments/1adewa0/what_do_people_use_proxmox_on_a_pi_for/)
> Some people have 2 node clusters that are susceptible to split-brain issues. Adding a Pi adds a third node purely for quorum, which negates the split-brain problem.
> > Just wanted to point out that you don't need to have proxmox installed to make a device a node in a proxmox cluster. See QDevice

[Everything You Need to Know to Start with Proxmox VE - YouTube](https://www.youtube.com/watch?v=5j0Zb6x_hOk)

## User Management

[User Management - Proxmox VE](https://pve.proxmox.com/wiki/User_Management#:~:text=OpenID%20Connect)
OpenID Connect integration.

## Automation

[Proxmox VE Helper-Scripts](https://community-scripts.github.io/ProxmoxVE/scripts?id=post-pve-install)
[Proxmox VE Helper-Scripts](https://tteck.github.io/Proxmox/#proxmox-backup-server-post-install)

### Ansible

[Simplify Your Proxmox VE Tasks: Ansible Automation Made Easy - YouTube](https://www.youtube.com/watch?v=4G9d5COhOvI)

[community.general.proxmox_user_info module – Retrieve information about one or more Proxmox VE users — Ansible Community Documentation](https://docs.ansible.com/ansible/latest/collections/community/general/proxmox_user_info_module.html#ansible-collections-community-general-proxmox-user-info-module)
There are a lot more plugins for Proxmox.

### Packer

[QEMU Builder | Integrations | Packer | HashiCorp Developer](https://developer.hashicorp.com/packer/integrations/hashicorp/qemu/latest/components/builder/qemu)

### Terraform

[Terraform with KVM - DEV Community](https://dev.to/ruanbekker/terraform-with-kvm-2d9e)

### Cloud Init

[Cloud-init - Proxmoxer Documentation](https://proxmoxer.github.io/docs/latest/examples/cloud-init/)

### K8s

[The Simplest Kubernetes Deployment? K3S, HA, Loadbalancer! Kubernetes At Home: Part 3 - YouTube](https://www.youtube.com/watch?v=6k8BABDXeZI)

[khanh-ph/proxmox-kubernetes: Enables you to create a Kubernetes cluster on Proxmox VE with Terraform & Kubespray in a declarative manner.](https://github.com/khanh-ph/proxmox-kubernetes)

[kubernetes-sigs/kubespray: Deploy a Production Ready Kubernetes Cluster](https://github.com/kubernetes-sigs/kubespray?tab=readme-ov-file#requirements)

[Proxmox with OpenTofu Kubespray and Kubernetes · blog.andreasm.io](https://blog.andreasm.io/2024/01/15/proxmox-with-opentofu-kubespray-and-kubernetes/)

[k8s-proxmox/cluster-api-provider-proxmox: Cluster API provider implementation for Proxmox VE](https://github.com/k8s-proxmox/cluster-api-provider-proxmox)

## Storage

[Rook](https://rook.io/)

[proxmox k3s longhorn or ceph shared storage : homelab](https://www.reddit.com/r/homelab/comments/1cmqec6/proxmox_k3s_longhorn_or_ceph_shared_storage/)
[Would you rather use Rook-Ceph or Longhorn for on premise cluster without dedicated storage nodes? : kubernetes](https://www.reddit.com/r/kubernetes/comments/1chude7/would_you_rather_use_rookceph_or_longhorn_for_on/)

[Ceph and SSDs : homelab](https://www.reddit.com/r/homelab/comments/12r5oyz/ceph_and_ssds/)
> Don't use consumer SSDs. Just trust me, don't.
> Performance was so bad with only a few VMs and workloads, that IO would completely stall.
> Even doing touch 'newfile' and rm 'newfile' had extremely noticeable delays.
...
> I ended up picking up a bunch of cheap enterprise SSDs. The end result- it works nearly flawlessly.

[Btrfs vs. ZFS: conceptual difference : btrfs](https://www.reddit.com/r/btrfs/comments/1621lno/btrfs_vs_zfs_conceptual_difference/)
[Recommended FileSystem for Single SSD Boot Drive? | Proxmox Support Forum](https://forum.proxmox.com/threads/recommended-filesystem-for-single-ssd-boot-drive.146894/)

[Longhorn Provides Highly Available Storage. Kubernetes At Home - Part 5 - YouTube](https://www.youtube.com/watch?v=ps0NKd59UkE)


## Sandbox

* [[SOLVED] - Running "POST /api2/json/nodes/{node}/qemu/{vmid}/clone" from my python program | Proxmox Support Forum](https://forum.proxmox.com/threads/running-post-api2-json-nodes-node-qemu-vmid-clone-from-my-python-program.74406/)
* [proxmox_vm_qemu | Resources | Telmate/proxmox | Terraform | Terraform Registry](https://registry.terraform.io/providers/Telmate/proxmox/latest/docs/resources/vm_qemu)
* [Proxmox Builder | Integrations | Packer | HashiCorp Developer](https://developer.hashicorp.com/packer/integrations/hashicorp/proxmox/latest/components/builder/iso#isos)
* [proxmox-api-go/proxmox/client.go at master · Telmate/proxmox-api-go](https://github.com/Telmate/proxmox-api-go/blob/master/proxmox/client.go#L505)
* [Unable to upload ISO with API user · Issue #81 · hashicorp/packer-plugin-proxmox](https://github.com/hashicorp/packer-plugin-proxmox/issues/81)
* [Download ISO files directly to PVE node by sebastian-de · Pull Request #148 · hashicorp/packer-plugin-proxmox](https://github.com/hashicorp/packer-plugin-proxmox/pull/148)
* [Proxmox Cloud-init image using Ansible | Tim's Blog](https://www.timatlee.com/post/proxmox-cloudinit-image-ansible/)
* [Looking for a script to automatically pull the latest Ubuntu cloud-init image and turn it into a Proxmox template? Look no further, I've got you covered! : r/Proxmox](https://www.reddit.com/r/Proxmox/comments/108ctxf/looking_for_a_script_to_automatically_pull_the/)
* [regenerate cloud init image using ansible | Proxmox Support Forum](https://forum.proxmox.com/threads/regenerate-cloud-init-image-using-ansible.89964/)
* [Creating cloud-init images on Proxmox using Ansible shell command fails, while shell script works? - Stack Overflow](https://stackoverflow.com/questions/76816367/creating-cloud-init-images-on-proxmox-using-ansible-shell-command-fails-while-s)

## Auto Installer

[Automated Installation - Proxmox VE](https://pve.proxmox.com/wiki/Automated_Installation)
[natankeddem/autopve: Containerized GUI application which hosts answers for Proxmox automated installation.](https://github.com/natankeddem/autopve)

From PXE Boot, remember to add the (very hard to find documentation) kernel parameter:

```
# The `proxmox-start-auto-installer` kernel parameter is necessary although I couldn't find any documentation on it
# Could only find a mention in https://forum.proxmox.com/threads/auto-install-through-pxe.146270/post-660430
kernel ${kernel_url}vmlinuz vga=791 video=vesafb:ywrap,mtrr ramdisk_size=16777216 rw quiet ${params} initrd=initrd.magic ${cmdline} proxmox-start-auto-installer
```

## Misc

[CPU Pinning in Proxmox: Configuration and Performance testing - YouTube](https://www.youtube.com/watch?v=-c_451HV6fE)

## Clustering

[More POWER for my HomeLab! // Proxmox - YouTube](https://www.youtube.com/watch?v=IhEE_QlI1MU)
[Cluster Manager - Proxmox VE](https://pve.proxmox.com/wiki/Cluster_Manager)
