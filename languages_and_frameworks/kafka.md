# Kafka

[Quickstart](https://kafka.apache.org/quickstart)



## [Why Kafka Is so Fast](https://medium.com/swlh/why-kafka-is-so-fast-bde0d987cd03)
Super great article!!! ✅
Goes over a lot of Kafka concepts, its internal workings and main advantages

> Apache Kafka is optimized for throughput at the expense of latency and jitter, while preserving other desirable qualities, such as durability, strict record order, and at-least-once delivery semantics.

Article mentions [Apache Pulsar](https://pulsar.apache.org/) as an alternative that achieves better / lower latencies.

> Kafka can be used to implement near-real-time (otherwise known as soft real-time) systems.

[Real time systems](https://en.wikipedia.org/wiki/Real-time_computing)

It's called real time not because its fast but "predictable" as it should always "meet a deadline".

* Log-structured persistence - append-only log and sequential I/O
* Record batching - before sending them over the network
* Batch compression
* Cheap consumers - record offsets instead of removing consumed messages
* Unflushed buffered writes - doesn't actually call `fsync`
* Client-side optimisations - "a lot" of work on the client side
* Zero-copy - `java.nio.channels.FileChannel::transferTo` to avoid context switches between kernel and user space
* Avoiding the GC
  Running Kafka on a machine with 32 GB of RAM will result in 28–30 GB, completely outside of the GC's scope
  To be fair this isn't as important with a properly tuned JVM and modern GCs like Shenandoah and ZGC
* Stream parallelism (need further reading)

## In practice

Consume (show) topic `rules` for broker `my-kafka-headless:29092`
```bash
kafkacat -C -b my-kafka-headless:29092  -t rules
```

List mode for <broker> (show topics / broekers /partitions)
```bash
kafkacat -L -b my-kafka-headless:29092
```

[Kafka Listeners - Explained](https://rmoff.net/2018/08/02/kafka-listeners-explained/)
[Configuring Kafka on Kubernetes makes available from an external client with helm](https://medium.com/@tsuyoshiushio/configuring-kafka-on-kubernetes-makes-available-from-an-external-client-with-helm-96e9308ee9f4)
[Configure Multi-Node Environment](https://docs.confluent.io/current/kafka/multi-node.html)

[Helm chart](https://github.com/bitnami/charts/tree/master/bitnami/kafka)