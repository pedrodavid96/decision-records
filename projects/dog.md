# Dog

## The other end of the leash

### Chapter 3 - Talking to Each Other

985 Habituation occurs when an organism (or even a single cell) begins to ignore something that occurs over and over with no relevant consequence.
    Note: Habituation -> Ignoring frequent actions without consequences
995 Start to pay careful attention to the words you use around your dog. You might even write down what you think your signal words are. Be specific about exactly what words you use.
    **Action item: write down signal words**
1010 A standard and proven method of behavior modification is to ask people who are dieting, stopping smoking, and the like to keep a record of when and what they ate or smoked.
1011 people begin to eat or smoke less, simply because they are focusing their awareness on that behavior
    Awareness by itself already reduces bad habits
1059 goofy, grinning dog saying: “Hi! My name is NO NO Bad Dog. What's yours?”

1123 Barking is rarely heard from experienced, confident adult wolves; it is mostly produced by juvenile wolves, usually in response to a situation that immature wolves perceive as alarming.
1129 If barking correlates with a juvenile and submissive condition, then it's doubtful that dogs read our loud vocal displays as dominant or impressive.
    **Note: Loudness will be perceived as jouvenille instead of dominance / impressive.**

1150 Be prepared with easily accessible yummy treats. (Don't be cheap. Go for chicken or beef or anything your dog really loves, but keep the pieces very small.)
    **Easily accessible treats in very small pieces**
1280 if you're communicating an action that inherently inhibits activity in your dog, like “Sit” or “Down,” try to say it only once, just as the handlers I interviewed did.
    **Say inhibiting commands (sit, down) only once.**
1300 There's a bonus, though: speaking in long, steady tones can help calm you down, too.
1332 Use short, repeated notes like claps, smooches, and short, repeated words to stimulate activity in your dog.
    **Stimulate activity with short repeated words (claps, "come come", "fast fast")**

1333 come
1333 speed up.
1333 Use one long, continuous flat sound to soothe or slow your dog,
    **Long flat sound to slow your dog**

1334 Use a burst of one short, highly modulated note to effect an immediate stop of a fast-moving dog, saying “No!” or “Hey!” or “Down”
    **One short burst to halt to complete stop (no!)**

### Chapter 4 - Planet Smell

1456 sniff in little short bursts rather than one long inhale. There's a reason that your dog sniffs in staccato notes.
1559 Once the area is odor-free and clean, sit down on the carpet with your dog and a paperback and spend a little time each day there. In just a few days, that place will smell like a living room instead of a toilet to your pup.
    **Trick: avoid misplaced pee**
1561 It also helps tremendously to give your dog a treat every time that she goes outside—right after she goes, not after she trots back to the house.
    **Action: treat every time you get to outside**

### Chapter 5 - Fun and Play

1887 Start with young dogs by throwing the ball only a short distance away.
1888 Don't throw it very often at first either; just two or three times is enough.
    **Action item: only short distance ball play, only 2/3 times**

1891 he stayed interested longer and longer, until now he stops only when he's out of breath or I'm worried that he's getting overheated.
1893 wait until the dog has his mouth on it. Once he does, your job is to move away from the ball, clapping and smooching to attract him in your direction.
1894 If you walk toward him, you've just initiated a chase game,
1898 Turn so that you're not facing your dog, clap, smooch, and take some steps away from him so that he'll start chasing you.
    **Move away (otherwise you're starting a chase game) and clap (stimulate activity) so he chases you**
1902 If the first time he comes forward three steps and drops the ball 10 feet from you, that's OK. Walk slowly up to it (perhaps turn sideways so he doesn't start to dash away) and throw it again. You can also throw a second ball as soon as he drops the first.
1914 whenever she does give it up, you throw it back instantly. That's instantly, as in an instant. Not two seconds later after you've clutched it to your bosom and said, “Good dog. What a good dog!” She doesn't want your praise or your petting, for heaven's sake, not then.
1933 I've never noticed any obvious differences between sexes in the frequency or intensity of rough-and-tumble play in any of my litters, in any of my
    **Bring 2 balls to send another right away if the ball is dropped far, if really needed, walk slowly and sideways . Throw ball back instantly, it's not a time for praise**

### Chapter 6 - Packmates

2139 “critical period” of socialization.
2160 as long as he continues to learn that people and dogs outside of your family are members of his extended social group.
    Action item: Introduce him to his extended social group
2241 Imagine if you were on a leash and couldn't escape.
2284 “Ah, Mo-om, cut it out.” Spike is in play mode, and there's a bit of a competition going on with the other dogs, and maybe, just maybe, he really doesn't want to be petted right then.
2295 I think it's useful, because it can explain the fact that some dogs will come less often if the “reward” for coming is getting patted on the head. To many dogs, in this circumstance, its a punishment, not a reward. You should see the faces of these dogs in our training classes as their owners stroke their heads: they turn away, lips pulled down like a human who just smelled rotten eggs.
    **Note: patting on head might be a punishment, notice if they turn away or lips are pulled down**
2299 saying “OK” and letting them go back to play with their buddies is a great way to make them glad that they came. It seems to dazzle them. “I can go play some more? Wow, you are so cool!”
    **Action item: "saying ok" and letting him play some more makes you "so cool!" 😂**
2317 I've seen several owners who were bitten by their dog after they tried petting the dog to calm him down when he was agitated and aroused.
2325 massage your dog with long, slow strokes when you want to soothe her, even if you yourself are nervous.
    **Long slow massage to calm your dog**
2352 a long time to come. In that sense we primates are more like dogs than most other

### Chapter 7 - The Truth About Dominance

2620 makes sense that a dog would bite, or at least threaten to, in this context. Within their social framework, you're acting like a lunatic.
2788 The irony is that dominance is actually a social construct designed to decrease aggression, not to facilitate it.

### Chapter 8 - Patient Dogs and Wise Humans

3056 don't do it because you feel that you have no choice. You do have a choice, and your
3061 A lot of young dogs don't want petting or attention as much as they want activity,
3065 either gei yourself outside and exercise with your dog or find someone else to do it for you.
    Exercise with your dog, specially yong pups might want that more than attention

3066 a large number of the behavioral problems that I see have their origin in boredom.
3075 give him what he needs before he has to pester you for it. But no matter how much exercise your dog needs, all dogs profit by learning how to cope with frustration.
3079 **say “Enough” in a low, quiet voice and then pat her briskly on the head two times.** If she doesn't go away (which most dogs won't the first several times you do this), stand up and walk your dog away from the couch a few feet, **using your body-blocking skills to back her away.** **Cross your arms and turn your head away to the side as you sit back down.** **If she comes right back as you sit down, “pat pat” on her head again and body-block her away a second time.** When she returns, be sure to do a **“look away” so that you're not making eye contact.** (I'm always amused at how often we humans tell our dogs to go away and yet continue to make eye contact with them. Meanwhile, the dog is desperately looking at your face, trying to find the cue to what the heck it is that you're trying to communicate. If you turn your head away from your dog, **you're saying that your interaction is over**, and many dogs will seemingly understand and go away. If you keep staring at him, using words to tell him to go away, he'll keep staring back at you, sure that you are trying to visually communicate something important and desperately trying to figure out what it is by looking at your face.)

### Chapter 9 - Personalities

3647 **The behavior of the parents will tell you a lot more about the eventual disposition of a puppy than the behavior of a seven-week-old puppy will.** If you can't pet the mother, the sweet little pup that you're about to take home may not let your visitors in the house once she's an adult.
3658 the dam and the sire allow grooming and handling? How about trimming their nails? **Taking away their favorite toys? How are they at the vet's? How about with other dogs, both familiar and unfamiliar ones? Do they bark out the window for a few seconds when visitors arrive, or do they bark nonstop for ten minutes? Have they ever growled, shown their teeth, snapped, nipped, or bitten anyone, for any reason? Is the dog different with unfamiliar people than he is with familiar people?**
3668 After all, Jeffrey Dahmer was nice all day long at the chocolate factory.
Note - Location 3669
Interesting reading this a few weeks after the netflix show pop
3672 How about exercise? Again, be sure to be specific, because exercise means different things to different people.
3707 “Pretty is as pretty does” applies just as readily to our dogs as it does to people. Remember when that good-looking date turned out to be a jerk? Picking the prettiest puppy can have the same result, so look more carefully at the plain black dog that everyone else ignores—he just might be the best one.
3716 Different environments bring out different aspects of your dog, and you don't really know your dog until you've seen her in a variety of situations.
3722 So don't get in a fight with your spouse about her bogus descriptions of your dog's behavior. She just might be **describing something that happens when you're not there to see it.**
3732 Probably for the same reason that you too are sweet one minute and grumpy the next.

### Chapter 10 - Love and Loss

3958 some animals do show signs of depression and behave in ways very similar to the ways that grieving humans behave.
3963 **What we do know is that in some cases it seems to help to let the living animals spend time with the body of the deceased one.**
4001 If you do rehome a dog, never give her away for free. If people can't afford $75 to buy a dog, then they can't afford to take care of her well either. If they can afford it, but they're not willing to pay it, then they've told you that the dog isn't very important to them, and they don't deserve her.