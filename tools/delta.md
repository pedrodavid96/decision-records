# Delta

## Troubleshoot

* OSC8 hyperlinks on commits hash doesn't seem to be working on bare repos
  Open bug?
* The `file://` OSC8 hyperlink on the diffs isn't really opening how I would expect - in $EDITOR
  Seems fixable:
  https://dandavison.github.io/delta/grep.html?highlight=hyperlink#grep
