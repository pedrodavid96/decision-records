# Test Search Engine

[Let's build a Full-Text Search engine](https://artem.krylysov.com/blog/2020/07/28/lets-build-a-full-text-search-engine/)
[Discussion](https://www.reddit.com/r/programming/comments/hzztgw/lets_build_a_fulltext_search_engine/)
Inverted Index
Text analysis
Tokenizer - Filters:
- Lowercase
- Dropping common words (https://en.wikipedia.org/wiki/Most_common_words_in_English)
- Stemming
  Stemming reduces words into their base form. For example, fishing, fished and fisher may be reduced to the base form (stem) fish.
Building the index
Querying

## References

* [Free and Open Search: The Creators of Elasticsearch, ELK & Kibana | Elastic](https://www.elastic.co/)
* [Apache Lucene - Welcome to Apache Lucene](https://lucene.apache.org/)
* https://github.com/valeriansaliou/sonic
