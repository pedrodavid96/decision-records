# Fitness

tags:
- health
- fitness
- workout

## Road to pull-ups

[The Most Effective Way To Master The Pull-Up (4-Step Progression)](https://builtwithscience.com/fitness-tips/4-step-pull-up-progression/)
[Research][1] has shown that practicing an exercise 3x/week can accelerate strength gains by about 56% compared to practicing it only once a week.
So, we're going to use 3 different pull-up variations to match that ideal training frequency.
[1]: https://link.springer.com/article/10.1007/s40279-016-0543-8?correlationId=4d9ea02c-da9a-44df-a279-bfae59b39531

### Concept 1: Start With The Right Variations

1. Kneeling Lat Pulldown
   Most similar muscle activation pattern
   Details:
   * medium grip
   * weight such that it's heavy enough to pull your knees slightly off the ground
2. Negative Pull-ups
   'downward' portion of an exercise has been found to play an important role in building strength (even thou that's not yet fully understood).
   It appears to do this by providing a unique stimulus to the muscle (that the muscle wouldn't have gotten otherwise from the upward portion of an exercise).
3. Banded Pull-ups

### Concept 2: Strengthen Key Muscles

1. Activated Hangs
   1. Flex your thighs, squeeze your glutes, point your toes forward, and engage your core
   2. pull your shoulders down and away from your ears - and hold that position
   Try to remain as stiff as possible As the seconds go by, you’ll start to feel a burn in your mid-back muscles, forearms, and core, as they work hard to hold that position.
2. Australian Pull-Ups
   Teaches your body how to control itself as you pull AND
   Will continue to strengthen the important core and core muscles used in the pull-up
3. Wide-Grip Lat Pull-Down & Close-Grip Underhand Pulldown
   Engages less of the core but since we’ve already filled this 'core gap' with our other exercises we can strategically use them to add more overall pulling volume
   To further minimize the risk of overworking the same muscles use an overhand wide grip in one of our weekly sessions and an underhand close grip in the other session.

### Step 2: Build The Strength - Adding volume

#### Variations

1. Kneeling Lat Pulldown
   Add more weight.
   Once you get to a point where your knees are well off the ground, simply try to do more reps rather than add more weight.

2. Negative Pull-ups
   Increase the time it takes you to descend from the top position every week.
   Start: 5 sets of a 1-second descent.
   Goal: 5 sets of a 5-second descent.

3. Banded Pull-ups
   Start: Band that allows you to do 3x4
   Goal: 3x8 -> Reduce or take one leg of band

#### Accessory movements

1. Australian Pull-Ups
   Goal: 3x10 -> Reduce height of bar -> elevate feet

2. Active Scapular Holds
   Try to add about 5-10 seconds to your time every week. A goal to aim for would be around 30 seconds.

3. Wide-Grip Lat Pull-Down & Close-Grip Underhand Pulldown
   3x12 -> Increase weight

### Schedule Example

Mon: Activated Hangs -> Kneeling Lat Pulldown -> Australian Pull ups
Wed: Activated Hangs -> Negative Pull-ups -> Wide-Grip Lat Pull-Down
Fri: Activated Hangs -> Banded Pull-ups -> Close-Grip Underhand Pulldown

### Other strategies

[The Fighter Pullup Program Revisited | StrongFirst](https://www.strongfirst.com/the-fighter-pullup-program-revisited/)
[5/3/1 on pull ups? : 531Discussion](https://www.reddit.com/r/531Discussion/comments/ef3urd/531_on_pull_ups/)

## Strength programs

[5/3/1 Primer | The Fitness Wiki](https://thefitness.wiki/5-3-1-primer/)

[Joker Sets](https://www.reddit.com/r/Fitness/comments/6bwz5o/eli5_fsl_and_joker_sets_in_531/di761n0/)
