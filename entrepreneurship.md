# Entrepreneurship

[5 years of Gruntwork. 5 lessons learned bootstrapping a… | by Yevgeniy Brikman | Mar, 2021 | Gruntwork](https://blog.gruntwork.io/5-years-of-gruntwork-bf8c5ed2d367)
* Serendipity, timing, and luck
* Talent is evenly distributed, opportunity is not
* It’s a marathon, not a sprint - usually around 10 years for a startup to succeed (if it even does)
* Life first, then work - Awesome policies and some cool automation around it
* It’s always day 1 - Amazon, Jeff Bezos "Mindset"

[The Shirky Principle: Institutions Try to Preserve the Problem to Which They Are the Solution – Effectiviology](https://effectiviology.com/shirky-principle/)
A well-known example of the Shirky effect in this context is the cobra effect.
It describes a case where British colonial officials in Delhi (India),
set a bounty on dead cobras, in order to reduce the cobra population.
However, this led citizens to breed the cobras for profit,
and eventually to release them when the bounty was canceled.