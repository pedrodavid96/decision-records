# Video Highlights

[10 Best Video Annotation & Labeling Tools In 2024](https://www.labellerr.com/blog/top-video-annotation-tools/)
[GitHub - dshin13/autohighlight: AutoHighlight : the automatic video highlight reel generator](https://github.com/dshin13/autohighlight)
[GitHub - adjaba/video-annotation-tool](https://github.com/adjaba/video-annotation-tool?tab=readme-ov-file)

[Video annotation / review system : selfhosted](https://www.reddit.com/r/selfhosted/comments/17bi995/video_annotation_review_system/)
[elonen/clapshot: Self hosted web based collaborative video review tool](https://github.com/elonen/clapshot)
[ClipThoughts | Video](https://clipthoughts.com/)

[<video>: The Video Embed element - HTML: HyperText Markup Language | MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video)
[<track>: The Embed Text Track element - HTML: HyperText Markup Language | MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/track)
