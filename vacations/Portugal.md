[Hotel Rural Vale Do Rio, Oliveira de Azeméis – Preços 2023 atualizados](https://www.booking.com/hotel/pt/rural-vale-do-rio.pt-pt.html?req_adults=2&no_rooms=1&req_children=0&checkin=2023-06-01&checkout=2023-06-03&group_children=0&group_adults=2&activeTab=main)

![[Pasted image 20230514125049.png]]
![[Pasted image 20230514125115.png]]

![[Pasted image 20230514125130.png]]


[Moinho Pangeia- Outside jacuzzi with stunning view - Villas for Rent in São Martinho do Porto, Leiria, Portugal - Airbnb](https://www.airbnb.com/rooms/54120610?source_impression_id=p3_1684066769_bMuY%2BdE1YZMg%2BOm5&check_in=2023-06-21&guests=1&adults=1&check_out=2023-06-23)

![[Pasted image 20230514132154.png]]
![[Pasted image 20230514132214.png]]

[Casa Agostinho - private pool - Casas para Alugar em Leiria, Leiria, Portugal - Airbnb](https://www.airbnb.pt/rooms/732410882001207518?adults=2&check_in=2023-06-01&check_out=2023-06-03&federated_search_id=186d210c-4d1a-449b-b5ea-8d7586106175&source_impression_id=p3_1684068005_RLiCihxyCONH896c)
[Maria da Vinha - Country Apartment - Apartamentos para Alugar em Góis, Coimbra, Portugal - Airbnb](https://www.airbnb.pt/rooms/875401200031297766?adults=2&check_in=2023-06-01&federated_search_id=d966599d-1300-4e48-88f5-f91d65e10fe8&source_impression_id=p3_1684066046_eF2ykxZJ5WwnVJYT&guests=1&check_out=2023-06-03)
[Casa da "Sabana" grande - Pousadas para Alugar em Casal de Ermio, Coimbra, Portugal - Airbnb](https://www.airbnb.pt/rooms/709467436710258440?check_in=2023-06-01&check_out=2023-06-03&federated_search_id=80fde4a2-08e5-4fe7-aa1f-b35f3f98d0bb&source_impression_id=p3_1684067181_iigkrddoLzD3lKeM&guests=1&adults=1)

[Casas Da Lapa, Nature & Spa Hotel, Seia – Updated 2023 Prices](https://www.booking.com/hotel/pt/casas-da-lapa.en-gb.html?aid=2400705&label=63676_BF-Activation-Hero-CTA_v2-&sid=3f4fd4b25485dab3d449d959747c6db8&atlas_src=sr_iw_btn&checkin=2023-11-24&checkout=2023-11-25&dest_id=2599&dest_type=region&group_adults=2&group_children=0&highlighted_blocks=18018002_377567402_2_1_0&keep_landing=1&nflt=class%3D5&no_rooms=1&room1=A%2CA&sb_price_type=total&type=total&ucfs=1&)
[Sleep & Nature Hotel, Lavre – Updated 2023 Prices](https://www.booking.com/hotel/pt/sleep-amp-nature.en-gb.html?aid=2400705&label=63676_BF-Activation-Hero-CTA_v2-&sid=6e26a7641edb082829ad8f0e1f7f5fce&activeTab=main&all_sr_blocks=0_0_2_1_0&checkin=2023-11-24&checkout=2023-11-25&dest_id=2599&dest_type=region&group_adults=2&group_children=0&hapos=17&highlighted_blocks=0_0_2_1_0&hpos=17&keep_landing=1&matching_block_id=0_0_2_1_0&no_rooms=1&req_adults=2&req_children=0&room1=A%2CA&sb_price_type=total&sr_order=black_friday_deals_upsorter&sr_pri_blocks=0_0_2_1_0__10830&srepoch=1700480369&srpvid=50f651f7207800d4&type=total&ucfs=1&)
[Lago Montargil & Villas, Montargil – Updated 2023 Prices](https://www.booking.com/hotel/pt/lago-montargil-and-villas.en-gb.html?aid=2400705&label=63676_BF-Activation-Hero-CTA_v2-&sid=07b348963a9716a47fb21df7786717a3&activeTab=main&all_sr_blocks=24235408_359940653_2_33_0&checkin=2023-11-24&checkout=2023-11-25&dest_id=2599&dest_type=region&group_adults=2&group_children=0&hapos=1&highlighted_blocks=24235408_359940653_2_33_0&hpos=1&keep_landing=1&matching_block_id=24235408_359940653_2_33_0&no_rooms=1&req_adults=2&req_children=0&room1=A%2CA&sb_price_type=total&sr_order=black_friday_deals_upsorter&sr_pri_blocks=24235408_359940653_2_33_0__14399&srepoch=1700480369&srpvid=50f651f7207800d4&type=total&ucfs=1&)