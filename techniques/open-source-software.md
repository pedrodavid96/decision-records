# Open Source Software (OSS)

[open collective](https://opencollective.com/)
> Make your community sustainable
>
> Community is about trust and sharing.
> Open Collective lets you manage your finances so everyone can see where money comes from and where it goes.
> **Collect and spend money transparently.**
Basically, an option to fund (by donations) your projects transparently.
[Introducing Open Web Docs](https://opencollective.com/open-web-docs/updates/introducing-open-web-docs)
open collection for MDN

[Build and Fund the Open Web Together | Gitcoin](https://gitcoin.co/)
[Gitpay - Bounties for git issues solved](https://gitpay.me/#/)
[Bountysource](https://www.bountysource.com/)

[Open Source & Saying "No"](https://connortumbleson.com/2022/11/28/open-source-saying-no/)
You may not want to continously support user added features, so even if it seems like a simple addition, tread carefully.

[jaypyles/open-spots](https://github.com/jaypyles/open-spots?tab=readme-ov-file)
Open Spots is a fork of Spots that is designed to help organizations deliver real-time building availability data to staff, employees, customers, or students.
