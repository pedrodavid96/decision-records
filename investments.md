[lunaticloser comments on Preço das casas em Portugal subiu 1,9% em novembro](https://www.reddit.com/r/portugal/comments/zey3g8/pre%C3%A7o_das_casas_em_portugal_subiu_19_em_novembro/iza98l2/)
One of the best explanations on inflaction I've ever seen
> Tende sim. O que não quer dizer nada, significa apenas que fazemos matemática com números maiores.
> Antigamente o conceito de uma empresa bilionária não existia, hoje em dia temos empresas a valerem mil vezes isso (há mais que se lhe diga do que inflação neste ponto, mas serve como exemplo). O dólar é o mesmo, simplesmente cada dólar vale menos.
> A parte: Se quiseres um pouco mais de teoria financeira, uma quantidade pequena e controlada de inflação é considerada por muita gente como uma coisa saudável (estamos a falar de valores de 0.5-2%). Isto porque é um incentivo a investir: ter o dinheiro parado no banco com inflação positiva (negativa chama-se deflação) significa que todos os anos o teu dinheiro está a perder valor. Ou seja tens um incentivo para o por a render nalgum sítio, o que faz com que outras pessoas possam usar o teu dinheiro para criar valor verdadeiro. Em retorno pagam te juros (ou ganhos capitais quando venderes o bem que compraste) e portanto tu sais a ganhar e a pessoa que precisava do dinheiro também. Assim se cria valor, em teoria.
> Muito se ouve falar mal de inflação, como se fosse um monstro a temer, mas isso é longe da verdade. O oposto de inflação (deflação) é infinitamente pior. Inflação só é um problema quando é demasiado elevada e imprevisível.

[An overview of DEGIRO](https://indexfundinvestor.eu/2018/12/16/an-overview-of-degiro/)
This whole blog seems pretty good.

[Degiro – Cuidado com os impostos escondidos. O seu IRS agradece](https://taofinance.pt/degiro-cuidado-com-os-impostos-escondidos-o-seu-irs-agradece/)

## IE00BKM4GZ66 - ISHARES EMIM

EUR Options:
* EAM - Euronext Amsterdan
* XET - Xetra
* MIL - Borsa Italiana S.p.A.
* FRA - Borse Frankfurt

Accumulating
Fund Currency - USD
Less Stable
0.18% p.a.

## IE00B5BMR087 - ISHARES CORE S&P 500 UCITS ETF

EUR Options:
* EAM - Euronext Amsterdan
* XET - Xetra
* MIL - Borsa Italiana S.p.A.
* FRA - Borse Frankfurt

0.07% p.a. 🎉

# Risky - Specialized

## LU0533033667 - LYXOR UCITS ETF MSCI WORLD INFORMAT TECH

EUR Options:
* EPA - Euronext Paris
* XET - Xetra
* MIL - Borsa Italiana S.p.A.
* FRA - Borse Frankfurt

Replication - Synthetic (Unfunded swap) ❌
0.30% p.a. ❌
Accumulating
Fund Currency - EUR

# MSCI World

## IE00B4L5Y983 - ISHARES MSCI WOR A

EUR Options:
* EAM - Euronext Amsterdan
* XET - Xetra
* MIL - Borsa Italiana S.p.A.

Accumulating
Fund Currency - USD
Stable (Market like growth) 🎉
0.20% p.a.

## IE00B60SX394 - Invesco MSCI World UCITS ETF

EUR Options:
* XET - Xetra
* MIL - Borsa Italiana S.p.A.

Replication - Synthetic (Unfunded swap) ❌
0.19% p.a.
Stable (Market like growth) 🎉

## LU1781541179 - Lyxor Core MSCI World (DR) UCITS ETF

EUR Options:
* EPA - Euronext Paris
* XET - Xetra
* MIL - Borsa Italiana S.p.A.

Replication - Physical (Optimized sampling)
Fund Currency - USD
0.12% p.a.
Looks less stable but is more recent

---

## IE00B3RBWM25 - VANGUARD FTSE AW

* EAM - Euronext Amsterdan
* XET - Xetra
* MIL - Borsa Italiana S.p.A.

Non Accumulating!!! ❌ -> See  IE00BK5BQT80 Which is much more recent but should be the same
Stable (Market like growth) 🎉

# Bonds

## LU0378818131 - Xtrackers Global Sovereign UCITS ETF 1C EUR hedged

Very Safe - low volatility even for the current situation
0.25% p.a. ❌

## IE00BGYWFK87 - Vanguard USD Corporate Bond UCITS ETF Accumulating

0.09% p.a. 🎉
Fairly recent, still quite stable due to current situation

---

# Breakdown

15% Bonds -> Low "stability" and more "risk" (recommended rule of thumb would be age so 20%-25%)
15% Emerging Markets?
30% S&P500
30% MSCI World (See ACWI as well)
10% LYXOR UCITS ETF MSCI WORLD INFORMAT TECH? -> Risky


# Final first buy!

So... the previous plan was too diverse for diversity sake and would probably result in a lot of transaction fees.
Ended up going with:
- 40% IE00B5BMR087 (iShares Core S&P 500 UCITS ETF (Acc))
- 60% IE00B4L5Y983 (iShares Core MSCI World UCITS ETF USD (Acc))

Both in Euronext Amsterdam since World was a Degiro eligible free ETF, keeping both in the same to avoid additional fees.

They might seem redundant since World already contains ~60% USA but
- I wanted to "risk" a bit on the US market which is usually more volatile but at a consistent (and some times more step) growth
- More importantly, World expense ratio is quite high (0.20%) while S&P is very low (0.07%) so the wins might have even more returns

---

# PPR (Planos Poupança Reforma)

[Razões Para NÃO Investires Em PPRs (Planos Poupança Reforma)](https://www.youtube.com/watch?v=w86yZTUaRuM)
[Reddit discussion](https://www.reddit.com/r/literaciafinanceira/comments/kgw7r6/raz%C3%B5es_para_n%C3%A3o_investires_em_pprs/)

* https://www.reddit.com/r/literaciafinanceira/comments/kchfbq/caixa_arrojado_ppr_vs_stoyk_vs_ar/
* https://www.reddit.com/r/literaciafinanceira/comments/k8r8bu/ppr_24_anos/

---

# Finances

[6 principles of personal finance and budgeting for 2021](https://www.youtube.com/watch?v=XYkwa1D1AC4)
* Take what you ear from personal finance with a grain of salt
* Guilt free environment
* main goals:
  1. Building an emergency fund
  2. Starting budgeting and tracking your expenses
  3. Getting rid of debt
* Schedule your bills to the same day if possible
[What is the 20/4/10 Rule for Buying a Car?](https://www.usautosales.info/blog/what-is-the-20-4-10-rule-for-buying-a-car/)

---

[Why mainstream media's slander of wallstreetbets pisses me off regarding GME.](https://www.youtube.com/watch?v=4EUbJcGoYQ4)

[TOP 8 ETF PARA INVESTIR!!! | Investimento RISCO baixo](https://www.youtube.com/watch?v=9bLowB5FUcc)
