# Alacritty

For some reason pasting into VIM doesn't work in alacritty.
Using tmux seems to workaround this issue.