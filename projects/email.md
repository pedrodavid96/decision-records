# Email

[Cloudflare email forwarding : selfhosted](https://www.reddit.com/r/selfhosted/comments/13p993z/cloudflare_email_forwarding/)
> Receiving email never was a problem, the trouble is sending them from an 'unreliable' IP address...

Options:
* [Pricing and Plans | SendGrid](https://sendgrid.com/en-us/pricing)
* [SimpleLogin | Open source anonymous email service](https://simplelogin.io/)
  [simple-login/app: The SimpleLogin back-end and web app](https://github.com/simple-login/app)
* [MXroute - Email Hosting](https://mxroute.com/)
  (discovered here [Custom domain for personal use - yes or no? Why? : selfhosted](https://www.reddit.com/r/selfhosted/comments/1723azj/custom_domain_for_personal_use_yes_or_no_why/))

[SMTP Server: In-Depth Guide & Answers to FAQs [2023]](https://mailtrap.io/blog/what-is-smtp-server/)

[Serverless email sender using OpenFaaS and .NET Core](https://medium.com/@paulius.juozelskis/serverless-email-sender-using-openfaas-and-net-core-afdef152359)

[What I wish I knew about email development before I started](https://www.reddit.com/r/programming/comments/l6mr63/what_i_wish_i_knew_about_email_development_before/)
[Reddit discussion](programming/comments/l6mr63/what_i_wish_i_knew_about_email_development_before/)
One of the biggest takeaways is that email still has very poor html/css support (still have to use tables instead of grid and flexbox for example)

[Filter Calendar responses from Gmail - Google Workspace Learning Center](https://support.google.com/a/users/answer/9308760?hl=en)
* In the To field, enter your email address.
* Next to Has the words, enter invite.ics OR invite.vcs.
* Check the Has attachment box.

[This Trick Can Take Your Inbox From Hundreds of Emails to Nearly Empty in Minutes](https://medium.com/inc./this-trick-can-take-your-inbox-from-hundreds-of-emails-to-nearly-empty-in-minutes-72042a92511b)
1. Move all emails older than 7 days to Archive/All Mail
   Gmail: in:inbox older_than:7d
2. Move all emails you are cc’ed on older than 3 days to Archive/All Mail
   Gmail: cc:me older_than:3d
3. Move emails that don’t have your name in them and are older than 3 days to Archive/All Mail
   Gmail: -matt older_than:3d
4. Move all newsletter and mailing list emails to your Readings folder
   Gmail: in:inbox label:^unsub
5. Move remaining mailing list emails to “Readings” by searching for common mailing list terms
   Also search the following terms:
   “privacy policy” OR “terms & conditions” OR “preferences” OR “view in browser” OR “view as a web page.”
6. Delete notifications of responses to calendar invitations
   Gmail: from: calendar-notification@google.com

## Mail Server

[James Enterprise Mail Server](https://james.apache.org/)
Components:
* Emailing protocols: SMTP, LMTP, POP3, IMAP, ManageSieve, JMAP
* Mailet container: independent, extensible and pluggable email processing agents
* Storage API: Mailbox API / Search API / User API
* Storage Implementations: Cassandra / PostgreSQL / HSQLDB / MySQL / OpenSearch...
* Administration: JMX / REST / Command Line
* James Core

[Amazon Simple Email Service (SES)](https://docs.aws.amazon.com/ses/index.html)

[Stalwart Mail Server](https://stalw.art/)
Some people in reddit mentioned this was unethical open source because some security "features"
 (OpenID Connect) were locked behind the commercial license.

[maildev/maildev: :mailbox: SMTP Server + Web Interface for viewing and testing emails during development.](https://github.com/maildev/maildev)
Seems to be for dev only.

## Inspiration

* [Mailspring - The best free email app](https://getmailspring.com/)
  [Open Source](https://github.com/Foundry376/Mailspring) with Premium features.
* [ProtonMail - Secure Email](https://protonmail.com/)
  [Open Source](https://github.com/ProtonMail)
* [Newton - Supercharged emailing on iOS, Android, Mac & Windows](https://newtonhq.com/#app)
  Very lean, with a single "Inbox" like view.
  "It isn’t [IA writer](https://ia.net/writer) for email, but it’s damn close." - The Verge
* [Transactional Email API Service For Developers | Mailgun](https://www.mailgun.com/)
* [Email markup | Overview  |  Gmail  |  Google Developers](https://developers.google.com/gmail/markup/overview)
* [Fastmail | We Respect Your Privacy & Put You in Control](https://www.fastmail.com/)


## Libraries

* [lettre/lettre: a mailer library for Rust](https://github.com/lettre/lettre)
* [jonhoo/rust-imap: IMAP client library for Rust](https://github.com/jonhoo/rust-imap)
* [jakartaee/mail-api: Jakarta Mail Specification project](https://github.com/jakartaee/mail-api)
* [SlothLabs/kotlin-mail: A kotlin-esque wrapper for JavaMail.](https://github.com/SlothLabs/kotlin-mail)

[Dovecot | The Secure IMAP server](https://www.dovecot.org/)

[use synology as smtp relay - Pesquisa Google](https://www.google.com/search?client=firefox-b-d&q=use+synology+as+smtp+relay)
[freinet/postfix-relay - Docker Image | Docker Hub](https://hub.docker.com/r/freinet/postfix-relay)
