Inspiration:

[miro - Where distributed teams get work done](https://miro.com/)

[Excalidraw](https://github.com/excalidraw/excalidraw)
* [Cursors](https://twitter.com/Vjeux/status/1238907727906127872?s=09)
* [twitter](https://mobile.twitter.com/excalidraw)

[Penpot: The Design Tool for Design & Code Collaboration](https://penpot.app/)
(Open Source [Figma: The Collaborative Interface Design Tool](https://www.figma.com/))

[Excalidraw blog | Rethinking the Component API](https://plus.excalidraw.com/blog/redesigning-editor-api)
[excalidraw/excalidraw-app/App.tsx at master · excalidraw/excalidraw](https://github.com/excalidraw/excalidraw/blob/master/excalidraw-app/App.tsx)
[Compose-Examples/examples/excalidraw at main · Haxxnet/Compose-Examples](https://github.com/Haxxnet/Compose-Examples/tree/main/examples%2Fexcalidraw)
[Development | Excalidraw developer docs](https://docs.excalidraw.com/docs/introduction/development)
[Couldn't connect to the collab server. Please reload the page and try again · Issue #4993 · excalidraw/excalidraw](https://github.com/excalidraw/excalidraw/issues/4993#issuecomment-1783669768)
