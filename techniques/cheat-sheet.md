[How to Cultivate Your Cheat Sheet](https://amir.rachum.com/blog/2020/06/25/cheat-sheet/)

So my strategy is this: whenever someone asks me how to do something, I sit with them and help as much as I can, and very politely ask that they document what they did in the cheat sheet.
Pros:
* They now know there’s a cheat sheet and how to access it, so that’s another tool in their belt.
* They now know how to edit the cheat sheet, and have less friction doing so again, increasing the usefulness of the cheat sheet.
* The next teammate to join will have more resources to ramp up in the team and the teammate mentoring them will have to spend less time explaining menial tasks and more time for deep discussions and project-specific guidance.
* Now I know there’s useful information in the cheat sheet and will also turn to it when I need to.
* Eventually the cheat sheet is so useful, the other jaded teammate will use it themselves.

The golden rule here is **keep friction as low as possible**:
- Don’t nitpick
- Go easy on the structure
- Optimize for copy and paste
  * Don’t include prompt symbols (leave the $ out of the $ cp /path/from /path/to snippet). (_personal note:_ how do we distinguish input and output?)
  * If you expect some part of the command line to be replaced, try to position those arguments at the end
- Keep it as a one pager
- Make it searchable
- Make headers linkable
