# Continuous Profiling

[Going from O(n) to O(log n) makes continuous profiling possible in production](https://github.com/pyroscope-io/pyroscope/blob/main/docs/storage-design.md) - looks awesome! 🥇⭐⭐⭐
[Reddit Discussion](https://www.reddit.com/r/programming/comments/m295kn/going_from_on_to_olog_n_makes_continuous/)

[Go Programs](https://medium.com/google-cloud/continuous-profiling-of-go-programs-96d4416af77b)

[Bringing Opsian's Continuous Profiling to GraalVM](https://www.opsian.com/blog/continuous-profiling-on-graalvm/)

[time-trace: timeline / flame chart profiler for Clang](https://aras-p.info/blog/2019/01/16/time-trace-timeline-flame-chart-profiler-for-Clang/)

[🚀 Performance as first class citizen — Chapter 1: continuous measurement and tooling](https://engineering.klarna.com/performance-as-first-class-citizen-chapter-1-continuous-measurement-and-tooling-1e6825ef153f)
[Google lighthouse](https://github.com/GoogleChrome/lighthouse/)

[Continuous Profiling in Production: What, Why and How](https://www.youtube.com/watch?v=guvSSGfkHW4)

[Introducing Prodfiler | Prodfiler](https://prodfiler.com/blog/introducing-prodfiler/)