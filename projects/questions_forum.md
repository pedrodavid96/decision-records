# Questions

## Market alternatives:

[Are there any clones/alternatives for running a Stack Exchange style Q&A site?](https://meta.stackexchange.com/questions/2267/are-there-any-clones-alternatives-for-running-a-stack-exchange-style-qa-site)

Both quite small projects (sub 2500 commits) with few contributors and not that active.
https://github.com/debiki/talkyard
A lot of unnecessary features

https://github.com/Erudika/scoold
Used at work and not that great experience

https://www.discourse.org/
More like a "normal forum" for discussions, with no concept of "accepted answer"

https://giscus.app/