# MAAS (Metal as a Service)

Official "MAAS"(^[1][maas]) is from canonical **but** the concept doesn't apply only to it.

[maas]: https://maas.io/

[alexellis/awesome-baremetal: Bare-metal is awesome. Let's share our favourite tools.](https://github.com/alexellis/awesome-baremetal?tab=readme-ov-file)

[How to Create a Ubuntu Packer Image and Deploy on a Bare Metal Server](https://www.infracloud.io/blogs/deploy-ubuntu-packer-image-bare-metal/)

[Has anyone used Ansible to provision bare metal like MaaS or something similar? : ansible](https://www.reddit.com/r/ansible/comments/13j3cwb/has_anyone_used_ansible_to_provision_bare_metal/)

[Cloud-init as PXE alternative for client machines? : linuxadmin](https://www.reddit.com/r/linuxadmin/comments/vi4twl/cloudinit_as_pxe_alternative_for_client_machines/)
Good discussion in comments.

## [Tinkerbell](https://tinkerbell.org/)

K8S native (requires k8s cluster)

[ContainerSolutions/tinkerbell-rpi4-workflow](https://github.com/ContainerSolutions/tinkerbell-rpi4-workflow?tab=readme-ov-file)
Instructions and configuration files to create tinkerbell workflow for raspberry pi 4

## [Metal³ - Metal Kubed](https://metal3.io/)

> The Metal³ project (pronounced: “Metal Kubed”) provides components for bare metal host management with Kubernetes.

Documentation seems more comprehensive than tinkerbell. Objective seems roughly the same.
It seems a little bit more powerful **but** relies on components that usually is only on server-grade hardware.

[How does it work?](https://book.metal3.io/project-overview#how-does-it-work)

> Metal3 relies on Ironic for interacting with the physical machines.
> Ironic in turn communicates with Baseboard Management Controllers (BMCs) to manage the machines.
> Ironic can communicate with the BMCs using protocols such as Redfish, IPMI, or iDRAC.
> In this way, it can power on or off the machines, change the boot device, and so on.

[Supported Hardware - Metal³ user-guide](https://book.metal3.io/bmo/supported_hardware#supported-hardware)

> Metal3 supports many vendors and models of enterprise-grade hardware with a BMC ([Baseboard Management Controller][bmc])
> that supports one of the remote management protocols described in this document.

[bmc]: https://en.wikipedia.org/wiki/Intelligent_Platform_Management_Interface#Baseboard_management_controller
