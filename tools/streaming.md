# Video Conferencing / Streaming

[Twelve Things I’ve Learned After a Year of Video Conferencing | by Forbes | Forbes | Mar, 2021 | Medium](https://medium.com/forbes/twelve-things-ive-learned-after-a-year-of-video-conferencing-90a167f298e2)
* Virtualize your camera
* Virtual backgrounds
* Chroma screen
* Lighting - soft light on each side + central light (ideally a ring) a **little* higher than the monitor.
* Cable connection
* Camera
* Microphone
* Second monitor
* Don’t share your screen - use the virtual camera for more flexibility
* Set up in the same place - avoid reorganizing etc...
* Ration your calls
