Aviary Attorney
Battlerite
Counter-Strike: Global Offensive
DARK SOULS III
Deceit
DOOM
Dying Light
League of Legends
Left 4 Dead 2
Little Nightmares
Overgrowth

# Local

Co-op
	Rayman
	Broforce
	BattleBlock Theater
	Human: Fall Flat
	Monaco
	Portal 2
	Spelunky
	Castle Crashers
    Overcooked
VS
	Brawlhalla
	Mortal Kombat X
	Mount Your Friends
	Sonic & All-Stars Racing Transformed
	SpeedRunners
	Spelunky
	Tricky Towers
	Castle Crashers
	Genital Jousting
	Rocket League
    Ultimate Chicken Horse

# To Play

Alien isolation
DeadSpace 2
Tales From The Borderlands

# Classics

Okami
Sillent Hill
Shadow of Colossus
Kingdom Hearts
Thief
Phoenix Wright Ace Attorney
Fallout
Deus Ex
Half Life
Zelda
Riven

# PS4

Red Dead Rendemption 2
Bloodborne
Horizon Zero Down
The Order: 1886
Killzone Shadowfall
Crash Bandicoot
✅ Uncharted 4 9/10
    The Lost Legacy
✅ Infamous: Second Son 8/10
    ❓ First Light
✅ Spider-Man 9/10
❌ Death Stranding
✅ God of War 9/10
✅ Detroit: Become Human 8.5/10
❌ Days Gone
✅ The Last of Us - Left Behind 9/10
✅ The Last of Us - Part 2 8.5/10
Ghost of Tsushima

# Private Servers for "Shutdown titles"

[Xero](https://xero.gg/news/) - S4 League Clone
[Rising Hub](https://risinghub.net/) - Battlefield Heroes Clone

# LAN

Blur
Serious Sam 3 BFE
Audiosurf
Dirt 3
Dirt Showdown
Dungeon Defenders
Rayman Origins
Rollcage
F1
Unreal Tournament 3 (Awesome for Split-Screen BTW, remember to download all the custom maps for extra fun)
Armagetron Advanced
LEGO Lord of the Rings
Awesomenaugts
Magicka
Portal 2
Left 4 Dead 2
Track Mania 2 : Stadium, Canyon, Valley.
Sonic & All-Stars Racing Transformed
Sonic and SEGA All Stars Racing
Shatter
Doom
Street Fighter IV
Street Fighter X Tekken
Trails Evolution Gold Edition
Trine
Trine 2
StepMania
MegaByte Punch (Also Awesome, fun progression system with co-op and versus)
Minecraft
Family Guy: Back to the Multiverse
ShootMania: Storm
FIFA

Borderlands 2
Outlast
Portal 2
Rainbow Six Vegas 2
Spliter Cell Chaos Theory
Colin Mcrae Rally 2005
Post Apocalyptic Mayhem
