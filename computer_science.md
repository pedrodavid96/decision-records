# Computer Science

> Je n’ai fait celle-ci plus longue que parce que je n’ai pas eu le loisir de la faire plus courte.
As Blaise Pascal said.
(often miss-attributed to Twain as “I apologise for such a long letter - I didn't have time to write a short one.”)

It's basically a variation of (or vice-versa)
> Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away. Antoine de Saint-Exupery


[My favourite interview questions · Issue #173 · donbonifacio/blog](https://github.com/donbonifacio/blog/issues/173)

[101 Tips For Being A Great Programmer (& Human)](https://dev.to/emmawedekind/101-tips-for-being-a-great-programmer-human-36nl)

## Developer Roadmaps
https://roadmap.sh/ This website looks super cool, with nice roadmaps and articles

## [OSSU Open Source Society University](https://github.com/ossu/computer-science)
🎓 Path to a free self-taught education in Computer Science!

## Networking

[(Stanford) Intro to Computer Networking](https://lagunita.stanford.edu/courses/Engineering/Networking-SP/SelfPaced/about)

[Youtube crash course](https://www.youtube.com/watch?v=3QhU9jd03a0)

[Virtual IP address](https://en.m.wikipedia.org/wiki/Virtual_IP_address)

---

[Implement Reader-Writer locks](https://eli.thegreenplace.net/2019/implementing-reader-writer-locks/)

## Thread Local Storage

[Thread Local Storage](https://stffrdhrn.github.io/hardware/embedded/openrisc/2020/01/19/tls.html)

## Relational vs Non Relational Databases
[Mongo](https://www.mongodb.com/scale/when-to-use-nosql-database)
[Stackoverflow 1](https://stackoverflow.com/questions/4981753/when-to-use-mongodb)
[Stackoverflow 2](https://stackoverflow.com/questions/1476295/when-to-use-mongodb-or-other-document-oriented-database-systems)
[What every developer should know about database consistency](https://robertovitillo.com/what-every-developer-should-know-about-database-consistency/)

---

[Monads](https://www.reddit.com/r/programming/comments/bnm7ay/monads_part_1_what_is_a_monad/)

## About time

[So you want to abolish time zones](https://qntm.org/abolish)
[You advocate a ___ approach to calendar reform](https://qntm.org/calendar)
[The 2038 problem is already affecting some systems - reddit discussion](https://www.reddit.com/r/programming/comments/erfd6h/the_2038_problem_is_already_affecting_some_systems/)

## Interview Questions

[My favorite programming interview questions](https://engineering-management.space/post/my-favorite-programming-interview-questions/)
[Unique interview questions to ask interviewers](https://engineering-management.space/post/unique-interview-questions-to-ask-interviewers/)
[Asking your prospective boss questions](https://www.jamsiedaly.io/2019/11/24/interviewing/)
* How many items are in your backlog?
* How much time does it take from idea to implementation?
* Does your team have stand-ups / sprint plannings / retrospectives?
* How many people attend the teams standup / sprint planning / retrospective?

## CAP theorem

https://stackoverflow.com/questions/3423193/why-nosql-say-traditional-rdbms-is-not-good-at-scalable
https://stackoverflow.com/questions/12346326/cap-theorem-availability-and-partition-tolerance
https://www.infoq.com/articles/cap-twelve-years-later-how-the-rules-have-changed/
https://stackoverflow.com/questions/12890367/starcounter-and-cap
https://codahale.com/you-cant-sacrifice-partition-tolerance/

http://blog.nahurst.com/visual-guide-to-nosql-systems
![nosql_cap](./nosql_cap.png)

[Working around the CAP Theorem with Eric Brewer](https://kislayverma.com/summary/working-around-the-cap-theorem/)

---

[I don't know how CPUs work so I simulated one in code - djhworld](https://djharper.dev/post/2019/05/21/i-dont-know-how-cpus-work-so-i-simulated-one-in-code/)

[Fiber](https://en.m.wikipedia.org/wiki/Fiber_(computer_science))

[OpenJDK Project Loom: JVM Threads With Light-Weight Concurrency](https://ahsensaeed.com/java-openjdk-project-loom-threading-example/)

https://cr.openjdk.java.net/~rpressler/loom/Loom-Proposal.html

[Fibry - The first Java Actor System supporting fibers from Project Loom](https://github.com/lucav76/Fibry)

[Fibers aren't useful for much any more](https://www.reddit.com/r/programming/comments/dgfxde/fibers_arent_useful_for_much_any_more/) - Whaaaat?

[Coroutine](https://en.m.wikipedia.org/wiki/Coroutine)

[How to make sense of Kotlin coroutines | Joffrey Bion](https://proandroiddev.com/how-to-make-sense-of-kotlin-coroutines-b666c7151b93)

[Dynamic vs. Static Dispatch | Lukas Atkinson](https://lukasatkinson.de/2016/dynamic-vs-static-dispatch/)

---

> There are two ways of constructing a software design: One way is to make it so simple that there are obviously no deficiencies, and the other way is to make it so complicated that there are no obvious deficiencies. The first method is far more difficult.
>
> -- <cite>Charles Antony Richard Hoare</cite>

## Algorithms / Data Structures

[R-tree](https://en.m.wikipedia.org/wiki/R-tree)

## Dynamic Programming

[What Is Dynamic Programming With Python Examples](https://skerritt.blog/dynamic-programming/)
[How to Solve Sliding Window Problems](https://medium.com/outco/how-to-solve-sliding-window-problems-28d67601a66)

## Database

[Introduction to Database Systems | Joseph Hellerstein | University of California, Berkeley](https://archive.org/details/ucberkeley_webcast_NSKvCVFmk2E)

[Thoughts on Arbitrary Pagination](https://blog.seantheprogrammer.com/thoughts-on-arbitrary-pagination)
> If you don’t care about the technical details of why OFFSET is slow, just know that OFFSET takes linear1 time.
> Balanced binary search trees and B-Trees (default index) are both very efficient for finding a specific value (or the first value that is greater/less than some specific value).
  But we’re not looking for some specific known value, we’re looking for the nth record in the set.
[Everything You Need to Know About API Pagination](https://nordicapis.com/everything-you-need-to-know-about-api-pagination/)
[Stop using offset for pagination - Why it's grossly inefficient](https://use-the-index-luke.com/no-offset)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/knlp8a/stop_using_offset_for_pagination_why_its_grossly/)
**DON'T stop!!!**. This article should be taken as "remember it is inefficient due to it's nature".
Let's not cripple UX due to technical details (not being able to arbitrarily search non-indexed fields or anything along those lines).
In fact, just like mentioned in this [comment](https://www.reddit.com/r/programming/comments/knlp8a/stop_using_offset_for_pagination_why_its_grossly/ghlgu6d/) (and others)
 is a huge offset (e.g. 1000) even a use case for your application?
 If not, why compromise UX for something that will seldomly be done?

[What’s Faster? COUNT(*) or COUNT(1)?](https://blog.jooq.org/2019/09/19/whats-faster-count-or-count1/)

[Visualizing City Cores with H3, Uber’s Open Source Geospatial Indexing System](https://eng.uber.com/visualizing-city-cores-with-h3/)

[Version Controlled Database](https://www.dolthub.com/)
> Dolt is the true Git for data experience in a SQL database, providing version control for schema and cell-wise for data, all optimized for collaboration.

[The Problem with Rails Active Migrations](https://www.liquibase.org/2007/06/the-problem-with-rails-active-migrations.html)

[ORM database migration tools](https://vsevolod.net/migrations/)
[Discussion](https://www.reddit.com/r/programming/comments/gka90h/orm_database_migration_tools/)

## Machine Learning

[Comics intro explanation](https://cloud.google.com/products/ai/ml-comic-1/)

## State machines
https://deniskyashif.com/a-practical-guide-to-state-machines/

[Myths Programmers Believe about CPU Caches](https://software.rajivprab.com/2018/04/29/myths-programmers-believe-about-cpu-caches/)

## UUIDs

[Universally Unique Lexicographically Sortable Identifier](https://github.com/ulid/spec)

Partly inspired by:
* http://instagram-engineering.tumblr.com/post/10853187575/sharding-ids-at-instagram
* https://firebase.googleblog.com/2015/02/the-2120-ways-to-ensure-unique_68.html


[Software disenchantment](https://tonsky.me/blog/disenchantment/)
[Reddit thread](https://www.reddit.com/r/programming/comments/eipb5c/software_disenchantment/)
* Everything is unbearably slow
* Everything is HUUUUGE
* Everything rots
  > A 16GB Android phone was perfectly fine 3 years ago.
    Today, with Android 8.1, it’s barely usable
* Worse is better
* Programming is the same mess
  > Build systems are inherently unreliable and periodically require full clean,
    even though all info for invalidation is there.

  > And build times? Nobody thinks compiler that works minutes or even hours is a problem.
    What happened to “programmer’s time is more important”?
* We’re stuck with it
* Business won’t care
  > Neither will users. They are only learned to expect what we can provide.
    We (engineers) say every Android app takes 350 MB? Ok, they’ll live with that.
* **It’s not all bad**
  > [Xi editor](https://github.com/xi-editor/xi-editor), LMAX-Exchange [disruptor](https://github.com/LMAX-Exchange/disruptor), Real Logic [sbe](https://github.com/real-logic/simple-binary-encoding) and [aeron](https://github.com/real-logic/aeron), Jonathan Blow ...

[Don’t try to sanitize input. Escape output.](https://benhoyt.com/writings/dont-sanitize-do-escape/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/fa7rn8/dont_try_to_sanitize_input_escape_output/)
* How does cross-site scripting happen?

* Why input filtering isn’t a great idea

  - A couple might sign up to NaiveSite as Bob & Jane Smith,
    but the filtering code strips the &, and suddenly Bob is on his own,
    with a middle name of Jane.
  - More zealous and also strips ' and ", someone like Bill O’Brien
    becomes Bill OBrien.
  - _Trivia:_
    [Falsehoods about names](https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/)
    [Falsehoods programmers believe about addresses](https://www.mjt.me.uk/posts/falsehoods-programmers-believe-about-addresses/)
  - Input filtering gives false sense of security, you're messing input
    and you're still vulnerable if you ever change your ouput.

* Escape your output instead

  - Note: **SQL is an output of your program to the Database engine**

* But what if you want raw input?

  Example: Allow user to enter HTML or Markdown for display
  - Most restrictive option: Allow markdown only
  - Less restrictive: Allow only a whitelist of allowed tags and attributes
    Both [Stack Exchange](https://meta.stackexchange.com/questions/1777/what-html-tags-are-allowed-on-stack-exchange-sites/135909#135909)
    and [GitHub](https://github.github.com/gfm/#disallowed-raw-html-extension-)
    take this second approach.
* What about validation?

  - Input sanitization is usually a bad idea, but input validation is a good thing.
  - You must do validation at least on the backend, otherwise an attacker could bypass the frontend validation
  - You can also validate early on the frontend to show errors more real-time, without a round trip to the server

[What is the difference between Serialization and Marshaling?](https://stackoverflow.com/questions/770474/what-is-the-difference-between-serialization-and-marshaling)

From the Marshalling (computer science) Wikipedia article:

> The term "marshal" is considered to be synonymous with "serialize" in the Python standard library1, but the terms are not synonymous in the Java-related RFC 2713:

> To "marshal" an object means to record its state and codebase(s) in such a way that when the marshalled object is "unmarshalled", a copy of the original object is obtained, possibly by automatically loading the class definitions of the object. You can marshal any object that is serializable or remote. Marshalling is like serialization, except marshalling also records codebases. Marshalling is different from serialization in that marshalling treats remote objects specially. (RFC 2713)

> To "serialize" an object means to convert its state into a byte stream in such a way that the byte stream can be converted back into a copy of the object.

[Working with strings in Rust](https://fasterthanli.me/blog/2020/working-with-strings-in-rust/)
Very good article about Strings in general, not even Rust or C related!!!

[The Builder pattern is a finite state machine!](https://blog.frankel.ch/builder-pattern-finite-state-machine/)
Interesting way of looking at Builder pattern.

[Simplest explanation of the math behind Public Key Cryptography](https://www.onebigfluke.com/2013/11/public-key-crypto-math-explained.html?m=1)

[Time Disorder | Don't order events by timestamp](https://caolan.uk/articles/time-disorder/)
Consider distributed clocks
If you need to generate points in a sequence at multiple sites, then you may need a more complex series of counters like Lamport timestamps or a vector clock. Distributed clocks like this provide a partial causal ordering of events and a means to detect conflicts (i.e. events that are seen as concurrent because they extend a shared point in history).

[The Missing Semester of Your CS Education](https://missing.csail.mit.edu/)
1/13: Course overview + the shell
1/14: Shell Tools and Scripting
1/15: Editors (Vim)
1/16: Data Wrangling
1/21: Command-line Environment
1/22: Version Control (Git)
1/23: Debugging and Profiling
1/27: Metaprogramming
1/28: Security and Cryptography
1/29: Potpourri
1/30: Q&A
[Reddit Discussion](https://www.reddit.com/r/programming/comments/io7nq3/the_missing_semester_of_your_cs_education_mit/)

[How we compress Pub/Sub messages and more, saving a load of money](https://blog.lawrencejones.dev/compress-everything/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/kmkysn/how_we_compress_pubsub_messages_and_more_saving_a/)
Simple gzip compression
Included grafana dashboards to see compression rates **and** cost savings, which is really awesome.
> compressed to at most 30% the original size. When measured over our entire corpus of logs, we average at a ~12% compression ratio, meaning 1GiB of logs becomes just 120MiB.
> $12,900 has become 12% x $12,900 = $1,548.
> This means we’ve saved about $11,500.
> **Compression is a trade-off, a decision we make to trade CPU for another resource that might be more expensive or less available.**

[Reverse Engineering the source code of the BioNTech/Pfizer SARS-CoV-2 Vaccine](https://berthub.eu/articles/posts/reverse-engineering-source-code-of-the-biontech-pfizer-vaccine/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/kk8pxz/this_programmer_reverse_engineered_the_pfizer/)
Can't possibly say how good and interesting this is 🥇⭐
Hoping for a better 2021!

[Oct 11, 2020 - A review of consensus protocols](https://thomasvilhena.com/2020/10/a-review-of-consensus-protocols)
Formally a consensus protocol must satisfy the following three properties:
* Termination - Eventually, every correct process decides some value.
* Integrity - If all the correct processes proposed the same value “v”, then any correct process must decide “v”.
* Agreement - Every correct process must agree on the same value.
Reviews the following protocols:
* Chandra–Toueg
* Ben-Or
* Basic Paxos
* Nakamoto Consensus
[Reddit discussion](https://www.reddit.com/r/programming/comments/jgplt0/a_review_of_consensus_protocols/)

[Interface ergonomics: automation isn't just about time saved : programming](https://www.reddit.com/r/programming/comments/q704tn/interface_ergonomics_automation_isnt_just_about/)
Contains the xkcd with the time saved graph.

https://github.com/hazelcast/hazelcast

[System Design](https://systemdesign.one/)
Good articles like
[What Is Service Discovery? - System Design](https://systemdesign.one/what-is-service-discovery/)
[Distributed Counter System Design - System Design](https://systemdesign.one/distributed-counter-system-design/)
[Slack Architecture - System Design](https://systemdesign.one/slack-architecture/)