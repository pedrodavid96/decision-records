# Animations

[Animation as a pure function of time](https://github.com/revery-ui/revery/pull/631)
This looks awesome!!!

[The Unreasonable Effectiveness Of Declarative Programming](https://bollu.github.io/mathemagic/declarative/index.html)
Declarative animations
Great explanations!! 🥇⭐
[Discussion](https://www.reddit.com/r/programming/comments/gmhyrk/the_unreasonable_effectiveness_of_declarative/)

[Sarah Drasner - Animating in React](https://www.youtube.com/watch?v=QlmaI7x7SYo)

[Advanced RxJS: State Management and Animations](https://www.youtube.com/watch?v=jKqWMvdTuE8)

[Remotion](https://www.remotion.dev/)
Create motion graphics in React
[Discovered on](https://mobile.twitter.com/pomber/status/1358837764033241092?s=09)

[Create interactive videos in React | RactivePlayer](https://ractive-player.org/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/lsbndt/create_videos_in_react_that_viewers_can_interact/)

[Practical magic with animations in Jetpack Compose](https://youtu.be/HNSKJIQtb4c?si=2FcQzw8_3NJ7ts8N)

[Motion - A modern animation library for JavaScript and React](https://motion.dev/)
