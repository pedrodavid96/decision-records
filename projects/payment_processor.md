[Predicting and Monitoring Payment Volumes with Spark and ElasticSearch by Adyen](https://medium.com/adyen/predicting-and-monitoring-payment-volumes-with-spark-and-elasticsearch-52ebf0099a26)

[Railgun: A new weapon for mission critical streaming tasks](https://medium.com/feedzaitech/railgun-a-new-weapon-for-mission-critical-streaming-tasks-6432dd66a244)
[Paper](https://arxiv.org/abs/2009.00361)
Feedzai new Streaming Engine.
