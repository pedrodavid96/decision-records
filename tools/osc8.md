# OSC8

https://github.com/Alhadis/OSC8-Adoption/
https://gist.github.com/egmontkob/eb114294efbcd5adb1944c9f3cb5feda

```bash
#!/bin/bash

# Define the OSC sequence function
osc_sequence() {
    echo -e "\033]8;;$1\a$2\033]8;;\a"
}

git log "$@" | sed -E "s|([A-Z0-9]+-[0-9]+): |$(osc_sequence 'https://example.com/issues/\1' '\1'): |g" | less -R
```
