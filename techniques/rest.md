Web Frameworks in various languages:

Haskell:
https://wiki.haskell.org/Web/Frameworks
https://www.reddit.com/r/haskell/comments/3a4qrl/libraryframework_suggestions_for_a_rest_api_in/


Prolog:
https://www.monolune.com/web-programming-in-swi-prolog/
https://www.swi-prolog.org/pldoc/man?section=httpjson


Erlang / elixir:

https://ninenines.eu/docs/en/cowboy/2.3/guide/rest_handlers/
https://elixir-lang.org/
