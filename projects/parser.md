[Parsing XML at the Speed of Light](https://aosabook.org/en/posa/parsing-xml-at-the-speed-of-light.html)

[Parse, don’t validate](https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/)
functional programming, haskell, types