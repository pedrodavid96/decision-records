# Mortal Kombat

## Mortal Kombat Legends: Scorpion's Revenge

7.5/10

Truly R-Rated, tons of gratuitous gore.

The action / fights choreography is incredible.
The story is pretty basic and predictable
(maybe because I already knew some lore) but good enough.

Tournament rules are very weird, it looks more like a Battle
Royal than a tournament.
Besides, the extremely useful (and unnecessary as they could
have just grabbed her) force field in the fight **before** the
tournament isn't used in the final - resulting in both Goro's
death and Shang Tsung being taken hostage.

About that, it is also kind of stupid and "easy plot", the
supposedly hard choice Scorpion has to make (revenge vs saving earth)
is avoided and he can both have the cake and eat it.

Shang Tsung is completely replaceable by Goro and he contributes
nothing to the movie.
