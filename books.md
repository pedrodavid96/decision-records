https://www.reddit.com/r/LivrosPortugal/comments/1cb1wx9/top_10_rlivrosportugal_2024/

GPU Pro
Vulkan Programming Language
Physically Based Rendering: From Theory to Implementation
Game Programming Gems
Game Engine Gems

----------------------------------------------------------

The Count of Monte Cristo
The brothers Karamazov - Crime & Punishment
It Can't Happen Here
Dante Divine Comedy
How to Win Friends and Influence People

----------------------------------------------------------

** Philosophy **
- Ishmael

** Psychology **
- ✅ Man's Search for a Meaning - 9.5/10
    "a human being is not one in pursuit of happiness but rather in search of a reason to become happy"
    "To suffer unecessarily is masochistic rather than heroic."
    "For the world is in a bad state, but everything will become still worse unless each of us does his best."
    Viktor E. Frankl

    "He who has a why to live for can bear almost any how"
    Friedrich Nietzsche

** Satire **
- ✅ Animal Farm - 10/10
    "No question now what has happened to the faces of the pigs. The creatures outside looked from pig to man, and from man to pig, and from pig to man again: but already it was impossible to say which was which."
    George Orwell

** Distopian **
- ✅ Fahrenheit 451 - 7.5/10

** Science-Fiction **
- ✅1984 - 9.5/10
- Slaughterhouse 5 -> ✅Breakfast of Champions -> Mother Night -> Galapagos
- Brave New World
- The Giver
- Do Androids Dream of Electric Sheep
- Flowers of Algernon
- ✅ 3 Body Problem - 9/10 - Incredible
  Less perfect because character development is a bit weird and pacing
  is sometimes lacking (first half of second book).
- Moonbound (Anth)
  Discovered in Wisereads Vol. 41 Newsletter.
  > Explores two futures at once: a near future of human success, and a more
  distant future — eleven thousand years from now — in which things have
  gone awry for Earth, thanks in part to mysterious creations called
  dragons.

** Speculative-Fiction **
- The Handmaids Tale

** Thriller **
- The Girl Next Door
- We Need to Talk About Kevin

** Erotic **
- 120 Days of Sodom

** Mistery **
- The Curious Incident of the Dog in the Nighttime

https://www.reddit.com/r/threebodyproblem/comments/158ewlw/just_finished_the_dark_forest_and_it_was_amazing/jt9m1eu/

---

- Lord of the flies
  At the dawn of the next world war, a plane crashes on an uncharted island, stranding a group of schoolboys.
- The Children of Men
  A world with no children and no future. The human race has become infertile, and the last generation to be born is now adult.
  (From attack on titan discussion)
