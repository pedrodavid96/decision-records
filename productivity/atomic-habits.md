[Pointing and Calling](https://www.youtube.com/watch?v=9LmdUz3rOQU)


> “Until you make the unconscious conscious, it will direct your life and you will call it fate.”
- Carl Gustav Jung

[Diderot effect](https://en.wikipedia.org/wiki/Diderot_effect)
> It is based on two ideas.
> The first idea is that goods purchased by consumers will align with their sense of **identity**,
>  and, as a result, will complement one another.
> The second idea states that the introduction of a new possession that deviates from the consumer's
>  current complementary goods can result in a process of spiraling consumption.

[I Finally Read Atomic Habits, Here Are My Top 5 Takeaways | by Tom Belskie | Tom Thoughts | Medium](https://medium.com/tom-thoughts/i-finally-read-atomic-habits-here-are-my-top-5-takeaways-57dd6f904ab4)
1. Beware of the motion vs. action trap
2. We live in a delayed return environment
3. Identity change first, the rest will follow
4. “The aggregation of marginal gains”
5. Winners and Losers* have the same goals