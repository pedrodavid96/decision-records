## Colors

```css
:root {
    --color: #fdfdfd;
    --high-contrast-accent-color: #e82c8e;
    --low-contrast-accent-color: #7538cb;
    --low-contrast-background-color: #1b1d22;
    --high-contrast-background-color: #101115;
    --orange-wow: #ecab1d;
    --green-wow: #2bae3c;
}
```
