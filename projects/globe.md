[How we built the GitHub globe](https://github.blog/2020-12-21-how-we-built-the-github-globe/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/khv9nx/how_we_built_the_github_globe/)
Shows fancy globe with "connections" between locations (a la Feedzai "Genome 2.0")
Pretty cool effect.
