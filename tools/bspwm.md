BSPWM

Focus monitor:
`bspc monitor -f DisplayPort-0`

[https://www.reddit.com/r/Polybar/comments/j1p0gm/bspwm_rofi_the_count_dracula_returns/](https://www.reddit.com/r/Polybar/comments/j1p0gm/bspwm_rofi_the_count_dracula_returns/)
Very cool theme

[sxhkd sequences](https://www.google.com/search?client=firefox-b-d&q=sxhkd+sequences)

[Basics of my BSPWM | Protesilaos Stavrou](https://protesilaos.com/pdfd/basics-my-bspwm/)

[Put window in preselected space? : bspwm](https://www.reddit.com/r/bspwm/comments/85cf0g/put_window_in_preselected_space/)
`bspc node -n last.\!automatic -t tiled` would move the focused window to
the last preselection area and change its state to tiled.
You could also modify how automatic splits are preformed with external
rules.

