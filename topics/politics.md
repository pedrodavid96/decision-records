# Politics

[Afinal, somos liberais - Portugal Amanhã](https://amanha.sapo.pt/2024/05/16/afinal-somos-liberais/)
tl;dr:
> Ainda nos salários, o adepto defende que se pague por objetivos: o avançado deve ganhar pelos golos que marca, o guarda-redes pelos jogos em que não sofre e todos pelas vitórias que têm. E só pelo que jogam. Se estiveram lesionados não recebem prémios.
> Mas o mesmo adepto, na empresa onde trabalha, defende o salário fixo porque os resultados não dependem dele. Prémios, bónus ou retribuição variável são uma invenção do grande capital para explorar o proletariado. O que o adepto defende o mesmo cidadão/trabalhador odeia.
> Se o adepto, na qualidade de cidadão, colocasse esta cultura ao serviço da economia e da sociedade, talvez o país discutisse o Top 10 europeu da mesma forma que o disputamos no futebol.
