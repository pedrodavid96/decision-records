# Web

[Well-known URI - Wikipedia](https://en.wikipedia.org/wiki/Well-known_URI)

[Smarter than 'Ctrl+F': Linking Directly to Web Page Content : programming](https://www.reddit.com/r/programming/comments/1gcjilc/smarter_than_ctrlf_linking_directly_to_web_page/)

[Redirect www.domain.com to domain.com with Cloudflare](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/#redirect-wwwdomaincom-to-domaincom-with-cloudflare)
If you use Cloudflare, you can redirect www to domain.com without adding both www.domain.com and domain.com to GitLab.
To do so, you can use Cloudflare’s page rules associated to a CNAME record to redirect www.domain.com to domain.com. You can use the following setup:

1. In Cloudflare, create a DNS A record pointing domain.com to 35.185.44.232.
2. In GitLab, add the domain to GitLab Pages and get the verification code.
3. In Cloudflare, create a DNS TXT record to verify your domain.
4. In GitLab, verify your domain.
5. In Cloudflare, create a DNS CNAME record pointing www to domain.com.
6. In Cloudflare, add a Page Rule pointing www.domain.com to domain.com:
   * Navigate to your domain’s dashboard and select Page Rules on the top nav.
   * Select Create Page Rule.
   * Enter the domain www.domain.com and select + Add a Setting.
   * From the dropdown list, choose Forwarding URL, then select the status code 301 - Permanent Redirect.
   * Enter the destination URL https://domain.com


https://web.dev/testing-web-design-color-contrast/

[Overview - Chrome Developers](https://developer.chrome.com/docs/lighthouse/overview/)

[Recommended Web Performance Timings: How long is too long? - Web Performance | MDN](https://developer.mozilla.org/en-US/docs/Web/Performance/How_long_is_too_long#responsiveness_goal)

> it is important to provide feedback and acknowledge the user's response or interaction and to do so within 100ms, preferably within 50ms. 50ms seconds feels immediate.

[Increase HTTP Performance by Fitting In the Initial TCP Slow Start Window](https://sirupsen.com/napkin/problem-15#problem-1-3-tls-roundtrips-rather-than-2)

[Developer Tools secrets that shouldn’t be secrets | Christian Heilmann](https://christianheilmann.com/2021/11/01/developer-tools-secrets-that-shouldnt-be-secrets/) | [Comments](https://www.reddit.com/r/programming/comments/r8wph3/web_developer_tools_secrets_that_shouldnt_be/)
1. Console is much more than `log()`!
(All browsers with developer tools following the standard)
2. You can log without source access – live expressions and logpoints
(Chromium browsers)
3. You can log outside the browser – VS Code debugger
(Chromium Browsers and VS Code)
4. You can inject code into any site – snippets and overrides.
(Chromium Browsers)
5. You can inspect and debug much more than you know!
(Chromium Browsers)
6. Some dirty secrets…
7. You’re the audience and the clients of Developer Tools!
(Applies to all browsers, but channels shown here are Microsoft Edge only)
My other work:


## ISEL PI: Programming on the Internet
https://github.com/isel-leic-pi/1617i-LI51d

[Google's Brotli compression format](https://github.com/google/brotli)

[The world is essentially out of IPv4 addresses](https://www.reddit.com/r/programming/comments/anfqt3/reminder_the_world_is_essentially_out_of_ipv4/)

[JSON Web Tokens](https://www.reddit.com/r/node/comments/bbya73/json_web_tokens_explanation_video/)

[Overreacted | Dan Abramov Blog](https://overreacted.io)

[HTTP headers for the responsible developer](https://www.twilio.com/blog/a-http-headers-for-the-responsible-developer)

---

[Maybe we could tone down the JavaScript | Reddit discussion](https://www.reddit.com/r/programming/comments/bh6tik/maybe_we_could_tone_down_the_javascript/)

[Maybe we could tone down the JavaScript](https://eev.ee/blog/2016/03/06/maybe-we-could-tone-down-the-javascript/#reinventing-the-square-wheel)
^seems a cool discussion

[Static Web - back to the roots](https://www.reddit.com/r/programming/comments/bsgw60/static_web_back_to_the_roots_exploring_the_shift/)

---

[Front-end Developer Handbook ](https://frontendmasters.com/books/front-end-handbook/2019/)
^looks awesome!!

## Architecture

[Design A Messaging Application Like Facebook Messenger](https://shandilya.dev/system-design/design-a-messaging-application-like-facebook-messenger/)

[API Gateway using .NET Core, Ocelot and Consul](https://medium.com/@paulius.juozelskis/api-gateway-using-net-core-ocelot-and-consul-f0adea97f57)

[Latency and fault tolerance library | Netflix | Java](https://github.com/Netflix/Hystrix)

## Performance

TTI - (Time to Interactive)

Notes from Cloudflare performance insights:
[Eliminate render-blocking resources  |  Lighthouse  |  Chrome for Developers](https://developer.chrome.com/docs/lighthouse/performance/render-blocking-resources/)
[Defer non-critical CSS  |  Articles  |  web.dev](https://web.dev/articles/defer-non-critical-css)
[critical/README.md at master · addyosmani/critical](https://github.com/addyosmani/critical/blob/master/README.md)

## Payloads and memory usage

[Reduce JavaScript Payloads with Code Splitting ](https://developers.google.com/web/fundamentals/performance/optimizing-javascript/code-splitting/)

[Remove unused code](https://web.dev/codelab-remove-unused-code/)

## Design Patterns

[Design patterns for modern web APIs | David Luecke](https://blog.feathersjs.com/design-patterns-for-modern-web-apis-1f046635215)

# Development

## Hot Reloading

[Live reload/refresh solution for HTML/CSS in 2017](https://stackoverflow.com/a/43807278)

1. [live-server](http://tapiov.net/live-server/) (looks super neat and open source 🎉) and [json-server](https://github.com/typicode/json-server) (looks great for moking APIs).
2. [http://livejs.com/](http://livejs.com/) seems intrusive (require to include it in my html)
3. [LiveReload](http://livereload.com/) looks old

---

[Should a website work without Javascript discussion](https://www.reddit.com/r/programming/comments/e0c4u3/should_a_website_work_without_javascript/)


[CNAME Cloaking, the dangerous disguise of third-party trackers](https://www.reddit.com/r/programming/comments/e00tqb/cname_cloaking_the_dangerous_disguise_of/)

[The cost of parsing JSON](https://v8.dev/blog/cost-of-javascript-2019#json)
[simdjson](https://github.com/simdjson/simdjson)
[Discussion](https://www.reddit.com/r/programming/comments/i4qvsj/simdjson_parsing_gigabytes_of_json_per_second/)

[How Uber Engineering Evaluated JSON Encoding and Compression Algorithms to Put the Squeeze on Trip Data](https://eng.uber.com/trip-data-squeeze/)

[How HTTPS works](https://howhttps.works/)

[Better HTTP/2 Prioritization for a Faster Web](https://blog.cloudflare.com/better-http-2-prioritization-for-a-faster-web/)
Very very good article ❤

[HTTP/3: the past, the present, and the future](https://blog.cloudflare.com/http3-the-past-present-and-future/)

[Performance testing HTTP/1.1 vs HTTP/2 vs HTTP/2 + Server Push for REST APIs](https://evertpot.com/h2-parallelism/)

[What are cookies?](https://www.cookiesandyou.com/)
Very cool explanatory video. Link to this site in all your sites that use cookies :D
[Firefox 85 Cracks Down on Supercookies](https://blog.mozilla.org/security/2021/01/26/supercookie-protections/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/l5liah/firefox_85_cracks_down_on_supercookies/)

[Web Architecture 101](https://engineering.videoblocks.com/web-architecture-101-a3224e126947)
Shows most common components in web applications

[Walking the wire: Mastering the Four Decisions in Microservices Architecture](https://medium.com/systems-architectures/walking-the-microservices-path-towards-loose-coupling-few-pitfalls-4067bf5e497a)

[GOTO 2019 • Interaction Protocols: It's All About Good Manners • Martin Thompson](https://www.youtube.com/watch?v=bzDAYlpSbrM)

[Web APIs You \[Probably\] Didn't Know Existed](https://www.youtube.com/watch?v=EZpdEljk5dY)
03:45 Page Visibility (changing tabs for example)
06:26 Online State (online / offline and maybe 2g / 3g later on)
09:24 Vibration
11:59 Device Orientation
14:00 Clipboard
17:37 Ambient Light (Great!) (and more sensors!)
20:52 Battery Status
23:05 Web Assembly
24:25 Gamepad

[Service Workers: an Introduction](https://developers.google.com/web/fundamentals/primers/service-workers)

[Payment Request API](https://www.w3.org/TR/payment-request/)

[Web Apps can’t really do *that*, can they? - Steve Sanderson](https://www.youtube.com/watch?v=MiLAE6HMr10)
Loads instantly even offline
Receives updates even when not loaded
Syncs edits when network available
Build with C# / SWIFT / Rust / etc... running client-side
Auto-log-in across devices
Takes One-Click payment via native UI

[Twitter Direct Message Caching and Firefox](https://hacks.mozilla.org/2020/04/twitter-direct-message-caching-and-firefox/)
TL;DR: Twitter should have used Http Header `Cache-Control`: `no-store` for it's direct messages.

[Firefox (75+) supposts native lazy loading of images](https://www.reddit.com/r/programming/comments/fy3eaz/firefox_75_now_supports_native_lazy_loading_of/)

[Things you can do with a browser in 2020 ☕️](https://github.com/luruke/browser-2020)

[prefers-color-scheme: Hello darkness, my old friend](https://web.dev/prefers-color-scheme/)
[A Better Approach to Dark Mode on Your Website](https://www.reddit.com/r/programming/comments/i51a1l/a_better_approach_to_dark_mode_on_your_website/)
Haven't read the article but didn't look that good, on the other hand the discussions look pretty awesome.

[Different versions of your site can be running at the same time](https://jakearchibald.com/2020/multiple-versions-same-time/)
1. A user opens your site.
2. You deploy an update.
3. The user opens one of your links in a new tab.
4. You deploy an update.
It's unlikely though, right? Maybe no, maybe:
You deploy often.
Your navigations are client-side.
The user is likely to have multiple tabs open to your site.
Your site uses offline-first patterns via a service worker.
[Caching best practices & max-age gotchas](https://jakearchibald.com/2016/caching-best-practices/)

[Site Reliability Engineering - 3 books](https://landing.google.com/sre/books/)

[Hyper Lightweight Websites](https://www.youtube.com/watch?v=VUwyYhNO63I)
responsive images using `srcset`
gzip compression (or `brotli`)
http/2 pipelining

[A case against text protocols](https://unmdplyr-new.bearblog.dev/a-case-against-text-protocols/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/knn9ap/a_case_against_text_protocols/)
Some arguments advocating for text protocols:
* Simpler to use text
* You can type out messages
* It's easy to debug
* Parsing
* Extensibility
* Error recovery

## [Schema.org](https://schema.org/)
> Schema.org is a collaborative, community activity with a mission to create, maintain, and promote schemas for structured data on the Internet, on web pages, in email messages, and beyond.

[Getting Started](https://schema.org/docs/gs.html)
Example:
```html
<div itemscope itemtype ="http://schema.org/Movie">
  <h1 itemprop="name">Avatar</h1>
  <div itemprop="director" itemscope itemtype="http://schema.org/Person">
  Director: <span itemprop="name">James Cameron</span> (born <span itemprop="birthDate">August 16, 1954</span>)
  </div>
  <span itemprop="genre">Science fiction</span>
  <a href="../movies/avatar-theatrical-trailer.html" itemprop="trailer">Trailer</a>
</div>
```

[We rendered a million web pages to find out what makes the web slow](https://catchjs.com/Blog/PerformanceInTheWild)
[Reddit discussion](https://www.reddit.com/r/programming/comments/kiq0ih/we_rendered_a_million_web_pages_to_find_out_what/)
Top 1 million pages on the web - in short summary this only gave an idea on why some
 requests could be slow but it's hard to give specific and empiric evidences on each without
 a more in-depth investigation.

Overall it seems like a good reference to some common third party Javascript API's that are
 somewhat correlated with slow page times (e.g.: JQuery, Google Ads and Analytics,
 as well as Facebook, Twitter...).
The slow down might not be directly because of these libraries themselves but because of their
 sheer size as well as bad usage.

Comments also referenced this interesting anecdote:
[90 Percent of Everything: Adding delays to increase perceived value: does it work?](https://90percentofeverything.com/2010/12/16/adding-delays-to-increase-perceived-value-does-it-work/)

[Building Webhooks Into Your Application: Guidelines and Best Practices](https://workos.com/blog/building-webhooks-into-your-application-guidelines-and-best-practices)
> Webhooks are reverse APIs
As in, they are a way in which you, with your service, proactively notify you customers
 on what's happening in your app.

1. Authentication: Customer wants to verify call is actually from your service
   a) Don’t send sensitive data through webhooks‍
   b) Sign and encrypt webhook payloads‍
      i. Encrypt the entire payload‍
        > fairly uncommon among major webhook providers (Dropbox, Stripe, Twilio, etc.) and requires some extra work on both your and your consumers’ end; but it ensures pretty tight security.‍
      ii. Certificate pinning‍
        > ... most common way ... require your consumer to provide the specific certificate they’re using. For example, Twilio won’t send webhook data to HTTPS endpoints with self signed certificates.

2. Sending events: error handling, ordering, and duplicates
   > Your webhook system will not be a perfect message queue, and you shouldn’t try to make it one
      - even companies like Stripe guarantee almost no integrity around ordering, number of events sent, and other ergonomics that you’d expect as a consumer
    > The general rule - and expectation from your consuming developers - is that
       you’ll send events at least once, but that’s about it.‍
    If you POST request fails, some general best practices:
    * Exponential backoff
    * If an endpoint hasn't been responding for a while, mark it as "broken"
    * Send email to the customer notifying them that you've been unable to reach
       the endpoint and they need to fix it.

    Webhook providers typically do not guarantee ordering and deduplication.

3. Development tips:
   1. Impossible Testing with live URLs‍ - have mock servers
   2. A sample events library‍ - worth investing time up front to build a library of sample events
   3. The database‍ - schema could look like: | URL | userId / email | last event sent time | is broken |
   4. Logging‍ - it's helpful to log each request
   5. Separating events from webhooks‍ - high level architectural note, you don't want something wrong with webhooks impacting application logic.

Contains example: how Stripe does webhooks

[Hands-on with Portals: seamless navigation on the web](https://web.dev/hands-on-portals/)

[A Last Call for QUIC, a giant leap for the Internet](https://blog.cloudflare.com/last-call-for-quic/)

[The Internet](https://explained-from-first-principles.com/internet/)

[Preload, Prefetch And Priorities in Chrome](https://medium.com/reloading/preload-prefetch-and-priorities-in-chrome-776165961bbf)

[WebRTC is now a W3C and IETF standard](https://web.dev/webrtc-standard-announcement/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/l6czhe/webrtc_is_now_a_w3c_and_ietf_standard/)
Web RTC is basically Peer 2 Peer on the web.
Also, it uses UDP instead of TCP so it should be one of the possible lowest latencies on the web, even less than WebSockets (TCP).

## Fonts

[How To Load and Use Custom Fonts with CSS | DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-load-and-use-custom-fonts-with-css)

[Performance tip: You can specify a `text=` value in your font request URL to reduce the size of the font file by up to 90%.](https://www.reddit.com/r/programming/comments/ll6akk/performance_tip_you_can_specify_a_text_value_in/)
> This suggests a corollary for those hosting their own fonts
> Performance tip: You can remove unused glyphs from a font to reduce the size of the font file by up to 90%

[webfonts - Including Google Fonts link or import? - Stack Overflow](https://stackoverflow.com/questions/12316501/including-google-fonts-link-or-import)
> For 90%+ of the cases you likely want the <link> tag. As a rule of thumb, you want to avoid @import rules because they defer the loading of the included resource until the file is fetched...
> If you are concerned about SEO (Search Engine Optimization) and performance, it's good to use the <link> tag because it can preload the font.

Google fonts recomment the following:
```html
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,700;1,400&display=swap">
```
Which basically seems to be:
1. Handshake with both Google Servers, the second one seeting crossorigin*
2. Setting the attribute name to an empty value, like crossorigin or crossorigin="", is the same as anonymous.
3. Anonymous: Request uses CORS headers and credentials flag is set to 'same-origin'.
   There is no exchange of user credentials via cookies, client-side SSL certificates or HTTP authentication, unless destination is the same origin.
4. Load stylesheet on `fonts.googleapis`, which contains multiple `@font-face`, linking to fonts with, for example, `src: url(https://fonts.gstatic.com/s/opensans/v34/memQYaGs126MiZpBA-UFUIcVXSCEkx2cmqvXlWq8tWZ0Pw86hd0Rk8ZkWV0exoMUdjFXmSU_.woff) format('woff');`

[A cartoon intro to DNS over HTTPS - Mozilla Hacks - the Web developer blog](https://hacks.mozilla.org/2018/05/a-cartoon-intro-to-dns-over-https)

[You need neither PWA nor AMP to make your website load fast @ tonsky.me](https://tonsky.me/blog/pwa/)

[mkcert: valid HTTPS certificates for localhost](https://words.filippo.io/mkcert-valid-https-certificates-for-localhost/)

[CORS is Stupid - Kevin Cox](https://kevincox.ca/2024/08/24/cors/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/1f18o5f/cors_is_stupid/)
