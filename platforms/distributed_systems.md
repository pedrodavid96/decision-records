# Distributed Systems

[Distributed Data for Microservices — Event Sourcing vs. Change Data Capture](https://debezium.io/blog/2020/02/10/event-sourcing-vs-cdc/)
Cool intro on Event Driven Systems, covering topics like:
* Event Sourcing
* Journal
  - Replaying
  - Snapshoting
* Change Data Capture (CDC)
* Command Query Responsibility Segregation pattern (CQRS)

[The reactive manifesto](https://www.reactivemanifesto.org/)
Reactive Systems are:
* Responsive
* Resilient
* Elastic
* Message Driven
