# DX (Developer Experience) / Developer Productivity Engineering

[Developer Productivity Engineering | Learning Center](https://gradle.com/learning-center-by-objective/)

[The Importance of a Great Developer Experience](https://medium.com/nick-tune-tech-strategy-blog/the-importance-of-a-great-developer-experience-40567abc0e9a)
The Elements of a Great Developer Experience
* Pleasant Onboarding
* Zero to Production in 1 Day
* Self-service Over Blocking Dependencies
* Red Carpet for Continuous Delivery
* Delightful Local Development Environment
* Learning & Sharing Community
How to Create a Great Developer Experience
* Measure Developer Experience
  - deployment frequency
  - time-to-production for a new service
  - number of tickets developers create for non-self service tasks
  - harder: local development environment satisfaction.
    In those situations, use surveys and speak to developers.
* Invest in Developer Experience
* Set out a Vision

[The Ultimate Guide to Onboarding New Developers: Industry Best Practices](https://codesubmit.io/blog/guide-to-onboarding-developers/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/oiu3g8/the_ultimate_guide_to_onboarding_new_developers/)
