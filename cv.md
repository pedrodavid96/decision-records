# CV / Resume

[20 Questions a Software Engineer Should Ask When Joining a New Team | Thomas Stringer](https://trstringer.com/20-questions-for-new-software-team/)
1. How do I locally build the software?
2. How do I locally test the software?
3. How do I setup my development environment?
4. Where does the source code live?
5. Where is the CI/CD pipeline and how does it work?
6. Where is the product backlog?
7. How does pre-production and production testing work?
8. What is on-call like?
9. Where is the internal documentation?
10. Who on the team is focusing on what?
11. What is the weekly cadence of the team?
12. Who should I contact with “beginner” questions?
13. Who/what drives new features?
14. How does the team primarily communicate?
15. How do we get customer feedback?
16. What are the support agreements for our customers?
17. Where is the public/customer documentation?
18. What are some high-level pain points for the software?
19. What is the focus from stakeholders?
20. What is the release cycle of the software?
[Reddit Discussion](https://www.reddit.com/r/programming/comments/ondjq2/20_questions_a_software_engineer_should_ask_when/)

[A Better Resume for Developers ](https://www.bennorthrop.com/Essays/2021/techrez-a-better-resume-for-tech.php)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/orypo5/building_a_better_resume_for_developers/)
Interactive resume for developers, with a Gantt chart like timeline view.
Ability to filter by technology / skills.

## [CV Compiler](https://cvcompiler.com/blog/facelift-your-it-resume-to-get-more-interviews/?reddit)

By making a few small improvements to your IT resume, you can increase the number of interviews several times. Just trust me: I see dozens of tech resumes every day and many of them lack just some minor upgrades to bring their owners more interviews and job offers.

These upgrades are:

    Focus on your recent projects. Your experience for the past 3-4 years is the most interesting for recruiters. Don’t describe older projects in detail.
    Enrich your resume with numbers and accomplishments. Instead of ‘Was building a web application with [X] and [Y], write ‘Led the development of [X] feature, integrated it across [Z] products, resulting in extra [Y] in revenue’.
    Avoid ‘false accomplishments’. These are your responsibilities, described in the past tense. Until you’ve shown an impact, it’s not meaningful.
    Include the relevant keywords. Read the vacancy carefully, and include in your resume all the necessary skills you possess. Recruiters will match your skills/title with their vacancies, as well as your experience within a particular domain (SaaS, eCommerce, fintech, etc).
    Establish and showcase an online presence. Your activity on GitHub, StackOverflow, HackerRank, etc. shows off your skills and increases your chances of being considered for a particular job. If you have some noteworthy profiles and links, include them in your resume.
    Don’t grade your skills in nebulous percentages and/or categories. Your potential employer will objectively evaluate your skills and knowledge during the technical interview or a test task. As for self-evaluation in a resume, it often leads to misunderstanding between a candidate and a recruiter.
    Check your resume for typos. Even the incorrect spelling of technologies like Jquery or Nginx make your resume look unprofessional.
    Omit the Summary and Objective sections. In 9 cases out of 10, summaries and objectives are not impressive. Instead of wasting time on creating the regular summary, I recommend omitting it.
    Use modern fonts. Forget about Comic Sans and similar fonts for good. Instead, try modern fonts like Palanquin, Merriweather, Lato, and Poppins.

Curious how your IT resume stacks up? Check the score of your resume for free with the CV Compiler. Covering over 30 IT specialties, it gives instant suggestions and compares your resume with the best practices in the industry.

## [Emelia CV](https://twitter.com/ThisIsMissEm/status/1141221911134953472)

[OpenResume - Free Open-source Resume Builder and Parser](https://www.open-resume.com/)
[Show HN: Open-source resume builder and parser | Hacker News](https://news.ycombinator.com/item?id=36470297)

[JSON Resume](https://jsonresume.org/)