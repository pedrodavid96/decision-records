# Chess

## Rules

* White on the right
* Queens lined up together
* White Queen on the light square, Black Queen on the dark square.
* White always goes first

---

### Castle

* King moves 2 squares, and the Rook from that side moves directly to its side
* Prevent from castling:
  - Can't castle while in check
  - Can't castle if the king would castle through a check
  - Can't castle to land in a check (well, duh)
  - Can't castle if there are pieces in between
  - You can't castle if the king has already made a move before
  - You can only castle to a Rook that hasn't moved yet
* Naming: 0-0 King side (2 squares between the 2 pieces), 0-0-0, Queen side (3 squares between the 2 pieces)

### En Passant (French for "in passing")

Introduced at same time the rule for pawns to move 2 squares on their first move.
Sometimes pawns would move through another pawns without giving opportunity to capture which didn't seem fair.

After a pawn moves 2 squares and **only in the next turn** you have the opportunity to capture that pawn as it only had moved 1 square.

## Namings

* Turn

* Column is called a file, a to h, from left to right
* Row is called a Rank, from 1 to 8 starting from White's
  However, customarily referred from perspective

* Square is identified by letter first and number second, e.g.: a1

* a1h8 diagonal

* Piece always begin with a capital letter:
  - King: K
  - Queen: Q
  - Rook: R
  - Bishop: B
  - Knight: N (K already taken)
  - Pawn: No Symbol

* Move identified by Piece, then File, then Rank, e.g.: Ke2
* Pawns are always written as the square they move to

* x for Capture, e.g.: Qxe6 (Queen Captures in e6)
* For pawns, write the current file, then the capture, e.g.: bxa7 (pawn on b captures a7)
* Pawns promoting are denoted by `=<piece>`, usually, Q
* The most complex example, capturing and promoting in the same turn, e.g.: bxa8=Q
* Check is denoted by + at the end, e.g.: Ng6+
* The notion for checkmate is #
