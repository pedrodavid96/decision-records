# Blog

[Rules for Writing Software Tutorials · Refactoring English](https://refactoringenglish.com/chapters/rules-for-software-tutorials/)

[The Bullet Journal | Writing | mary.codes](https://mary.codes/blog/writing/i_write_every_day_but_you_dont_see_it/)

[Jonas Hietala: Why I still blog after 15 years](https://www.jonashietala.se/blog/2024/09/25/why_i_still_blog_after_15_years/)
Incredible and inspiring post ⭐
[Why I still blog after 15 years | Hacker News](https://news.ycombinator.com/item?id=41646531)

## Technology Stack

[Writing a Book with Pandoc, Make, and Vim](https://keleshev.com/my-book-writing-setup/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/g11hd2/writing_a_book_with_pandoc_make_and_vim/)
* pandoc (maybe inside docker to avoid installing a lot of packages)
```bash
$ pandoc book.md -o book.pdf           \
    --table-of-contents                \
    --number-sections                  \
    --pdf-engine=xelatex               \
    --indented-code-classes=javascript \
    --highlight-style=monochrome       \
    -V mainfont="Palatino"             \
    -V documentclass=report            \
    -V papersize=A5                    \
    -V geometry:margin=1in
```
* draw.io
* Makefile
[Typesetting Markdown – Part 1: Build Script](https://dave.autonoma.ca/blog/2019/05/22/typesetting-markdown-part-1/)
This series describes a way to typeset Markdown content using the powerful typesetting engine ConTeXt.

[How I use Pandoc to create programming eBooks](https://scastiel.dev/posts/2021-01-21-how-i-use-pandoc-to-create-my-programming-ebooks/)

[Pandoc templates](https://pandoc.org/MANUAL.html#templates)

[The Idea Of Makefile Blog](https://metin.nextc.org/posts/The_Idea_Of_Makefile_Blog.html)
[Discussion](https://www.reddit.com/r/programming/comments/hgfo9t/the_idea_of_makefile_blog/)
Using `envsubst` or `m4` as a template engine.
[reStructuredText](https://docutils.sourceforge.io/rst.html)
Alternative to Markdown

Example: https://github.com/ozy/makefile-blog/blob/master/makefile

[How To Blog | Jeffrey Paul](https://sneak.berlin/20150717/how-to-blog/)
Simple tech stack example. Cool blog

[Makefile rule for Jekyll drafts](https://blog.gnclmorais.com/makefile-rule-for-jekyll-drafts)

[Blogs away!](https://christianheilmann.com/2005/01/27/blogs-away/)
> Even though I haven’t come around to creating my template yet, I think it is time to start the blog, after all it is so much easier to design it when content is already available.

## Make your software opinionated

[What is opinionated software](https://stackoverflow.com/questions/802050/what-is-opinionated-software)
[The rise of opinionated Software](https://medium.com/@stueccles/the-rise-of-opinionated-software-ca1ba0140d5b)

https://twitter.com/gatsbytemplates/status/1250325295011250177
Code on the side of the content

[Stop Spreading Crap at My $HOME](https://hauleth.dev/post/stop-spreading-crap-at-my-home/)
