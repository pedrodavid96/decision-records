# Machine Learning

[PRML algorithms implemented in Python](https://github.com/ctgk/PRML)

> Jupyter notebooks with Python examples for reproducing examples from each chapter of Christopher Bishop's “Pattern Recognition and Machine Learning” textbook (also available for free in link above)

> This is awesome!  To be as explicit as possible: working through the examples and problems in this text is probably a strict superset of the training received in the grad-level intro to classical ML sequence at any top-caliber university.
[Source](https://twitter.com/IAmSamFin/status/1234293337559044096?s=09)

[CppCon 2018:Nicolas Fleury & Mathieu Nayrolles “Better C++ using Machine Learning on Large Projects”](https://www.youtube.com/watch?v=QDvic0QNtOY)
Awesome awesome awesome talk 🥇⭐⭐⭐

[What is Machine Learning ? A.I., Models, Algorithm and Learning Explained](https://www.youtube.com/watch?v=rADYARpj-V8)

[(1) How I would learn Machine Learning (if I could start over) - YouTube](https://www.youtube.com/watch?v=wtolixa9XTg)

[GitHub - eugeneyan/open-llms: 🤖 A list of open LLMs available for commercial use.](https://github.com/eugeneyan/open-llms)