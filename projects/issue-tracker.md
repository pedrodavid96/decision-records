# Issue Tracker

## Reference

See [Linear](https://linear.app/)

Keyboard efficient.
Modern design.
Custom workflows through API.

[Scrumpy — A Beautiful Project Management Tool for Agile Teams](https://scrumpy.io/)

[Plane - The open source project management tool](https://plane.so/)

Jira
Clickup
Asana
Monday

## Distributed bug tracker integrated with Git

[git-bug: Distributed bug tracker embedded in git : programming](https://www.reddit.com/r/programming/comments/981rtw/gitbug_distributed_bug_tracker_embedded_in_git/)
[Git-bug: Distributed, offline-first bug tracker embedded in Git | Hacker News](https://news.ycombinator.com/item?id=33730417)
[git-bug/git-bug: Distributed, offline-first bug tracker embedded in git, with bridges](https://github.com/git-bug/git-bug)
[Tracking SQLite Database Changes in Git | Garrit's Notes](https://garrit.xyz/posts/2023-11-01-tracking-sqlite-database-changes-in-git)
