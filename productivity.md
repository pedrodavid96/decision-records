# Productivity

## Productivity tools

[Alfred - Productivity App for macOS](https://www.alfredapp.com/)
[Bento: The Do Less To-do List](https://www.bentomethod.com/)
[Sunsama - The daily planner for busy professionals](https://www.sunsama.com/)
[Otter.ai - Voice Meeting Notes & Real-time Transcription](https://otter.ai/)

## Misc

[How to set priorities as a software engineer? : programming](https://www.reddit.com/r/programming/comments/1e0r9k7/how_to_set_priorities_as_a_software_engineer/)

### This 1 sentence can cure procrastination
> eric@ericpartaker.com (Newsletter)
> "I choose to start for just 5 minutes."
Usually this would be phrased like "I have to finish this presentation".
1. You almost always start self-talk with "I have" - you feel like you're being forced to do something you don't want to, which creates resistance and procrastination.
   Choose choose so it feels more under your control.
2. You almost always aim to "finish". This creates feelings of anxiety and overwhelm by thinking about all the things we need to do to "finish".
   Instead, just focus on continually starting and finishing will take care of itself.
3. No matter the dread or complexity associated with a piece of work we can easily start working on it for just 5 minutes.
   > And here's the magical part - I never know when the 5 minutes are up. They go by in a flash and suddenly, without realizing it, I've been working on a task for 20 minutes, 30 minutes, or an hour and more.

[Crafting To-Do Lists That Spark Productivity and Joy](https://hulry.com/productive-to-do-list/)
[How I Finally Made Sense of Todoist’s Priority Levels](https://hulry.com/todoist-priority-levels-moscow/)
[The Exit Checklist](https://hulry.com/firesides/exit-checklist/)
[The Weekend Bridge](https://hulry.com/firesides/weekend-bridge/)

[Don’t drown in email! How to use Gmail more efficiently.](https://klinger.io/post/71640845938/dont-drown-in-email-how-to-use-gmail-more)

[3 Ways to Trick Your Brain Into Doing Hard Work](https://www.youtube.com/watch?v=o22pjf9obA0)
1. Start Incredibly Small
2. Allow yourself to create garbage
3. Jot things down in non-ideal places

[Tiny Changes, Remarkable Results - Atomic Habits by James Clear](https://www.youtube.com/watch?v=YT7tQzmGRLA)
- [01:20](https://www.youtube.com/watch?v=YT7tQzmGRLA&t=80s) - Why does 1% matter?

  Compounding - Bad habits lead to bad habits and good habits to good habits. They multiply themselves.
  "Plateau of latent potential"
  Outcomes are delayed - Results don't follow the linear progress we expect

- [03:08](https://www.youtube.com/watch?v=YT7tQzmGRLA&t=188s) - The Importance of Systems Rather Than Goals

  Screw goals and focus on systems, problems with goal settings:

  1. Winners and losers have the same goals
     e.g.: every candidate has the goal of getting the job
     It can't be the goal that differentiates people

  2. Achieving a goal is only momentary change
     Cleaning the room is a temporary goal, we need to change
     our systems so the room doesn't get as messy again

  3. Goals restrict our happiness
     We put off our happiness until we reach our goals

  4. Goals are at odds with long term progress
     > The purpose of **setting goals** is to **win the game**.
     > The purpose of **building systems** is to continue **playing the game**.

     > The score takes care of itself.

- [05:10](https://www.youtube.com/watch?v=YT7tQzmGRLA&t=310s) - Identity Change is the North Star of Habit Change

- [06:30](https://www.youtube.com/watch?v=YT7tQzmGRLA&t=390s) - How to Build A Habit

  Cue -> Craving -> Response -> Reward

- [06:50](https://www.youtube.com/watch?v=YT7tQzmGRLA&t=410s) - The 4 Laws of Behaviour Change

  1. Make it obvious
     Fewer steps between you and good behaviors.
     More steps between you and bad behaviors.

  2. Make it attractive
     Ali says a "reward" for going to the gym was listening to fantasy audio books.

  3. Make it easy
     > Friction is the most powerfull force in the entire universe
     Ali puts guitar and keyboard next to him in his computer so they're his first "procrastination".

  4. Make it (immediately) satisfying

[How I Manage to Work a 5-Hour Workday](https://forge.medium.com/how-i-manage-to-work-a-5-hour-workday-c633370dcfa3)
1. First, I tracked how I spent my time (spoiler: I was a mess)
2. I prioritized weekly time blocking
3. I started “Mise en Place Sundays”
   A kitchen term that refers to gathering your ingredients in one place so your prep work is done before the actual cooking begins.
4. I began making daily hotlists -  three things I need to get done
5. I employed the Hemingway method
   I stop work in the “middle” of things so I can easily resume my flow during my next time block. I always have something to return to that will excite me.
   (ps.: sounds similar to an advice I saw lately to finish a lot of personal projects)
6. I automated whatever I can
7. I made my daily “thinking and planning walks” non-negotiable

[Every second matters - the hidden costs of unoptimized developer workflows]()
![is it worth the time?](is_it_worth_the_time.png)
(source: https://xkcd.com/1205/)
Reasons for losing time in the engineering workflow
- Insufficient test-automation
- Vulnerability scans before commit vs. after
- Time spend waiting for container build times
- Static vs. Dynamic environment setups
- Scripting Hell

[Stop procrastinating: a quick guide for gamers](https://selfrespec.com/gamer-procrastination/)
Main Takeaways:
- Procrastinators aren’t inherently lazy
- Procrastination is triggered by negative affect (negative emotions)
- Games are a gamer’s procrastination mechanism of choice, but:
- Links between hours spent gaming and procrastination are weak at best
- To reduce procrastination, you need to lower your high levels of negative affect
- Do this by intentionally meditating, using a gratitude journal, listening to music
- You could even try to play video games for short periods before starting work
- You should also use implementation intentions, which involves:
- Writing down what you’re going to do, when you’re going to do it, and how you’re going to approach it.
[Discussion](https://www.reddit.com/r/Games/comments/hw0r7a/im_a_phd_researcher_who_studies_games_and/)

[How to Achieve Your Most Ambitious Goals | Stephen Duneier | TEDxTucson](https://www.youtube.com/watch?v=TQMbvJNRpLE)
Great talk 🥇⭐
knitting - yarn bombs

[The First Question to Ask Yourself When You Sit Down to Work](https://forge.medium.com/the-first-question-to-ask-yourself-when-you-sit-down-to-work-69366165d8a)
* “Power out” your procrastination
  Pretend the power is going to go out soon
* Reconfigure the morning
  We often "ease into our workdays" (checking email, check few small doable items in the to-do list, etc...)
  This "light procrastination" is often inefficient.
* Create urgency

[10 Habits That Are Having a Strong, Positive Impact on My Life](https://medium.com/mind-cafe/habits-strong-positive-impact-life-840655a4826a)
1. Do What Others Don’t Want to Do
2. Write Down a Blacklist of the Habits that Are Slowing You Down
3. Focus on the Circle of Influence
4. Work in Micro-shifts
5. Don’t Make Your Bed in the Morning
6. Show Genuine Curiosity in Others by Using Follow-up Questions
7. Avoid Meetings or Calls with No Clear Agenda or End Time
8. Eliminate Small Decisions
9. Practice Walking Meditation
10. Ask Someone to Hide Your Phone (or Hide It Yourself)

Ali Abdaal [How To Be More Productive ft. My Productivity Coach - Chris Sparks](https://www.youtube.com/watch?v=LPEzVpCU39o)
Incredible episode with Poker pro, incredible view of life
Ali Abdaal [How To Approach Life in 2021 ft. Khe Hy](https://www.youtube.com/watch?v=49Bbp60LSJU)

[Working fewer hours and getting more done](https://www.uptheretheylove.com/blog/production/working-fewer-hours-and-getting-more-done)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/l5d8hy/working_fewer_hours_and_getting_more_done/)
* Clearing the mornings
* Explicit, clear and detailed schedule
[The illusion of productivity](https://nesslabs.com/illusion-of-productivity)
> we would rather fill the time with activities—any kind of activity—than to take the risk of finding ourselves alone with our thoughts.
> And our society encourages this behavior: being idle or having too much free time is often considered a sign of laziness.
Getting off the hamster wheel:
1. Change your perspective.
   First, stop saying “I don’t have time”. Instead, say “It’s not a priority.”
   There is enough time in a day to do the important things.
2. Less doing, more achieving.
   Don't measure productivity in terms of how many things you get done.
   Shift focus from tasks to outcomes.
3. Do a busyness audit.
   Track your time and record what you spend your time on.
4. Start saying no
5. Make peace with inaction
   To help you get comfortable with doing nothing, schedule time with yourself for dedicated downtime.
   Reflect or take a short walk.
[Make Time: How to Focus on What Matters Every Day](https://www.goodreads.com/book/show/37880811-make-time)

Slow Growth InstaStories ideas on ideas
> Record my voice, I'll just babble everything out and structure it afterwards.
> I like to go to the bookstore,  feels like I'm picking thousand of new ideas in paper
> Spaghetti wall ideas on post it notes
> Put on some lofi, open my sketchbook, and start mindlessly doodling. It rides fear of starting
> Walk in the garden, helps to stop overthinking and also stimulates your creativity!
> Voice notes! Cause im a musician so i'll hum something to a voice note
> Mind mapping
> I write the topic in the middle of the page and spread thoughts from there.
> Journal. I write them down until I am able to execute each point.

[Plan Your Best Year Ever! My 7 Step Goal Setting Process](https://www.youtube.com/watch?v=uL5r8QnDuWE)
1. Select your main area for growth
   Health | Mindset | Career | Relationships | Wealth | Spirituality | Legacy
   Idea - Score them from 0 - 10 and focus on the lowest score?
   This doesn't mean you don't have goals for other areas, it just means it will be the main goal and serves as source of **focus**
2. Define your *SMART* annual goal for that main are of growth
   *SMART* - Specific, Measurable, Achievable, Relevant and Time-Bound
3. Write it down
4. Define your boss move
   Something small that will let you convince yourself to stay in the right path
   e.g.: From "Only use credit card for emergency" to "Put your credit card in the freezer"
5. Brain Storm the rest of (SMART) goals for other areas that contribute to the main goal
   Here "Relevant" is much more important as the main goal would be, by default, relevant
6. Quarterly milestones
7. Weekly and Monthly Reviews? (Weekly should be very quick)

[How I Remember Everything I Read](https://www.youtube.com/watch?v=AjoxkxM_I5g)
[How Ali Abdaal Uses Tech to Remember Everything He Reads | by Eva Keiffenheim, MSc | SkillUp Ed | Medium](https://medium.com/skilluped/how-ali-abdaal-uses-tech-to-remember-everything-he-reads-6c1878fd77f3)
1. Passive Reading - The way most people read
2. Highlighting - Take the next step after passive reading
3. Systematic Highlight Revision - Make your highlights work for you
4. A Central Highlight Storing Location - Find your holy storage palace
5. Summarizing Key Principles with Elaborative Rehearsal - Unlock the power of elaborative rehearsal
6. Writing Literary Summaries - Become an expert for your content
7. Organize Your Life With Evergreen Notes - Connecting the dots to a bigger picture

[How This Google Calendar Hack Helps Me Save More Than 16% of Meeting Time Every Month | Avoma Blog](https://www.avoma.com/blog/google-calendar-hack-save-meeting-time)
How This Google Calendar Hack Helps Me Save More Than 16% of Meeting Time Every Month
* change your default meeting duration to 15 or 30 minutes using Google Calendar’s settings.
* Enable “Speedy meetings” - 30m -> 25m, 60m -> 50m, you just saved 16% of your meeting time.

[21 Tiny Habits to Improve Your Life in 2021 Effortlessly | by Mathias Barra | The Ascent | Dec, 2020 | Medium](https://medium.com/the-ascent/21-tiny-habits-to-improve-your-life-in-2021-without-much-effort-2ab56e680947)
1. Write three things you’re grateful for.
2. Refuse once a week.
3. Exercise for a minute when you wake up.
4. Invest in experiences.
5. Organize your home.
6. Throw things away.
7. Manage your money.
8. Stand up every hour.
9. Follow this simple proverb.
10. Ask open-ended questions.
11. Turn off notifications.
12. Prepare your clothes.
13. Watch the news less.
14. Drink more water.
15. Discover one new thing a month.
16. Make your bed.
17. Spend a minute a day with yourself.
18. Ask yourself whether what you’re doing is worth it.
19. If a task takes 2 minutes, do it right away.
20. Take care of your posture.
21. Read for 15 minutes a day.

[Why I'm able to study 70+ hours a week and not burn out (how to stay efficient)](https://www.youtube.com/watch?v=FARXrLsBNJY)
- Sleep - Book, Why we Sleep - Matthew Walker PhD
- Exercise
- Environment
- Mindset

[6 life lessons in under 60 seconds](https://www.youtube.com/watch?v=rkRN_Z2UKGg)
1. Don't press the try harder button - build a system so you don't need to try so hard
2. Energizers vs drainers
3. Choose to be satisfied - don't beat yourself about not being productive enough
4. Planning and doing are two different modes - don't try to do both together
5. Wake up at the same time everyday
6. The pain of rejection is way better than the pain of regret

[Productivity myths that are wasting your time. - YouTube](https://www.youtube.com/watch?v=qc4GU_wHdqw)
1. I should have been more productive
   You can just choose to be satisfied regarding what you've done (see point 3 in the previous snippet)
2. Consistency is more important than intensity
   Intensity is completely fine in some occasions and even helpful, you can't simply relly on it in a sustainable way
3. Motivation leads to action
   The more you do something and the more small successes, the more you will feel motivated
4. Hustle culture is bad
5. Productivity is not self care
   Book: Working hard is hardly working
   It can be, but it can't become an excuse to not do things that you must do
6. Goals should be S.M.A.R.T.
   You should add "Proximal"
   Favor Intrinsic against Extrinsical (e.g.: getting better at something is completely in your hands, winning a competition is not fully)
7. You can do everything
   Book: Four thousand weeks
8. I don't have time
   I'm lying to myself
   I'm taking away control from myself
   Say instead "It's not enough of a priority"
9. Productivity about is doing more
   It's actually "doing more of the right things".
   Formula:
   (meaningful output / time) * fun(enjoy the journey)
10. You need large amounts of time to do good work
   Book: Someday is the day
   Be more intentional in the little moments of the day
11. Your environment has to be perfect
   They help but their not requirements, you can still get work done
   Stoics call: Preferred indifferents
12. The small stuff doesn't matter
   Small stuff actually helps - e.g.: typing faster so you don't need to defer notes to later on and so that there's less friction between having a thought and typing it
13. Reading too many books is useless
   2 modes of reading:
   Exploration:
   - Listening at 3x speed to get the overal picture and core message
   Exploitation:
   - Actively trying to apply the learnings

[Taking Book Notes (How to Start) - YouTube](https://www.youtube.com/watch?v=fES9ZrLXY9s)
Ideas from "Alex & Books"
I. The method:
1. Capture:
   Highlight important information
   Create your own table of contents (inside of the cover)
   If it's super important put a "star" on it
2. After finishing the books, **distill** its information
   Write the lessons in your own words (back of the book)
   When reviewing later on look straight at those lessons.
   Ask yourself: What did I want to apply that I haven't applied?
3. Add starting and ending date to front and back of the cover, respectively
II. Should you avoid notetacking during active reading?
* if you write it, it breaks the flow of reading
* often just mark the page / sentences and then distill the infromation in the end of the chapter
III. How to take your physical notes to digital
1. Let a few days to cool off after finishing a book
2. Revisit the back cover
3. Rule of Three:
   - 3 Lessons
   - 3 Pieces of actual advice
> What would this look like if it was easy?
> - Tim Ferriss
IV. Why most people are using the wrong notes app
Move to the next app when you need to promote to something better

[How to build your Bullet Journal (& Second Brain)](https://youtu.be/FeZ4rXHEzrU)
> I like to think of it as a mindfulness practice that is disguised as a productivity system.
Tasks Events and Notes
Let it happen organically
Collections - Collect related information
   e.g.: Notes about a book
   Start collection in the next blank page
   Other examples: Tracking water intake and other habit trackers
Delay between taking and making Notes
   Taking notes is just highlighting, he highlights a lot
Articles -> Matter (Pocket / Instappaper)
Websites -> Roam research
Photos -> Apple Photos -> Apple Notes / Photos as Notes
Voice memos -> Otter -> A lot of thinking happens when he is walking, at some point the rambling starts to be something coherent -> Readwise
"Inbox" on roam -> to be reviewed and move to the right place -> processing the inbox counts as "work/writing time"
How do physical notes go to digital?
Daily / Weekly / Monthly rituals - go back and reflect on whats written -> remove everything that doesn't add any value
during the day it's just capturing and during the evening, figure out where things have to live, often notebook is enough, other times trash, and the ones that "deserve to" are marked as "migrated" and go to external systems, e.g.: Google Calendar, Roam Research

[The biggest habit building mistake - YouTube](https://www.youtube.com/watch?v=B-d79DtMTiE)
Maybe you've done it successfully a few days in a row.
Mounting pressure.
Suddenly you have a bad day, and it feels like the whole thing comes crashing down - the house of cards is finally coming down - you fallback to the thing you were avoiding and binge it (forever?)
**Myth of vertical progress**
    1. People fantasize that if they get to a streak of x days, they're life suddenly changes.
    2. No habit that you currently have works like this.
    3. Every single successful day that you had, you were building progress, you were strengthening the muscle of that lifestyle.
    4. (could possibly move below 1.) People believe that all we need to do is hit a certain amount of days and then everything makes sense
    5. Even a less dramatic view - oh, if I just hit a certain amount of days things will become easier, change from unnatural to natural. Officially locked in as a habit and I don't need to try really hard anymore, it's just automatic.
    6. **Not only this is psychological inaccurate, it adds immense pressure to hit this magical day**
    7. If we view this as a Jenga tower, we're stacking days on top of days, which each block you add it becomes more unstable. The pressure of messing up becomes bigger and bigger... If you mess up after 30 you think the flood gates are open and you knew you were never able to do it and then binge on the bad habit
Even your bad habits you do them because you get something out of it
The problem is that over time you learn that this road isn't so good. The road that might have existed since early in your life isn't the one that you now desire. The issue is that this road will take some work.
Every single day that you take this new road the old one starts to grow over (some weeds, overgrowth...), and the new one will become more comfortable and more likely that we use this road in the future because we're telling our brain that this road leads to what you want.
This new lifestyle that you've been working on and occasionally try (every January 1st) its still there, it has always been there, but the degree to which its developed is solely based on how much you've been using it.
Just because you have a bad day and choose the old road one day, doesn't mean that the other doesn't exist anyway.
**Relinquish yourself from the pressure of vertical progress**
Try to find ways to make building this new road as easy and efficient as possible.
Personal note: You can choose to go back to the old road but just **be mindful that going back to the old habit is not effortless**, you're making an active effort to cover up the new habit you've been building.

[John Grisham's Daily Writing Routine ✍️ - YouTube](https://www.youtube.com/watch?v=P5S299Gy03M)
* establish a routine
* setting baily goals
* eliminate bistractions

[Here's what a normal week looks like 👀 / Matt Ragland on X](https://twitter.com/mattragland/status/1700492092034760777)
I stick to core hours at work between 10a-4p every day...
My best work days are the ones where I go straight from a morning workout to the office. I stay dialed in and get a lot done, which carries to the rest of the week.
I avoid work on the weekends
My wife and I set our workout times and stick to them. If I want to go to Jiu-Jitsu, then I need to go at 6am. If I'm tired, that's fine, but I don't get to go in the evening instead. I have my time and it's up to me to go.
We try to bring friends to our activities ... I call this "happy stacking" because I'm doing multiple things I enjoy at the same time.
It's hard not to want to do MORE. I honestly want to work more than I do, there's so much energy and momentum in the business. But I also want to spend more time with my kids, and hell I want to train more than I do (CrossFit and BJJ).
That's the biggest challenge
- Fitting in everything I want to do
- Realizing I can't do everything I want to do
- Not comparing to other people's social posts
- Remembering I'm on my own path and timing

> "The simplest way to clarify your thinking is to write a full page about whatever you are dealing with and then delete everything except the 1-2 sentences that explain it best."
  Mar 7, 2024, 5:23 PM - James Clear Newsletter

> "The reason people get good ideas in the shower is because it's the only time during the day when most people are away from screens long enough to think clearly. The lesson is not to take more showers, but rather to make more time to think."
  Mar 7, 2024, 5:23 PM - James Clear Newsletter

> Are you dealing with a leak or a puddle?
  * Some problems are like muddy puddles.
    The way to clear a muddy puddle is to leave it alone.
    The more you mess with it, the muddier it becomes. Many of the problems I dream up when I'm overthinking or   worrying or ruminating fall into this category. Is life really falling apart or am I just in a sour mood? Is this as hard as I'm making it or do I just need to go workout? Drink some water. Go for a walk. Get some sleep.
    Go do something else and give the puddle time to turn clear.
  * Other problems are like a leaky ceiling.
    Ignore a small leak and it will always widen. Relationship tension that goes unaddressed. Overspending that becomes a habit. One missed workout drifting into months of inactivity. Some problems multiply when left unattended.
    You need to intervene now.

  Paraphrased - Thu, Mar 28, 5:51 PM - James Clear Newsletter

> "Figure out what you're good at without trying, then try."
  https://twitter.com/isabelunraveled/status/1727399000926785847

> "Is that assumed or confirmed?"
  Thu, Mar 28, 5:51 PM - James Clear Newsletter

> "My friend, who sometimes shares his writing with me, once said my feedback always falls into three categories:
  1. Make it shorter.
  2. Make it more appealing.
  3. How could it apply to more people?"
  Mar 21, 2024, 7:46 PM - James Clear Newsletter

> "Flexibility alone is not a great strategy, but the lack of it can ruin one."
  Thu, Mar 14, 5:18 PM - James Clear Newsletter

> "Most people, when asked if they are the same person they were 10 years ago, will say no — but we have a much harder time seeing potential for change in the future… Gilbert puts it simply: 'Human beings are works in progress that mistakenly think they’re finished.'"
  https://hbr.org/2020/08/take-ownership-of-your-future-self


[7 Life Lessons I Know at 30 But Wish I Knew at 20](https://youtu.be/B4U7vNtXBTI?si=dchqVRSlz57L3DhL)
* Have a bias towards action
* Remove passive barriers / friction
* Design your life intentionally
...

[5 Small Changes to Supercharge Your Focus](https://youtu.be/3bamBYQS8io?si=LW5BNyPzRUUJaKSZ)
* The ready to resume plan - aka Hemingway Bridge
* The 40hz tactic (meh...)
* The 10 minute De-Clutter - Study says that chronic procrastinators report too much clutter (doesn't go into reasons why but it should be pretty obvious)
  Before starting a focus session, take 10 minutes to de-clutter your workspace
* The 90-20 Rule: Ultradian Performance Rythm. For every 90 minutes of work take 15-30 minute break.
  If you start feeling tired at 1 hour (60 minutes) take ~10 minutes of rest
* Single tasking - attention residue - The calendar color coding part didn't seem that useful...

[Why You’re Always Distracted - 5 Mistakes Ruining Your Focus](https://youtu.be/5Rqiba5mqLk?si=41x04G2JjzUNLBiz)
* Not having a plan
  Social Media platforms are the clear example of this.
  We're outsourcing how we're spending our time to developers / algorithms
  If you're asking why you're distracted ask yourself:
  "what is in your calendar and what were you intending to do with your time?"
* Ignoring how you feel
  What emotion am I avoiding? Boredoom, anxiety, self doubt? Fear?
  You're often just avoiding one of these "bad feelings"
  FBW - Fast, Bad, Wrong - If that's the goal, I'm pretty comfident I can do that
  Task may also be really really boring - What would this look like if it  was fun?
* Attention resideu (again) - Do one thing at the time - Turn off notifications - phone face down
* The Draugiem Group used DeskTime to analyze work habits.
  Elite workers took more breaks, working intensily for an average of 52 minutes and taking off 17 minute breaks.
* You're not hitting the golden trio - Sleep, Exercise, Diet
  > There's no such thing as a loser who wakes up at 5AM and does a workout.
    Sahil Bloom

[Stop Wasting Time - 11 Tools to Double Your Focus](https://youtu.be/uYTEFWVPx_8?si=9QNzIl795bhEnma-)
* Group 1: Time management
  - Forest - Pomodoro & Gamification by seeing a visual reprentation of focus sessions
  - Time trackers (manual or automatic): Google Doc, Toggl, Rize
* Group 2: Combat distractions
  - One Sec (https://regainapp.ai/, https://www.screenzen.co/)
  - https://www.opal.so/ (app limits / limit by time of day)
  - Using grayscale on "catchy" apps - e.g.: instagram, youtube, tiktok
  - Focus mode
* Group 3: Make work fun
  - Background music
  - Writing on ToDo List and ticking them off
  - Do it with other people

### read on JamesClear.com | June 6, 2024
I. "If you're having trouble sticking to a new habit, try a smaller version until it becomes automatic. Do less than you're capable of, but do it more consistently than you have before."
​II. "It's remarkable how often the real problem is not what happened, but how it was communicated."
III. "There are at least 4 types of wealth:
      1. Financial wealth (money)
      2. Social wealth (status)
      3. Time wealth (freedom)
      4. Physical wealth (health)
      Be wary of jobs that lure you in with 1 and 2, but rob you of 3 and 4."

[Deep Dive With Ali Abdaal: Cal Newport: The Secrets of Slow Productivity](https://youtu.be/oyI-HO7moKc?si=1cpobatKBLHAw3zV)
Setting boundaries and managing distractions:
* Have a budget / quotas for specific tasks (that you're asked to do regularly)
  * I can only do 5 of these in the week and I've already done those
    They can only compain your quota is wrong, they aren't going to do it
  * Your boss comes to you to take their stress away so they ask you
    to do it right away since they want their stress to go away right away
  * If you're clear about what is in your plate and when you can actually deal
    with the task most often than not it is enough as they see you're organized

### The counter-intuitive power of failure | Eric Partaker Newsletter

1. Failure is a necessary step towards growth and innovation​

   > "I have not failed. I've just found 10,000 ways that won't work."​
   Thomas Edison

2. ​Failure builds resilience and courage​

3. ​Failure leads to personal growth and self-discovery​

   > ​"It is impossible to live without failing at something, unless you live so cautiously that you might as well not have lived at all - in which case, you fail by default."​
   J.K. Rowling

### The Magic of Friction Reduction 🙏 | Ali Abdaal Newsletter

> “Reducing the friction associated with good behaviors makes them easier to perform and increasing the friction associated with bad behaviors makes them harder to do.”
James Clear Atomic Habits

Examples:
1. lay out our running shoes the night before, plan a route, and have our running clothes ready to go
2. leaving a yoga mat on the floor next to my bed. Now, every morning when I wake up, I see the yoga mat and it’s a reminder to do some stretches

### 3 Meetings 99% of People Don't Schedule, But Absolutely Need | Eric Partaker Newsletter

1. The Shutdown Routine

   **Next action:**
   Create an appointment in your calendar named "Shutdown (See description)".
   In the meeting description paste in the following checklist (example):

   1. Complete final email review
   2. Update to-do list with any notes from the day
   3. Review next day on calendar
   4. Choose your top 3 objectives for your day
   5. Schedule those objectives as appointments with yourself, in the white space of your calendar.

2. Always Creative Before Reactive

   **Next action:**
   Schedule an appointment for the first hour of your workday named "Creative, before reactive (See description)".
   In the meeting description paste in this checklist:

   1. Shut-off phone
   2. Close inbox and any desktop apps like Slack
   3. Close down any browser tabs
   4. Work on your number 1 priority for the day, distraction-free, for 60 minutes (and if you can manage 90 minutes you score bonus points!)

3. The Weekly Review

   **Next action:**
   Schedule a 60 minute appointment on either a Friday (or a Sunday if you prefer) named "Weekly Review (See description)".
   In the meeting description paste in this checklist:

   1. Review the week just past
   2. Review the next 2 weeks ahead
   3. Revisit your goals
   4. Revisit your to-do list
   5. Choose your top 3 outcomes for the week ahead
   6. Schedule the time to work against those outcomes as appointments in your calendar


### This will make you super productive | Eric Partaker Newsletter

1. Create an Ideal Week​
​   Create your should be doing list - eg, gym, deep work/focus time, date nights, etc.
   **Schedule everything!** If it's not scheduled, there's little chance it's going to get done.
   Color-code similar activities to get a sense of where your time is going and any needed rebalancing.

2. ​Install the 3 Power Routines​ (Shutdown | Creative before Reactive | Weekly Review)

3. Set Clear Goals & KPIs​
​   Every quarter (or month if you prefer) identify your No. 1 goal. Ask yourself:
   "If everything were to stay exactly as it is now, where would change have the greatest impact?"​
   1) Map out your next 5 moves to make progress against your goal.
   2) Ensure the work to complete them is scheduled in your calendar.
   3) Track Key Performance Indicator(s) against your goal.
   4) I track minutes worked weekly against my top goal.

4. Optimize your environment​
   1) Create a designated workspace
   2) Disable notifications on your phone and PC
   3) Block out noise with headphones if needed

5. Practice my "3S Rule"​
   Respect yourself as much as you respect others.
   1) Schedule your work as self-appointments
   2) Show-up for them as you would with someone else
   3) Single-task and only work on the appointment topic

6. Swap negative self-talk with positive self-talk
   Already explained elsewhere.
   Favor "I choose to start (for just 5 minutes)"
   instead of e.g.: "I have to finish by..."

7. Practice the 80/20 rule
   Rapidly create that imperfect "v1" draft.
   Try putting in just 20% of the effort for 80% of the result.
   Many times you may find that's in fact good enough.
   And done is better than perfect.

8. Prioritize self-care
   Treat yourself like an athlete.
   Protect your sleep and practice a digital sunset, turning devices off 1 hour before bed.
   Eat a nutritious diet based on whole foods. If it has an ingredient list, probably best to avoid it.
   Don't stay seated. This includes daily exercise and frequent breaks. Stand up. Move often.

[James Shore: A Useful Productivity Measure?](https://www.jamesshore.com/v2/blog/2024/a-useful-productivity-measure)

[Managing the Burnout Burndown - Anjuan Simmons - NDC Sydney 2024 - YouTube](https://www.youtube.com/watch?v=b14Q8Rzz5Yg)
Tips for saying no:
* Workload
  I have a lot of things in my plate right now (- I'm at capacity)
* Focus
  I commited to doing X so I need to focus on that
* Impact
  This will push out the other things I've to do
* Castle (from "Chess")
  I can't do this, but I can do that
* Polish
  I can't do this at the level it deserves

[Shallow Work rituals | Cal Newport](https://youtu.be/2gmonGUL2VM?si=2g3T8JTVzhk-J6sN)
* Schedule "shutdown" rituals to process shallow work and get it out of your mind
  e.g.: Schedule time after Zoom meeting and **walk** around the building processing
        your notes and future actions needed
* Bundle together physical shallow work (get something from store + go send a letter, etc...)

[How To Get Back Into A Game, According to Science](https://youtu.be/k2HrRSWHYJc?si=JdRhUljHl1tq2ZiE)
Hemingway Bridge - Suggestion of using voice recordings
Seeking positive feedback online (which also taps into the sense of community)
2 minutes rule from Atomic Habits


[You're Destroying Your Mind - How to Control Dopamine - YouTube](https://www.youtube.com/watch?v=tjjqyiHczcc)
## Part 1 - Understanding dopamine
* You have a base level of dopamine
* Cue's (always) raises a spike and depending on the outcome you either
  * Reward prediction error -> Increase in dopamine release which feels good
  * Correct prediction -> No dopamine release
  * Absent Reward prediction error -> decrease in dopamine release
## Part 2 - Control your dopamine
* Dopamine Detox - Detox from dopamine inducing activities (usually addictive activities), e.g:
  Suggary food, Video Games, etc...
* If you use those activities too often you actually increase your tonic (base) dopamine level
  This means you need an "exponential" amount of that activity to actually have a spike on
  dopamine that actually feels good
* The idea of the dopamine detox is to reset your "baseline levels of enjoyment"
## Part 3 - 4 actionable strategies to control your dopamine
* Pain before pleasure
  If I'm about to do a dopamine inducing activity I want to "pay for it" before.
  (by going to the gym, for a walk...)
  On the days I just play all day it doesn't feel that great, by going to the gym and using it as a
  "reward" it actually feels better.
* The rule of avoidance
  Cultivate moments when you're not doing any high dopamine activity
  Takes around 4 weeks to reset dopamine levels
  Avoid triggers (things that takes you to do the high dopamine activity)
* The Rule of Barriers
  (and personal rules)
  Chronological - limit time on apps, video-games, etc...
  Geographical - not buying chocolate
  Categorical - you no longer will drink sugary drinks
* The Rule of Boredom
  There's always something you could be doing (e.g.: you can always reach to your phone and "do something").
  If you go for a walk in the park or driving -> tell yourself the goal is to be bored,
  so don't try to listen to a podcast or something.
  Default Mode Network - "Day Dreaming" - A lot of creative ideas

## Sharing your work

Why you don’t have to be an expert to share your experience (team@aliabdaal.com July 5th 2024)

> I like to call it the rule of 10%.
> Imagine that you only need to be a few steps ahead of another person, roughly ten percent.
> Here’s a way to think about it. You didn't wait until you could pop wheelies and perform jumps to teach your younger sister or friend while you were learning to ride a bike, right?

> The most successful YouTubers are also generally the ones that are continuously learning, curious, and constantly trying to improve—and simply taking you and everyone else with them along the way.
  ...
  They don't present themselves as having all the solutions because if they did, they would come off like gurus, which few people like anyway.
  Instead, along the road, they discuss their path, their experiments, and their "aha" moments.

1. Adopting the 10% Rule
2. Recording Your Method
3. Stress value rather than perfection
4. Focus on your audience, not yourself

> It lets us perceive our errors as chances for development rather than as failures.

## Productivity Killers

Meet your Chief Productivity Killer (it’s hiding in plain sight)
eric@ericpartaker.com July 28, 2024

It's name is "Meetings"

1. Limit Recurring Meetings​
2. Implement No-Meetings Fridays
3. Require an Agenda
4. Keep It Small​
5. Watch the Clock
6. End with Action
7. Use Tech Instead

## How to set better priorities (when your plate is too full)

eric@ericpartaker.com August, 18, 2024

Step 1: Define Your Goals
• State your primary goal: What is the one thing that will make the most impact?
• Break it down: Dissect this goal into smaller, actionable subsets.
• Execute with focus: Every decision you make, and every action you take, should bring you closer to this goal.

Step 2: Choose Your Tools

1. Eisenhower Matrix: Helps you focus on what’s urgent and important. Delegate or delete the rest.
2. Oliver Burkeman's 3-3-3 Method: Simplify your day by breaking it down into three blocks: three hours of deep work, three urgent to-dos, and three maintenance tasks.
3. Time Blocking: Assign specific time slots to work through your most important tasks. Then stick to the plan.
4. ABCDE Method: Rank your tasks by importance. Tackle the ones ranked ‘A’ first.
5. MoSCoW Method: Be clear about what must happen and what can wait.
6. Kanban Board: Better visualize your tasks and your team’s progress.
7. Warren Buffett’s 25/5 Rule: Make a list of your top 25 projects. Circle the top 5 and only work on them.
8. Pareto Principle: Focus your effort on the 20% of tasks that are going to produce 80% of the results.
9. Theory of Constraints: Identify bottlenecks and fix them to amplify your output.

Step 3: Prioritize and Execute

• Review your goals daily: Make sure you’re on track and adjust as needed.
• Apply your chosen tools: Whether it’s the Eisenhower Matrix or the Pareto Principle, integrate these tools into your daily workflow.
• Measure your progress: Regularly assess what’s working and add or subtract.

**Remember, success isn’t achieved by doing more. It’s achieved by doing what matters the most.**

### The Secret to Productivity

Ali Abdaal - Sept, 11th

🥳 Play keeps work fun. Maybe it's turning tasks into games or not taking ourselves too seriously.
💪 Power lets us shape our work and see good results.
🙏 People around us give us energy and push us forward.

## 💭 Stop Trying So Hard | ali@productivitylab.com | Oct 22

> These moments weren’t the result of intense focus. Instead, they happened when their minds were allowed to drift.
  Psychologists have a name for this phenomenon: the default mode network (DMN).


## Are you setting yourself up to fail? | hi@aliabdaal.com | Oct 21

> What happens in our brains when we process “failure”?
  creates a cocktail of emotions and memories that make us want to avoid similar situations in the future.
> It’s not very “personal development” friendly, but the idea of "if at first you don't succeed, try, try again" isn't actually that helpful for our survival.
> a study that Mark Rober ...
  got 50,000 people to play a game trying to solve something impossible.
  Half the group was told "You failed. Please try again."
  The other half was told "You failed. You've lost 5 points. You now have 195 points. Please try again."
    These were totally arbitrary “points” that had absolutely no bearing in the real world.
> the group who were told they lost points (i.e., where the failure was emphasised)
  tried less than half as many times to solve the puzzle compared to the group that was simply told they failed.

> when we're trying to learn new things or accomplish anything,
  we should try to set things up in a way that makes it hard for us to fail.
  For example, if I'm starting a new diet and my rule is that
  I “succeed” only if I stick to my calorie goal everyday,
  but I “fail” if I go over even once, it's going to be really easy to fail.

> The trick is to set up the rules of the game so that
  even if you don’t stay as consistent or as intentional as you’d like,
  you avoid registering that as a “failure”.

## Hemingway

In Wisereads Vol. 61 — Dario Amodei's Machines of Loving Grace, Journey to the Center of the Earth, and more
Inbox
[Tap into the “Hemingway effect” to finish what you start - Big Think](https://bigthink.com/the-learning-curve/the-hemingway-effect/)
> Because Hemingway left his work at an interesting moment, it became easier to return to his typewriter the next day. Think of it like a TV show cliffhanger.
> Hemingway essentially incorporated self-made cliffhangers into his productivity schedule to maintain his desire to see things through.


## 3-2-1: On finding games worth winning, the power of making a lot and choosing the best, and two types of kindness | Oct 17

"Before you worry about how to win the game, figure out whether the game is worth winning."

## ⏳ Protect Your Time | Oct 14

“Focus is a matter of deciding what things you're not going to do.” - John Carmack

## Wisereads Vol. 59 — Good Work by Paul Millerd, Steve Jobs' 10-Minute Rule, and more

[Too much efficiency makes everything worse: overfitting and the strong version of Goodhart’s law | Jascha’s blog](https://sohl-dickstein.github.io/2022/11/06/strong-Goodhart.html)

"As we continue optimizing the proxy though, we eventually exhaust the useable similarity between proxy and goal. The proxy keeps on getting better, but the goal stops improving. In machine learning we call this overfitting, but it is also an example of Goodhart's law."


## 3-2-1: On the power of going for it, the value of sharing what you know, and how to figure out what you really want

Musician and comedian Tim Minchin on sharing what you know:

"Even if you're not a teacher, be a teacher. Share your ideas. Don't take for granted your education. Rejoice in what you learn and spray it."

Source: [9 Life Lessons](https://jamesclear.com/great-speeches/9-life-lessons)

## This conversation changed my perspective  👀 | ali@aliabdaal.com | Sept 12, 2024

> content team feels like they don't have direction .. churning out content to make numbers go up.
  It feels... pointless and arbitrary sometimes.
  It's draining, to be honest

> Q: approach this from a different angle.
     Can you describe the lifecycle of one of your audience members? What's their journey with you like?

> They find it useful and implement a tip or two.
  Over the next few months, some become ore regular viewers.
  They might be seeing real changes - getting more done, feeling less stressed,
  making progress on personal projects.

> Q: what do you think is the real purpose of the content you're creating?

> You know, I think I'm having an epiphany here.
  It's... repetition and reminders for existing fans, and an on-ramp for new audience members.
  For our existing fans, it's about reinforcement.
  It's like... spaced repetition for life skills.
  ...
  my goal as a creator, and our goal as a team, is to basically say the same core messages over and over again, but in different formats for different audiences and in different ways?
  ..
  That's... actually incredibly liberating. I often feel this pressure to constantly come up with new, groundbreaking ideas, especially when I see comments saying: “Ali’s content has become repetitive”... But now I see that's not the point at all.

## Last 48 Hrs CEO Accelerator + 6 questions to help you crush the next 90 days | eric@ericpartaker.com | 30 Sept, 2024

1) What were your Top 3 Big Wins from July 1st through September 30th? Why?
   In our constant pursuit of more, we miss the amazing things we've already achieved.
   Take a moment right now to think this through. Don't rush. What are you proud of? Why?
2) What were the Biggest Challenges? What did you learn?
3) What 'Unfinished Business' should you complete by the end of October?
   Unless our targets aren't ambitious enough there will always be unfinished business.
4) If you were fired and someone came in to replace you, what would they immediately start doing and stop doing?
5) If you had to achieve your 10 year plan in 6 months what would you do?
   This question helps us think bigger and almost always requires taking a **who, not how** approach
   "Who can we be partnering with or adding to the team to fill experience gaps and move much more quickly?"
6) What are your Top 3 Goals for the next 90 days? And what is the #1 Obstacle for each + Plan to Overcome?
   Double-down on one of your Big Wins, explore one of your Biggest Challenges, or simply crush any Unfinished Business. Or perhaps it's time for something completely new.

## 🤞 Stop Wasting Your Life | ali@productivitylab.com | Oct 25, 2024

Track Your Focus Hours

* Identify Patterns
  Do you focus better in the morning? Is there a certain type of task that pulls you in more deeply?
* Spot Distractions
  Notice any common distractions that pull you away? Recognising these can help you minimise them in the future.
* Set New Goals
  Based on your tracked data, set some new focus goals. For example, you might aim to add 30 minutes of deep work to your day or cut down on distractions during your most productive times.


* Start Tracking Your Focus Hours
  Whether it’s using an app like Toggl or a simple spreadsheet, start logging the minutes you spend on focused, deep work.
* Review & Reflect Weekly
  At the end of each week, review your focus time. What went well? What can be improved?
  Use this reflection time to set goals for the week ahead.
* Measure Other Key Areas
  Think about other aspects of your life ... like your project milestones, fitness goals, or even personal downtime - that could benefit from being tracked? Think about your health, relationships, hobbies, or even how much time you’re dedicating to learning and growth.

## 😌 How to Quiet the Little Voice in Your Head | ali@aliabdaal.com | Jan 17, 2025

(3) I was surprised by how much more progress I made during sprints of intensity than during periods of consistency

## 6 Powerful Questions to Help You Achieve Your Goals | eric@ericpartaker.com | Fri, Jan 17

3) What 'Unfinished Business' should you complete within Q1?
4) If you were fired and someone came in to replace you, what would they immediately start doing and stop doing?
6) What are your Top 3 Goals for 2025? And what is the #1 Obstacle for each + Plan to Overcome?

## 👨‍🌾 The Farmer’s Mindset |  ali@productivitylab.com | Oct 28, 2024

> But if you think about it, progress is more like farming: you plant seeds, tend to them, and wait.
> The plant doesn’t show up immediately, but with time and effort, it grows.

> “Everyone wants to live on top of the mountain, but all the happiness and growth occurs while you’re climbing it.”
> - Andy Rooney

> It’s not about quick wins, but about showing up consistently and letting time do the work.

## ✌️ 3 Steps to a More Productive Day | ali@productivitylab.com | Nov 1, 2024

🧑‍💻 Three Hours of Deep Work
    e.g. book writing, plan next video
🏅 Three Quick Wins
    e.g. review goals for the week, answer emails, prepare presentation
🧹 Three Maintenance Activities
    e.g. go to the gym, tidy up workspace, grab coffee with a friend

> The secret of getting ahead is getting started.
> - Mark Twain

## 3-2-1: On getting what you deserve, the power of flexibility, and how good decisions are made | james@jamesclear.com | 2025 Feb 13

> If you do not bend, you will break.
> The adaptable prevail. Determined, but flexible.
