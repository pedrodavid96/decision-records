# CPU

CPU Architectures...

> RISC-V (pronounced "risk-five") is an open standard instruction set architecture (ISA) based on established reduced instruction set computer (RISC) principles.

[Pineapple ONE | Hackaday.io](https://hackaday.io/project/178826-pineapple-one)
> In this article I will describe how I designed and made a functional 32 bit RISC-V CPU at home.

[Build a RISC-V CPU From Scratch - IEEE Spectrum](https://spectrum.ieee.org/build-a-riscv-cpu-from-scratch)
