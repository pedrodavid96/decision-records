# Bash

* [Bash-it](https://bash-it.readthedocs.io/en/latest/)
  [GitHub - Bash-it/bash-it: A community Bash framework.](https://github.com/Bash-it/bash-it)
* [Oh My Bash](https://ohmybash.nntoan.com/)

[Bash tips and tricks](https://pragmaticcoders.com/blog/bash-tips-and-tricks/)
https://www.silvestar.codes/articles/my-favorite-bash-shortcuts/


[101 bash commands and tips for beginneres to experts](https://dev.to/awwsmm/101-bash-commands-and-tips-for-beginners-to-experts-30je#nautilus-date-cal-bc)
[Article itself](https://dev.to/awwsmm/101-bash-commands-and-tips-for-beginners-to-experts-30je)

[command history - Why would anyone not set 'histappend' in bash? - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/6501/why-would-anyone-not-set-histappend-in-bash)

## Cheatsheet: Productivity-boosting Bash shortcuts
https://blog.ssdnodes.com/blog/cheatsheet-bash-shortcuts/

```
# Navigation
Ctrl + a 	Go to the beginning of the line.
Alt + f 	Move the cursor forward one word.
Alt + b 	Move the cursor back one word.
Ctrl + e 	Go to the end of the line.
Ctrl + x, x 	Toggle between the current cursor position and the beginning of the line.

# Editing
Ctrl + _ 	Undo! (And, yes, that’s an underscore, so you’ll probably need to use Shift as well.)
Ctrl + x, Ctrl + e 	Edit the current command in your $EDITOR.
Alt + d 	Delete the word after the cursor.
Alt + Delete 	Delete the word before the cursor.
Ctrl + d 	Cut the word after the cursor to the clipboard.
Ctrl + w 	Cut the word before the cursor to the clipboard.
Ctrl + y 	Paste the last item to be cut.

Ctrl + k 	Cut the line after the cursor to the clipboard.
Ctrl + u 	Cut the line before the cursor to the clipboard.

# Processes
Ctrl + z 	Place the currently running process into a suspended background process (and then use `fg` to restore it or `bg` to run it in the background).

# History
Ctrl + r 	Bring up the history search.
Ctrl + g 	Exit the history search.
Ctrl + p 	See the previous command in the history.
Ctrl + n 	See the next command in the history.
```

[Bash keyboard shortcuts](https://gist.github.com/tuxfight3r/60051ac67c5f0445efee)


[How Bash completion works](https://tuzz.tech/blog/how-bash-completion-works)
[navi - An interactive cheatsheet tool for the command-line](https://github.com/denisidoro/navi)

## TLDR Pages: Simplified, community-driven man pages

https://tldr.sh/

[Reddit Discussion](https://www.reddit.com/r/programming/comments/ese24w/tldr_pages_simplified_communitydriven_man_pages/)

[By @pandaMoniumHUN](https://www.reddit.com/r/programming/comments/ese24w/tldr_pages_simplified_communitydriven_man_pages/ff9kk47/)
> Honestly, this is how the first part of all man pages should look like. A list of most commonly used options illustrated with one-line examples. Currently man pages are informative but rarely useful when I simply forget one of the thousand available options for any CLI tool.

## Anybody can write good bash (with a little effort)
[Blog post](https://blog.yossarian.net/2020/01/23/Anybody-can-write-good-bash-with-a-little-effort)

```
Personal favorites (horror stories) include:

- The ominous run.sh, which regularly:
    * Runs something, somewhere
    * Lacks the executable bit
    * Doesn't specify its shell with a shebang
    * Expects to be run as a particular user, or runs itself again in a different context
    * Does very bad things if run from the wrong directory
    * May or may not fork
    * May or may not write a pidfile correctly, or at all
    * May or may not check that pidfile, and subsequently clobber itself on the next run
    * All of the above

- make.sh (or build.sh, or compile.sh, or ...), which:
    * Doesn't understand CC, CXX, CFLAGS or any other standard build environment variable
    * Gets clever and tries to implement its own preprocessor
    * Contains a half-baked -j implementation
    * Contains a half-baked make implementation, including (broken) install and clean targets
    * Decides that it knows better than you where it should be installed
    * Fails if anything, anywhere has a space in it
    * Leaves the build in an indeterminate state if interrupted
    * Happily chugs along after a command fails, leaving the build undiagnosable
    * All of the above

- test.sh, which:
    * Expects to be run in some kind of virtual environment (a venv, a container, a folder containing a bundle, &c)
    * ...tries to install and/or configure and/or load that virtual environment if not run inside it correctly, usually breaking more things
    * Incorrectly detects that is or isn't in the environment it wants, and tries to do the wrong thing
    * Masks and/or ignores the exit codes of the test runner(s) it invokes internally
    * Swallows and/or clobbers the output of the runner(s) it invokes internally
    * Contains a half-baked unit test implementation that doesn't clean up intermediates or handle signals correctly
    * Gets really clever with colored output and doesn't bother to check isatty
    * All of the above

- env.sh, which:
    * May or may not actually be a shell script
    * May or may evaled into a shell process of indeterminate privilege and state somewhere in your stack
    * May or may not just be split on = in Python by your burnt-out DevOps person
    * All of the above, at different stages and on different machines
```

```
A bash script (i.e., a bash file that's meant to be run directly) doesn't end up in my codebases unless it:

- Has the executable bit
- Has a shebang and that shebang is #!/usr/bin/env bash
  Explanation: not all systems have a (good) version of GNU bash at /bin/bash: macOS infamously supplies an ancient version at that path, and other platforms may use other paths.
- Has a top level comment that briefly explains its functionality
- Has set -e (and ideally set -euo pipefail)4
  Explanation: set -e, while not perfect, catches and makes fatal many types of (otherwise silent) failure. set -u makes expansions of undefined variables fatal, which catches the classic case of rm -rf "${PERFIX}/usr/bin". set -o pipefail extends -e by making any failure anywhere in a pipeline fatal, rather than just the last command.
- Can either:
    * Be run from any directory, or
    * Fail immediately and loudly if it isn't run from the correct directory
```

---

readlink - print resolved symbolic links or canonical file names

[Create symlink, overwrite if one exists](https://unix.stackexchange.com/questions/207294/create-symlink-overwrite-if-one-exists)

[What is the preferred Bash shebang?](https://stackoverflow.com/questions/10376206/what-is-the-preferred-bash-shebang)
> You should use `#!/usr/bin/env bash` for portability: different *nixes
  put bash in different places, and using `/usr/bin/env` is a workaround
  to run the first bash found on the `PATH`. And `sh` is not `bash`.

[Echo output to stderr](https://stackoverflow.com/questions/2990414/echo-that-outputs-to-stderr)
```bash
>&2 echo "error"
```

[Bash get exit code of command on a Linux / Unix](https://www.cyberciti.biz/faq/bash-get-exit-code-of-command/)
How to find exit code of a command? `$?`
How do I set an exit code for my own shell scripts? `exit <N>`

[Use Bash Strict Mode (Unless You Love Debugging)](http://redsymbol.net/articles/unofficial-bash-strict-mode/)
```bash
#!/bin/bash
set -euo pipefail
IFS=$'\n\t'
```

* `set -e` instructs bash to immediately exit if any command has a non-zero exit status.
* `set -u` a reference to any variable you haven't previously defined (exception of `$*` and `$@`)
         is an error
* `set -o pipefail` prevents errors in a pipeline from being masked. If any command in a pipeline fails, that return code will be used as the return code of the whole pipeline.
* Setting IFS (**I**nternal **F**ield **S**eparator)
  By default (space) the following script could have trouble parsing args
  `myscript.sh notes todo-list 'My Resume.doc'` (My Resume.doc would be 2 different args)

[5 bash tricks to make you a cooler programmer](https://www.youtube.com/watch?v=SVYnX6A9_hQ)
* `Alt-Shift-3` - Comments current line
* `Ctrl-R` - Find commands you used before
* `!!`, `!$`, and `!*` -  last command, last argument, last command except for start (everything but the co`mmand)
* `{a,b,c}` - bash expansion, useful to rename files, e.g.: `mv file1{,-old}`
* `cd -` - get back to last directory

[Minimal safe Bash script template](https://betterdev.blog/minimal-safe-bash-script-template/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/kcxnag/minimal_safe_bash_script_template/)
1. Choose Bash `#!/usr/bin/env bash`
2. Fail fast `set -Eeuo pipefail`
3. Get the location `script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)`
4. Try to clean up `trap cleanup SIGINT SIGTERM ERR EXIT` ... (implement `cleanup` function)
5. Display helpful help
6. Print nice messages
   You can use colors for messages
   Citing the great 12 Factor CLI Apps article:
   > In short: stdout is for output, stderr is for messaging.
7. Parse any parameters


[12 Factor CLI Apps](https://medium.com/@jdxcode/12-factor-cli-apps-dd3c227a0e46)
1. Great help is essential
2. Prefer flags to args
3. What version am I on?
4. Mind the streams
5. Handle things going wrong
6. Be fancy!
7. Prompt if you can
8. Use tables
9. Be speedy
10. Encourage contributions
11. Be clear about subcommands
12. Follow XDG-spec

[You Can And Should Write Tests For Your Shell Scripts](https://blog.gds-gov.tech/you-can-and-should-write-tests-for-your-shell-scripts-4cc383866b19)
> DevOps Engineer, I spend a lot of time writing tools, scripts and
   Ansible playbooks to automate engineering processes.
> Most of the time I verify that my scripts work by running them on live
   non-production environments and I admit that this is bad practice.
Using [shUnit2](https://github.com/kward/shunit2)

[How does “cat << EOF” work in bash?](https://stackoverflow.com/questions/2500436/how-does-cat-eof-work-in-bash)
> This is called heredoc format to provide a string into stdin. See https://en.wikipedia.org/wiki/Here_document#Unix_shells for more details.
> From `man bash`:
> This type of redirection instructs the shell to read input from the current source
   until a line containing only word (with no trailing blanks) is seen.
> ...

[Prevent expressions enclosed in backticks from being evaluated in heredocs [duplicate]](https://stackoverflow.com/questions/13122147/prevent-expressions-enclosed-in-backticks-from-being-evaluated-in-heredocs)
> Quote the label to prevent the backticks from being evaluated.
```bash
$ cat << "EOT" > out
foo bar
`which which`
EOT
```

[6 Bash Tricks You Can Use Daily](https://medium.com/for-linux-users/6-bash-tricks-you-can-use-daily-a32abdd8b13)
Unusual tricks! Great article 🥇⭐
* Make Output into Columns
```
cat /etc/passwd | column -t -s :
```
* Search and Replace the Last Command
```
# only replaces first instance
^<old>^<new>
# replace all
!!:gs/<old>/<new>/
```
* Remove Duplicate Lines in Unsorted Files
```
awk ‘!seen[$0]++’ file
```
* Edit Command Line in Your Editor
Ctrl+X -> Ctrl+E
* Fill Autocomplete on Command Line
Esc -> *
* More at `man bash`

[Alert yourself after a long-running task in terminal : programming](https://www.reddit.com/r/programming/comments/lsdiue/alert_yourself_after_a_longrunning_task_in/)
```bash
longrunningcommand ; beep
```
```bash
function veep() {
  (echo "DONE" | dzen2 -p 1 -bg green -fg black)
}
```
