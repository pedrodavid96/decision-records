If you simply want a task runner with Makefile like syntax:
[just](https://github.com/casey/just#just)
> handy way to save and run project-specific commands.

[Your Makefiles are wrong](https://tech.davis-hansson.com/p/make/)
- Sensible defaults
  * Don’t use tabs
  * Always use (a recent) bash
  * Use bash strict mode
  * Change some Make defaults
  tl;dr: Use this preamble in your Makefiles:
  ```makefile
  SHELL := bash
  .ONESHELL:
  .SHELLFLAGS := -eu -o pipefail -c
  .DELETE_ON_ERROR:
  MAKEFLAGS += --warn-undefined-variables
  MAKEFLAGS += --no-builtin-rules

  ifeq ($(origin .RECIPEPREFIX), undefined)
    $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
  endif
  .RECIPEPREFIX = >
  ```
- The file system
  * Use `.PHONY: <target>` if your target doesn't generate files
  * Generating output files
  * Specifying inputs

## Working with Makefiles

https://www.gnu.org/software/make/manual/html_node/Prerequisite-Types.html
```
targets : normal-prerequisites | order-only-prerequisites
```
order-only-prerequesites won't run on timestamp updates

.PHONY

[A Simple Makefile Tutorial](https://www.cs.colby.edu/maxwell/courses/tutorials/maketutor/)
`%` - pattern match
In the goal itself, `$@` is used to reference what is before `:`, `$<` is
the first item in the dependencies list. `$^` is the right side of `:`.

[makefile basics](https://gist.github.com/isaacs/62a2d1825d04437c6f08)
Cool "interactive" tutorial

[Makefile cheatsheet](https://devhints.io/makefile)

[What is a Makefile and how does it work? | Opensource.com](https://opensource.com/article/18/8/what-how-makefile)


[Makefile Tutorial By Example](https://makefiletutorial.com/)
