[API authentication and authorization](https://idratherbewriting.com/learnapidoc/docapis_more_about_authorization.html)

[4 ways to obtain access token in OAuth 2.0](https://www.reddit.com/r/programming/comments/c775ao/4_ways_to_obtain_access_token_in_oauth_20/)

[OAuth vs. OpenID – What’s the difference?](https://www.gluu.org/blog/oauth-vs-openid-whats-the-difference/)
[OpenID Connect (OIDC)](https://www.ssh.com/iam/openid-connect)

[Authorization Code Flow with Proof Key for Code Exchange (PKCE)](https://auth0.com/docs/flows/concepts/auth-code-pkce)
[PKCE flow of OpenID Connect](https://medium.com/swlh/pkce-flow-of-openid-connect-9b10ddbabd66)

[API authentication and authorization](https://idratherbewriting.com/learnapidoc/docapis_more_about_authorization.html)

[An Illustrated Guide to OAuth and OpenID Connect](https://www.youtube.com/watch?v=t18YB3xDfXI)

[Web Authentication API - Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API/Web_Authentication_API)