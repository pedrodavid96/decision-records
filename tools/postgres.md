# PostgreSQL

[Just Use Postgres for Everything | Amazing CTO](https://www.amazingcto.com/postgres-for-everything/)
* Use Postgres for caching instead of Redis with [UNLOGGED tables](https://www.compose.com/articles/faster-performance-with-unlogged-tables-in-postgresql/)
  and TEXT as a JSON data type.
* Use stored procedures to add and enforce an expiry date for the data just like in Redis.
* Use Postgres as a message queue with [SKIP LOCKED](https://www.enterprisedb.com/blog/what-skip-locked-postgresql-95) instead of Kafka
  (if you only need a message queue).
* Use Postgres with [Timescale](https://www.timescale.com/) as a data warehouse.
* Use Postgres with [JSONB](https://scalegrid.io/blog/using-jsonb-in-postgresql-how-to-effectively-store-index-json-data-in-postgresql/) to store
  Json documents in a database, search and index them - instead of Mongo.
* Use Postgres as a cron demon to take actions at certain times, like sending mails, with [pg_cron](https://github.com/citusdata/pg_cron)
  adding events to a message queue.
* Use Postgres for [Geospacial queries](https://postgis.net/).
* Use Postgres for [Fulltext Search](https://supabase.com/blog/postgres-full-text-search-vs-the-rest) instead of Elastic.
* Use Postgres to [generate JSON in the database](https://www.amazingcto.com/graphql-for-server-development/), write no server side code and directly give it to the API.
* Use Postgres with a [GraphQL adapter](https://graphjin.com/) to deliver GraphQL if needed.

## [Don't do this!](https://wiki.postgresql.org/wiki/Don%27t_Do_This)
[Discussion](https://www.reddit.com/r/programming/comments/bk7wei/dont_do_this/)

[Lesser Known PostgreSQL Features | Haki Benita](https://hakibenita.com/postgresql-unknown-features#get-the-number-of-updated-and-inserted-rows-in-an-upsert)

[Rotating PostgreSQL Passwords with no downtime](https://www.jannikarndt.de/blog/2018/08/rotating_postgresql_passwords_with_no_downtime/)

[A tale of query optimization](https://parallelthoughts.xyz/2019/05/a-tale-of-query-optimization/)

[To ORM or not to ORM](https://eli.thegreenplace.net/2019/to-orm-or-not-to-orm/)
[To ORM or not to ORM | Discussion](https://www.reddit.com/r/programming/comments/bqsle5/to_orm_or_not_to_orm/)

[Fine Tuning Full Text Search with PostgreSQL 12](https://rob.conery.io/2019/10/29/fine-tuning-full-text-search-with-postgresql-12/)

[The internals of PostgreSQL](http://www.interdb.jp/pg/index.html)

[Database design standards](https://ovid.github.io/articles/database-design-standards.html)
[Reddit discussion](https://www.reddit.com/r/programming/comments/eo46wp/database_design_standards/)


[PostgreSQL triggers and isolation levels](https://vladmihalcea.com/postgresql-triggers-isolation-levels/)
Awesome article on isolation levels!!!

[10 Things I Hate About PostgreSQL](https://medium.com/@rbranson/10-things-i-hate-about-postgresql-20dbab8c2791)
#1: Disastrous XID Wraparound
#2: Failover Will Probably Lose Data
#3: Inefficient Replication That Spreads Corruption
#4: MVCC Garbage Frequently Painful
#5: Process-Per-Connection = Pain at Scale
#6: Primary Key Index is a Space Hog
#7: Major Version Upgrades Can Require Downtime
#8: Somewhat Cumbersome Replication Setup
#9: Ridiculous No-Planner-Hints Dogma
#10: No Block Compression

[PostgreSQL v13 new feature: tuning autovacuum on insert-only tables](https://www.cybertec-postgresql.com/en/postgresql-autovacuum-insert-only-tables/)

[31.15. The Password File](https://www.postgresql.org/docs/9.3/libpq-pgpass.html)
[20.12. Certificate Authentication](https://www.postgresql.org/docs/12/auth-cert.html)
[17.9. Secure TCP/IP Connections with SSL](https://www.postgresql.org/docs/9.5/ssl-tcp.html)

[Cleaning Up Your Postgres Database](https://info.crunchydata.com/blog/cleaning-up-your-postgres-database)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/lb65m6/cleaning_up_your_postgres_database/)
* The high level metrics - cache and index hit ratio:
* Cleaning up unused indexes
* Keep certain things outside Postgres
* Dig into query performance with pg_stat_statements

[Reconnecting your application after a Postgres failover](https://www.citusdata.com/blog/2021/02/12/reconnecting-your-app-after-a-postgres-failover/)
* Client-side HA and Fault-Tolerance: a job for the whole team
  Failure is inevitable and if you domain allows some user experience degradation you should take advantage of that while maintaining a minimum of your features.
  That's why this includes Product and Business people as well
* Multi-Hosts Connection Strings, thanks to libpq
* Automated Failover must include client-side automated reconnections
* Automatic retry of a transaction is dangerous at best

[By using a BRIN index instead of a BTREE index for certain queries, I can get huge index size savings with very similar performance.](https://www.postgresql.fastware.com/blog/brin-indexes-what-are-they-and-how-do-you-use-them)

[Five Easy to Miss PostgreSQL Query Performance Bottlenecks](https://pawelurbanek.com/postgresql-query-bottleneck)
1. Searching by a function call
2. Searching by a pattern
3. Ordering by NULLS LAST
4. Bloated null_indexes
5. Update transaction scope

[PostgreSQL: How to update large tables - in Postgres | Codacy | Tips](https://blog.codacy.com/how-to-update-large-tables-in-postgresql/)
1. Incremental updates
2. Create a new table
3. Recreate the existing table
3. Handling Concurrent Writes

[How do I do large non-blocking updates in PostgreSQL?](https://stackoverflow.com/questions/1113277/how-do-i-do-large-non-blocking-updates-in-postgresql)

[mysql - how to create partitioned table with liquibase](https://stackoverflow.com/questions/36218169/how-to-create-partitioned-table-with-liquibase)

[Is your Postgres ready for production?](https://www.crunchydata.com/blog/is-your-postgres-ready-for-production)
* Backups
* High availability
* Logs properly configured
  - Archived and persisted
  - Logging slow queries
  - Auto explain
* Statement timeout <----
* Connection pooling

[Step 2: Enable auto_explain · pganalyze](https://pganalyze.com/docs/explain/setup/google_cloud_sql/02_enable_auto_explain)

[Using Vim as my PostgreSQL Client | Thomas Stringer](https://trstringer.com/postgres-client-vim/)

https://github.com/xataio/pgroll
PostgreSQL zero-downtime migrations made easy

