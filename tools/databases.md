# Databases

[Presto | Distributed SQL Query Engine for Big Data](https://prestodb.io/)

## Timeouts

* [Database timeouts - Piotr Mionskowski](https://miensol.pl/database-timeouts/#read)
* [postgresql - How to set statement timeout per user? - Database Administrators Stack Exchange](https://dba.stackexchange.com/questions/290714/how-to-set-statement-timeout-per-user)

[Soft Deletion Probably Isn't Worth It](https://brandur.org/soft-deletion)

---

## Performance

[SQL Indexing and Tuning e-Book for developers](https://use-the-index-luke.com/)

[PostgreSQL execution plan operations](https://use-the-index-luke.com/sql/explain-plan/postgresql/operations)
Different types of Join methods

Index Only scan:
[Covering index: an index containing all queried columns](https://use-the-index-luke.com/sql/clustering/index-only-scan-covering-index)
[PostgreSQL: Documentation: 17: 11.9. Index-Only Scans and Covering Indexes](https://www.postgresql.org/docs/current/indexes-index-only-scans.html)
[Waiting for 9.2 – Index only scans – select * from depesz;](https://www.depesz.com/2011/10/08/waiting-for-9-2-index-only-scans/)

[PostgreSQL: Documentation: 17: 11.3. Multicolumn Indexes](https://www.postgresql.org/docs/current/indexes-multicolumn.html)
> A multicolumn B-tree index can be used with query conditions that
> involve any subset of the index's columns, but the index is most
> efficient when there are constraints on the leading (leftmost) columns.

---

## Misc

[My Notes on GitLab Postgres Schema Design – Shekhar Gulati](https://shekhargulati.com/2022/07/08/my-notes-on-gitlabs-postgres-schema-design/)

[Things I Wished More Developers Knew About Databases | by Jaana Dogan | Medium](https://rakyll.medium.com/things-i-wished-more-developers-knew-about-databases-2d0178464f78)
[Reddit discussion](https://www.reddit.com/r/programming/comments/1f8244w/things_i_wished_more_developers_knew_about/)

