# Infrastructure as Code

[Pulumi - Modern Infrastructure as Code](https://www.pulumi.com/)
Write real code instead of yaml <3
[Make it more obvious *why* you would log in · Issue #3152 · pulumi/pulumi · GitHub](https://github.com/pulumi/pulumi/issues/3152)
[Support XDG directories · Issue #2534 · pulumi/pulumi · GitHub](https://github.com/pulumi/pulumi/issues/2534)

[Infrastructure As Code Tutorial](https://github.com/Artemmkin/infrastructure-as-code-tutorial)

[5 Lessons Learned From Writing Over 300,000 Lines of Infrastructure Code](https://blog.gruntwork.io/5-lessons-learned-from-writing-over-300-000-lines-of-infrastructure-code-36ba7fadeac1)

[(Almost) Every infrastructure decision I endorse or regret after 4 years running infrastructure at a startup · Jack's home on the web](https://cep.dev/posts/every-infrastructure-decision-i-endorse-or-regret-after-4-years-running-infrastructure-at-a-startup/)
