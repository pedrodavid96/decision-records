# Reading

[READ A BOOK IN A DAY (how to speed-read and remember it all)](https://www.youtube.com/watch?v=0Q0we4LjSws)

1. Intent
2. Turn off distractions
3. Don't "read" all the words
   Draw a vertical line in each side "cutting" 2 words
   Limit yourself to "reading" the words in between
4. Eliminate going back
   Do not go back, use index finger for pointing
--- Remembering --
5. Visualize
   Imagine you're seeing a movie
6. Read for longer
7. Take breaks
8. Switch places
9. Practice

[These 38 Reading Rules Changed My Life - RyanHoliday.net](https://ryanholiday.net/these-38-reading-rules-changed-my-life/)
–Do it all the time. Bring a book with you everywhere. ...waited for a tow truck.
–Bring a pen with you too. Reading is better if you’re taking notes.
–Keep a commonplace book.
–Err on the side of age. Classics are classics for a reason.
-Beat them up. Books are not precious things. As an author, I love it when people hand me a book to sign that has had real miles put on it. When people hand me a pristine copy and tell me it’s their favorite, I assume they are just flattering me. It’s obvious what my favorite books are…because they’re falling apart ([here's](https://www.instagram.com/p/CtwUeUdL_rB/?igshid=MzRlODBiNWFlZA%3D%3D&img_index=1) my copy of Meditations for instance).
–If you see a book you want, just buy it. Don’t worry about the price. Reading is not a luxury. It’s not something you splurge on. It’s a necessity. Even if all you get is one life-changing idea from a book, that’s still a pretty good ROI.
–Don’t just read books, re-read books.
–As I said, speed reading is a scam. You just have to spend a lot of time reading.
–If a book sucks, stop reading it. The best readers actually quit a lot of books. Life is too short to read books you don’t enjoy reading.
–The rule I like is ‘one hundred pages minus your age.’ Say you’re 30 years old—if a book hasn’t captivated you by page 70, stop reading it. So as you age, you have less time to endure crap.
-Embrace serendipity. So many of my favorite books are just random things I grabbed at bookstores
-Don’t just build a library, build an anti-library—a stack of unread books that humbles you and reminds you just how much there is still to learn. It’s a sign of what you don’t yet know. It’s also a resource there whenever you might need to do a deep dive into that topic.
-Every time I would meet a successful or important person I admire, I would ask them: What’s a book that changed your life?
-Emerson:“1. Never read a book that is not a year old [because only good books survive]. 2. Never read any but famed books [same reason]. 3. Never read any but what you like.”
-I find myself sometimes reluctant to read something that’s super popular. That snobbishness never serves me well. More often than not, when I get around to those bestsellers I kick myself–they were bestsellers for a reason! They’re great! Don’t be a book snob.
–You say you don’t have time to read but what does the screen time app on your phone say? What does your calendar say?
–If you want to understand current events, don’t rely on breaking news. Find a book about a similar event in the past. Read history. Read psychology. Read biographies. Go for information that has a long half-life, not something that’s going to be contradicted in the next bulletin.
 Examples: Read The Great Influenza to understand COVID. Read It Can’t Happen Here to understand modern threats to democracy. Read First Principles to understand American politics.
–One of the things that people in publishing know is that readers tend to skip prefaces and forewords. This is crazy! Those things are there for a reason. They often have a ton of helpful and interesting stuff about the context around when the person was writing, who the work ended up influencing, and other tidbits that sometimes stick with you longer than even the work itself.
–When intelligent people read, they ask themselves a simple question: What do I plan to do with this information?
-Pretentiousness is bullshit. Epictetus once heard a student talking proudly about having made their way through the dense works of Chryssipus. You know, Epictetus told him, if Chryssipus had been a better writer, you’d have less to brag about.
–If a book is good, recommend it and pass it along to other people.


[Ryan Holiday's 3-Step System for Reading Like a Pro](https://youtu.be/gT1EExZkzMM?si=CqP1SwDlzC7rWW3E)
1. Have a conversation in a book
2. Let the book sit for a couple of months just pick later on the best idea from it.
3. Have a common place book, where you put notes.
   Write the theme why it touched you