# SQLite

[SQLite is not a toy database](https://antonz.org/sqlite-is-not-a-toy-database/).

Create a temporary database simply by doing `sqlite3 temp.db`.

## Import CSV

```
sqlite> .mode csv
sqlite> .import <file.csv> <table-name>
```

## Check table schema

```
sqlite> .schema <table>
```
