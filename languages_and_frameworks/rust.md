# Rust

[How do I convert a &str to a String in Rust?](https://blog.mgattozzi.dev/how-do-i-str-string/)
* .to_string() if you want a simple conversion and show you're making
  a String
* .to_owned() if you want a simple conversion and to show you're taking
  ownership of it.
* String::from() a more explicit way to say what you're doing
* String::push_str() if you need to append to a String
* format!() if you have predefined text that you wanted formatted in
  a specific way and don't care about the speed cost
* .into() if you want a simple conversion and don't care about the
  ambiguity of it

https://fasterthanli.me/articles/working-with-strings-in-rust

[Effective Rust (2021) | Hacker News](https://news.ycombinator.com/item?id=36338529)

[Leveraging Rust and the GPU to render user interfaces at 120 FPS](https://zed.dev/blog/videogame)

[A Minecraft server written in Rust : programming](https://www.reddit.com/r/programming/comments/1etq0jk/a_minecraft_server_written_in_rust/)
[Introduction · WASI.dev](https://wasi.dev/)
