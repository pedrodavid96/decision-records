# Media Tracker

Your Media (Movie, TV Shows, Games, Books (?)) Diary.

Track and plan your media ownership / consumption.

For Reference:
https://play.google.com/store/apps/details?id=com.cinetrak.mobile&hl=en

> • Full integration and support of Trakt.tv
> Trakt is a platform that does many things, but primarily keeps track of TV shows and movies you watch. It integrates with your media center or home theater PC to enable scrobbling, so everything is automatic.

[GitHub - akhilrex/podgrab: A self-hosted podcast manager/downloader/archiver tool to download podcast episodes as soon as they become live with an integrated player.](https://github.com/akhilrex/podgrab)

[Watcharr](https://watcharr.app/)

[ryot](https://ryot.io/)
Roll your own tracker

[[FEATURE REQUEST] - Allow Ryot to track anything · Issue #73 · IgnisDa/ryot](https://github.com/IgnisDa/ryot/issues/73)
[Telemetry · Issue #1078 · IgnisDa/ryot](https://github.com/IgnisDa/ryot/issues/1078)

[bayang/jelu](https://github.com/bayang/jelu)
Self hosted read and to-read list book tracker (mainly kotlin!)
