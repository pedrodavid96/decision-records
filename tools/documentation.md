# Documentation

https://www.makeareadme.com/

[Good tools for documentation](https://www.process.st/software-documentation/)

[What open source projects have the best docs?](https://twitter.com/adamwathan/status/1257641015835611138)
[Ask HN: What's the best documentation you've ever read?](https://news.ycombinator.com/item?id=17399340)
[react-tiniest-router **troll**](https://github.com/kitze/react-tiniest-router#faq)

[Welcome to the Mailchimp Content Style Guide](https://styleguide.mailchimp.com/)
[Monzo tone of voice](https://monzo.com/tone-of-voice/)

[Amazon Writing Style Tips](https://twitter.com/maxinel_/status/1258644065995730944)
[(Notion) TM brand book](https://www.notion.so/Brand-book-e3e2678e40df453eaea065bd0e7a8f10)
[Technical Writing Courses](https://developers.google.com/tech-writing)
[17 tips for great copywriting](https://twitter.com/GoodMarketingHQ/status/1258052550739275776)

[How to Draw Useful Technical Architecture Diagrams](https://medium.com/the-internal-startup/how-to-draw-useful-technical-architecture-diagrams-2d20c9fda90d)
Very insightfull blog post, splitting diagrams into their own functions, in the article:
- Application Architecture Diagram
- Integration Architecture Diagram
- Deployment Architecture Diagram
- DevOps Architecture Diagram
- Data Architecture Diagram

[How to write CHANGELOGs](https://nosleepjavascript.com/how-to-write-changelogs/)
[Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#specification)
[What Makes a Good Changelog : programming](https://www.reddit.com/r/programming/comments/pcpq1e/what_makes_a_good_changelog/)
1. Changelogs Need to Be Clear
2. Highlight Changes in Images
3. Spotlight the People Behind the Product
4. Formatting Versions and Dates
5. Changelogs Need Engineering Time

[DOKS](https://github.com/wlezzar/doks)
> If you have documentation spread all over the place (github, google drive, etc.)
   and you want a centralized search tool over it, then doks is for you.

[Best Practices When Documenting Your Code for Software Engineers](https://betterprogramming.pub/best-practices-when-documenting-your-code-for-software-engineers-941f0897aa0)
* Google Tech Writing Course
* Using the [Divio](https://www.divio.com/)
            [Documentation Framework](https://documentation.divio.com/):

  Classify documentation in the following types:
    - Tutorials — Learning-oriented
    - How-To Guides — Problem-solving-oriented
    - Explanation — Understanding-oriented
    - Reference — Information-oriented

* Use Markdown-Based Systems for Documentation
  The biggest problem with MS Word / Excel etc... is that they're not searchable

* Using Mermaid JS for Diagrams

* Use Templates
  - Software Architecture Review Template
  - Architecture Decision Record Template
  - Incident Postmortem Template
  - DevOps Runbook
  - Decision Template
  - Writing Guidelines
  - OKR Template
  - Etc.

* Refer to Style Guides
  - [Microsoft Style Guide](https://docs.microsoft.com/en-us/style-guide/welcome/)
  - [Google Developer Documentation Style Guide](https://developers.google.com/style)



---

# Quest for the perfect Documentation

[Gitlab Documentation site architecture](https://docs.gitlab.com/ee/development/documentation/site_architecture/index.html)
* Algolia search engine
  The docs site uses [Algolia DocSearch](https://docsearch.algolia.com/) for its search function. This is how it works:
  GitLab is a member of the DocSearch program, which is the free tier of [Algolia](https://www.algolia.com/).

[Docusauros](https://docusaurus.io/)
Also uses Algolia for content search
May be interesting to look on how they handle i18n - See https://crowdin.com/ and [Chrome i18n](https://developer.chrome.com/docs/extensions/reference/i18n/)

## Documentation Structure

A lot of "famous" documentation seems to somewhat follow the [Divio Documentation system](https://documentation.divio.com/)

**Summary:**

|             |              Tutorials             |             How-to guides            |             Reference             |              Explanation              |
|:-----------:|:----------------------------------:|:------------------------------------:|:---------------------------------:|:-------------------------------------:|
| oriented to | learning                           | a goal                               | information                       | understanding                         |
| must        | allow the newcomer to get started  | show how to solve a specific problem | describe the machinery            | explain                               |
| its form    | a lesson                           | a series of steps                    | dry description                   | discursive explanation                |
| analogy     | teaching a small child how to cook | a recipe in a cookery book           | a reference encyclopedia article  | an article on culinary social history |

[ReactJS](https://reactjs.org/docs/getting-started.html)
* Installation
* Main Concepts
* Advanced Guides
* API Reference
* ... other products ... / recent / beta features

[Gatsby](https://www.gatsbyjs.com/docs/)
* Tutorial
* How-to Guides
* Reference
* Conceptual Guide

[Django](https://docs.djangoproject.com/en/3.1/#how-the-documentation-is-organized)
* Tutorials
* Topic Guides
* Reference Guides
* How-to guides

[Gitlab Document Type metadata](https://docs.gitlab.com/ee/development/documentation/index.html#document-type-metadata)
* `index`: It consists mostly of a list of links to other pages. Example page.
* `concepts`: The background or context of a subject. Example page.
* `howto`: Specific use case instructions. Example page.
* `tutorial`: Learn a process/concept by doing. Example page.
* `reference`: A collection of information used as a reference to use a feature or a functionality. Example page.
[More info at](https://gitlab.com/groups/gitlab-org/-/epics/1280)

## Documentation Testing

https://docs.gitlab.com/ee/development/documentation/index.html#live-example
* [Automatic Screenshot Generator](https://docs.gitlab.com/ee/development/documentation/index.html#automatic-screenshot-generator)
  Uses https://pngquant.org/ for lossy compression (often as much as 70% compression)

https://docs.gitlab.com/ee/development/documentation/testing.html
* [Lint checks](https://gitlab.com/gitlab-org/gitlab/-/blob/c39d19e9606575d9533aa9f6855b993bf98759c8/scripts/lint-doc.sh)
* Documentation link tests
* markdownlint
* Vale - is a grammar, style, and word usage linter for the English language.


## Misc

* Add a "Production Checklist" - [CockroachDB example](https://www.cockroachlabs.com/docs/stable/recommended-production-settings.html)

[The Surprising Power of Documentation](https://vadimkravcenko.com/shorts/proper-documentation/)
Documentation first, meetings second
$ Cost of meeting in Google Calendar event image, would be a great feature
[Reddit discussion](https://www.reddit.com/r/programming/comments/146vt0t/proper_documentation/)