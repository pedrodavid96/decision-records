[Part 1: Are We Really Engineers?](https://www.hillelwayne.com/post/crossover-project/are-we-really-engineers/)
[Part 2: We Are Not Special](https://www.hillelwayne.com/post/crossover-project/we-are-not-special/)
[Part 3: What engineering can teach (and learn from) us](https://hillelwayne.com/post/crossover-project/what-we-can-learn/)

[Code is run more than read](https://olano.dev/2023-11-30-code-is-run-more-than-read/)
[Code is run more than read : programming](https://www.reddit.com/r/programming/comments/187zf1d/code_is_run_more_than_read/)

[Three Terms in Software That We Should Consider Retiring](https://morethancoding.com/2020/01/27/three-terms-in-software-that-we-should-consider-retiring/)
1. “Sprint”
    > Long-term sustainable development is better done in pulses
    > Athletes can’t sprint all the time, and software teams can’t either.
2. “Culture Fit”
3. “Root Cause Analysis”

https://bekk.christmas/
A lot of articles about various topics (ux, functional programming, jvm...)

[I read ThoughtWorks Technology Radar vol.21 so you don’t have to](https://medium.com/virtuslab/i-read-thoughtworks-technology-radar-vol-21-so-you-dont-have-to-d75a5b0e0c85)
1. React Testing Library overcome Enzyme
2. GraphQL
3. JVM:
    * Micronaut
    * [Quarkus](https://quarkus.io/)
      A Kubernetes Native Java stack tailored for OpenJDK HotSpot and GraalVM, crafted from the best of breed Java libraries and standards.
    * GraalVM
    * Kotlin
        - Arrow
        - KotlinTest
        - Detekt

4. Security:
   [Open Policy Agent](https://www.openpolicyagent.org/)
5. Chaos testing Docker Network with [Pumba](https://github.com/alexei-led/pumba)
6. JIB
7. microK8s

[How to write a good software design doc](https://medium.com/free-code-camp/how-to-write-a-good-software-design-document-66fcf019569c)

[Mythical man month : 10 lines per developer day](https://blog.ndepend.com/mythical-man-month-10-lines-per-developer-day/)
Interesting take on LOC for a different metric.
Anedocte of a product where LOC were used not as a quality standard or to measure productivity but to measure estimated times per feature.

[Programming is not a goal.](https://ralsina.me/weblog/posts/programming-is-not-a-goal.html)
[Reddit discussion](https://www.reddit.com/r/programming/comments/g7bdzo/programming_is_not_a_goal/)
> You may be feel­ing like you are trapped in a cy­cle of just learn­ing things, us­ing them in a toy project and then... what? You learn some­thing else? And do an­oth­er toy pro­jec­t? And so on?
Your toy projects feel mean­ing­less be­cause they are not re­al.
STEP 1: Find something you need your computer to do that it doesn't do
STEP 2: Decide some basic details about your goal
STEP 3: Convince yourself that's a possible thing computers can do
STEP 4: Come up with a semi-rational mechanism to implement it
STEP 5: Come up with the most stupid version of the goal you can imagine
STEP 6: sit the fuck down and implement the stupid version
STEP 7: Show the stupid version to someone you respect, and listen
STEP 8: repeat step 5 to 7 with a slightly less stupid version
STEP 9: get help
STEP 10: get stuck

[Essays on Programming I Think About a Lot](https://www.jakeworth.com/essays-on-programming-i-think-about-a-lot/)
1. [Jeff Atwood, The First Rule of Programming: It’s Always Your Fault.](https://blog.codinghorror.com/the-first-rule-of-programming-its-always-your-fault/)
2. [Shawn Wang, Learn in Public.](https://www.swyx.io/learn-in-public/)
3. [David B. Hayes, Rubber Duck Debugging: The Psychology of How it Works.](https://www.thoughtfulcode.com/rubber-duck-debugging-psychology/)
4. [XY Problem](https://xyproblem.info/)
5. [Dan McKinley, Choose Boring Technology](https://mcfunley.com/choose-boring-technology)
6. [Elisabeth Hendrickson, Momentum > Urgency.](https://testobsessed.com/2020/02/momentum-urgency/)
7. [Scott Hanselman, Do they deserve the gift of your keystrokes?](https://www.hanselman.com/blog/do-they-deserve-the-gift-of-your-keystrokes)
8. [Joel Spolsky, The Iceberg Secret, Revealed.](https://www.joelonsoftware.com/2002/02/13/the-iceberg-secret-revealed/)
9. [Paul Slaughter, Conventional comments](https://conventionalcomments.org/)
10. [Kent C. Dodds, AHA Programming. The AHA (Avoid Hasty Abstractions)](https://kentcdodds.com/blog/aha-programming)
11. [Zed Shaw via Abhishek Nagekar, Advice From An Old Programmer.](https://www.nagekar.com/2018/06/advice-from-an-old-programmer-zed-shaw.html)
[Discussion](https://www.reddit.com/r/programming/comments/k7b2vj/essays_on_programming_i_think_about_a_lot/)
* A Plea for Lean Software, Niklaus Wirth
* The Emperor's Old Clothes, Charles Antony Richard Hoare.
* https://leancrew.com/all-this/2011/12/more-shell-less-egg/
* http://sasamat.xen.prgmr.com/michaelochurch/wp/2012/04/13/java-shop-politics/

[Software development topics I've changed my mind on after 6 years in the industry](https://chriskiehl.com/article/thoughts-after-6-years)
Solid list of changed opinions, new opinions and old opinions unchanged.
[Opinions after a decade of professional software engineering - Stargirl (Thea) Flowers](https://blog.thea.codes/opinions-after-a-decade/)
Inspired by the previous article.
[Found in](https://twitter.com/munificentbob/status/1356695750361575424)
[Links to shared twitter](https://mobile.twitter.com/theavalkyrie/status/1356685976341471232)
* What I've changed my mind about
    > Code isn't even close to most important thing about software engineering.

    > Writing code with others leads to better software and better software engineers.

    > Legacy code isn't inherently bad or wrong. It's software created by people who did the best they could with the knowledge and constraints they had at the time, and it's very rare to be able to understand the complete context around a legacy codebase.

    ...

* New opinions
    > Communication skills and empathy are the most important skills for any engineering discipline

    > While we're talking about people, Accessibility isn't optional.

    > Documentation, tutorials, examples, etc are critical. Technical writers are extremely valuable.

    > Mentorship is critical for software engineering

    > Code review is necessary, important, and hard

    > Use an automatic formatter whenever possible

    ...

* Opinions that have stayed the same
    > Open source is the heart of software engineering and the key to empowering people with technology.

    > Being involved with a community is the best way to grow and learn.

    > Software can be art

    > Gatekeeping and elitist attitudes are horseshit.

    ...

[About Pool Sizing](https://github.com/brettwooldridge/HikariCP/wiki/About-Pool-Sizing)
Awesome article 🥇⭐
Effective starting point (provided by the PostgreSQL project):
```
connections = ((core_count * 2) + effective_spindle_count)
```
In short:
* smaller pools are better
* the faster the bottlenecks (disk, network) the smaller the pool can be
* the most difficult cases are often a mix of short and long running tasks.
  In those cases consider different dedicated pools

---

[Suspended from Google Play for listing supported subtitle formats](https://github.com/moneytoo/Player/issues/37) - one of which was the `ASS` format
[Reddit Discussion](https://www.reddit.com/r/programming/comments/l4ws93/app_suspended_from_google_play_for_listing/)
https://en.wikipedia.org/wiki/Scunthorpe_problem
> The Scunthorpe problem is the unintentional blocking of websites, e-mails, forum posts or search results by a spam filter or search engine because their text contains a string (or substring) of letters that appear to have an obscene or otherwise unacceptable meaning.

[Technologies & Tools to Watch in 2021 | by Yitaek Hwang | Dev Genius | Medium](https://medium.com/dev-genius/technologies-tools-to-watch-in-2021-a216dfc30f25)
* Pulumi
* Terragrunt & TFSEC
* Tekton - CI / CD focused in k8s
* Trivy - vulnerability scanning
* ShellCheck
* Pitest/Stryker - mutation testing
* Litmus - Chaos Monkey - lightweight Kubernetes operator consisting of ChaosEngine, ChaosExperiment, and ChaosResult.

[10 Software Engineering Laws Everybody Loves to Ignore](https://www.netmeister.org/blog/software-engineering-laws.html)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/lpq7ix/10_software_engineering_laws_everybody_loves_to/)
1. Conway's Law
2. Brooks's Law
3. Zawinski's Law
4. Parkinson's Law
5. Pareto's Fallacy
6. Sturgeon's Revelation
7. The Peter Principle
8. Eagleson's Law
9. Greenspun's 10th Rule of Programming
10. The Iceberg Fallacy
11. The LGTM Dilemma

[Use Google like a pro](https://markodenic.com/use-google-like-a-pro/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/mwnz3s/googling_is_one_of_the_most_important_skills_for/)
1. Use quotes to force an exact-match search:
2. AND operator will return only results related to both terms:
3. You can use the OR operator to get the results related to one of the search terms
4. - operator will exclude results that contain a term or phrase:
5. You can use the (*) wildcards as placeholders, which will be replaced by any word or phrase.
6. Search inside a single website:
7. You can also use a very useful feature that helps to find a specific file type.
8. Search for a range of numbers:
9. Use the `before` operator to return only results before a given date
10. Use the `after` operator to return only results after a given date
