# Kotlin

[Static Analysis](https://proandroiddev.com/kotlin-static-analysis-why-and-how-a12042e34a98)
[ktlint](https://ktlint.github.io/)
[ktlint | github](https://github.com/pinterest/ktlint)

[Making Custom Kotlin Code Inspection Plugin](https://medium.com/@elye.project/making-custom-kotlin-code-inspection-plugin-ec1aedeffa48)

[How to make sense of Kotlin coroutines | Joffrey Bion](https://proandroiddev.com/how-to-make-sense-of-kotlin-coroutines-b666c7151b93)

[Sealed Classes Instead of Exceptions in Kotlin](https://phauer.com/2019/sealed-classes-exceptions-kotlin/)

[Kotlin: The Next Frontier in Modern (Meta)Programming](https://towardsdatascience.com/kotlin-the-next-frontier-in-modern-meta-programming-8c0ac2babfaa)

[Type-Safe Domain Modeling in Kotlin](https://medium.com/better-programming/type-safe-domain-modeling-in-kotlin-425ddbc73732)
* Valiktor to implement readable data checks, with few lines of code.
* Konad to improve compile-time safety. -> `Result`

[Any news on Java 11 support? : Kotlin](https://www.reddit.com/r/Kotlin/comments/90cqno/any_news_on_java_11_support/)
[Kotlin Support for Java 9 Module System? - Kotlin Discussions](https://discuss.kotlinlang.org/t/kotlin-support-for-java-9-module-system/2499/10)

[Comprehension questions: initial table creation and migration · Issue #1605 · cashapp/sqldelight](https://github.com/cashapp/sqldelight/issues/1605)
> Personally I use user_version pragma to detect if database is new or existed before.
>  If user_version = 0 I create schema and I upgrade schema if needed otherwise.
> Yes, you should update sq files and add migration scripts.

[Gurupreet/ComposeCookBook: A Collection on all Jetpack compose UI elements, Layouts, Widgets and Demo screens to see it's potential](https://github.com/Gurupreet/ComposeCookBook)

[GitHub - dokar3/mini-coroutines: Kotlin coroutines runtime in 400 lines](https://github.com/dokar3/mini-coroutines/tree/main)

---

[Coroutines, Java Virtual Threads and Scoped Values - Language Design - Kotlin Discussions](https://discuss.kotlinlang.org/t/coroutines-java-virtual-threads-and-scoped-values/28004/5)
> Kotlin coroutines can already be run on Loom’s virtual threads and in some scenarios it performs better, according to this article:
  [Running Kotlin coroutines on Project Loom's virtual threads](https://kt.academy/article/dispatcher-loom)
  ```
  val Dispatchers.LOOM: CoroutineDispatcher
    get() = Executors.newVirtualThreadPerTaskExecutor().asCoroutineDispatcher()
  ```
> as it was said above, not everything can be done with VTs. Coroutines use cooperative scheduling, VTs are pre-emptive.
> Everything which can be done using Kotlin coroutines can be done utilizing non-public API from the Project Loom. My example: GitHub - Anamorphosee/loomoroutines: Library for the native Java coroutines utilizing Project Loom
  https://github.com/Anamorphosee/loomoroutines