# Java

[A Java geek](https://blog.frankel.ch/page/5/) - Looks like a very cool blog

[A categorized list of all Java and JVM features since JDK 8 to 16](https://advancedweb.hu/a-categorized-list-of-all-java-and-jvm-features-since-jdk-8-to-16/)

[JSpecify Main | JSpecify](https://jspecify.dev/)
Standard Annotations for Java Static Analysis
(Nullable)

[java - Can the Pluggable Annotation Processor API retrieve source code comments? - Stack Overflow](https://stackoverflow.com/questions/7932336/can-the-pluggable-annotation-processor-api-retrieve-source-code-comments)

## XDG

[harawata/appdirs: A small utility that provides cross platform access to the special folders/directories like application data.](https://github.com/harawata/appdirs)
[Technical Q&A QA1170: Important Java Directories on Mac OS X](https://developer.apple.com/library/archive/qa/qa1170/_index.html)
[macos - Equivalents of XDG_CONFIG_HOME and XDG_DATA_HOME on Mac OS X? - Stack Overflow](https://stackoverflow.com/questions/3373948/equivalents-of-xdg-config-home-and-xdg-data-home-on-mac-os-x)

## Project Jigsaw / Module System

[OpenJDK Quick-Start Guide](http://openjdk.java.net/projects/jigsaw/quick-start)
[The State of the Module System](http://openjdk.java.net/projects/jigsaw/spec/sotms/)
[Code-First Java Module System Tutorial](https://blog.codefx.org/java/java-module-system-tutorial/)
[Gradle Java 9 modules tutorial](https://guides.gradle.org/building-java-9-modules/#what_you_ll_need)
[Right way to expose a private Java 9 module's package to JUnit](https://stackoverflow.com/questions/49285412/what-is-the-right-way-to-expose-a-private-java9-modules-package-to-junit)
[A Guide to Java 9 Modularity | Baeldung](https://www.baeldung.com/java-9-modularity)
[Java 9 refflection and package access Changes](https://dzone.com/articles/java-19-reflection-and-package-access-changes)

[jlink](https://docs.oracle.com/javase/9/tools/jlink.htm#JSWOR-GUID-CECAC52B-CFEE-46CB-8166-F17A8E9280E9)
> You can use the jlink tool to assemble and optimize a set of modules and their dependencies into a custom runtime image.
[Java Modules: Why and How?](https://youtu.be/DItYExUOPeM) by Venkat Subramaniam
[Code-First Java Module System Tutorial // nipafx](https://nipafx.dev/java-module-system-tutorial/)

## Profiling

[JVM async profiler](https://github.com/jvm-profiling-tools/async-profiler/)

## Java on the Client

[OpenJFX + GraalVM](https://www.reddit.com/r/programming/comments/btz8nx/a_boost_for_java_on_the_client/)

[Bringing Opsian's Continuous Profiling to GraalVM](https://www.opsian.com/blog/continuous-profiling-on-graalvm/)

## Docker

[Remote Debugging (in the containers)](https://solidstudio.io/blog/debugging-java-applications-running-on-docker.html)

[Java Dev Environments with Containers](https://medium.com/@brunoborges/java-dev-environments-with-containers-66d6797b2753)

## Dependency Injection with Service Loader

[serviceloader and file watcher | Google search](https://www.google.com/search?client=firefox-b-d&q=serviceloader+and+file+watcher)

[Simple Dependency Injection with ServiceLoader in JDK 6 ](https://dzone.com/articles/simple-dependency-injection-wi)

[Dynamically loading plugin jars using ServiceLoader](https://stackoverflow.com/questions/16102010/dynamically-loading-plugin-jars-using-serviceloader)

[Dependency Injection is a 25-dollar term for a 5-cent concept.](https://www.reddit.com/r/programming/comments/8cwa4o/dependency_injection_is_a_25dollar_term_for_a/)

[Inversion of Control Containers and the Dependency Injection pattern | Martin Fowler](https://www.martinfowler.com/articles/injection.html)

[Butter Knife](https://jakewharton.github.io/butterknife/)

## Misc

[Variance in Java](https://llorllale.github.io/java-variance/)

[Call Blocking method with a timeout](https://stackoverflow.com/questions/1164301/how-do-i-call-some-blocking-method-with-a-timeout-in-java)

[Java Bindings for Rust: A Comprehensive Guide](https://akilmohideen.github.io/java-rust-bindings-manual/title.html)

## [Micronaut](https://micronaut.io/index.html)

A modern, JVM-based, full-stack framework for building modular, eassily testable microservice and serverless applications.

**Leverages compile time computation**, leading to:

* Fast startup time
* Lower memory consumption

**Leverages GraalVM**

### [Micronaut for Microservices](https://medium.com/software-tidbits/micronaut-for-microservices-7322cc00fb6f)

## JVM / GC

[G1GC Fundamentals: Lessons from Taming Garbage Collection](https://product.hubspot.com/blog/g1gc-fundamentals-lessons-from-taming-garbage-collection)

[Why 35GB Heap is Less Than 32GB – Java JVM Memory Oddities](https://blog.codecentric.de/en/2014/02/35gb-heap-less-32gb-java-jvm-memory-oddities/)

[High performance at low cost – choose the best JVM and the best Garbage Collector for your needs](https://blog.oio.de/2020/01/13/high-performance-at-low-cost-choose-the-best-jvm-and-the-best-garbage-collector-for-your-needs/)

[Tricks of the Trade: Tuning JVM Memory for Large-scale Services](https://eng.uber.com/jvm-tuning-garbage-collection/)
1. Discern if JVM memory tuning is needed
2. Choose the right total heap size
3. Choose the right Young Generation heap size
4. Determine the most impactful GC parameters
5. Test next generation GC algorithms

## JavaFX

[(JavaFX) SmartGraph](https://github.com/brunomnsilva/JavaFXSmartGraph)
Very cool graphs and animations

[JavaFX HiDPI Support](https://stackoverflow.com/questions/26182460/javafx-8-hidpi-support)

---

[Unit Test Your Architecture with ArchUnit](https://blogs.oracle.com/javamagazine/unit-test-your-architecture-with-archunit)
* Package dependency checks
* Class dependency checks
* Class and package containment checks
* Inheritance checks
* Annotation checks
* Layer checks
* Cycle checks

[Java Objects Inside Out](https://shipilev.net/jvm/objects-inside-out/)

[Continuous Profiling of a JVM application in Kubernetes](https://www.opsian.com/blog/profiling-jvm-applications-kubernetes/)


[Java Flight Recorder (Java 14 Profiler)](https://blogs.oracle.com/javamagazine/java-flight-recorder-and-jfr-event-streaming-in-java-14)


[Oracle GraalVM announces support for Nashorn migration](https://medium.com/graalvm/oracle-graalvm-announces-support-for-nashorn-migration-c04810d75c1f)

[Isolates and Compressed References: More Flexible and Efficient Memory Management via GraalVM](https://medium.com/graalvm/isolates-and-compressed-references-more-flexible-and-efficient-memory-management-for-graalvm-a044cc50b67e)

[Lessons learned about monitoring the JVM in the era of containers](https://medium.com/thron-tech/lessons-learned-about-monitoring-the-jvm-in-the-era-of-containers-47e7fe0b77dc)
[reddit discussion](https://www.reddit.com/r/programming/comments/fgbmgp/lessons_learned_about_monitoring_the_jvm_in_the/)

## Instrumentation

[Package java.lang.instrument](https://docs.oracle.com/en/java/javase/16/docs/api/java.instrument/java/lang/instrument/package-summary.html)
Provides services that allow Java programming language agents to instrument programs running on the JVM.
The mechanism for instrumentation is modification of the byte-codes of methods.

[How to Create a Java Agent and Why Would You Need One?](https://www.youtube.com/watch?v=tlcF8awgUEE)

Example of usage:
[JRebel](https://www.jrebel.com/products/jrebel)
> Reload Code Changes Instantly / HotReload / Hot Reloading

Their tutorial on [How to Write a Javaagent](https://www.jrebel.com/blog/how-write-javaagent)

---

[How much faster is Java 17? : programming](https://www.reddit.com/r/programming/comments/pow55r/how_much_faster_is_java_17/)


[Introduction to OpenRewrite - OpenRewrite](https://docs.openrewrite.org/)
[GitHub - openrewrite/rewrite-checkstyle: Eliminate Checkstyle issues. Automatically.](https://github.com/openrewrite/rewrite-checkstyle)

[The jpackage Command](https://docs.oracle.com/en/java/javase/14/docs/specs/man/jpackage.html)

## Annotation Processors

[Annotation Processing : Don’t Repeat Yourself, Generate Your Code. | by Mert SIMSEK | Medium](https://iammert.medium.com/annotation-processing-dont-repeat-yourself-generate-your-code-8425e60c6657)
[square/javapoet: A Java API for generating .java source files.](https://github.com/square/javapoet)

## Misc

[Sandboxing in Java Applications](https://blog.pterodactylus.net/2013/07/19/sandboxing/)
[Scripting Java 11, Shebang And All // nipafx](https://nipafx.dev/scripting-java-shebang/)
[How to make a Windows Notification in Java - Stack Overflow](https://stackoverflow.com/questions/34490218/how-to-make-a-windows-notification-in-java)

## Patterns

### Singleton

There is a lot of bad things to say about Singleton.
That said there are resources on the computer that are exactly that, a Single Instance, and should be treated that way, and If we're already using this pattern we might as well use it correctly.

In Java:

Implemented through Factory or Public Static Field.
Static Field makes it clear it's a Singleton and it's simpler to implement.
Factory makes it possible to later change to a no Singleton implementation (but If that is an alternative, just do that beforehand?).

Special cares should be taken to account Serialization. (ReadResolve...)
Another approach is using an enum type with a Single Element, exactly the same as the public static field but more concise and provides the serialization machinery for free.

### Factory

Unlike constructors, they have names. Avoid multiple confusing constructors with different parameters (avoided if the language supports named parameters).
They do not require to create a new Object each time they're invoked.
They can return an object of any subtype of their return type.

In some languages that use generics, templates, constructor can be more verbose than a method call.

### Builder

Named parameters and/or a good API can usually avoid the need of a builder.

Still some advantages:
An object being built doesn't have to be in an inconsistent state, and the final object can be immutable.

---

[Selfie JVM Snapshot Testing](https://selfie.dev/jvm)

## Debugging

[GitHub - spotify/threaddump-analyzer: A JVM threaddump analyzer](https://github.com/spotify/threaddump-analyzer)
[Java Thread Synchronization Problems & Thread Dump Analysis | by Tharindu Sathischandra | Medium](https://tharindus.medium.com/java-thread-dump-analysis-why-how-141c4fbea837)

## Build Tools

### Maven

#### `mvnd`

Maven downloading from every repo sequentially is super slow.
`mvnd` helps with that.

That said, `mvnd` is not respecting my XDG setup.
A few quick searches:

[Local settings.xml results current folder used as repository · Issue #588 · apache/maven-mvnd · GitHub](https://github.com/apache/maven-mvnd/issues/588)
Not exactly the same problem but links to potentially relevant issues.

[The ${MVND_HOME}/mvn/conf/setting.xml is not used · Issue #553 · apache/maven-mvnd · GitHub](https://github.com/apache/maven-mvnd/issues/553)

[mvnd assumes an .m2 folder in home directory · Issue #360 · apache/maven-mvnd · GitHub](https://github.com/apache/maven-mvnd/issues/360)