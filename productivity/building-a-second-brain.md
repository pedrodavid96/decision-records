# Building a Second Brain

[Recall - Summarize Anything, Forget Nothing](https://www.getrecall.ai/)

Tiago Forte

## Introduction: The Promise of a Second Brain

32 Most commonly, information is for the future instead of now.
33 We need a way to package it to our future self
36 "It all begins with the simple act of writing things down"
   Capture
37 Field of PKM (Personal Knowledge Management)
42 Building a Second Brain system:
   Find anything you've learned, touched, or thought about
   Organize your knowledge to move your **projects** and **goals** forward more consistently
   Save your best thinking so that you don't have to do it again.
   Connect ideas and notice patterns across different areas
   Reliable system to share your work more confidently and with more ease
   **Turn work "off and relax, knowing you have a trusted system keeping track of all details**
   Spend less time looking for things, and more time doing the best, most creative work you are capable of.
50 Like a bicycle for the mind, enchance cognitive abilities to accelerate us toward our goals
54 Digital archive to help you manage your life without having to keep every detail in your head
58 "System to manage the ever-increasing volume of information puring into their brains."
61 "Those who continue to rely on their fragile biological brains will become ever more overwhelmed by the explosive growth in the complexity of our lives."

## Part One: The Foundation: Understanding What’s Possible

### Chapter 1: Where it all Started

84 Your mind is for having ideas, not holding them.—David Allen, author of Getting Things Done
98 Writing things down (36)
   To be efficient at this, writing / typing fast can have compound benefits. See Ali Abdaal advice and how typing fast during patient meetings helped a lot.
132 school in the eastern Ukrainian countryside,
   Memo: first read this while flying to krakow
141 "Moving straight from rural Ukraine to the epicenter of Silicon Valley, I was utterly unprepared for the constant barrage of inputs that is a normal part of modern workplaces."
190 "You may find this book in the “self-improvement” category, but in a deeper sense it is the opposite of self-improvement. It is about optimizing a system outside yourself, a system not subject to your limitations and constraints, leaving you happily unoptimized and free to roam, to wonder,"

### Chapter 2: What is a Second Brain?

204 Your quality of life depends on your ability to manage information effectively.
    Stat: New York Times: Average person's daily consumption of information now adds up to 34 gigabytes
207 Stat: The Times estimates that we consume the equivalent of 174 full newspapers' worth of content every day, 5x higher than in 1986
209 "Information Overload has become Information Exhaustion, taxing our mental resources and leaving us constantly anxious that we’re forgetting something."
214 Stat: International Data Corporation found that 26% of typical knowledge worker's day is looking and consolidating spread information
    Only 56% of the time they're able to find the information required to do their job
221 Energy spent straining to recall things is energy not spent on human strengths: inventing new things, craftings stories, recognizing patterns, following intuition, collaborating with others, investigating new subjects, planning and testing theories.
    It's also **time not used in more meaningful pursuits like cooking, self-care, hobbies, resting and spending time with loved ones**.
240 "by keeping an account of your readings, you made a book of your own, one stamped with your personality."
270 In past centuries, only intellectual elite needed commonplace books. ... . Nowadays, almost everyone needs a way to manage information
279 Most people notetaking notion was formed in school, where learning was treated as essetially disposable (could be forgotten after the exam)
    Idea: This made me think that schools shouldn't be teaching you and testing on easibly googld facts (e.g.: almost all of history class). What if it was treated more like a book reading club? With space for more debate / discussin and "no exams".
282 In the professional world it's not clear what you should be taking notes on. No one tells you when or how your notes will be used.
    The "test" can come at any time and in any form.
    You're allowed to reference your notes.
    You're expected to take action on your notes, not just regurgitate them.
289 A note can be:
    * a passage from a book or article
    * a photo or image from the web with your annnotations (pinterest self notes)
    * bullet-point list of your madering thoughts on a topic
    * a single quote from a film that really struck you
    * thousands of words saved from an in-depth book
292 "has been interpreted through your lens, curated according to your taste, translated into your own words,"
312 Example: after a morning spent fighting fires, she’s far too scatterbrained and tired to focus.
313 "Like so many times before, Nina lowers her expectations, settling for chipping away slowly at her ever-expanding to-do list full of other people’s priorities"
318 "By the time Nina gets set up and ready to go, she’s far too tired to make real progress. This pattern repeats itself day after day. After enough of these false starts, she starts to give up."
322 "She isn’t meeting her own standards for what she knows she’s capable of."
337 You dictate a quick audio memo to your phone as you drive, which gets automatically transcribed and saved in your notes.
    TODO: I should leverage this one more
366 Your brain is no longer the bottleneck on your potential

### Chapter 3: How a Second Brain Works

415 "The same way you would hold your assistant accountable to a certain standard of performance, the same is true for your Second Brain."
    Your Second Brain can be thought as your assistant, and should be accountable to the same performance standards.
422 "Revealing new associations between ideas." (221 recognizing patterns and intuition)
    Note: This is specially true when using something like roam research or obsidian
431 Linus approach of building physical models of molecules: Seems like a name for success
437 turned to the most basic, ancient medium available: physical stuff. Digital notes aren’t physical, but they are visual.
    Note: They are concrete instead of something just "in the mind"
457 Even during a "brainstorm", we're relying on current ideas.
    What are the chances that the most creative & innovative will instantnly pop? This tendency is known as **recency bias**
463 "It is a calmer, more sustainable approach to creativity that relies on the gradual accumulation of ideas, instead of all-out binges of manic hustle."
466 Gathering the ideas of others
    Note: Capturing
491 "If it feels like inspirations has run dry, it’s because you need a deeper well full of examples, illustrations, stories, statistics, diagrams, analogies, metaphors, photos, mindmaps, conversation notes, quotes—anything that will help you argue for your perspective or fight for a cause you believe in."
491 Just like a paper notebook might contain drawings and sketches, quotes and ideas, and even a pasted photo or Post-it,
    TODO: Improve my second brain to ease media imports
493 "notes are inherently messy"
    TODO: Don't stress about it
495 Taking notes is a continuous process that never really ends,
    Note: Don't stress about this as well
510 "Most important of all, don’t get caught in the trap of perfectionism: insisting that you have to have the “perfect”app with a precise set of features before you take a single note. It’s not about having the perfect tools—it’s about having a reliable set of tools you can depend on, knowing you can always change them later."
529 The ultimate purpose of a Second Brain is creating new things.
    A lot of knowledge on a subject turn into something concrete and shareable
    Note: Show your work from Austin Kleon
581 The best way to organize notes is "for action", according to the active projects. "How is this going to help me move forward on my current projects?".
    Surprisingly, the vast amount of information out there gets radically streamlined and simplified.
    **There are relatively few things that are actionable and relevant at any given time,**
585 "Organizing for action gives you a sense of tremendous clarity, because you know that everything you’re keeping actually has a purpose. You know that it aligns with your goals and priorities."
595 "distill your notes down to their essence."
597 "Einstein famously summarized his revolutionary new theory of physics with the equation E =mc2. If he can distill his thinking into such an elegant equation, you can surely summarize the main points of any article, book, video, or presentation so that the main point is easy to identify."
    This is important so you can quickly find just the main takeaways when you need.
602 "Every time you take a note, ask yourself, “How can I make this as useful as possible for my future self?”"
605 "Think of yourself not just as a taker of notes, but as a giver of notes—you are giving your future self the gift of knowledge that is easy to find and understand."
608 The ultimate purpose is sharing your own ideas (529), your own story
614 "We study every piece of relationship advice available, but never ask anyone out on a date."
    Memo: Or we buy dog training books without having a dog
619 This is why I recommend you shift as much of your time and effort as possible from consuming to creating.
630 "There is no need to wait to get strated"
635 "note the dozens of tabs you likely have open in your browser."
    Memo: I took that personally
637 "All you need is a slightly more intentional, more deliberate way to manage that information, plus a few practical habits to ensure it gets done."

## Part Two: The Method: The Four Steps of CODE

### Chapter 4: Capture—Keep What Resonates

664 "Everything not saved will be lost. —Nintendo “Quit Screen” message"
    Memo: 😂😂😂
677 "Maybe you are already a diligent organizer, but you’ve fallen into a habit of “digital hoarding” that doesn’t end up enriching your life."
    Memo: I'm somewhere there
694 For Swift, writing songs is not a discrete activity that she can do only at certain times and in certain places. It is a side effect of the way her mind works, spinning off new metaphors and turns of phrase at the most unexpected times:
757 choose the two to three kinds of content from the two lists above that you already have the most of and already value
    TODO: do this
759 start small and get your feet wet before diving into the deep end.
763 The content you save in your notes is easily accessible from any device, **which is great for accessibility but not for security.**
771 if you **need multiple people to be able to collaboratively edit a document in real time**, then you’ll need to **use a different platform**.
788 "This cross-disciplinary approach allowed him to make connections across seemingly unrelated subjects, while continuing to follow his sense of curiosity." about Richard Feynman, Nobel Prize-winning physicist
797 When a fellow physicist and mentor asked what the use of such an insight was, Feynman responded: **“It doesn’t have any importance… I don’t care whether a thing has importance. Isn’t it fun?”**
812 How do I start reading all the books I already have instead of buying more?
    TODO: Stop buying books for now...
820 may change, but even as we move between projects, jobs, relationships, and careers, our favorite problems tend to follow us across the years.
833 Don’t worry about getting the list perfect (this is just a first pass, and it will always be evolving).
    TODO: Take a moment to write down some of your own favorite problems.
842 Your first instinct might be to save the article in its entirety.
    The problem is that even if you spend 20/30 minutes to consume it now, in the future you'll have to spend all that time again.
845 "You also don’t want to just bookmark the link and save it to read later, because then you won’t know what it contains in the first place!"
851 "Don’t save entire chapters of a book—save only select passages."
855 "The biggest pitfall I see people falling into once they begin capturing digital notes is saving too much."
    Note: First time I read this I was thinking that up until then the book just recommended "just capture more" and this was against that. I would say this should be already the first pass, where we remoce unnecessary information
859 "The more economical you can be with the material you capture in the first place , the less time and effort your future self will have to spend organizing, distilling, and expressing it."
865 "There is a way to evoke a sense of inspiration more regularly: keep a collection of inspiring quotes, photos, ideas, and stories."
867 "For example, I keep a folder full of customer testimonials I’ve received over the years. Any time I think what I’m doing doesn’t matter or isn’t good enough, all I have to do is open up that folder and my perspective is completely shifted."
881 "I often save screenshots of text messages sent between my family and friends. The small moments of warmth and humor that take place in these threads are precious to me, since I can’t always be with them in person. It takes mere moments, and I love knowing that I’ll forever have memories from my conversations with the people closest to me."
885 **"We have a natural bias as humans to seek evidence that confirms what we already believe, a well-studied phenomenon known as “confirmation bias.”"**
    That isn’t what a Second Brain is for.
889 "If you’re not surprised, then you already knew it at some level, so why take note of it?"
894 "We are already surrounded by algorithms that feed us only what we already believe and social networks that continually reinforce what we already think."
902 When you **use up too much energy taking notes, you have little left over** for the subsequent steps that add far more value: **making connections, imagining possibilities, formulating theories, and creating new ideas of your own.**
914 Designing for Behavior Change:
    TODO: To check
924 I can’t think of anything more important for your creative life—and your life in general—than **learning to listen to the voice of intuition inside.**
927 It’s a good idea to capture key information about the source of a note, such as the **original web page address, the title of the piece, the author or publisher, and the date it was published**.
    TODO: Add this to my Second Brain automations
978 **You're more likely to remember information you've written in your own words**
986 stat: significant evidence that expressing our thoughts in writing can lead to benefits for our health and well-being.
988 “translating emotional events into words leads to profound social , psychological , and neural changes.”
995 **The moment you first encounter an idea is the worst time to decide what it means.**
998 Many of the items previously saved to read later are clearly trivial and unneeded
1005 **What would capturing ideas look like if it was easy?**

### Chapter 5: Organize—Save for Actionability

1123 **Organize by project**
    TODO: This
1126 PARA: Projects, Areas, Resources, and Archives.
1129 it organizes information based on how actionable it is, not what kind of information it is.
1161 "The promise of PARA is that it changes “getting organized” **from a herculean, never-ending endeavor into a straightforward task to get over with so you can move on to more important work.**"
1186 **Knowing which projects you’re currently committed to is crucial to being able to prioritize** your week, plan your progress, and say no to things that aren’t important.
1211 While there is no goal to reach, there is a standard that you want to uphold in each of these areas.
1218 This is basically a catchall for anything that doesn’t belong to a project or an area and could include any topic you’re interested in gathering information about.
1234 Unlike with your house or garage, there is no penalty for keeping digital stuff forever, **as long as it doesn’t distract from your day-to-day focus.**
1273 "the moment you first capture an idea is the worst time to try to decide what it relates to." (995)
1275 **"forcing yourself to make decisions every time you capture something adds a lot of friction to the process. This makes the experience mentally taxing and thus less likely to happen in the first place."**
1277 “keeping what resonates”in the moment is a separate decision from deciding to save something for the long term.
1286 In which project will this be most useful?
     If none: In which area will this be most useful?
     If none: Which resource does this belong to?
     If none: Place in archives.
1299 **The goal of organizing our knowledge is to move our goals forward, not get a PhD in notetaking.** Knowledge is best applied through execution, which means whatever doesn’t help you make progress on your projects is probably detracting from them.
1303 Analogy:
    1. The archives are like the freezer— items are in cold storage until they are needed, which could be far into the future.
    2. Resources are like the pantry— available for use in any meal you make, but neatly tucked away out of sight in the meantime.
    3. Areas are like the fridge— items that you plan on using relatively soon, and that you want to check on more frequently.
    4. Projects are like the pots and pans cooking on the stove— the items you are actively preparing right now.
1307 **Imagine how absurd it would be to organize a kitchen instead by kind of food: fresh fruit, dried fruit, fruit juice, and frozen fruit would all be stored in the same place, just because they all happen to be made of fruit.**
1310 Organize ideas according to where they are going / the outcomes that they can realize instead of where they come from.
1330 Using PARA is not just about creating a bunch of folders.
     It is about identifying the structure of your work & life and what you are commited, what you want to change and **where you want to go**.
1372 Move quickly and touch lightly
1372 author previous standard work approach was "brute force"
     Stay late at the office, productivity every single minute and power through mountains of work.
     This was a path to burnout - exhaustion of mental and physical reserves.
     It wasn't even effective.
     Intentionless and without strategy.
     Could be acomplished with minimal effort.
1377 **"look for the path of least resistance and make progress in short steps."**
1377 **"don’t make organizing your Second Brain into yet another heavy obligation. Ask yourself: “What is the smallest, easiest step I can take that moves me in the right direction?”"**

### Chapter 6: Distill—Find the Essence

1443 "I think it’s important to put your impressions down on the first reading because those are the initial instincts about what you thought was good or what you didn’t understand or what you thought was bad.” - Francis Cappola's Notebook
1459 Creating a movie depends on collecting and refining source material.
1462 **"This is the moment we begin turning the ideas we’ve captured and organized into our own message."**

1467 Our notes are things to use, not just things to collect.
1468 When you initially capture a note, you may have only seconds to get it into your Second Brain
1469 Not nearly enough time to fully understand what it means or how it might be used.
1488 The most important factor in whether your notes can survive that journey into the future is their discoverability
1495 enhance discoverability by highlighting the most important points.
1503 Paradoxically, the more notes they collect, the less discoverable they become!
1504 This realization tends to either discourage them from taking any notes in the first place, or alternatively, to **keep switching from one notetaking tool to another every time the volume gets overwhelming.**
1506 _You already distill when critical, i.e._ "What do you do when communicating with a very busy, very impatient, very important person? You distill your message down to the key points and action steps. When you send an email to your boss, you don’t bury your request somewhere near the bottom of a massive wall of text. You identify the most urgent questions that you need them to respond to right at the top of the email."
1510 **Distillation is at the very heart of all effective communication.**
1513 Highlighting 2.0: The Progressive Summarization Technique
1515 It is a simple process of taking the raw notes you’ve captured and organized and distilling them into usable material that can directly inform a current project.
1518 and then highlight the main points of those highlights, and so on, distilling the essence of a note in several “layers.”
1526 I have my read later app synced to my digital notes app, so any passage I highlight there automatically gets saved in my notes, including a link to the source.
     TODO: Automate this
1534 I need to add a second layer of distillation. I usually do this when I have free time during breaks or on evenings or weekends, when I come across the note while working on other projects, or when I don’t have the energy for more focused work.
    Own words: This is a good "low effort" task for when you don't have energy for more focused work.
    It can be merged together with the advice (TODO: find source) of that one guy that wrote a bit on every break, even if only for 2 minutes.
1536 keywords that provide hints of what this text is about, phrases that capture what the original author was trying to say, or sentences that especially resonated with me even if I can’t explain why.
1552 For only the very few sources that are truly unique and valuable, I’ll add an “executive summary” at the top of the note with a few bullet points summarizing the article in my own words.
1554 The best sign that a fourth layer is needed is when I find myself visiting a note again and again, clearly indicating that it is one of the cornerstones of my thinking.
1561 Since the takeaways are already in my own words, they are easy to incorporate into whatever I’m working on. Speed is everything when it comes to recall: you have only a limited amount of time and energy, and the faster you can move through your notes, the more diverse and interesting ideas you can connect together.
1572 sometimes you want to zoom in and examine one specific research finding, while other times you want to zoom out and see the broad sweep of an argument all at once.
    Personal note: roam research or obsidian seems good for this zoom out
1578 Highlighting can sometimes feel risky.
1579 The multiple layers of Progressive Summarization are like a safety net; if you go in the wrong direction, or make a mistake, you can always just go back to the original version and try again. Nothing gets forgotten or deleted.
1581 Progressive Summarization helps you focus on the content and the presentation of your notes, IV instead of spending too much time on labeling, tagging, linking, or other advanced features offered by many information management tools.
1583 It gives you a practical, easy thing to do that adds value even when you don’t have the energy for more challenging tasks.
    (same as 1534)
1584 it keeps your attention on the substance of what you’re reading or learning, which is what matters in the long term.
1595 When I first captured the note, I didn’t have time to add tags, highlights, or an executive summary of my own. I saved it to a resource folder for “Economics” to revisit later.
1596 when it came up in a search for “wages,” I took a few moments to bold a couple of key sentences and highlight the most important one,
1612 To get started, I moved the entire folder from resources to projects. Then I spent about thirty minutes to review the notes it contained and highlight the most relevant takeaways. Those highlights were the starting point for the hiring process I ended up using for my own business, inspired by one of the most innovative and desirable employers in the world.
1621 It was a relief to hear that we weren’t alone in our struggles.
     While earing a podcast about starting a business
1622 I sat in the car for a few minutes and captured the ideas I remembered. This is actually a great way to filter down the volume of notes you’re taking— the best stuff always sticks in your mind for an hour or two.
1626 I found within an area folder for “Online education”)
1632 I take notes during most meetings of new ideas, suggestions, feedback, and action steps that come up.
1666 **mysterious aspect of the creative process: it can end up with a result that looks so simple, it seems like anyone could have made it. That simplicity masks the effort that was needed to get there.**
1668 Ken Burns, the renowned creator of award-winning films like The Civil War, Baseball, and Jazz, has said that only a tiny percentage of the raw footage he captures eventually makes it into the final cut. This ratio can be as high as 40- or 50-to-1, which means that for every forty to fifty hours of footage hecaptures, only one hour makes it into the final film.
1673 **Progressive Summarization is not a method for remembering as much as possible— it is a method for forgetting as much as possible.**
    ⭐
1677 **You cannot give an effective presentation without leaving out some slides.**
1681 The biggest mistake people make when they start to distill their notes is that they highlight way too much.
1683 less is more.
1685 such volume will only create a lot more work later on when you have to figure out what all that information means.
1694 **you should do it when you’re getting ready to create something.**
1695 If you try to do it with every note up front, you’ll quickly be mired in hours of meticulous highlighting with no clear purpose in mind.
1701 **I’ll often prepare by highlighting my notes from our last call and drawing out decision points and action items into an agenda.**
1701 He always thinks I’m well prepared, when in fact I just want to finish the call quickly to minimize the time I’m being billed for!
    😂
1702 **always assume that, until proven otherwise, any given note won’t necessarily ever be useful.**
1703 You have no idea what your future self will need, want, or be working on.
1705 **every time you “touch” a note, you should make it a little more discoverable for your future self**
1707 **This is the “campsite rule” applied to information— leave it better than you found it.**
1708 **the notes you interact with most often will naturally become the most discoverable**
1709 Mistake #3: Making Highlighting Difficult
    Personal note: I found that most of this "mini chapter" went over the actual title, which seems most important
1726 If you can’t locate a piece of information quickly, in a format that’s convenient and ready to be put to use, then you might as well not have it at all.
1727 Our most scarce resource is time,
1728 **When the opportunity arrives to do our best work, it’s not the time to start reading books and doing research.**
1732 To put what you’ve just learned into practice immediately, find an interesting piece of content you consumed recently, such as an article, audiobook, or YouTube video. This could be content you’ve captured already and organized in one of your PARA folders. Or it could be a new piece of content floating around your email inbox or in a read later app.
    Personal note: I'm already somewhat doing this with this book
1740 The true test of whether a note you’ve created is discoverable is whether you can get the gist of it at a glance. Put it aside for a few days and set a reminder to revisit it once you’ve forgotten most of the details. When you come back to it, give yourself no more than thirty seconds and see if you can rapidly get up to speed on what it’s about using the highlights you previously made.
1745 This is a skill you can become better at over time. The more you exercise your judgment, the more efficient and enjoyable your notetaking will become because you know that every minute of attention you invest is creating lasting value.
1747 feeling of making consistent progress.
1755 In his book A New Method of Making Common-Place-Books, John Locke similarly advised that “We extract only those Things which are Choice and Excellent, either for the Matter itself, or else the Elegancy of the Expression, and not what comes next.”

### Chapter 7: Express—Show Your Work

1800 **Don’t leave your home without a notebook, paper scraps, something to write with. Don’t walk into the world without your eyes and ears focused and open. Don’t make excuses about what you don’t have or what you would do if you did, use that energy to “find a way, make a way.”**
1843 As knowledge workers, attention is our most scarce and precious resource.
1849 The challenge we face in building a Second Brain is how to establish a system for personal knowledge that frees up attention, instead of taking more of it.
1852 there is a flaw in focusing only on the final results: all the intermediate work— the notes, the drafts, the outlines, the feedback— tends to be underappreciated and undervalued.
1854 Because we manage most of our “work-in-process” in our head, as soon as we finish the project and step away from our desks, all that valuable knowledge we worked so hard to acquire dissolves from our memory
1862 The final stage of the creative process, Express, is about refusing to wait until you have everything perfectly ready before you share what you know. It is about **expressing your ideas earlier, more frequently, and in smaller chunks to test what works and gather feedback from others.** That feedback in turn gets drawn in to your Second Brain, where it becomes the starting point for the next iteration of your work.
1873 it’s not enough to simply divide tasks into smaller pieces— **you then need a system for managing those pieces.** Otherwise, you’re just creating a lot of extra work for yourself trying to keep track of them.
    Note: Splitting stories into technical tasks
1883 **instead of starting your next project with a blank slate,** you started with a set of building blocks— research findings, web clippings, PDF highlights, book notes, back-of-the-envelope sketches— that represent your long-term effort to make sense of your field, your industry, and the world at large.
1889 There are five kinds of Intermediate Packets you can create and reuse in your work:
    * Distilled notes:
        Books or articles you’ve read and distilled so it’s easy to get the gist of what they contain (using the Progressive Summarization technique you learned in the previous chapter, for example).
    * Outtakes:
        The material or ideas that didn’t make it into a past project but could be used in future ones.
    * Work-in-process:
        The documents, graphics, agendas, or plans you produced during past projects.
    * Final deliverables:
        Concrete pieces of work you’ve delivered as part of past projects, which could become components of something new.
    * Documents created by others:
        Knowledge assets created by people on your team, contractors or consultants, or even clients or customers, that you can reference and incorporate into your work.
1902 **You should always cite your sources and give credit where credit is due.**
    Personal Note: Second brain project should do this "automagically"
1903 **We all stand on the shoulders of giants, and it’s smart to build on the thinking they’ve done rather than try to reinvent the wheel.**
1905 **you’ll become interruption-proof because you are focusing only on one small packet at a time,**
1907 **you’ll be able to make progress in any span of time.** Instead of waiting until you have multiple uninterrupted hours— which, let’s face it, is rare and getting rarer— you
1914 eventually you’ll have **so many IPs** at your disposal that **you can execute entire projects just by assembling previously created IPs**.
1919 **If they are truly valuable assets, then they deserve to be managed, just like any other asset you possess.**
1933 While you can sit down to purposefully create an IP, it is far more powerful to **simply notice the IPs that you have already produced** and then to take an extra moment to save them in your Second Brain.
1942 How could you acquire or assemble each of these components, instead of having to make them yourself?
1950 Your Second Brain is the repository of things you are already creating and using anyway. All we are doing is **adding a little bit of structure and intentionality** to how we use them:
1957 together a particular document right when you need it, but there will be a slowly accumulating, invisible cost. The cost of not being quite sure whether you have what you need. The stress of wondering whether you’ve already completed a task before. **There is a cost to your sleep, your peace of mind, and your time with family when the full burden of constantly coming up with good ideas rests solely on your fickle biological brain.**
1966 These are some of the most valuable connections— **when an idea crosses the boundaries between subjects. They can’t be planned or predicted.** They can emerge only when many kinds of ideas in different shapes and sizes are mixed together.
1968 This inherent unpredictability means that **there is no single, perfectly reliable retrieval system**
1971 four retrieval methods are: Search Browsing Tags Serendipity
1987 from brief notes dashed off during a phone call to polished Intermediate Packets that you’ve already used in past projects.
1993 Browsing allows us to gradually home in on the information we are looking for, starting with the general and getting more and more specific.
1994 This kind of browsing uses **older parts of the brain that developed to navigate physical environments, and thus comes to us more naturally.II**
2002 often you have no idea where to put it. Many notes end up being useful in completely unexpected ways. **We want to encourage that kind of serendipity, not fight it! It is for the unforeseen and the unexpected that tags really shine.**
2006 **Tags** can overcome this limitation by infusing your Second Brain with connections, making it **easier to see cross-disciplinary themes and patterns that defy simple categorization.**
2031 you can focus only on the highlighted passages and review notes far faster than having to read every word.
2033 **I strongly suggest saving not only text notes but images as well**
2051 **The time that he had spent reading and understanding a complex subject paid off in time savings for his friends, while also giving them a new interest to connect over.**
2062 **recording his conversations using an automatic transcription app**
2064 He saved all the transcripts from his conversations, plus obituaries, photos, and other related documents into the PARA project folder for each memorial, so he could see it all in one place.
2065 **Instead of spending five to seven hours at the end of all his interviews to distill everything he had heard, he began spending fifteen minutes** after each interview to highlight only the parts that resonated.
2067 “Using my first brain only for what it is best at is freedom. Freedom to be present and not multitasking as I sit with grieving people hearing stories of their loved ones. Freedom to know I have everything recorded. Freedom to know that when I go to pull the memorial together, 80 percent of the work is already done.”
2076 Rebecca began dropping short notes into her notes inbox with ideas of things she might want to include.
2077 she already had all the Intermediate Packets— metaphors, research facts, stories, diagrams— she
2086 **It doesn’t matter what medium you work in; sooner or later you must work with others.**
2089 A book is created out of a dance between an author and their editor.
2089 Reframing your work in terms of Intermediate Packets isn’t just about doing the same old stuff in smaller chunks. That doesn’t unlock your true potential. The transformation comes from the fact that **smaller chunks are inherently more shareable and collaborative.**
2103 you can’t usually know which notes are cornerstones up front.
2107 an important aspect of creativity: that it is always a remix of existing parts.
2133 Giambattista Vico: Verum ipsum factum. Translated to English, it means “We only know what we make.” To truly “know” something, it’s not enough to read about it in a book. **Ideas are merely thoughts until you put them into action.** ⭐
2139 You’ll naturally seek venues to show your work,
2152 Intermediate Packets are abbreviated as IPs, a lucky coincidence that is appropriate, because they are absolutely your Intellectual Property. You created them, you own them, and you have the right to use them again and again in any future project.
2161 “Only start projects that are already 80 percent done.” That might seem like a paradox, but committing to finish projects only when I’ve already done most of the work to capture, organize, and distill the relevant material means I never run the risk of starting something I can’t finish.

## Part Three: The Shift: Making Things Happen

### Chapter 8: The Art of Creative Execution

2178 He had a series of what he called his **“strategies.”** These were habits and tricks that he used to integrate creativity into every aspect of his life and **quickly get into a creative state of mind whenever he had time to paint.**
2194 All the steps of the CODE Method are designed to do one thing: to help you **put your digital tools to work for you so that your human, fallible, endlessly creative first brain can do what it does best. Imagine. Invent. Innovate. Create.**
2247 **Trust that you have enough** ideas and enough sources, and it’s time to turn inward and sprint toward your goal.
2248 Of the two stages of this process, **convergence is where most people struggle.**
2254 Those actions are tempting because they feel like productivity. They feel like forward progress, when in fact they are divergent acts that postpone the moment of completion.
     This actually goes in line with the anti productivity video i just saw and seemed like  an argument against a second brain (https://youtu.be/baKCC2uTbRc)
2261 It gives you a way to plan your progress even when performing tasks that are inherently unpredictable.
2266 Before I hit on this approach, I used to lose weeks **stalling** before each new chapter, **because it was just a big empty sea of nothingness.** Now each chapter starts life as a kind of **archipelago of inspiring quotes, which makes it seem far less daunting. All I have to do is build bridges between the islands.**
2294 two activities your brain has the most difficulty performing at the same time: choosing ideas (known as selection) and arranging them into a logical flow (known as sequencing).
2307 He would **always end a writing session only when he knew what came next in the story.**
2309 He built himself a bridge to the next day, **using today’s energy and momentum to fuel tomorrow’s writing.**
2318 current biggest challenge, most important open question, or future roadblocks you expect.
2320 Write out your intention for the next work session: Set an intention for what you plan on tackling next, the problem you intend to solve, or a certain milestone you want to reach.
2325 one more thing you can do as you wrap up the day’s work: **send off your draft or beta or proposal for feedback.**
    Action item
2327 The next time you sit down to work on it again, you’ll have their input and suggestions to add to the mix of material you’re working with.
2335 You have to design the user interface, but also build the backend system to make it work. You have to hire customer support representatives and train them how to troubleshoot problems. You need a whole finance operation to keep track of payments and comply with regulations. Not to mention all the responsibilities of managing employees, dealing with investors, and developing a long-term strategy.
     Small entrepreneur intro.
2341 The first parts to be dialed down are the ones **that are most difficult or expensive to build**, that have **the most uncertainty or risk**, or **that aren’t central to the purpose of the app.**
2346 When the full complexity of a project starts to reveal itself, **most people choose to delay it.** This is true of projects at work, and **even more true of side projects we take on in our spare time.**
2351 The problem isn’t a lack of time. It is that we forget that we have control over the scope of the project. We can “dial it down” to a more manageable size, and **we must if we ever want to see it finished.**
2353 **Waiting until you have everything ready before getting started is like sitting in your car and waiting to leave your driveway until all the traffic lights across town are green at the same time.** ⭐
2360 That doesn’t mean you have to throw away those parts. One of the best uses for a Second Brain is to collect and save the scraps on the cutting-room floor in case they can be used elsewhere.
2364 **Knowing that nothing I write or create truly gets lost— only saved for later use— gives me the confidence to aggressively cut my creative works down** to size without fearing that I’ve wasted effort or that I’ll lose the results of my thinking forever.⭐
2366 gives me the **courage to share my ideas before they’re perfectly ready** and before I have them all figured out.
2372 start with a group exercise or book club for a handful of colleagues or friends.
2378 chicken-and-egg problem of creativity: **you don’t know what you should create, but you can’t discover what people want until you create something.**
2408 added **several constraints to the project, such as the budget we were willing to spend, and a deadline** to have the remodel done by a certain date. These constraints helped us reduce the scope of the project to something reasonable and manageable.
2412 Start by picking one project you want to move forward on. It could be one you identified in Chapter 5, when I asked you to make folders for each active project.
2423 **fifteen or twenty minutes, and in one sitting see if you can complete a first pass on your project using only the notes you’ve gathered in front of you. No searching online, no browsing social media, and no opening multiple browser tabs that you swear you’re going to get to eventually. Only work with what you already have.**
    Action item.
2430 “What is the smallest version of this I can produce **to get useful feedback from others?**”
2431 If you find that you can’t complete the first iteration in one sitting, **start by building a Hemingway Bridge to the next time you can work on it.** List open questions, remaining to-dos, new avenues to explore, or people to consult. Share what you’ve produced with someone who can give you feedback while you’re away and save their comments in a new note in the same project folder.
2436 postpone the hardest decisions for later,
2436 find someone to help you with the parts you’re least familiar with.
2437 keep notes on anything you learn or discover,

### Chapter 9: The Essential Habits of Digital Organizers

2452 Habits reduce cognitive load and free up mental capacity, so you can allocate your attention to other tasks… It’s only by making the fundamentals of life easier that you can create the mental space needed for free thinking and creativity.—James Clear, author of Atomic Habits
2460 **Like every system, a Second Brain needs regular maintenance.**
2470 **We are like sprinters who are also trying to run a marathon.** ⭐
2477 Chefs use mise en place— a **philosophy and mindset embodied in a set of practical techniques**— as their “external brain.” 1 It gives them a way to externalize their thinking into their environment and **automate the repetitive parts of cooking so they can focus completely on the creative parts.**
2484 Your business won’t last long if you turn customers away because you’re “maintaining your systems.”
2486 We tend to notice our systems need maintenance only when they break down, which we then blame on our lack of self-discipline or our failure to be sufficiently productive.
2531 This step can and should be messy:
2548 This is in stark contrast to searching the **open Internet, which is full of distracting ads, misleading headlines, superficial content, and pointless controversy, all of which can throw me off track.**
2552 any notes found in the previous two steps I move to the project folder,
2553 you can also tag or link any relevant notes with the project, so you don’t have to move them from their original location.
2556 create an outline (an Archipelago of Ideas) for the project.
2573 make a budget and timeline, and write out the goals and objectives
2575 **an official kickoff is useful even if it’s a solo project!**
2577 The completion of a project is a very special time in a knowledge worker’s life because it’s **one of the rare moments when something actually ends.**
2595 I keep all my goals in a single digital note, sorted from short-term goals for the next year to long-term goals for years to come.
2597 If I successfully achieved it, what factors led to that success? How can I repeat or double down on those strengths? If I fell short, what happened? What can I learn or change to avoid making the same mistakes next time?
2602 It doesn’t matter if the goal is big or small— **keeping an inventory of your victories and successes is a wonderful use for your Second Brain.**
2608 If you can **start** your **thinking where you left off last time**, you’ll be far ahead **compared to starting from zero every time.**
2618 I add a new note to the project folder titled “Current status,” and jot down a few comments so I can pick it back up in the future.
2619 I might describe the last actions I took, details on why it was postponed or canceled, who was working on it and what role they played, and any lessons or best practices learned. This **Hemingway Bridge gives me the confidence to put the project on ice knowing I can bring it back to life anytime.**
2632 The first pass on your **Project Completion Checklist should be completed in even less time than the Project Kickoff Checklist— no more than ten or fifteen minutes** to grab any stand-alone materials and insights.
2634 Since you don’t know for sure that **any of this material will ever be useful again, you should minimize how much extra time and attention you invest in it.** Put in just enough effort that your future self will be able to decide if the material is relevant to their needs.
2636 The purpose of using project checklists isn’t to make the way you work rigid and formulaic. It is to **help you start and finish projects cleanly and decisively, so you don’t have “orphaned” commitments that linger on with no end in sight.**
2643 The practice of conducting a **“Weekly Review”** was pioneered by executive coach and **author David Allen** in his influential book **Getting Things Done.**
2647 review the notes you’ve created over the past week, **give them succinct titles** that tell you what’s inside, and **sort them into the appropriate PARA folders**.
2668 By the time I get to the fourth step, the inbox in my notes app is chock-full of interesting tidbits from the previous three steps— from my email, calendar, and computer desktop.
2673 It is a simple and practical process of going through my notes inbox, giving each note an informative title, and moving them into the appropriate PARA folders. **I don’t highlight or summarize them. I don’t try to understand or absorb their contents. I don’t worry about all the topics they could potentially relate to.**
2681 **select the tasks I’m committing to for the upcoming week.**
     Action item
2711 I’m often surprised just how much can change in a month. To-dos that seemed critical last month might become irrelevant this month, and vice versa.
2725 The nice thing about **notes, unlike to-dos, aren’t urgent.** If one important to-do gets overlooked, the results could be catastrophic. Notes, on the other hand, can easily be put on hold any time you get busy,
2728 The most common **misconception** about organizing I see when I’m working with clients is the belief that organizing requires a **heavy lift.**
2731 They tend to get bogged down in minutiae and barely make a dent in the mountain of accumulated stuff they wanted to tackle. Then they’re saddled with a feeling of guilt that they weren’t able to make progress even with so much time at their disposal.
2735 **it needs to be done a little at a time in the flow of our normal lives.**
2745 We should avoid doing a lot of heavy lifting up front, not only because it takes up precious time and energy, but because it locks us into a course of action that might not end up being right.
2748 Over time, this will gradually produce an environment far more suited to your real needs than anything you could have planned up front.
2751 **Your Turn: A Perfect System You Don’t Use Isn’t Perfect** ⭐
2751 Each of the three kinds of habits I’ve introduced you to— Project Kickoff and Completion Checklists, Weekly and Monthly Reviews, and Noticing Habits— are **all meant to be performed quickly in the in-between spaces of your day.**
2758 the maintenance of your Second Brain is very forgiving. Unlike a car engine, nothing will explode, break down, or burst into flames if you let things slide for days, weeks, or even months.
2762 There’s **no need to capture every idea; the best ones will always come back around eventually.** ⭐
2767 **any system that must be perfect to be reliable is deeply flawed.** ⭐
2773 By asking what is likely to go wrong, you can take action to prevent it from happening in the first place.

### Chapter 10: The Path of Self-Expression

2797 it is never a person’s toolset that constrains their potential, it’s their mindset.
2828 Your fears, doubts, mistakes, missteps, failures, and self-criticism— it’s all just information to be taken in, processed, and made sense of.
2839 **Your mind will become calmer, knowing that every idea is being tracked. It will become more focused, knowing it can put thoughts on hold and access them later.**
2845 asking your biological brain to hand over the job of remembering to an external system, and by doing so, freeing it to absorb and integrate new knowledge in more creative ways.
2856 Delegating a job you’ve been doing for a long time is always intimidating. The voice of fear creeps up in the back of your mind: **“Will there be anything left for me to do?” “Will I still be valued and needed?”**
    Self-note: Is this something that common?
2874 **treating every meme and random post on social media as if it was just as important as the most profound piece of wisdom. It is driven by fear— the fear of missing out**
2892 **I believe most people have a natural desire within them to serve others. They want to teach, to mentor, to help, to contribute. The desire to give back is a fundamental part of what makes us human.**
2897 Faced with the evidence of everything they already know, suddenly there’s no longer any reason to wait.
2899 **What’s the point of knowing something if it doesn’t positively impact anyone, not even yourself?**
2906 Ryder Carroll says in The Bullet Journal Method, “Your singular perspective may patch some small hole in the vast tattered fabric of humanity.”
2914 At its core, it is about cultivating self-awareness and self-knowledge.
     Note: Just like the bullet journal is a mindful practice
2919 there are many tasks we can easily perform as humans that we can’t fully explain.
     it resides in our subconscious and muscle memory where language cannot reach.
     has been a major roadblock in the development of artificial intelligence and other computer systems.
2933 what if you don’t know what your goals and desires are?
2993 **Focus on creating one Intermediate Packet at a time and looking for opportunities to share them in ever more bold ways.**
3031 There have been **practices that you’d never heard of before, that are now integral parts of your life**. There have been **habits and skills that seemed impossible to master**, that you now can’t imagine living without.
3034 **chase what excites you.**
3036 **Run after your obsessions with everything you have. Just be sure to take notes along the way.**
Notes
3233 “The spatial metaphor for user interfaces: experimental tests of reference by location versus name,”
