# Virtual Box

## [VirtualBox Fails After Windows 11 Upgrade (NtCreateFile failed) - E1Tips.com](https://e1tips.com/2024/04/02/virtualbox-fails-after-windows-11-upgrade-ntcreatefile-failed/)

After upgrading from Windows 10 to Windows 11, VirtualBox fails to start your Virtual Machine. You receive this error:

> NtCreateFile (\Device\VBoxDrvStub) failed: 0xc0000034
> STATUS_OBJECT_NAME_NOT_FOUND (0 retries) (rc=-101)

1. Locate the VBoxSup.inf file. Usually found here: C:\Program Files\Oracle\VirtualBox\drivers\vboxsup

2. Right-Click the VBoxSup.inf file and select [Install]

3. Open a command prompt as Administrator and run the following command:
   `sc start vboxsup`
