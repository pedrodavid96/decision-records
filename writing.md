## Helpers

https://www.grammarly.com/
http://www.hemingwayapp.com/

[Write to Express, Not to Impress](https://medium.com/swlh/write-to-express-not-to-impress-465d628f39fe)

[Where does “emphasis mine” go in a quotation?](https://english.stackexchange.com/questions/90161/where-does-emphasis-mine-go-in-a-quotation)
> Do not put quotations in italics unless the material would otherwise call for italics, such as for emphasis and the use of non-English words. Indicate whether italics were used in the original text or whether they were added later. For example, directly from the Manual of Style:
> "Now cracks a noble heart. Good night sweet prince: And flights of angels sing thee to thy rest." [emphasis added]
