# Clean Code

[Code Smells](https://refactoring.guru/refactoring/smells)

[Reflecting architecture and domain in code](https://herbertograca.com/2019/06/05/reflecting-architecture-and-domain-in-code/)

[Exception Handling and "Program to Abstraction"](https://softwareengineering.stackexchange.com/questions/224350/does-exception-handling-violates-program-to-abstraction)

[Kotlin Clean Architecture](https://proandroiddev.com/kotlin-clean-architecture-1ad42fcd97fa)

[Domain-Driven Design: When is a Bounded Context no longer a Bounded Context? | Christopher Laine
](https://medium.com/it-dead-inside/domain-driven-design-when-is-a-bounded-context-no-longer-a-bounded-context-80e941cde79f)

code-quality
[Engineering code quality in the Firefox browser: A look at our tools and challenges](https://hacks.mozilla.org/2020/04/code-quality-tools-at-mozilla/)
[Firefox code quality tools](https://firefox-source-docs.mozilla.org/code-quality/)
This is an awesome resource! 🥇⭐

[Ready for changes with Hexagonal Architecture](https://netflixtechblog.com/ready-for-changes-with-hexagonal-architecture-b315ec967749)

Swappable data sources
We managed to transfer reads from a JSON API to a GraphQL data source within 2 hours.

Leveraging Hexagonal Architecture
> swap data sources without impacting business logic
> The idea of Hexagonal Architecture is to put inputs and outputs at the edges of our design. Business logic should not depend on whether we expose a REST or a GraphQL API, and it should not depend on where we get data from — a database, a microservice API exposed via gRPC or REST, or just a simple CSV file.

Defining the core concepts
- Entities are the domain object
- Repositories are the interfaces to getting entities as well as creating and changing them.
- Interactors are classes that orchestrate and perform domain actions — think of Service Objects or Use Case Objects

Hiding data source details

Delaying decisions

[Preventing Software Rot](https://software.rajivprab.com/2020/04/25/preventing-software-rot/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/g7tjzn/preventing_software_rot/)
The Cost of Keeping The Lights On
Preventing Catastrophic Failure
An Idle Mind

[Array Functions and the Rule of Least Power](https://jesseduffield.com/array-functions-and-the-rule-of-least-power/)
for loop > forEach > reduce > map | filter > find > every | some
Less power often means clearer code and reduces "surface area"
[discussion](https://www.reddit.com/r/programming/comments/hozfec/guide_to_array_functions_why_you_should_pick_the/)

[It's probably time to stop recommending Clean Code](https://qntm.org/clean)
[Very good discussion](https://www.reddit.com/r/programming/comments/hhlvqq/its_probably_time_to_stop_recommending_clean_code/)
[An Alternative to "Clean Code": A Philosophy of Software Design](https://johz.bearblog.dev/book-review-philosophy-software-design/)
[More discussion](https://www.reddit.com/r/programming/comments/hjasqj/an_alternative_to_clean_code_a_philosophy_of/)
[Book Review: A Philosophy of Software Design | Johz Blog](https://johz.bearblog.dev/book-review-philosophy-software-design/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/lb8zrn/an_alternative_to_clean_code_a_philosophy_of/)

# Code Quality / API Evolution

[Revapi](https://revapi.org/)
> Revapi is an API analysis and change tracking tool written in Java.

[rewrite](https://github.com/openrewrite/rewrite)
> Eliminate Tech-Debt. Automatically.
> The Rewrite project is a mass refactoring ecosystem for Java and other source code, designed to eliminate technical debt across an engineering organization.
[rewrite-checkstyle](https://github.com/openrewrite/rewrite-checkstyle)
[How to add Checkstyle and FindBugs plugins in a gradle based project](https://medium.com/@raveensr/how-to-add-checkstyle-and-findbugs-plugins-in-a-gradle-based-project-51759aa843be)

[Spotless](https://github.com/diffplug/spotless)
> Spotless can format <java | kotlin | scala | sql | groovy | javascript | flow | typeScript | css | scss | less | jsx | vue | graphql | json | yaml | markdown | license headers | anything> using <gradle | maven | anything>.

[Clean Coders Hate What Happens to Your Code When You Use These Enterprise Programming Tricks](https://www.youtube.com/watch?v=FyCYva9DhsI)

[GOTO 2017 • Code as Risk • Kevlin Henney](https://www.youtube.com/watch?v=YyhfK-aBo-Y)
Awesome talk! 🥇⭐

[Always leave the code better than you found it](https://letterstoanewdeveloper.com/2020/11/23/always-leave-the-code-better-than-you-found-it/)
Document
Write a test or improve a test
Refactor it
Upgrade a dependency
[Discussion](https://www.reddit.com/r/programming/comments/k2cbtb/always_leave_the_code_better_than_you_found_it/)

[The Hidden Costs of Software Dependencies](https://www.youtube.com/watch?v=rQegYUsU7ec)

[Step 8: The Boy Scout Rule ~Robert C. Martin (Uncle Bob)](https://biratkirat.medium.com/step-8-the-boy-scout-rule-robert-c-martin-uncle-bob-9ac839778385)
> TL;DR Continual Improvement is the way to learn and improve, be it as a developer or a software. Embrace Kaizen.
> Kayzen: The small and simple changes that result in a lifetime of improvements.

[Where a curly bracket belongs - stitcher.io](https://stitcher.io/blog/where-a-curly-bracket-belongs)