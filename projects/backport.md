# Backport

[sorenlouv/backport: A simple CLI tool that automates the process of backporting commits on a GitHub repo](https://github.com/sorenlouv/backport/tree/main)
This only works for Github (multiple functionalities via Github API only).

Ideas for implementation for other providers:

[GitoxideLabs/gitoxide: An idiomatic, lean, fast & safe pure Rust implementation of Git](https://github.com/GitoxideLabs/gitoxide)
If tool in rust this crate might be useful to interact with git.

[gitlab - crates.io: Rust Package Registry](https://crates.io/crates/gitlab)
This one with gitlab.
[REST API resources | GitLab](https://docs.gitlab.com/ee/api/api_resources.html)

gitea might be slightly harder as it only has Go sdk
[gitea package - code.gitea.io/sdk/gitea - Go Packages](https://pkg.go.dev/code.gitea.io/sdk/gitea)

Github
[Octokit](https://github.com/octokit)