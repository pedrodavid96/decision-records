# HTML

[htmllint](https://github.com/htmllint/htmllint)

[HTMHell](https://www.htmhell.dev/)
> A collection of bad practices in HTML, copied from real websites.
Very cool tips

[The Open Graph protocol](https://ogp.me/)
[Portable thoughts](https://portable.fyi/#_)

## Is type="text/css" Needed for Style and Link Tags?

>
	When used in either an inline stylesheet or an external stylesheet, the attribute type="text/css" is optional as of HTML5. In the HTML4 spec it was needed, though browsers were forgiving. If omitted, browsers will default to text/css.

	You can safely do this:

	<style></style>
	<link rel="stylesheet" href="style.css">

	Just make sure you're using the new HTML5 doctype at the top of your HTML document:

	<!DOCTYPE html>

Source: https://www.h3xed.com/web-development/is-type-text-css-needed-for-style-and-link-tags

## https://stackoverflow.com/questions/12989931/body-height-100-displaying-vertical-scrollbar
https://stackoverflow.com/questions/13327532/body-height-not-filling-100-page-height

---

[Obsolete features](https://html.spec.whatwg.org/dev/obsolete.html#obsolete)
[Rarely used but handy](https://medium.com/swlh/rarely-used-but-handy-html-tags-d000cd3050b3)
[Rarely used but handy | discussion](https://www.reddit.com/r/programming/comments/bowp77/some_rarely_used_but_handy_html_tags/)
**Note**:
>
I was about to criticize the example for acronym for not using it correctly... and checked the spec for the exact semantic meaning and found it is deprecated now. They say just use abbr instead.

[Methodical UI: removing frustration from HTML and CSS development](https://jdauriemma.com/programming/methodical-html-css-development)

[Meanwhile I can't even center a <div> on a page. - discussion](https://www.reddit.com/r/programming/comments/e00dbn/meanwhile_i_cant_even_center_a_div_on_a_page/)

[HTML: Underrated tags](https://itnext.io/html-underrated-tags-119ef3e45b94)
- <picture>
  > with Picture element you can use media queries to tell the browser which image should display and the other sources will be ignored so the images can load faster.
  https://developer.mozilla.org/pt-BR/docs/Web/HTML/Element/picture
- <fieldset>
  group forms
- <progress>
  https://codepen.io/adrian-legaspi/pen/QWwpRPJ
  > As you can see is quite easy to style and to manipulate the value with JavaScript. The next time that you need a progress bar, consider this old friend.
- <base>
  > This will help to handle better the relative <a> links because you only need to declare the root URL
- More input tags
  * Color picker 	- type="color"
  * Range			- type="range"
- <details>
- Text formatting tags
  * <mark>
  * <abbr>
  * <pre>
  * <hr>
- [contenteditable]
  If you want to create a WYSIWYG (What You See Is What You Get) editor.
  Use this instead of <textarea>

# Accessibility

[Common accessibility issues that you can fix today | hidde.blog](https://hidde.blog/common-a11y-issues/?ref=sidebar)

[The Links vs Buttons Showndown - Marcy Sutton: #ID24 Nov 2017](https://www.youtube.com/watch?v=8XjwDq9zG4I)
Fantastic talk.

[Tooltips & Toggletips](https://inclusive-components.design/tooltips-toggletips/)
[Using the HTML title attribute - updated March 2020 - TPGi](https://www.tpgi.com/using-the-html-title-attribute-updated/)
[Accessible name - MDN Web Docs Glossary: Definitions of Web-related terms | MDN](https://developer.mozilla.org/en-US/docs/Glossary/Accessible_name)
[aria-label - Accessibility | MDN](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-label)
[ARIA: tooltip role - Accessibility | MDN](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/tooltip_role)


---

[Writing HTML Documents](https://www.w3.org/TR/html52/syntax.html#writing-html-documents)

[Doctype](https://www.w3.org/TR/html52/syntax.html#doctype)

[The `html` element](https://www.w3.org/TR/html52/semantics.html#elementdef-html)

[The `head` element](https://www.w3.org/TR/html52/document-metadata.html#elementdef-head)

[The `meta` element](https://www.w3.org/TR/html52/document-metadata.html#elementdef-meta)
[HTML <meta> tag](https://www.w3schools.com/tags/tag_meta.asp)

---

[How to Build HTML Forms Right: Styling](https://austingil.com/build-html-forms-right-styling/#Styling_Forms_is_Not_Great)
[How to build HTML Forms Right: User Experience](https://austingil.com/build-html-forms-right-user-experience/)
1. Content
   * Request the least amount of information
   * Keep it simple
   * Semantics are good for a11y and UX
   * Put country selector before city/state (in case the city/state dropdown shows only the specific    cities / states)
   * Paginate long forms
2. General Functionality
   * Prevent browser refresh/navigation
   * Store unsaved changes
   * Do not prevent copy/paste
3. Input Functionality
   * inputmode
   * autocomplete
   * autofocus
   * Auto-expanding textarea
   * Disable scroll event on number inputs
4. Validation
   * Delay validation to blur or submit events
   * Don’t hide validation criteria
   * Send all server validation errors back at once
5. Submissions
   * Submit with JavaScript
   * Including status indicators
   * Scroll to errors
[Small Reddit Discussion](https://www.reddit.com/r/programming/comments/jxrs0h/how_to_build_html_forms_right_user_experience/)
> **The best web form is the one that user does not need to fill out.**
> Because here is the secret. Users hate forms. In any form, shape or color.

[Front End Checklist](https://github.com/thedaviddias/Front-End-Checklist)
> The Front-End Checklist is an exhaustive list of all elements you need to have / to test before launching your website / HTML page to production.

["translate" attribute on <p>](https://twitter.com/dotnafis/status/1371197860419493893)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/m53lbo/today_i_learned_about_the_translate_attribute_on/)
Use the "translate" attribute and set its value to "no" for your company name. In case, the webpage is translated into another language, your BRAND name will remain intact.

[HTML tips](https://markodenic.com/html-tips/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/mkh5y3/html_tips_hidden_gems/)
1. The `loading=lazy` attribute
2. Email, call, and SMS links
3. Ordered lists `start` attribute
4. The `meter` element
5. HTML Native Search
6. Fieldset Element
7. Window.opener
8. Base Element
9. Favicon cache busting
10. The `spellcheck` attribute
11. Native HTML sliders
12. HTML Accordion
13. `mark` tag
14. `download` attribute
15. Performance tip - `.webp` image format

[State of HTML 2023: Accessibility](https://2023.stateofhtml.com/en-US/features/accessibility/)
