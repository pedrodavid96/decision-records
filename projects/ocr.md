# OCR (Optical Character Recognition)

[Is there an Open-Source application where I can scan receipts with OCR? Just like Lemon Wallet and so many others. - Quora](https://www.quora.com/Is-there-an-Open-Source-application-where-I-can-scan-receipts-with-OCR-Just-like-Lemon-Wallet-and-so-many-others)

(source: [How we tuned Tesseract to perform as well as a commercial OCR package – vBridge](https://vbridge.co.uk/2012/11/05/how-we-tuned-tesseract-to-perform-as-well-as-a-commercial-ocr-package/))
Tesseract-ocr

is probably the best open source solution for this, but you'll probably need to use additional tools and methodologies to get the last 20% of the way toward reliable reads off a mobile phone camera.

https://shreeshivpatel.medium.com/how-to-extract-invoice-key-parameter-using-tesseract-and-end2end-sequential-model-13cf2ffa9414
https://nanonets.com/blog/receipt-ocr/

[Tesseract OCR in Python with Pytesseract andOpenCV](https://nanonets.com/blog/ocr-with-tesseract/)
Interesting topics:
* Preprocessing (with OpenCV) for Tesseract
* Getting boxes around text
* Whitelisting / Blacklisting characters
* Limitations of Tesseract

[Parsing structured data within PDF documents with Apache PDFBox](https://robinhowlett.com/blog/2019/11/29/parsing-structured-data-complex-pdf-layouts/)
Very good article with a few tricks to read PDFs
https://github.com/robinhowlett/breeders-cup-betting-challenge-parser/blob/master/src/main/java/com/robinhowlett/parsers/BCBCParser.java

[Your First JavaFX Application with OpenCV — OpenCV Java Tutorials 1.0 documentation](https://opencv-java-tutorials.readthedocs.io/en/latest/03-first-javafx-application-with-opencv.html)

[Write Your Own Imaging Processing UI in 15 Minutes - DZone](https://dzone.com/articles/write-your-own-imaging-processing-ui-in-15-minutes)
Great article on how to integrate other tools (OpenCV) with Jetbrains Compose
http://origamidocs.hellonico.info/#/

I need to understand how to get the correct area from the PDF preview:
https://github.com/JetBrains/compose-jb/blob/master/tutorials/Mouse_Events/README.md
https://www.google.com/search?client=firefox-b-d&q=java+awt+get+mouse+position
https://stackoverflow.com/questions/64855189/clickable-areas-of-image-mouseover-event-jetpack-compose-desktop

PDFRenderer to Preview in Jetbrains Compose (/Bitmap):
* https://stackoverflow.com/questions/6331068/java-bufferedimage-to-bitmap-format
* https://stackoverflow.com/questions/66002696/how-to-load-image-from-remote-url-in-kotlin-compose-desktop

[Receipt OCR Part 1: Image segmentation by OpenCV | Kaggle](https://www.kaggle.com/code/dmitryyemelyanov/receipt-ocr-part-1-image-segmentation-by-opencv)
[Receipt OCR Part 2: Text recognition by Tesseract | Kaggle](https://www.kaggle.com/code/dmitryyemelyanov/receipt-ocr-part-2-text-recognition-by-tesseract)
