# Backup

[What is your solution for a remote backup ? : selfhosted](https://www.reddit.com/r/selfhosted/comments/1gwkz21/what_is_your_solution_for_a_remote_backup/)

[jareware/docker-volume-backup: Utility container for periodically backing up Docker volumes](https://github.com/jareware/docker-volume-backup)

[alexklibisz/bdv2s3: backup docker volume to s3](https://github.com/alexklibisz/bdv2s3)
Based on the previous but "basically a simplified, opinionated version"

[Backing up Docker volumes with ease](https://sysadminsjournal.com/backing-up-docker-volumes-with-ease/)

[What is the ultimate backup solution for the Immich DB and files? : immich](https://www.reddit.com/r/immich/comments/1bydti1/what_is_the_ultimate_backup_solution_for_the/)
[restic · Backups done right!](https://restic.net/)
[Bye Bye Minio, Hello Restic Rest Server!](https://blog.fuzzymistborn.com/restic-server-over-minio/)

[How to backup sqlite database? - Stack Overflow](https://stackoverflow.com/questions/25675314/how-to-backup-sqlite-database)

[Do I need to use in-app backup if I regularly back up the Radarr docker container? : r/radarr](https://www.reddit.com/r/radarr/comments/110dt0m/do_i_need_to_use_inapp_backup_if_i_regularly_back/)
