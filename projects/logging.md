# Logging

[Application Logging in Java: Creating a Logging Framework · Terse Systems](https://tersesystems.com/blog/2019/04/23/application-logging-in-java-part-1)
TL;DR: If you are at a company and you need to have all your projects do logging the same way, instead of having everyone copy a logback.xml file and directly depend on logback in their projects, create a company-wide framework containing logback and the logback.xml configuration and have everyone depend on slf4j-api and your framework. I wrote up a very long sample logging project at https://github.com/tersesystems/terse-logback. You can read through it and use it as a guide for your own logging framework.

[Best practices for loggers - Kotlin Discussions](https://discuss.kotlinlang.org/t/best-practices-for-loggers/226/6)
[Idiomatic way of logging in Kotlin - Stack Overflow](https://stackoverflow.com/questions/34416869/idiomatic-way-of-logging-in-kotlin)

[Google Cloud Structured Logging for Java Applications](http://www.java-allandsundry.com/2022/05/google-cloud-structured-logging-for.html)
