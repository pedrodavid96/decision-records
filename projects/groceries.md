[Grocy - ERP beyond your fridge](https://grocy.info/)

[GitHub - open-eats/OpenEats: :pizza: Self Hosted Recipe Management App](https://github.com/open-eats/OpenEats)
[News | Open Eats](https://open-eats.github.io/)

[Megroplan - Meal & Grocery Planner](https://megroplan.com/)

[Our Groceries Shopping List – Apps no Google Play](https://play.google.com/store/apps/details?id=com.headcode.ourgroceries&pli=1)

[Bring! Shopping List App for iOS & Android](https://www.getbring.com/en/home)

[Best open source groceries shopping-list apps? : opensource](https://www.reddit.com/r/opensource/comments/t7856v/best_open_source_groceries_shoppinglist_apps/)
[Looking for a shared grocery list : selfhosted](https://www.reddit.com/r/selfhosted/comments/tciwcj/looking_for_a_shared_grocery_list/)
[Shared grocery/shopping lists? : selfhosted](https://www.reddit.com/r/selfhosted/comments/8shzfv/shared_groceryshopping_lists/)

[GitHub - TomBursch/kitchenowl: KitchenOwl is a self-hosted grocery list and recipe manager](https://github.com/tombursch/kitchenowl/)
The backend is made with Flask and the frontend with Flutter. Easily add items to your shopping list before you go shopping. You can also create recipes and add items based on what you want to cook.


[Megroplan - Meal & Grocery Planner](https://megroplan.com/)
