# tmux

https://leimao.github.io/blog/Tmux-Tutorial/

[Boost Your Productivity In The Terminal With tmux](https://thevaluable.dev/tmux-boost-productivity-terminal/)

[2012 UTOSC - Screen vs. tmux faceoff - Jon Jensen](https://www.youtube.com/watch?v=QxTse5Elq8s)

[Tmux Cheat Sheet: Essential Commands And Quick References](https://www.stationx.net/tmux-cheat-sheet/)
[Reddit discussion](https://www.reddit.com/r/programming/comments/13xuf73/tmux_cheat_sheet_essential_commands_and_quick/)

[scroll - How do I increase the scrollback buffer size in tmux? - Stack Overflow](https://stackoverflow.com/questions/18760281/how-do-i-increase-the-scrollback-buffer-size-in-tmux)

[Scroll shell output with mouse in tmux - Super User](https://superuser.com/questions/210125/scroll-shell-output-with-mouse-in-tmux)

[How to Make tmux’s “Windows” Behave like Browser Tabs | seanh.cc](https://www.seanh.cc/2020/12/30/how-to-make-tmux%27s-windows-behave-like-browser-tabs/)

[tmux status bar border - Pesquisa Google](https://www.google.com/search?channel=fs&client=ubuntu-sn&q=tmux+status+bar+border)
[Adding space between tmux status bar and prompt : r/tmux](https://www.reddit.com/r/tmux/comments/rascjp/adding_space_between_tmux_status_bar_and_prompt/)
[Basic Tmux Status Bar · GitHub](https://gist.github.com/markandrewj/ead05ebc20f3968ec07e)

[How to use status-format[1] · Issue #1886 · tmux/tmux · GitHub](https://github.com/tmux/tmux/issues/1886)

---

[tmux on macOS - copy to system clipboard | JDeen](https://www.jdeen.com/blog/tmux-on-macos-copy-to-system-clipboard)
[tmux copy mouse selected text to clipboard automatically on mouse release - Stack Overflow](https://stackoverflow.com/questions/36815879/tmux-copy-mouse-selected-text-to-clipboard-automatically-on-mouse-release)
[ChrisJohnsen/tmux-MacOSX-pasteboard: Notes and workarounds for accessing the Mac OS X pasteboard in tmux sessions. Note: The pu branch (“Proposed Updates”) may be rewound without notice.](https://github.com/ChrisJohnsen/tmux-MacOSX-pasteboard)
[tmux configuration conditional to OS - Super User](https://superuser.com/questions/539595/tmux-configuration-conditional-to-os)

---

[3. Detach (Break) and Join Pane](https://www.baeldung.com/linux/tmux-pane-hide#detach-break-and-join-pane)
This actually moves a pane into its own window.

`break-pane -dP`
`join-pane -vs break-join:1.0`

`-s` is the source pane we want to join
where `break-join:1.0` was the output given by break-pane `-P` option.

`-v` is a vertial split, you could use `-h` for horizontal instead.

---

* Trigger factory update