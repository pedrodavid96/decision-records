[Find Unique Colors and gradients](https://colorspark.app/)
[Colors & Fonts](https://www.colorsandfonts.com/)

["Contrast Linter"](https://getstark.co/)

[Design books of 2019](https://uxdesign.cc/design-books-of-2019-52416f9cb120)
[RefactoringUI](https://refactoringui.com/)
[Twitter](https://mobile.twitter.com/steveschoger)
> Tired of relying on Bootstrap?
> Learn how to design awesome UIs by yourself using specific tactics explained from a developer's point-of-view.

[10 Rules of Dashboard Design](https://medium.muz.li/10-rules-of-dashboard-design-f1a4123028a2)
Very cool Dashboards!!!

[The art of Frontend Engineering](https://www.narative.co/articles/the-art-of-frontend-engineering)

[7 Practical Tips for Cheating at Design by Adam Wathan & Steve Schoger](https://medium.com/refactoring-ui/7-practical-tips-for-cheating-at-design-40c736799886)
1. Use color and weight to create hierarchy instead of size
2. Don’t use grey text on colored backgrounds
    1. Reduce the opacity of white text
    2. Hand-pick a color that’s based on the background color
3. Offset your shadows
4. Use fewer borders
    1. Use a box shadow
    2. Use two different background colors
    3. Add extra spacing
5. Don’t blow up icons that are meant to be small
6. Use accent borders to add color to a bland design
7. Not every button needs a background color
[Refactoring UI book](https://refactoringui.com/book/)

## [Prototyping on paper](https://marvelapp.com/pop/?popref=1)

## [Redesigning popular social media apps using a mini-Design System](https://uxdesign.cc/redesigning-popular-social-media-apps-using-a-mini-design-system-46c7a63804d7)

Awesome looks.
More examples: https://dribbble.com/hype4

[UI/UX Case Study: Nutrition Tracking App](https://medium.muz.li/ui-ux-case-study-nutrition-tracking-app-5908c8df02c2)
Very cool and minimalist app

## [Wikicamps — A UX Case Study](https://medium.com/better-marketing/wikicamps-ux-case-study-8da46aad2eb3)


## [50 Things You [Probably] Forgot To Design](https://medium.com/ux-power-tools/50-things-you-probably-forgot-to-design-7a288b0ef914)

## [When Motion Creates Emotion](https://uxplanet.org/when-motion-creates-emotion-ad29befeb1b3)

## [Small Stars of Big Design: Review of Interactive UI Elements](https://uxplanet.org/small-stars-of-big-design-review-of-interactive-ui-elements-fe856eafe0a2)

## [Low Poly Scenes Made Easy](https://medium.com/swlh/low-poly-scenes-made-easy-ea5489049746)

## [Using gray, shade, and tint in UI design](https://blog.prototypr.io/using-gray-shade-and-tint-in-ui-design-589d0e638dfd)

## [Brand, creative direction and responsive marketing site design for Adobe Portfolio](https://roomfive.net/adobe-portfolio-brand-marketing/)
Development of the design in-depth ("all" final pages sketches, showing page scrolling, etc..)

## [11 form design guidelines](https://uxdesign.cc/11-form-design-guidelines-4b2f1c659414)

## [Design Guide](https://medium.com/swlh/design-guide-alerts-f563fa139853)

## [Gestalt principles in UI design.](https://medium.muz.li/gestalt-principles-in-ui-design-6b75a41e9965)

## [UI cheat sheet: text fields](https://uxdesign.cc/ui-cheat-sheet-text-fields-2152112615f8)

## [7 Rules for Creating Gorgeous UI — Part 2 (Updated for 2019)](https://medium.com/@erikdkennedy/7-rules-for-creating-gorgeous-ui-part-2-430de537ba96)

## [Make sense of rounded corners on buttons](https://uxdesign.cc/make-sense-of-rounded-corners-on-buttons-dfc8e13ea7f7)

## [How to Design Scannable App Screenshots](https://medium.com/free-code-camp/ux-best-practices-how-to-design-scannable-app-screenshots-89e370bf433e)

[Neumorphism in user interfaces](https://uxdesign.cc/neumorphism-in-user-interfaces-b47cef3bf3a6)

[UX design trends retrospective 2019](https://uxdesign.cc/ux-design-trends-retrospective-2019-8a3daaa61c62)
Huge article but with really cool examples:
* Illustrations Everywhere
* Let’s Go Dark: Dark Mode/Dark Themes (very cool sleep tracking app concept)
* Minimalism/Maximalism
* Neumorphism
* Meaningful Microinteractions
* More Gesture Interactions on Mobile
* 3D Visualization Brings a Real-world Feel to Products
* Voice Interactions
* Conversational UIs and Chatbots
* Scroll-triggered Animations Tell Stories, Sell Products and Services
* Mixed Media Sites with Strong Typography
* eCommerce Comes Alive with 3D, AR, and Motion

[8 rules for a perfect card design by Dorjan Vulaj](https://uxdesign.cc/8-rules-for-perfect-card-design-4fb7eef32e09)
1. Understand the structure - Media, primary text, supporting text, primary action(s) (e.g.: buy) and secondary actions (e.g.: like, share...)
2. Shadows / Stroke - on card outline, depending on the style of the UI
3. Play with the background - but be carefull to not create a lot of contrast
4. Text size and **weight** - Users read more when the font is smaller, and scan more when the font is larger
5. Contrast - Cards are really minimal, so contrast plays a major role in dividing the information and giving priorities.
6. Spacing & Padding - Include appropriate padding between focusable elements also enough spacing to group the information inside.
7. Focus and hover - To make the cards more noticeable on hover a darker shadow or a small elevation is usually used.

[Helping Hand: 15 Creative UI Design Concepts for Everyday Needs by Tubik Studio](https://uxplanet.org/helping-hand-15-creative-ui-design-concepts-for-everyday-needs-f5a970716c0e)
Somewhat of a case study with Very very cool designs.

[10 ways to spice up a UI design - Danny Sapio](https://uxdesign.cc/10-ways-to-spice-up-a-ui-design-f6025b2f4a8c)
1. Inject life into your copy - humor in messages insteadd of lifeless forms
2. Throw in some icons & emojis
3. Make your product more human with Illustrations (e.g.: product landing page)
4. Add a dark mode option silly
5. Use high-quality imagery
6. Make error states that don’t suck
7. Give your design some motion
8. Focus on the small details by including micro-interactions
9. Throw in some patterns and gradients
10. Utilize shadows to add depth

[10 inspiring progress bars that delight users by Justinmind](https://uxplanet.org/10-inspiring-progress-bars-that-delight-users-460f3b3f913b)

[10 rules of thumb in UI design by Danny Sapio](https://uxdesign.cc/10-rules-of-thumb-in-ui-design-aa5f91885444)
1. Design for density, not pixels
2. Use 8dp increments
The reason we use the magic number 8 as opposed to 5, for example, is that if the device has a 1.5x resolution, it won’t render an odd number properly.
Additionally, the vast majority of modern screen dimensions are divisible by 8.
3. Remove lines and boxes
4. Pay attention to contrast
To ensure you’re meeting this standard, download [Stark](https://getstark.co/) which will allow you to check if your designs are accessible or not.
5. Familiarity is good
6. Use color weight to establish hierarchy
7. Avoid using more than two typefaces
8. Recognition, not recall
On a checkout page (if designed well), I shouldn’t have to remember what items I am paying for. I should clearly be able to identify the items that I’m purchasing without having to jog my memory.
In my Gmail inbox, at a glance, I can determine which emails I’ve read and which ones I haven’t without having to think about it.
9. Don’t slow me down
“Numerous researches have discovered that optimal speed for interface animation is between 200 and 500 ms. These figures are based on the particular qualities of the human brain. Any animation shorter than 100 ms is instantaneous and won’t be recognized at all. Whereas the animation longer than 1 second would convey a sense of delay and thus be boring for the user.” — [The ultimate guide to proper use of animation in UX](https://uxdesign.cc/the-ultimate-guide-to-proper-use-of-animation-in-ux-10bd98614fa9)
10. Less is more
Example: Google search page (very minimalist) vs Yahoo (news and et all)

[Front-end Style Guides](https://24ways.org/2011/front-end-style-guides/)

[Mobile UI Design Trends In 2018 by Moses Kim](https://uxplanet.org/mobile-ui-design-trends-in-2018-ccd26031dfd8) - Very cool ideas ❤

[6 Best Website Design Tips for Beginners and Small Business in 2018](https://blog.prototypr.io/6-best-website-design-tips-for-beginners-and-small-business-in-2018-28cba8f28a56)
Very "abstract" rules and ideas 😟
But examples are pretty cool

[What's New in Material Design for the Web](https://www.youtube.com/watch?v=E23IWqE4nac)

[9 tips to quickly improve your UI designs](https://uxdesign.cc/9-simple-tips-to-improve-your-ui-designs-fast-377c5113ac82)
1. Make your elements appear more defined
2. Using just one typeface in your design is all good
   Ignore the ‘Always use 2 Typefaces. Minimum.’ crowd. Using a combination of Weights, Sizes, and Colour you can still produce perfectly acceptable results.
3. Creating long-form content? Give 20pt, and up a try
4. Improve your users’ onboarding experience (allow skip)
5. Your shadows are coming from one light source right?
6. Improve Contrast between Text and Images with a subtle, but simple Overlay
7. Use Centred Text in moderation. Keep it on the low
8. Whitespace is your friend. Use it generously!
9. Darken up that text on light backgrounds

[8 UI design trends for 2020](https://uxdesign.cc/8-ui-ux-design-trends-for-2020-68e37b0278f6)
1. Animated Illustrations
2. Microinteractions
3. 3D Graphic in web & mobile UI
4. Virtual Reality
5. Augmented Reality
6. Neumorphism
7. Asymmetrical Layouts
8. Storytelling

[Are Gradients The New Colors?](https://medium.muz.li/why-gradients-are-the-new-colors-3d8d42a7a6fc)
1. Gradients are memorable
2. Gradients are unique
3. Gradients feel realistic
4. Gradients are just more colorful and playful
Very cool designs and animations!!!

[Fundamentals of Hierarchy in Interface Design (UI)](https://medium.com/swlh/fundamentals-of-hierarchy-in-interface-design-ui-ba8e3017dceb)
Very cool examples!!!

* Size - The larger the element, the more it will attract attention.
* Color - Bright colors stand out more than muted tones.
* Proximity - Elements close to each other attract more attention than distant elements.
* Alignment - Any element that separates from the alignment of the others will attract attention.
* Repetition - Repeated styles give the impression that the elements are related.
* Negative space - The more space around the element, the more attention it generates.
* Texture - Varied and complex textures attract more attention than flat ones.

[UI Design Principles](https://medium.com/17seven/ui-design-principles-c6e5e63690f0)
Cool ilustrations / examples
1. Hierarchy
2. Contrast
3. Proximity
4. Balance

[A guide to adding animations to your product — examples and tools](https://uxdesign.cc/a-guide-to-adding-animations-to-your-product-examples-and-tools-7350af66cf78)
It is always frustrating to see nothing on the page, and it causes bounce.
https://www.awwwards.com/websites/animation/
Tools

[Discussion on dark theme](https://mobile.twitter.com/adamwathan/status/1350454882059358208?s=09)
> This is a great example of why you need more control when implementing dark mode than just reversing the color scheme or overriding basic variables like "--color-primary" or similar.
* Dark sections shouldn't be inverted to light in dark mode, you'd get a blinding light block — bad in dark environments.
* The background switches from light to dark, but in the light design it's a solid color, and in the dark design it's a gradient. You can't do this by changing a color variable.
* The little badge/well above the headline is already dark, but it gets darker in dark mode to retain enough contrast against the background. A naive dark implementation would make this well light, and it wouldn't look like a well anymore.
* Call to action buttons switch from indigo in light mode to pink in dark mode, again not an inversion.
