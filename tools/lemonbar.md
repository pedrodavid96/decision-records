# Lemonbar

## Show task being done in bottom of screen

```bash
while true; do echo "%{l}%{B#000000}%{c}%{B#FF0000} Test %{r}%{B#000000}"; sleep 1; done  | lemonbar -b
```

```
while true; do echo "%{l}%{B#000000}%{c}%{B#FF0000} $(timew | head -1 | sed 's/Tracking //g') %{r}%{B#000000}"; sleep 2; done  | lemonbar -b &
```

## Perfect bar

Clickable interface

### "Productivity bar" (Could be hidden in focus mode ?)

Current task and status (? elapsed time? pomodoro timer? might be too distracting in focus mode)
Up next (? might be too distracting in focus mode)

[[bspwm] Interactive & Animated lemonbar, tmux and Anki : r/unixporn](https://www.reddit.com/r/unixporn/comments/hmeh6h/bspwm_interactive_animated_lemonbar_tmux_and_anki/)
[lemonbuddy/config at master · NateBrune/lemonbuddy · GitHub](https://github.com/NateBrune/lemonbuddy/blob/master/config)
[GitHub - b3nj5m1n/lemonblocks: A status bar generator for lemonbar, inspired by i3blocks and dwmblocks.](https://github.com/b3nj5m1n/lemonblocks)
[i3config/i3barconky at master · nicklan/i3config ·GitHub](https://github.com/nicklan/i3config/blob/master/i3barconky)
[Succade - Run, feed and style your Lemonbar with ease - (succade)](https://opensourcelibs.com/lib/succade)
