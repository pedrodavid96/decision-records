# rsync

`sudo rsync -vHAXS --delete folder_to_backup/ [ssh:]backup_folder/`

```
-a, --archive         indicates that files should be archived, meaning that most of their characteristics are preserved (but not ACLs, hard links or extended attributes such as capabilities)
-H, --hard-links      preserve hard links
-A, --acls            preserve ACLs (implies -p)
-X, --xattrs          preserve extended attributes
-S, --sparse          handle sparse files efficiently

-q, --quiet
-v, --verbose
```
