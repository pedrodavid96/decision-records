[CppCon 2017: Herb Sutter “Meta: Thoughts on generative C++”](https://www.youtube.com/watch?v=4AfRAVcThyA&list=WL&index=7)
Awesome talk with practical examples of generative C++.
 Enphasis on tools to support (show generated classes).
We can think people will abuse this, but they're already abusing
 the languageg with more convuluted ways in order to achieve the
 same results.
Same counter-argument can be used against the worst compile-time.

[Kotlin extension function generation 🚀… | by Bartek Lipinski | Medium](https://medium.com/@blipinsk/kotlin-extension-methods-generation-15b5e6499dc8)
[GitHub - Hervian/log-weaver: Various @Log-annotations, that weave log statements into the byte code upon compilation](https://github.com/Hervian/log-weaver)
[Aspect4Log - one cross-cutting concern less to worry about! –](https://aspect4log.sourceforge.net/usage.html)