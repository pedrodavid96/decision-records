[Frugal skateboards that don't suck?](https://www.reddit.com/r/NewSkaters/comments/5bua85/frugal_skateboards_that_dont_suck/)
Completes can be fine for a start.

Deck Cold weather maple makes the best deck, brand doesn't matter. Canadian maple is the best :) So just find a deck you like. If you want some background on which companies make their decks where, here is a list: Skateboard Deck Woodshops.

Trucks The top truck brands are Independent, Thunder, and Venture. If you're on a budget, Krux also makes some quality stuff at a low price. Trucks are the part that last the longest, so it's worth going in on a good set of trucks. All the other parts will be replaced multiple times before you need new trucks.

Wheels If you're new to skating, don't bother with small, hard wheels. Your average skater and the industry in general will push you to get small, hard wheels because "they trick so much better." But really, as a new skater, you need to be cruising around, building your board control. The more pleasurable riding around is, the more likely you are to do it. And the best way to do that is on larger, softer wheels. Something in the 54-56mm range and 78-86a will still trick great, they will not hold you back from learning flip tricks, but will ride so much smoother. The most recommended around here are the Ricta Clouds and the Spitfire Chargers. I'm partial to the latter as they really are made for both cruising and tricking.

Bearings Bones Reds are the de facto bearings, no reason to get anything else if you're on a budget. If you can swing the cash, take a look at the Bronson G3 bearings.

Grip Jessup grip is the best bang for your buck grip tape. Sometimes it's even free with the purchase of a deck. It's great. Mob, Shake Junt, Grizzly, they're all great, too. But Jessup is usually the best choice on a budget and it's honestly some of the best grip out there.

Hardware (Nuts & Bolts) Hardware is not better between brands. Heck, you might even get it cheaper at a hardware store than a skate shop. Just make sure you get locking nuts, with nylon locking nuts or nyloc being the most popular. I also recommend Allen head bolts over Phillips, as the latter is designed to strip over a certain torque, making you more likely to strip them and find it harder to remove them.

Miscellaneous You don't need riser/shock pads.

After you get your setup, you'll have to break in your bushings. Or, when you have the cash, just snag some Bones HardCore bushings. They're a great first upgrade (stock bushings, even on the best trucks, aren't all that great) and don't require a break-in. But you may want to get them after you ride around a bit to see if you prefer loose or tight trucks. Then you'll known which hardness to buy.

Shoes You can get by with just about anything, but if you want to get the best grip and most durable shoe for skateboarding: get some suede shoes. My favorites are the Etnies Jameson, but any brand is fine so long as the uppers are made of suede and the sole is a nice rubber. You shouldn't need to spend more than $60 on a pair of skate shoes, but they will make a world of difference. NEVER get canvas shoes. They tear up way too easily.

That should cover it. Have fun!

---

Ricta Clouds, Thunder light trucks, Bones Reds, and free grip tape. Easy decision.
