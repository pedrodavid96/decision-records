# VPN

[WireGuard](https://wiki.r-selfhosted.com/guides/software/virtual-private-networks/wireguard/)

[WireGuard VPN setup in Terraform CDK for Road Warriors](https://ulrichkautz.com/posts/2021-12-05_roadwarrior-wireguard-vpn-in-terraform-cdk/)

[GitHub - ukautz/roadwarrior-vpn: A Wireguard based Road Warrior VPN deployed with Terraform CDK](https://github.com/ukautz/roadwarrior-vpn)

[[How To] Configure WireGuard via the NetworkManager GUI (Advanced Network Manager)](https://forum.manjaro.org/t/how-to-configure-wireguard-via-the-networkmanager-gui-advanced-network-manager/138040)
[GitHub - lucasheld/wireguard-high-availability: Creates a high available WireGuard cloud infrastructure using Terraform and Ansible.](https://github.com/lucasheld/wireguard-high-availability/tree/master)
[High Availability WireGuard on AWS | Pro Custodibus](https://www.procustodibus.com/blog/2021/02/ha-wireguard-on-aws/)

[Monitoring & Alerting for WireGuard VPN | by Puru Tuladhar | Nerd For Tech | Medium](https://medium.com/nerd-for-tech/wireguard-vpn-monitoring-alerting-e1e1d1eaaa4e)

[Set Up Your Own Wireguard VPN Server with 2FA in 5 Minutes! - YouTube](https://www.youtube.com/watch?v=SMF301vQqJo)
[Build your OWN WireGuard VPN! Here's how - YouTube](https://www.youtube.com/watch?v=5NJ6V8i1Xd8)

[How To Build Your Own Wireguard VPN Server in The Cloud - YouTube](https://www.youtube.com/watch?v=7yC-gJtl9mQ)
[Getting Started Building Your Own Wireguard VPN Server - Networking & Firewalls - Lawrence Systems Forums](https://forums.lawrencesystems.com/t/getting-started-building-your-own-wireguard-vpn-server/7425)

[Troubleshooting WireGuard DNS Issues | Pro Custodibus](https://www.procustodibus.com/blog/2023/09/troubleshooting-wireguard-dns-issues/)

[How To Set Up WireGuard Firewall Rules in Linux - nixCraft](https://www.cyberciti.biz/faq/how-to-set-up-wireguard-firewall-rules-in-linux/)
This guide made my setup finally work! (even though I haven't understanded why yet).

I was able to connect to WireGuard already but basically I couldn't access the internet.
Initially this looked like a DNS issue but after some discovery I found I wasn't able to
ping ip addresses directly as well.

[VPN | Kasad BookStack](https://web.archive.org/web/20230206040403/https://books.kasad.com/books/kasadcom/page/vpn)

Enable IP Forwarding (was actually already enabled by default in my case, but has to be part of steps).

TODO:
[Use DNS server inside vpn-network : WireGuard](https://www.reddit.com/r/WireGuard/comments/twq3x7/use_dns_server_inside_vpnnetwork/)
I've configured it but it doesn't seem to be enabled by default in the clients.
[Should WireGuard point to router which points to PiHole?](https://www.reddit.com/r/WireGuard/comments/twq3x7/use_dns_server_inside_vpnnetwork/#:~:text=another%20option,for%20primary%20dns)
Could this be for the same reason I don't get automatically set DNS server in my NAS?

---

[wg-easy/wg-easy: The easiest way to run WireGuard VPN + Web-based Admin UI.](https://github.com/wg-easy/wg-easy)
[wg-easy/docker-compose.yml at master · wg-easy/wg-easy](https://github.com/wg-easy/wg-easy/blob/master/docker-compose.yml)

## Dealing with Network clashes

[Connecting to services through VPN when the subnets of the remote network are the same. : WireGuard](https://www.reddit.com/r/WireGuard/comments/bp01ci/connecting_to_services_through_vpn_when_the/)
[How do you deal with VPN access that has the same subnet as your corporate environment? : sysadmin](https://www.reddit.com/r/sysadmin/comments/437y5p/how_do_you_deal_with_vpn_access_that_has_the_same/?st=jvpkq86r&sh=d0cc4bec)
[Connecting to a remote server through a VPN when the local network subnet address conflicts with a remote network - Server Fault](https://serverfault.com/questions/548888/connecting-to-a-remote-server-through-a-vpn-when-the-local-network-subnet-addres)
[networking - Correct Network IP addressing if your users have ability to VPN in - Server Fault](https://serverfault.com/questions/854178/correct-network-ip-addressing-if-your-users-have-ability-to-vpn-in)

Discovered this while investigating why I couldn't connect to my home devices
via their internal IPs from my parents home while connecting through the VPN.

This seemed to be because mine and my parents network are configured to both
192.168.1.X... as such my Laptop didn't know if that IP was in one network or
the other. Also my laptop (MacOS) seems to be forcing the default route and I
can't change it...

After searching this seems a widely "known issue" that I wasn't aware of:

> Or at least don't use 192.168.1.X... That's the default subnet of 99% of consumer routers..
> VPNs are sort of an unplanned abstraction of networks.
  IP address range overlap breaks it, as pointed out.
  Normally these sort of overlaps get planned ahead, to avoid this issue.

TODO: Implement workaround. Either change my own network or create a subnetwork?
[UniFi - How to manage VLANs on the USW Flex Mini - YouTube](https://www.youtube.com/watch?v=T76oIwnjMnU)

[Private network#Private IPv4 addresses](https://en.wikipedia.org/wiki/Private_network#Private_IPv4_addresses)

The following on the guest kinda worked:
```
sudo route -n add -host 192.168.1.255 -interface en0
sudo route -n add -host 192.168.1.254 -interface en0
sudo route -n add -net 192.168.1.0/24 -interface utun4
sudo route delete -host 192.168.1.254
sudo route delete -host 192.168.1.255
# For some reason this delete seems to be what actually makes it work
sudo route delete -net 192.168.1.0/24
```

## SAAS Offerings

[Tailscale · Best VPN Service for Secure Networks](https://tailscale.com/)
[juanfont/headscale: An open source, self-hosted implementation of the Tailscale control server](https://github.com/juanfont/headscale)
[Twingate](https://www.twingate.com/)
[NetBird - Connect and Secure Your IT Infrastructure in Minutes](https://netbird.io/)

[Accessing a Docker Compose application via Tailscale with TLS (HTTPS) - Alex Klibisz](https://alexklibisz.com/2024/09/07/accessing-docker-compose-application-tailscale-tls)
[What is a tailnet? · Tailscale Docs](https://tailscale.com/kb/1136/tailnet)
