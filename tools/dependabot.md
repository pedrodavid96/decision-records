# [Dependabot](https://dependabot.com/)
# [Renovate](https://renovate.whitesourcesoftware.com/)
[Automated Dependency Updates](https://jamiemagee.co.uk/blog/automated-dependency-updates/)

On the "opposite side" -> https://deadpendency.com/
Checks "inactive" / archived dependencies and warns about them.
Project seems somewhat new and without much following.
Also doesn't look like OSS and plans to be paid for private repositories in the future.
