On Github:
[Rendering and diffing images](https://docs.github.com/en/github/managing-files-in-a-repository/rendering-and-diffing-images)
Different modes:
2-up
Swipe
Onion skin

[Uber-archive - image-diff](https://github.com/uber-archive/image-diff)
Deprecated, the following replacements are recommended:
[looks-same](https://github.com/gemini-testing/looks-same)
[pixelmatch](https://github.com/mapbox/pixelmatch)

[git-diff-image](https://github.com/ewanmellor/git-diff-image)

[lcs-image-diff](https://github.com/bokuweb/lcs-image-diff-rs)
> Image diff library and tool with LCS algorithm. rust port of murooka/go-diff-image
[Longest common subsequence problem](https://en.wikipedia.org/wiki/Longest_common_subsequence_problem)
This might be usefull if we're creating our own.

[Resemble.JS](https://github.com/rsmbl/Resemble.js)
Analyse and compare images with Javascript and HTML5

[percy](https://percy.io/)
Visual Testing SAAS
[Open-source Visual Review & Testing Platform | Pixeleye](https://pixeleye.io/home)

[Loki · Visual Regression Testing for Storybook](https://loki.js.org/)
