# Keycloak

## Configuration

[Declarative configuration API · keycloak/keycloak · Discussion #33049](https://github.com/keycloak/keycloak/discussions/33049)

[adorsys/keycloak-config-cli: Import YAML/JSON-formatted configuration files into Keycloak - Configuration as Code for Keycloak.](https://github.com/adorsys/keycloak-config-cli)

[Docs overview | mrparkers/keycloak | Terraform | Terraform Registry](https://registry.terraform.io/providers/mrparkers/keycloak/latest/docs)

[Keycloak Provider | Pulumi Registry](https://www.pulumi.com/registry/packages/keycloak/)

[keycloak/terraform-provider-keycloak: Terraform provider for Keycloak](https://github.com/keycloak/terraform-provider-keycloak)
Ownership of tf provider is changing to keycloak:
https://github.com/keycloak/terraform-provider-keycloak/issues/964#issuecomment-2499926505

> Currently preparing a new minor release v4.5.0 with only include essential security updates and "namespace change
   (mrparkers/terraform-provider-keycloak -> keycloak/terraform-provider-keycloak).
> After that, we will prepare a new v5.0.0 release with the goal of supporting at least Keycloak 24 and Keycloak 26.

[Upstream repo is abandoned · Issue #573 · pulumi/pulumi-keycloak](https://github.com/pulumi/pulumi-keycloak/issues/573)
(Pulumi projects seems to be using "upstream" terraform repo).

## Testing

[Introducing the Keycloak Test Framework - Keycloak](https://www.keycloak.org/2024/11/preview-keycloak-test-framework)
Seems like a great example on how to approach complex platform testing in Java (Junit 5).

## Monitoring

https://grafana.com/grafana/dashboards/10441-keycloak-metrics-dashboard/

[Grafana Dashboards · Issue #34651 · keycloak/keycloak](https://github.com/keycloak/keycloak/issues/34651)
Initiative on integrating grafana dashboards into the project.


## Customization

### UI

[Keycloakify](https://www.keycloakify.dev/)

> [!TODO]
> Should we get an icon for the linked Identity Provider Apps?

### Backend

Example: [Extending Keycloak for All Your Identity Use Cases by GR Patil - YouTube](https://www.youtube.com/watch?v=t8u-QO02Qn8)
Seems like an excellent resource to see a practical example of keycloak extensions "e2e" (includes notes on liquibase, tests, etc...).

> [!TODO]
> Spike on `lldap` custom integration usings its GraphQL API.

## SSO Integration

### OpenId Connect with Google

> [!TODO]
> Review Authorized redirect URIs

#### Sandbox

[gcp.applicationintegration.AuthConfig | Pulumi Registry](https://www.pulumi.com/registry/packages/gcp/api-docs/applicationintegration/authconfig/)
[Docker Provider | Pulumi Registry](https://www.pulumi.com/registry/packages/docker/)
[proxmoxve.User.Token | Pulumi Registry](https://www.pulumi.com/registry/packages/proxmoxve/api-docs/user/token/)
[Set up ADC for a local development environment  |  Authentication  |  Google Cloud](https://cloud.google.com/docs/authentication/set-up-adc-local-dev-environment)
[Configure Workload Identity Federation with AWS or Azure  |  IAM Documentation  |  Google Cloud](https://cloud.google.com/iam/docs/workload-identity-federation-with-other-clouds#gcloud_3)
[Troubleshoot your ADC setup  |  Authentication  |  Google Cloud](https://cloud.google.com/docs/authentication/troubleshoot-adc#user-creds-client-based)
[Import Existing Cloud Infrastructure | Pulumi Docs](https://www.pulumi.com/docs/iac/adopting-pulumi/import/)
[googleapi: Error 400: Invalid value for field 'region': 'global'. Unknown region., invalid when running import google without regions flag · Issue #1956 · GoogleCloudPlatform/terraformer](https://github.com/GoogleCloudPlatform/terraformer/issues/1956)

---

Didn't work as it seems Google Cloud doesn't expose the necessary APIs to automate this.

The closest I could get:

```
$ gcloud iam oauth-clients create "my-client-id" \
    --project=$PROJECT_ID \
    --location=global \
    --client-type="public-client" \
    --display-name="My OAuth application 2" \
    --description="An application registration for MyApp" \
    --allowed-scopes="https://www.googleapis.com/auth/cloud-platform" \
    --allowed-redirect-uris="https://example.com" \
    --allowed-grant-types="authorization_code_grant"
$ gcloud iam oauth-clients create your-oauth-client \
  --display-name="Your OAuth Client Display Name" \
  --location="global" \
  --client-type="confidential-client" \
  --allowed-grant-types="authorization-code-grant,refresh-token-grant" \
  --allowed-scopes="openid,email" \
  --allowed-redirect-uris="https://foo.bar.com/login/callback"
$ gcloud iam oauth-clients update "my-client-id" \
    --project=$PROJECT_ID \
    --location=global \
    --allowed-redirect-uris="https://keycloak.pedrodavid.cloud/realms/master/broker/google-2/endpoint" \
    --allowed-grant-types="authorization_code_grant,refresh_token_grant" \
    --allowed-scopes="openid,email
```

```
$ gcloud iam oauth-clients credentials create my-oath-client-credential --location="global" --oauth-client="my-client-id"
$ gcloud iam oauth-clients credentials describe my-oath-client-credential --location="global" --oauth-client="my-client-id"
```
The describe returns the clientSecret

**Misc notes:**
https://cloud.google.com/iam/docs/reference/rest/v1/projects.locations.oauthClients/create
> Required. The parent resource to create the OauthClient in. The only supported location is global.

> ERROR: (gcloud.alpha.iam.oauth-clients.credentials.delete) INVALID_ARGUMENT: A credential can only be deleted if it is disabled
```
$ gcloud alpha iam oauth-clients credentials update my-oath-client-credential --location="global" --oauth-client="my-client-id" --disabled
$ gcloud alpha iam oauth-clients credentials delete my-oath-client-credential --location="global" --oauth-client="my-client-id"
$ gcloud iam oauth-clients delete "my-client-id" --location="global"
```

Identity Platform is found in some related threads, namely it has a Terraform resource but it seems a completely
separte authentication mechanism - controlled by Google - that leverages firebase and other Google tech to
authentication into Google Resources.

This is completly different to allowing access to Google (GMail) OAuth / OpenID information.
For that the Google API doesn't seem even close to ready.

I've created https://issuetracker.google.com/issues/386671297 and commented on https://issuetracker.google.com/issues/35907249 with discoveries on the lackluster APIs.

Somewhat tracked on https://github.com/hashicorp/terraform-provider-google/issues/6074 (closed for inactivity)
 but the problems seems to be on Google Cloud APIs themselves.

### Synology NAS

Currently a PITA.

[SAML SSO - Stuck with "configuration error" | Synology Community](https://community.synology.com/enu/forum/1/post/161084)
[SAML SSO fails (Can't get user uid ()) | Synology Community](https://community.synology.com/enu/forum/1/post/185349)
> To allow local users to sign in via SAML SSO, go to your IdP and make sure that it contains local users with the same usernames as those in your Synology NAS.
In the future we might want a complex mapper that removes the email domain or something like that.

Troubleshooting these type of issues (after custom scopes / mappers and manual changes to the username):
```bash
$ sudo tail -f /var/log/synoscgi.log
2024-11-30T10:07:14+00:00 Home-NAS synoscgi_SYNO.API.Auth_7_login[22465]: oidc_auth.cpp:56 user is local: pedro.david, pedro.david
2024-11-30T10:07:14+00:00 Home-NAS synoscgi_SYNO.API.Auth_7_login[22465]: pam_syno_sso.cpp:128 (euid=0)(pam_syno_sso.cpp:128)(Success)Failed [username.empty()]
2024-11-30T10:07:14+00:00 Home-NAS synoscgi_SYNO.API.Auth_7_login[22465]: pam_syno_log_fail(sso:auth): Can't get user uid ().
2024-11-30T10:07:14+00:00 Home-NAS synoscgi_SYNO.API.Auth_7_login[22465]: login.c:1130 Get uid/gid fail for user []. [0x1D00 user_db_get.c:36]
```

So it seems to detect the right username but then it just assumes it is empty (?).

[SAML SSO fails (Can't get user uid ()) | Synology Community | Page 2](https://community.synology.com/enu/forum/1/post/185349?page=2&sort=oldest)
> TL;DR The Username has to match the LDAP "Name" exactly when being passed in the SSO.
  Change your NameID to pass the username@domain that matches your LDAP configuration.  Being able to type just the username in the normal login doesn't mean the SSO passing the username will allow it to link to the LDAP user.

This might suggest I need an LDAP configuration regardless (note that this is for SAML so not exactly the same setup).

But seems to corroborate with this more similar setup:
[OpenID SSO : synology](https://www.reddit.com/r/synology/comments/v7u50s/openid_sso/)
> Is it possible to achieve this without adding the nas to my LDAP?
> > DSM needs to map user logins to users that are granted permissions across the system -- that makes the same LDAP or AD for both DSM and SSO.

[Using Authelia for auth to Synology NAS : synology](https://www.reddit.com/r/synology/comments/1auxzs4/using_authelia_for_auth_to_synology_nas/)
Also suggests that LDAP might work but notes specificities with `lldap` integration.

Additional misc resources:
* [Setting up SSO with a Synology NAS (DSM 7.2) - Auth0 Community](https://community.auth0.com/t/setting-up-sso-with-a-synology-nas-dsm-7-2/123268)
* [How to activate SAML SSO on Synology NAS with Microsoft Entra ID (formerly Azure AD) - Synology Knowledge Center](https://kb.synology.com/en-us/DSM/tutorial/How_to_activate_Entra_ID_SAML_SSO)
  Most similar setup in the knowledge base but still SAML.

#### A few resources found later

[Synology OIDC SSO with Keycloak : synology](https://www.reddit.com/r/synology/comments/17eiqn0/synology_oidc_sso_with_keycloak/)

---

Trying to setup with `lldap` and facing huge issues trying to connect Synology and `lldap` as well.
Not sure if facing the same but there are mentions to this issue:
> [In that category, the most prominent is Synology. It is, to date, the only service that seems definitely incompatible with LLDAP.](https://github.com/lldap/lldap/tree/main#:~:text=in%20that%20category%2C%20the%20most%20prominent%20is%20synology.%20it%20is%2C%20to%20date%2C%20the%20only%20service%20that%20seems%20definitely%20incompatible%20with%20lldap.)
[Support Synology · Issue #340 · lldap/lldap](https://github.com/lldap/lldap/issues/340) (marked as `wontfix`)


---
[Sync users between Synology's - Setting up an LDAP server on Synology NAS - YouTube](https://www.youtube.com/watch?v=Ac4FVy9N068)

Trying synology LDAP Server I can't seem to make it work

Created DDNS entry to have a FQDN available for the server (pointing to the internal network).
Server supposedly sets up correctly but when I try to go to "Users" it says I need an LDAP Server up and running.
```
$ sudo tail -f /var/log/messages
synoldapbrowser[5668]: ldap_browser_get_dn.c:42 SYNOLDAPSearchGetNext get next failed, ldap_no_such_object (32)
synoldapbrowser[5677]: synoldapbrowser.cpp:1705 Failed to get LDAP ppolicy. [0x0000 file_get_section.c:83]
synoldapbrowser[19115]: ldap_server_enable_overlay.cpp:40 Empty Data
synoldapbrowser[19115]: ldap_server_enable_overlay.cpp:162 SYNOLDAPModify failed to enable module socket. [0x0000 file_get_section.c:83]
synoldapbrowser[19121]: ldap_browser_get_dn.c:42 SYNOLDAPSearchGetNext get next failed, ldap_no_such_object (32)
synoldapbrowser[19130]: synoldapbrowser.cpp:1705 Failed to get LDAP ppolicy. [0x0000 file_get_section.c:83]
synoldapbrowser[19509]: ldap_server_enable_overlay.cpp:40 Empty Data
synoldapbrowser[19509]: ldap_server_enable_overlay.cpp:162 SYNOLDAPModify failed to enable module socket. [0x0000 file_get_section.c:83]
synoldapbrowser[19520]: ldap_browser_get_dn.c:42 SYNOLDAPSearchGetNext get next failed, ldap_no_such_object (32)
synoldapbrowser[19540]: synoldapbrowser.cpp:1705 Failed to get LDAP ppolicy. [0x0000 file_get_section.c:83]1
```
