# Git repository analysis tool

Create metrics
Create pretty front end for those metrics (independent of the metrics themselves)

Alternatives

[hercules - Gaining advanced insights from Git repository history.](https://github.com/src-d/hercules)
Seems to be available as a Github Action, interesting idea
[gitinspector - 📊 The statistical analysis tool for git repositories](https://github.com/ejwa/gitinspector)
Looks like it gives very few statistics

---

[Use the Git History to Identify Pain Points in Any Project](https://preslav.me/2020/03/01/use-the-git-history/)
[Reddit discussion and other cool commands](https://www.reddit.com/r/programming/comments/fbwuxe/use_the_git_history_to_identify_pain_points_in/)

```bash
$ git log --format=format: --name-only | egrep -v '^$' | sort | uniq -c | sort -rg | head -10
```

i tried it on one of my projects and it basically just showed me the oldest files in the project, but that can be improved upon :)

starting with this:

```
git log --format=format: --name-only | egrep -v '^$' | sort | uniq -c | sort -rg | head -10
```

filter for "interesting" files (python and html code for me in this project):

```
git log --format=format: --name-only | egrep -v '^$' | sort | grep -e \.py -e \.html | uniq -c | sort -rg | head -10
```

in addition i don't care about the ever changing version.py file, so i exclude it:

```
git log --format=format: --name-only | egrep -v '^$' | sort | grep -e \.py -e \.html | grep -v -e version\.py | uniq -c | sort -rg | head -10
```

and i only care about what happened last year:

```
git log --format=format: --name-only --after=2019-01-01 --before=2020-01-01| egrep -v '^$' | sort | grep -e \.py -e \.html | grep -v -e version\.py | uniq -c | sort -rg | head -10
```

git built-in filtering

```
git log --after=2019-01-01 -- '*.py' '*.html' ':!*version.py'
```

---

[https://visualize-your-git.herokuapp.com/display/223/sparse](https://visualize-your-git.herokuapp.com/display/223/sparse)
Cool visualization of most used git commands

[Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/#specification)
