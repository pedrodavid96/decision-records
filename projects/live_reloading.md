[Gaze](https://github.com/wtetsu/gaze)
👁️ Gaze runs a command, right after you save a file.
[Discussion](https://www.reddit.com/r/programming/comments/hbetyd/gaze_a_cli_tool_that_accelerates_your_quick_coding/)
Alternative:
```bash
gaze() { while inotifywait -e modify $1; do eval $2; done }
```
