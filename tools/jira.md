# JIRA

[What is the deployments feature? | Atlassian Support](https://support.atlassian.com/jira-cloud-administration/docs/what-is-the-deployments-feature/)
[Use the release page to check the progress of a version | Jira Software Cloud | Atlassian Support](https://support.atlassian.com/jira-software-cloud/docs/use-the-release-page-to-check-the-progress-of-a-version/)

[Sharing some Custom Commands · Issue #455 · go-jira/jira · GitHub](https://github.com/go-jira/jira/issues/455)