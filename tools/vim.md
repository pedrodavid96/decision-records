#Vim

[Asynchronous Lint Engine](https://github.com/w0rp/ale)
Acts as a Vim [Language Server Protocol](https://langserver.org/) client.

## Does vi silently add a newline (`LF`) at the end of file?

[StackExchange discussion](https://unix.stackexchange.com/questions/263869/does-vi-silently-add-a-newline-lf-at-the-end-of-file)
This is the expected vi behavior.

## Save buffer whenever text is changed
`autocmd TextChanged,TextChangedI <buffer> silent write`

## File navigation

### Set the working directory to wherever the open file lives
`:set autochdir`

### Find file
either use wild cards
`:sp **/vim<tab>`
or find (works better with tabmenu, otherwise you need to specify the match number)
`:find vim<tab>`
to open directly on a split
`:vsp | :find vim<tab>`

## Vim spellcheck

[Highlight style](https://stackoverflow.com/questions/6008921/how-do-i-change-the-highlight-style-in-vim-spellcheck)

## File Explorer
Open in split using
```
:Ve
:Vex
:Se
:Sex
```
(The x is optional and results in the same)
Create file using `%`, rename with `R`...

## Minimalist

[Goyo - Distraction-free writing](https://github.com/junegunn/goyo.vim)

[Limelight - Hyperfocus-writing](https://github.com/junegunn/limelight.vim)

---

[Project specific .vimrc files | Andrew Stewart](https://andrew.stwrt.ca/posts/project-specific-vimrc/)

## Vim Functions

[Custom Vim Functions to Format Your Text](https://towardsdatascience.com/custom-vim-functions-to-format-your-text-b694295f7764)

## [HyperStyle VIM](http://ricostacruz.com/vim-hyperstyle/)
Vim Plugin for CSS

[vim-closer](https://github.com/rstacruz/vim-closer) - closes functions

## Folds

zf (close fold) zo (open fold) zO (open fold recursively)

## Marks

* ma (mark letter a)
* <verb>?'a (go to line of mark a)
* <verb>?`a (go to position of mark a)
* :marks (show marks)
* special marks:
    - . (last change in current buffer)
    - " (last exited current buffer)
    - 0 (last file edited)
    - ' (line jumped from)
    - ` (position jumped from)
    - [/] (begin / end of last change / yank)
    - </> (begin / end of last visual selection)

## Search word under cursor

`*`

## Replace search selection

* \zs (selection start)
* \ze (selection end)

## Ranges, confirmation:

* :%s/pattern/replace/gc (g -> all occurrences in line, c -> ask confirmation)
* % (1,$ (end of file))
* 1,. (current line)

## Registers

CR CW -> word under cursor
yanked text goes to register 0
CR <register>: (nice trick, yank text you want to search for (if more than 1 word * doesn't work), and CR 0)

## Tabs

tabedit: create tab
gt: next tab
gT: prev tab
<tab number (starting at 1)>gt

## Configure Git Gutter

[Can I disable continuation of comments to the next line in Vim?](https://superuser.com/questions/271023/can-i-disable-continuation-of-comments-to-the-next-line-in-vim)
```
:set formatoptions-=cro
```
```
:set paste
```
```
nnoremap <silent> <cr> :set paste<cr>o<esc>:set nopaste<cr>
```

## shortcut that maps space bar to save current file and run previous command:
```
map <Space> :w<Enter>:!!<Enter>
```

[Learn Vim (the Smart Way)](https://github.com/iggredible/Learn-Vim)

[Vim: Tutorial on Editing, Navigation, and File Management (2018)](https://www.youtube.com/watch?v=E-ZbrtoSuzw)
Very nice talk 🥇⭐

[A Vim Guide for Intermediate Users](https://thevaluable.dev/vim-intermediate/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/kloop5/a_vim_guide_for_intermediate_users/) - mentions alternative, [kak](http://kakoune.org/)
* Vim’s Spatial Organization: Buffers | Windows | Tabs | Argument List (arglist)
* Mapping Keystrokes
* Jump! Jump! Jump!: Jump list | Change list | Methods Jumping
* Repeating Keystrokes: Single Repeat | Complex Repeat: The Macro
* Command Line Window
* Undo Tree
* Plugins: Plugin Manager
  - Closing Buffers Without Closing Windows - winresizer
  - Navigating through your buffer - fzf
  - Manipulating the Undo Tree - MundoTree
* The Path to Mastery

[Tweet using VIM as full blown IDE](https://mobile.twitter.com/Manu343726/status/1328822112677670918?s=09)

[Vim Autocomplete Mini-Overview](https://www.youtube.com/watch?v=bu_AIAp7hCY)
> six ways of doing autocomplete in Vim:
* based on strings in the current buffer
* functions in the codebase (using Universal Ctags)
* file names in your project
* full lines in the buffer
* dictionary words
* any class/constant/function in your codebase using the Language Server Protocol (LSP).

[Vim's Versatile CLI](https://www.youtube.com/watch?v=pt36X1OJRG4)
Ctrl-Z to suspend VIM in background
Pipe into vim temp file -> `vim -`
Integration with `fzf`
`nvim -t` to open by finding ctag
`nvim <file>:<line-number>` - useful for debug
`nvim <file> +/<string>` - run vim command with `+`, in this case we will search for `<string>` right away

[Advanced Vim Workflows](https://www.youtube.com/watch?v=futay9NjOac)
Macros
`:args` -> `:argdo`
Insert filename in buffer
Change word "casing"

[Language Server | Seeking help before I spend nights digging.](https://www.reddit.com/r/vim/comments/kwgz9c/language_server_seeking_help_before_i_spend/)

[Opening specific file types changes shiftwidth and softtabstop](https://www.reddit.com/r/vim/comments/kwdlme/opening_specific_file_types_changes_shiftwidth/)

[[Tutorial] Fzf and Ripgrep](https://www.reddit.com/r/vim/comments/gc6d7v/tutorial_fzf_and_ripgrep/)

[Moving fast with the core Vim motions | Hacker News](https://news.ycombinator.com/item?id=36312027)

[Helix](https://helix-editor.com/)
[Kakoune - Official site](https://kakoune.org/)

[phaazon.net](https://phaazon.net/blog/more-hindsight-vim-helix-kakoune)
[Reddit discussion](https://www.reddit.com/r/programming/comments/13qj7nw/hindsight_on_vim_helix_and_kakoune/)