# etcd

[etcd](https://etcd.io/)

## CMD tool usage examples

```
etcdctl put /message Hello
etcdctl get /message 
etcdctl put greeting "Hello, World!"


etcdctl get --prefix '' -w=json
```
