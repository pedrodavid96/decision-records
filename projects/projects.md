[I couldn’t abandon another side project](https://www.zainrizvi.io/blog/do-more-by-doing-less/?utm_source=reddit&utm_campaign=side_projects)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/iauwyt/how_i_stopped_abandoning_my_side_projects/)
Reddit TL;DR:
> Without any real consequence for failing to deliver, we tend to lose motivation at about 2 weeks.
> So reduce the scope of your project to fit a 2-week timeline.
**Note** that the motivation window is different for each person.

[Motivation and why finishing a personal project is hard.](https://ciaranonuallainblog.wordpress.com/2020/10/04/motivation-and-why-finishing-a-personal-project-is-hard/)
So how to improve?
* Planning
* Research
* Creation
  Have a Unix mindset of write small components that do one thing well.
  As soon as you can, deploy that way you can fix it early.
Conclusion:
* Commit to things that have meaning and importance, this meaning and importance allows you to finish.
* Really plan and have everything laid out before you start.
* Finishing in some form, is better than never finishing.
[Reddit Discussion](https://www.reddit.com/r/programming/comments/j52i17/motivation_and_why_finishing_a_personal_project/)
> By far 2 biggest reasons for me are:
> * I did all the fun things already, and only thing left are the boring ones
> * It scratches my itch but would require decent effort to make it usable by others.

[An app can be a home-cooked meal (2020) | Hacker News](https://news.ycombinator.com/item?id=38877423)

---

https://github.com/danistefanovic/build-your-own-x

short term:
    Objective Wall

    Blog
        - arch tutorial
        - git add -p (Add part of file StackOverflow)
    Website
    CV upgrade

long term:

    Hardware:
        - Arduino (vs// RaspberriePie)
        - Arcade

    Software:
        - Jira integration
            * Detect Windows Lock Screen
            * Detect Git branch changes
        - Site Generator (linked with short term blog) (possibility to learn Rust (non-performance critical, but having a distributable binary that doesn't bundle VM and/or GC and is not interpreted would be fine, safe for a "fairly simple" application))
        - Renderer
        - Text Editor
        - Compiler / Language
        - Game Engine
        - Emulator (e.g.: https://www.retroarch.com/)

ideas:
    Manager(reddit, social networks, mails, calendar, project schedule...)
    Wish List
    Business Simulator

    Review PDM project
    Movie Randomizer (possibility of machine Learning! Top priority)
    Movies Application (imdb like)
    Auto Download Episode, Subtitles and Create Folder (movie manager)

    pseudo-code to chart

Games:
    Pong
    Tetris (HTML5 vs super duper optimized C)
    Asteroids
    Top Down 2D
    2.5D FPS (DOOM...)

    Android Game


Diagrams as code
[example](https://medium.com/better-programming/diagrams-diagram-as-code-56fec222cdf6)

Personal Knowledge Base

[How to Choose Your Next Side Project](https://medium.com/better-programming/how-to-choose-your-next-side-project-9dbe429e6f86)
Find out why?
- I will learn something from it
- I will use what I built afterward
- I estimate a short-medium completion time
- I estimate a short-medium completion time
- I want a new source of income
- I can’t shake the idea out of my head

[What to Code](https://what-to-code.com/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/gcp8z8/what_to_code_search_or_create_ideas_for_your_next/)

[10 Valuable iPhone Apps for February | by Nima Sakhtemani | Mac O’Clock | Feb, 2021 | Medium](https://medium.com/macoclock/10-valuable-iphone-apps-for-february-ebbbfbfffec6)
1. Nudget: Spending Tracke‪r
2. The Great Coffee App: For Those Who Love Coffee
3. Make Time App: Focus On Your Tasks
5. Mindlist — Todo List & Planner
6. Ananda: Binaural Programs to the Rescue
7. Tweet 2 Image: Turn Tweets into Images
8. Sticky Widgets: iOS 14 Sticky Note Widget
9. Denim: Playlist Cover Maker
10. Henry: Habits & Wellbeing

https://www.makeareadme.com/

[Introducing MonoBox: How & Why I Built My Very Own Music Player](https://ozzs.dev/introducing-monobox)

Website that shows graph on which politician is taking more time in their phone
[Criei um software que permite identificar quando os políticos portugueses estão ao telemóvel durante o parlamento!! Passaram mais de 80% do tempo no telemóvel! : portugal](https://www.reddit.com/r/portugal/comments/zewygj/criei_um_software_que_permite_identificar_quando/)