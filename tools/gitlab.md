# GitLab

[How to improve your daily GitLab experience](https://about.gitlab.com/blog/2019/11/26/gitlab-daily-tools/)
Bookmarks with shortcuts

[18 GitLab features are moving to open source](https://about.gitlab.com/blog/2020/03/30/new-features-to-core/)
  [Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)
  [GitLab Maven Repository](https://docs.gitlab.com/ee/user/packages/maven_repository/index.html)
  [Canary Deployments](https://gitlab.com/gitlab-org/gitlab/-/issues/212319)
  [Feature flags](https://gitlab.com/gitlab-org/gitlab/-/issues/212318)

[Project access tokens](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html)
e.g.: bots
Sadly only available in for Gitlab Bronze and above (~4$/month).

[Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)

[Push rules](https://docs.gitlab.com/ee/push_rules/push_rules.html)

[Coverage Guided Fuzz Testing | GitLab](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/)
https://gitlab.com/gitlab-org/gitlab/-/blob/fa33a6a4294ca63a06741a77a83b07e60475dd4b/lib/gitlab/ci/templates/Security/Coverage-Fuzzing.gitlab-ci.yml

[Code Quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)
Generate code quality reports in Gitlab
[Implementing a Custom Tool](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool)

[Unit test reports | GitLab](https://docs.gitlab.com/ee/ci/unit_test_reports.html)

[Metrics Reports | GitLab](https://docs.gitlab.com/ee/ci/metrics_reports.html)
Use cases:
* Memory usage
* Load testing results
* Code complexity
* Code coverage stats

## CI

[Using Docker Images](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)
[Using Docker Images - What is a service?](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service)
[Using docker build](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
A lot of idiosyncrasies since it's running already in a docker container and might need root which we might not have.
[Building a Docker image with kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)

[Only changes](https://docs.gitlab.com/ee/ci/yaml/#onlychangesexceptchanges)
Trigger pipeline only when certain files are changed

[Issue: Multiple .gitlab-ci.ymls MVC](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/18157)
Possible solutions in the comments (use `include` in root file?)

[GitLab Triage](https://gitlab.com/gitlab-org/gitlab-triage)
> This project allows to automate triaging of issues and merge requests for GitLab projects or groups.
[Triage Operations](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#ensure-quick-feedback-for-community-contributions)

[Cancel potentially redundant pipelines automatically](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/8998)


---

[Tools and tips | GitLab](https://about.gitlab.com/handbook/tools-and-tips/)
[Data Infrastructure | GitLab](https://about.gitlab.com/handbook/business-ops/data-team/platform/infrastructure/)
