## [Facebook Real-time Chat Architecture Scaling With Over Multi-Billion Messages Daily](https://www.8bitmen.com/facebook-real-time-chat-architecture-scaling-with-over-multi-billion-messages-daily/)
[Discussion](https://www.reddit.com/r/programming/comments/ccph76/an_insight_into_facebook_realtime_chat/)

[Creating a chat application with WebRTC - LogRocket Blog](https://blog.logrocket.com/creating-chat-application-with-webrtc/)
This would allow an almost "serverless" architecture.

[simplex-chat/simplex-chat: SimpleX - the first messaging network operating without user identifiers of any kind - 100% private by design! iOS, Android and desktop apps 📱!](https://github.com/simplex-chat/simplex-chat)
Mainly Kotlin and Haskell
