# Coffee

[The Beginner’s Guide to Coffee Machine Maintenance](https://youtu.be/Bl7kuC1IQ-g?si=ntgo7heCRjN_-Zu9)

## Speciality Coffee

https://www.reddit.com/r/cafept/comments/19ct871/recomenda%C3%A7%C3%B5es_de_lojas_online_de_caf%C3%A9_em_gr%C3%A3o_de/
https://www.cafesfeb.com/loja/cafe-grao-moidos/cafe-em-grao-estrela-saudade/
https://cafedefinca.eu/producto/pack-exotico-espresso/
https://tasteology.pt/shop/12-cafes-natal-ideias-prenda/


## Expresso

[DEEP Dive Review: Delonghi Dedica vs. Breville/Sage Bambino - YouTube](https://www.youtube.com/watch?v=Z-NWn3Z-5Mw)
Better portafilter - Standard size, heavier, nice curve at end of handle
Has non-pressurized baskets
Valve that releases pressure on the puck and stops dripping at end of extraction
Has some sort of sensor to alarm when low on water
Amazing steamer... (not thinking about that for now)

[Almofada de prensa de café Mat de silicone de campador de café almofada antiderrapante de café (preta) : Amazon.es: Casa e Cozinha](https://www.amazon.es/-/pt/dp/B07LDC1N4M/ref=sr_1_8?keywords=coffee%2Btamper%2Bmat&qid=1674740086&sprefix=coffee%2Btamper%2B%2Caps%2C175&sr=8-8&th=1)
[Best Espresso Knock Box | Reviews - YouTube](https://www.youtube.com/watch?v=P9GC1GARHGQ)
[Dreamfarm DFGR1181 Marco Noir - pote para puxar os bordos do café : Amazon.es: Casa e Cozinha](https://www.amazon.es/-/pt/dp/B0016J7YQM/ref=sr_1_6?crid=1SFJV7UMNPGRX&keywords=knock%2Bbox&qid=1674738549&sprefix=knock%2Bbox%2Caps%2C111&sr=8-6&th=1)


[Best coffee machines to buy in 2021 - tried and tested top picks - BBC Good Food](https://www.bbcgoodfood.com/review/best-coffee-machines)

[The Ultimate Hand Grinder Showdown - YouTube](https://www.youtube.com/watch?v=dn9OuRl1F3k)
[BEST COFFEE HAND GRINDER UNDER $350?! - ($90 VS $325?!😱) - YouTube](https://www.youtube.com/watch?v=MV6awoNQXHU)
[Aergrind – Made By Knock](https://madebyknock.com/products/aergrind)

[How I Make Espresso: Tools and Techniques - YouTube](https://www.youtube.com/watch?v=xb3IxAr4RCo&t=542s)

[ONA Coffee Distributor OCD V3 - Titanium](https://rhinocoffeegear.com/ona-coffee-distributor-ocd-v3-titanium.html)
[53.3mm Tamper & Distributor Combo – Crema Coffee Products](https://cremacoffeeproducts.com/products/53mm-tamper-distributor-combo?variant=35350816522394)

[MATOW Porta-filtro sem fundo de 54 mm, sem fundo, compatível com máquinas Breville Barista Series e Breville de 54 mm, cesto de filtro incluído : Amazon.es: Casa e Cozinha](https://www.amazon.es/-/pt/dp/B08D89QWY5/ref=cm_cr_arp_d_product_top?ie=UTF8)

https://www.javapresse.com/blogs/espresso/quick-guide-to-espresso-machine-portafilter-types
Non-Pressurized Portafilters
> in the end, pressurized portafilters leave you will less control over your shot. And if you’re investing in the skill of espresso, why would you allow a portafilter to limit you?

https://www.deltaslowcoffee.com/pt/pt/

1. French Press
2. V60
3. Chemex

## French press

### Slow coffee

**12g coffee -  200ml de agua quente**
mexa com uma colher
posicione o êmbolo e, **sem o pressionar, deixe repousar durante 4 min**
pressione ao ao fundo e sirva

## Leveling the machine?

So, in my case I've always noticed more flow on the left side of the portafilter than the right,
while not critical this isn't ideal if I want 2 identical shots (usually there's not a big problem
because my so prefers "more" and I'm ok with less).

Today I finally decided to check the level and indeed there's a slight tilt on the machine
(unnoticeable to the eye).

I've slid a towel underneat the left side and it seems much better now...

So I searched for it and found the following post:

[Tip: Make sure your machine is level and doesn't distribute water like this! : espresso](https://www.reddit.com/r/espresso/comments/t2njaa/tip_make_sure_your_machine_is_level_and_doesnt/)
Everyone seems to be mostly moking the guy (besides his awesome photography skills)
so now I'm not sure if the comments are just mean and dismissive without backing
or there's indeed something wrong with the pressure of my shots as it is making
more difference than it should in theory.
