# [Shitlist Driven Development](https://sirupsen.com/shitlists/)

[Reddit Discussion](https://www.reddit.com/r/programming/comments/ke7ab0/shitlist_driven_development_dec_2016/)

In very short summary, create tests that ensure that "shitlisted" APIs
 are still usable from legacy code but new code won't be able to use them.
