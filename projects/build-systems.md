# Build Systems

[Build System Schism: The Curse of Meta Build Systems | Yzena, LLC](https://yzena.com/2024/03/build-system-schism-the-curse-of-meta-build-systems/)
[Build System Schism: The Curse of Meta Build Systems : programming](https://www.reddit.com/r/programming/comments/1bii91u/build_system_schism_the_curse_of_meta_build/)

[Don't require people to change 'source code' to configure your programs : programming](https://www.reddit.com/r/programming/comments/1c0pche/dont_require_people_to_change_source_code_to/)
