# OCaml

[Using, Understanding, and Unraveling The OCaml Language](https://caml.inria.fr/pub/docs/u3-ocaml/index.html)
[Discussion](https://www.reddit.com/r/programming/comments/ic21kp/using_understanding_and_unraveling_the_ocaml/)
> OCaml is so cool. Algebraic data types, match, higher order functions, and this weird module system that allows you to do all sorts of neat things.

TODO: Search about its module system.
