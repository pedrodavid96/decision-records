# Development Workflows

[Postmortem culture](https://engineering-management.space/post/postmortem-culture/)
Typical postmortem information:
* Incident or background of the problem overview
* Timeline
* Contributing factors
* Impact
* Corrective actions
* **How are we going to make sure this doesn’t happen again?**

Review postmortems
A good exercise, specially when onboarding new teammates, is to review old postmortems and debate on the root causes and the proposed solutions.

[RFC driven development](https://engineering-management.space/post/rfc-driven-development/)
Advantages:
* Some code was already implemented elsewhere
* We have some business constraints that clash with the implementation
* The implemented code will fail our performance targets
Downsides:
* takes time. How much time should we invest in it?
* may clash with other processes the team might have (example, SCRUM, planning poker...)
* There’s also the question of when to do a RFC. Should we do it all the times? Even for smaller things?
  (Rust guidelines on this actually seem pretty reasonable: http://rust-lang.github.io/rfcs/#when-you-need-to-follow-this-process)
* help getting context but won't account for everything and might actually differ from the actual implementation

[conventional: comments | Comments that are easy to grok and grep](https://conventionalcomments.org/)

## Build tools in Docker
[Docker the sh#t out of your build](https://www.youtube.com/watch?v=pB13VQigL6Y)
[tim.js meetup 51: Ionic build tools in Docker, by Petru Isfan [english]](https://www.youtube.com/watch?v=uT64OF0mbB8)
