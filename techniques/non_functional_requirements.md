# Non Functional Requirements

[Non-Functional requirements and Micro-services (Israel)](https://www.youtube.com/watch?v=Sl9fMkFmkH8)

* Operability
  Monitoring / Logging / Alerting
* Security
* Performance
* Supportability - tools to enable support
* Scalability
* Reliability
* Sizing
* (Multi-tenancy)
* Extendability
* Usability (UX)
