# Electronics

## Courses

[Beginner Electronics - 1 - Introduction (updated) - YouTube](https://www.youtube.com/watch?v=r-X9coYTOV4&list=PLah6faXAgguOeMUIxS22ZU4w5nDvCl5gs)
[Practical Electronics & Circuits 101 - Intro, Ohm's Law, Loads, AC/DC, Power & Energy - YouTube](https://www.youtube.com/watch?v=6IvXZLYgCE4&list=PLlLR7EXXYZ0YcMaDjlh_1T-_dr_zAZpFM)
[index - AskElectronics](https://old.reddit.com/r/AskElectronics/wiki/index)


## Cables

The neutral wire provides the return path for current back to the power source, completing the circuit.
It's standard that this cable **should be blue**.

H07 VU is a solid-core, rigid wire for fixed installations,
H07 VK is a flexible stranded wire used in applications requiring frequent movement (usually not permanent)
H07 VR is another rigid variant but with slightly different mechanical properties.

The "V" means it's PVC isolation.
