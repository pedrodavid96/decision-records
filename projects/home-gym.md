# Home Gym

[Half rack or full rack? : homegym](https://www.reddit.com/r/homegym/comments/95s64a/half_rack_or_full_rack/)
[Mini budget, mini footprint living room gym (Europe) : homegym](https://www.reddit.com/r/homegym/comments/164w982/mini_budget_mini_footprint_living_room_gym_europe/)
[Current gym. Just ordered a plate loaded pull down. I’m also looking for a quality belt squat (Europe) : homegym](https://www.reddit.com/r/homegym/comments/qilk63/current_gym_just_ordered_a_plate_loaded_pull_down/)
[Lot of waiting and a lot of checking out r/homegym but finally got this in place. Hoping this is helpful for those in Europe/UK in particular. : homegym](https://www.reddit.com/r/homegym/comments/iem5u0/lot_of_waiting_and_a_lot_of_checking_out_rhomegym/)
[Finding a good bench in Italy (Europe) : homegym](https://www.reddit.com/r/homegym/comments/9g8ncr/finding_a_good_bench_in_italy_europe/)
[New rack: Rep PR-4000 Half-rack : homegym](https://www.reddit.com/r/homegym/comments/14ioqhq/new_rack_rep_pr4000_halfrack/)
[AB-3100 Adjustable Weight Bench | REP Fitness](https://pt.repfitness.com/products/ab-3100-adjustable-weight-bench?variant=42040880103563)
[Strength Shop Europe](https://strengthshop.eu/)
[simple products GmbH](https://simpleproducts.de/en/search?search=bench)
[Klimmzugstangen & Pull-Up-Bars](https://simpleproducts.de/en/bodyweight/pull-up-bars/)
[LMX31.01 LMX.® Olympic bar 50mm 220cm - Lifemaxx](https://www.lifemaxx.com/en/lmx3101-lmx-olympic-bar-50mm-220cm.html)
[Shipping & Return Policy – PRx Performance](https://prxperformance.com/pages/return-policy)
[Profile® PRO Blackout Rack – PRx Performance](https://prxperformance.com/products/profile-pro-blackout)
[Barra Olímpica de Competição 20kg - BOXPT](https://boxpt.com/produto/barra-olimpica-de-competicao-20kg/)
[DISCOS - BOXPT - Equipamento de Treino](https://boxpt.com/categoria-produto/peso-livre/discos/)
[Rack Desdobrável - BOXPT](https://boxpt.com/produto/rack-desdobravel/)
[Folding Wall-Mounted Power Rack 2.0 Red I Again Faster Europe](https://againfaster.eu/de_de/strength-equipment/strength-training/folding-squat-rack/again-faster-inward-folding-rack-red.html)
[Power Rack Desdobrável - Fittest Equipment](https://fittestequipment.com/produto/power-rack-desdobravel/)
[Rogue HR-2 Half Rack - Weight Training - Monster Lite Unit | Rogue Fitness PT](https://www.roguefitness.com/eu/rogue-hr-2-half-rack-eu)
[Halteres 1090i SelectTech Bowflex (par) | fitnessdigital](https://www.fitnessdigital.pt/halteres-1090i-selecttech-bowflex-par-/p/10000593/)
[The Absolutely Best Weight Benches for 2023... Flat, Adjustable, Cheap, Expensive, and More! - YouTube](https://www.youtube.com/watch?v=78EjyqRHlSo)
[The BEST Adjustable Dumbbells of 2024 for the Home Gym! - YouTube](https://www.youtube.com/watch?v=aczVjwyve2s&t=6s)

[Sidewinder – Dialed Motion](https://dialedmotion.com/products/sidewinder-system?variant=42959897886892)
[Dialed Motion AMA - A New Universal Rack Mounted Cable System : homegym](https://www.reddit.com/r/homegym/comments/1bobito/dialed_motion_ama_a_new_universal_rack_mounted/)

[Amazon.es : loading pin](https://www.amazon.es/s?k=loading+pin&crid=1MLK87QF8IRV0&sprefix=loading+pin%2Caps%2C106&ref=nb_sb_noss_1)
[Rogue Loading Pin - Grip Strength Training | Rogue Fitness](https://www.roguefitness.com/rogue-loading-pin)

## Rack

[Rack squat de treino, crossfit musculação Valongo • OLX.pt](https://www.olx.pt/d/anuncio/rack-squat-de-treino-crossfit-musculao-IDG2cG3.html?isPreviewActive=0&sliderIndex=1)
[Rack TK Modular – Semperfit](https://semperfit.pt/products/rack-amovivel-modelar?variant=32968753021028)

### Accessories

[Dip Station |TK – Semperfit](https://semperfit.pt/collections/rigs-rags-1-1/products/dip-station)
[Spotter Arms | TK – Semperfit](https://semperfit.pt/products/spotter-arms-para-rig-tk?_pos=2&_sid=72e9f774b&_ss=r)

## Plates

[Pack Challenger com Bumpers easy grip coloridos-usados = 170kg – Semperfit](https://semperfit.pt/collections/barras-e-discos/products/pack-challenger-easy-grip-colorido-170kg)
Pack com barra

[Strength Shop Calibrated Plates Sets 157.5kg or 159kg - IPF Approved](https://strengthshop.eu/collections/weightlifting-plates-sets/products/157-5kg-strength-shop-calibrated-plate-set-ipf-approved)

### Fractional

[Placas de mudança coloridas [KG] – Kensui](https://kensuifitness.com/products/colored-change-plates-kg)

## DumbBells

[AdaptaBELL™ – Kensui](https://kensuifitness.com/products/adjustabell)

## Bench

[Banco Ajustável 2.1 – Semperfit](https://semperfit.pt/products/banco-ajustavel-2-1?_pos=15&_sid=8fbe7b256&_ss=r)

## DIY

[DIY Gym Pulley System for Home Gym, Cable Pulley, Easy Set Up - YouTube](https://www.youtube.com/watch?v=BEPVXShJJ9I)
Incredible ideas for how to use them for lots of exercises.

[DIY Leg Curl & Extension Machine Rack Attachment | Build Your Ultimate Home Gym - YouTube](https://www.youtube.com/watch?v=dEM99ZPpYQs)



[DIY Drink Spotter, a drink holder that mounts to a power rack | home gym hacks - YouTube](https://www.youtube.com/watch?v=-XVxk-gc6wc)

[Leg ext/curl attachment for any bench : homegym](https://www.reddit.com/r/homegym/comments/18dpc0b/leg_extcurl_attachment_for_any_bench/)
[Free Standing Leg Curl Extensão com Foam Roller, Weight Bench Attachment, W4050D-B, Latest Design - AliExpress 1420](https://pt.aliexpress.com/item/1005005966120117.html?gatewayAdapt=glo2bra4itemAdapt)
[FREESTANDING leg extension leg curl attachment | Gear for Fit](https://www.gearforfit.com/p420-freestanding-leg-extension-leg-curl-attachment)


[How to Build a Weightlifting Platform | The Art of Manliness](https://www.artofmanliness.com/health-fitness/fitness/how-to-build-a-weight-lifting-platform/)

[Fitness Floor Tile Pair - 500mm x 500mm x 43mm | Rogue Fitness UK](https://www.roguefitness.com/gb/500mm-x-500mm-x-43mm-floor-tile-pair-eu)
[Granuflex Rubber Flooring, Home Gym Tile - 50cm X 50cm x 43mm – Strength Shop](https://strengthshop.eu/collections/gym-mats/products/granuflex-home-gym-tile-50cm-x-50cm-x-43mm)
