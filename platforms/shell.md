# Shell

[Shell startup scripts — flowblok’s blog](https://blog.flowblok.id.au/2013-02/shell-startup-scripts.html)
Logical diagram, taken from the gitlab repo and added `sh`
```
digraph {
    init [label="~/.shell/env"]
    login [label="~/.shell/login"]
    interactive [label="~/.shell/interactive"]
    logout [label="~/.shell/logout"]

    sh_init [label="~/.sh/env"]
    sh_login [label="~/.sh/login"]
    sh_interactive [label="~/.sh/interactive"]
    sh_logout [label="~/.sh/logout"]

    bash_init [label="~/.bash/env"]
    bash_login [label="~/.bash/login"]
    bash_interactive [label="~/.bash/interactive"]
    bash_logout [label="~/.bash/logout"]

    zsh_init [label="~/.zsh/env"]
    zsh_login [label="~/.zsh/login"]
    zsh_interactive [label="~/.zsh/interactive"]
    zsh_logout [label="~/.zsh/logout"]

    graph [ranksep=1]
    edge [style=dotted]

    init -> login
    init -> interactive

    login -> interactive

    edge [style=solid]

    edge [minlen=1]

    sh_init -> init
    sh_login -> login
    sh_interactive -> interactive
    sh_logout -> logout

    bash_init -> init
    bash_login -> login
    bash_interactive -> interactive
    bash_logout -> logout

    zsh_init -> init
    zsh_login -> login
    zsh_interactive -> interactive
    zsh_logout -> logout

    edge [minlen=2]

    bash_init -> bash_login
    bash_init -> bash_interactive
    bash_login -> bash_interactive

    sh_init -> sh_login
    sh_init -> sh_interactive
    sh_login -> sh_interactive

    zsh_init -> zsh_login
    zsh_init -> zsh_interactive
    zsh_login -> zsh_interactive
}
```

### What really IS the difference between the source and dot commands anyways?

source, on most systems, is just a more readable alias for the dot command (.).
**Since the IEEE POSIX standards outline that the dot command is the only method to source commands in a shell file**, using the dot command is the safest option if portability is a concern.

The source and dot commands both execute the script under the same process as the shell it is executed in, while ./ executes the script under a different process, meaning a new shell is used to run the process, which, upon completion, is closed.

[Performance optimizations for the shell prompt - Sebastian Jambor's blog](https://seb.jambor.dev/posts/performance-optimizations-for-the-shell-prompt/)
[Reddit Discussion](https://www.reddit.com/r/programming/comments/nm7m9c/performance_optimizations_for_the_shell_prompt/)
General performance tips with a cool k8s example.

[Profiling zsh startup time](https://stevenvanbael.com/profiling-zsh-startup)

## Meaning of $, #, % in shell prompt
https://ubuntuforums.org/showthread.php?t=1852263&p=11297940#post11297940

## Session Management

[Abduco + DVTM a Lightweight Alternative to Tmux and Screen · Marc André Tanner](https://www.brain-dump.org/blog/abduco-dvtm-a-lightweight-alternative-to-tmux-and-screen/)
[abduco a tool for session {at,de}tach support](https://www.brain-dump.org/projects/abduco/)
[dvtm - dynamic virtual terminal manager](https://www.brain-dump.org/projects/dvtm/)

* Keeping state
* Scroll buffers

Alternatives:
* `GNU screen` - https://linuxize.com/post/how-to-use-linux-screen/
* `tmux`
* `abduco` (session management) + `dvtm` (tiling window management in the console)
  [Closing a shell causes window to hang on OS X 10.11 · Issue #19 · martanne/dvtm · GitHub](https://github.com/martanne/dvtm/issues/19)
  [Support for mouse wheel scrolling · Issue #103 · martanne/dvtm · GitHub](https://github.com/martanne/dvtm/issues/103)

## `source` vs `.`

man bash

```
source filename

A synonym for . (see Bourne Shell Builtins).
```

The only difference is portability. `.` is the POSIX-standard command for executing commands from file;
`source` is a more readable synonym provided by `bash` and some other shells.

---

[Should the .bashrc in the home directory load automatically?](https://stackoverflow.com/a/9954208)

---

`!!` is a synonym for `!-1`

---

## `exec` vs `eval`

exec is used to replace current shell process with new and handle stream
redirection/file descriptors if no command has been specified. eval is used to
evaluate strings as commands.

Both may be used to built up and execute a command with arguments known at
run-time, but exec replaces process of the current shell in addition to
executing commands.

See more at [Stack Overflow](https://unix.stackexchange.com/a/366228)

## Environment variables, "setting" vs `export`

```
$ foo="Hello, World"
$ echo $foo
Hello, World
$ bar="Goodbye"
$ export foo
$ bash
bash-3.2$ echo $foo
Hello, World
bash-3.2$ echo $bar

bash-3.2$
```

## Shell History

[Shell History Is Your Best Productivity Tool : programming](https://www.reddit.com/r/programming/comments/1c1j4nd/shell_history_is_your_best_productivity_tool/)
> This is great and all but just use https://github.com/atuinsh/atuin
